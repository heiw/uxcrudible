#Translatable
Uxcrud supports English and Welsh locales. Additional locales can be added if required.

There are two types of translatable content.

1. Fields that are stored in the database (see https://docs.astrotomic.info/laravel-translatable/)
2. "Hard" coded translatable strings https://laravel.com/docs/localization used in code.

# Translations in code
Translation in code use the __() or trans_choice() functions. These translations are stored in ##.json files for PHP/laravel and ##.js for JavaScript. To make translation easier a script can be run that finds all translable strings and stores them in the translation table. These translations can then be exported and send of for translation. Once the translations are loaded back into the translation table the script can be run again and the translations are put back in the correct json/js language files.

# Installation

To enable run

`php artisan vendor:publish --tag=translatable`

Then in `config\translable.php` update the following settings:

````
/*
|--------------------------------------------------------------------------
| Application Locales
|--------------------------------------------------------------------------
|
| Contains an array with the applications available locales.
|
*/
'locales' => [
    'en',
    'cy'
],
````
````
    'locale' => 'en',
````
````
    'use_fallback' => true,
````
````
    'fallback_locale' => null,
````

If you want to add or remove locales also update `config\uxcrud.php`

````
    /**
     * Application Locale Configuration
     * The available locales that will be provided.
     * locale code
     *      name    Localised name
     *      icon    Flag icon
     *      host    Host for locale
     */
    'locales' => [
        'en' => [
            'name' => 'English',
            'icon' => 'flag-icon-gb',
            'host' => env('HOST_EN')
        ],
        'cy' => [
            'name' => 'Cymraeg',
            'icon' => 'flag-icon-wales',
            'host' => env('HOST_CY')
        ]
    ],

````

# Configuration
In the `.env` configuration file setup your hosts.
````
APP_URL=https://desap.heiw.wales

URI_SCHEME=https://
HOST_EN=desap.heiw.wales
HOST_CY=pagad.heiw.wales
````

# Translator role
There is a translator role which if given read and update permission, will have permission to edit translations but no other fields.

It is recommended to add the translator page permission to a seeder so it will have the same permissions across all versions of the site.

# Synchronise translatable strings with DB.
Use configuration `config/uxcrud-translation-sync-app.php` file
````
<?php
/*
 * To run command
 * php artisan uxcrud:translation-sync --options=config/uxcrud-translation-sync-app.php
 */
return [
    'locale' => 'cy',

    'namespace' => 'App',

    // For packages set to true to copy before and after translatable command.
    'copyFirstFile' => false,

    // List of files to export. Ensure the first file is the main file when using copyFirstFile
    'files' => [
        'resources/lang/cy.json' => 0,
        'resources/lang/cy/auth.php' => 1,
        'resources/lang/cy/pagination.php' => 1,
        'resources/lang/cy/passwords.php' => 1,
        'resources/lang/cy/slugs.php' => 0,
        'resources/lang/cy/validation.php' => 1,
    ]
];
````
Run command `php artisan uxcrud:translation-sync --options=config/uxcrud-translation-sync-app.php`


