# Modal options
By default the modals used by Uxcrud only close when you explicitly click on the close button or X.

You can enable closing of modals by clicking on the background or using the escape key by adding
`public static $closeOnBackground = true;` to your UxcrudController class.   

````
class PartA extends UxcrudController
{
    public static $closeOnBackground = true;
}
````
