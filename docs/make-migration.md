To make a migration from a Uxcrud form run the following command:
````
 php artisan uxcrud:make-migration create_round_table --from-uxcrud-controller 
 ````
 
If the command doesn't automatically pick up the table name you can specify it using the --table option.
````
 php artisan uxcrud:make-migration create_round_table --from-uxcrud-controller --table=SessionalCommitment
````

If you want to create a migration for a change to a table run:
````
php artisan uxcrud:make-migration add_session_gdsct_column_sessional_commitment_table --from-uxcrud-controller --table=SessionalCommitment
````
In the generated file delete the columns not needed.


Also see commands.md
