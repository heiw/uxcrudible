# How to create a page with a custom blade

## Default blade
Add blade named `resources/view/[slug].blade.php` where slug is the full fall back url.

Content of blade file:
````
@extends('uxcrud::page')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    Content
                    @dump($currentPage)
                </div>
            </div>
        </div>
    </div>

    @parent
@endsection
````   

## Completely custom route

Add the route to `/routes/web.php`
````
Route::get(Str::kebab('example'), '\App\Uxcrud\Example@index')->middleware(['web']);
````

In the UxcrudController e.g. `/App/Uxcrud/Example.php` add the following:
````php
/**
 * Show the form
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
 * @throws \Throwable
 */
public function index() {
    $model = new UxcrudModel();
    $modal = $this->getEditModal($model, self::ABILITY_CREATE);
    $modal->form->onSuccess('onSuccessPreApplicationChecklistSubmit');
    $modal->buttons[0] = "<button type='submit' class='ajax-action-uxcrud-submit btn btn-success m-1'  data-dt-refresh='dt-admin-pre-application-checklist'><i class='fas fa-check'></i> Apply</button>";
    $controller = $this;
    return view("apply", compact(['controller', 'modal']));
}
````