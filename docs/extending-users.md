# Extending Users in your application.
Ensure that the base user class in `app\User.php` is extended from Uxcrud.  
````
<?php

namespace App;

use Heiw\Uxcrudible\UxcrudController;

class User extends Models\User
{

}
````
In the above example the class is extended from a version in `app\Models\User.php` to allow for customisation.


# Add Edit profile option
Add the following code to `app\Policies\UserPolicy.php`:
````
<?php

namespace App\Policies;

use Heiw\Uxcrudible\Policies\UxcrudUserPolicy;

class UserPolicy extends UxcrudUserPolicy
{

}
````