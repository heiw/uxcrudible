# Customisation of the User Dropdown
You can add your own options to the user dropdown. Copy `vendor/heiw/uxcrudible/src/resources/views/user-dropdown-custom.blade.php` to `resources/views/vendor/uxcrud/user-dropdown-custom.blade.php`

Or create a file
`resources/views/vendor/uxcrud/user-dropdown-custom.blade.php` with the following content:

````php
@section('user-dropdown')
    <li role="separator" class="divider"></li>
    <li>
        <div class="dw-user-box">
            <div class="u-text">
                Custom option
            </div>
        </div>
    </li>
@endsection
````