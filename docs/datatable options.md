You can customise the data table by adding a dataTAbleOptions method to your Uxcrud class: 

````
    public function dataTableOptions() {
        parent::dataTableOptions();
        $this->dataTableOptions->noExports()->noFilters()->noSearch();
    }
````
The following options are available.

Disable all exports
````
noExports()
````

Disable Excel export
````
noExcelExport()
````

Disable CSV export
````
noCSVExport()
````

Disable search
````
noSearch($search = false);
````

Disable filters
````
noFilters($filters = false);
````

Also see add-button.md
