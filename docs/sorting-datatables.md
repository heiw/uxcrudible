# Sorting Uxcrud results in datatables

## Basic sort
The following example will sort on `name`.
````php
Text::create('name')
    ->defaultSort('asc')
````

## Reversing the sort
The following example will reverse sort on `name`.
````php
Text::create('name')
    ->defaultSort('desc')
````

### Sorting on multiple columns
To sort on multiple columns add multiple default sorts. The following example will sort on `Firstname` and then on `Lastname`.
````php
Text::create('Firstname')
    ->defaultSort('asc')
,
Text::create('Lastname')
    ->defaultSort('asc')
````

### Altering the priority of the column sort
To prioritise a column add a sequential priority to the default sort. The following example will sort on `Lastname` and then on `Firstname`. 
````php
Text::create('Firstname')
    ->defaultSort('asc', 2)
,
Text::create('Lastname')
    ->defaultSort('asc', 1)
````

