#Installation from scratch
This document provides step-by-step guide how to setup a Uxcrud installation from scratch.

##Server configuration
This configuration is based on Apache with Xampp running on Windows machine. If you use another server please amend steps as appropriate.

###Add hosts
Uxcrud uses separate Welsh and English language domain names that point to the same server locations.

Add the following hosts to your hosts file ````c:\windows\systems32\drivers\etc\hosts````
````
127.0.0.1	demo.local
127.0.0.1	dangos.lleol		# demo.local
````
