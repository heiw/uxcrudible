# Uxcrud fields

Specify the fields in the `fields()` function.
````
public function fields($model = null)
{
    $fields = [
        Text::create('title')
    ];
    
    return $fields;
}
````

## Options
### Validation
````php
Text::create('Firstname')
    ->icon('fas fa-user')
    ->validation('required')
    ->defaultSort('asc', 2)
````

### Icon
Add a font awesome icon to the field.
````php
Text::create('Firstname')
    ->icon('fas fa-user')
````

### Visibility
Visibility can be set for different stages of each field. `hideOnIndex`, `hideOnView`, `hideOnCreate`, `hideOnEdit` 
````php
Text::create('Firstname')
    ->hideOnIndex()
````

#### Conditional
You can set the visibility properties based on a condition. E.g. to only allow sys admins to see the firstname column on the index use:
````php
Text::create('Firstname')
    ->hideOnIndex(auth()->user() && !auth()->user()->hasRole('sys_admin'))
````

### Read Only
A field can be set to read only for create, editing or both.
````php
Text::create('Firstname')
    ->readOnly()
````

Make field read 
````php
Text::create('Firstname')
    ->readOnlyCreate()
````

````php
Text::create('Firstname')
    ->readOnlyEdit()
````

 
#### Conditional
You can set the read only property based on a condition. E.g. to only allow sys admins to edit a firstname use:
````php
Text::create('Firstname')
    ->readOnlyOnEdit(auth()->user() && !auth()->user()->hasRole('sys_admin'))
````

````php
/**
 * @param bool $readOnly
 * @param bool $defaultVisibility Default visibility
 * @return $this
 */
public function readOnly($readOnly = true, $defaultVisibility = true) {
    $this->readOnlyOnCreate($readOnly, $defaultVisibility);
    $this->readOnlyOnEdit($readOnly, $defaultVisibility);
    return $this;
}
````
````php
/**
 * @param bool $readOnly
 * @param bool $defaultVisibility Default visibility
 * @return $this
 */
public function readOnlyOnCreate($readOnly = true, $defaultVisibility = true) {
    $this->permissions[Permission::CREATE] = ($readOnly) ? self::READ_ONLY : $defaultVisibility;
    return $this;
}
````
````php
/**
 * @param bool $readOnly
 * @param bool $defaultVisibility Default visibility
 * @return $this
 */
public function readOnlyOnEdit($readOnly = true, $defaultVisibility = true) {
    $this->permissions[Permission::UPDATE] = ($readOnly) ? self::READ_ONLY : $defaultVisibility;
    return $this;
}
````
````php

public function isReadOnly($ability) {
    return ($this->permissions[$ability] !== true);
}
````
````php

/**
 * Alias for removeSearch
 * @param bool $canSearch
 * @return FormField
 */
public function noSearch($canSearch = false) {
    return $this->removeSearch($canSearch);
}
````
````php

/**
 * Remove search option for field.
 * @param bool $canSearch
 * @return $this
 */
public function removeSearch($canSearch = false) {
    $this->search = $canSearch;
    return $this;
}
````
````php

/**
 * Alias for removeFilter
 * @param bool $canFilter
 * @return FormField
 */
public function noFilter($canFilter = false) {
    return $this->removeFilter($canFilter);
}
````
````php

/**
 * Remove filter for field.
 * @param bool $canFilter
 * @return $this
 */
public function removeFilter($canFilter = false) {
    $this->filter = $canFilter;
    return $this;
}
````
````php

/**
 * Sets the default filter value.
 * @param $value
 * @return $this
 */
public function defaultFilterValue($value) {
    $this->defaultFilterValue = $value;
    return $this;
}
````

### Sorting
Add default sort order and priority to field.

````php
Text::create('Firstname')
    ->defaultSort($direction, $priority)
````

See [Sorting DataTables](sorting-datatables.md)

### Remove sorting
To remove sorting for a field where a parent object previously has a sort order defined use `removeStorging`
````php
Text::create('Firstname')
    ->removeSorting()
````


## Field types specific
`TextArea` and `CKEditor` fields are automatically show condensed in their datatable. An arrow `v` is shown that allows the user to expand the content. To show the entire field's content initially use:
````
TextArea::create('fieldname')
    ->condensed(false)
````

## Validation
Validation can be added using standard Laravel

````
TextArea::crate('fieldname')
    ->validation('required|nullable')
````

# Return error message
To manually return an error message for a form throw a ValidationException

````
throw ValidationException::withMessages([
    "email_template_id" => [__("Email template not found")]
]);
````
# Alter widths of labels/fields
````
            HasOne::create('client', __('Client'))
                ->setModel(Client::class)
                ->labelName('summary()')
                ->validation('nullable')
                ->readOnlyOnEdit()
                ->labelWidth(2, 10)
            ,
            Group::create()
                ->addClass('row')
                ->setFields([
                    Date::create('start_date', __('Start date'))
                        ->addClass('col-md-6')
                        ->labelWidth(4, 8)
                        ->icon('fas fa-play')
                        ->defaultSort('desc')
                    ,
                    Date::create('end_date', __('End date'))
                        ->addClass('col-md-6')
                        ->icon('fas fa-stop')
                        ->defaultSort('desc')
                    ,
                ])
            ,
            TextArea::create('address', __('Address'))
                ->icon('fas fa-map-marker')
                ->labelWidth(2, 10)
            ,
````
