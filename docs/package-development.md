#Tips for package development
##Customise location
* Checkout git to `\packages\heiw\uxcrudible`   
* `project\composer.json` 
    * Remove
    ````
    "require-dev": {
            ::
        "maatwebsite/excel": "^3.1",
        "astrotomic/laravel-translatable": "^11.3"
    }    
    ````
    * Add
    ````
    "autoload": {
        "psr-4": {
                ::
            "Heiw\\Uxcrudible\\": "packages/heiw/uxcrudible/src",
            "Heiw\\Nhs\\": "packages/heiw/nhs/src"

    ````
* Add symlink from `\public\vendor\heiw\uxcrud` to `\packages\heiw\uxcrudible\src\assets`. Note: Update base path as required.
````
mklink /J "C:\xampp\htdocs\desap\public\vendor\heiw\uxcrud" "C:\xampp\htdocs\desap\packages\heiw\uxcrudible\src\assets"   
````    

##Unit testing
