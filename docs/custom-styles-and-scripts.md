#Include custom scripts and styles
Uxcrud offers several levels - from site wide to page specific - methods of including custom scripts and styles. This document describes how to use them.

##Site wide
You can add site wide scripts and styles in the following locations:

* *styles*: `public/css/uxcrudible-site.css`
* *scripts*: `public/js/uxcrudible-site.js`

##Page specific
Uxcrud automatically includes script and styles based on the full url of the uxcrud page.    

* *styles*: `public/css/<path uri>.css`
* *scripts*: `public/js/<path uri>.js`

E.g. to create custom files for the users page that sits under admin use:
* *styles*: `public/css/admin/users.css`
* *scripts*: `public/js/admin/users.js`

##From within code
_Please note for all examples the following namespace is used:
`use Heiw\Uxcrudible\Services\Menu`_

From within the code you can use the following:

```php
$currentPage = resolve(Menu::class)->currentPage();
if ($currentPage) {
    $currentPage->addRawCss('.topbar .top-navbar { background-color: red; }');
}
```
The following functions are available:

To add a css file. All file paths are relative from the public folder.
```
$currentPage->addCss($file)
```
    
To add raw css:
```
$currentPage->addRawCss($css)
```

To add a script file. All file paths are relative from the public folder.
```
$currentPage->addJs($file)
```
    
To add raw js:
```
$currentPage->addRawJs($js)
```
       

##From blade files
```php
@php($currentPage->addRawCSS('.topbar .top-navbar { background-color: purple; }'))
```