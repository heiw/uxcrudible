# Default values
Add default values to Uxcrud fields(...) method. If the model is present but no 
value set the value to its default.  
````php
public function fields($model = null) {
    // Ensure that model is set
    if (!is_null($model)) {
        if (is_null($model->date)) {
            // Set default value
            $model->date = date('Y-m-d');
        }

        if ($user && $user->hasRole('tpd')) {
            $model->tpd_id = $user->id;
        }
    }
````

# Alternative method
To set default values override the constructor in the **Model**.

E.g. `App\Models\PracticeVisit`

````
/**
 * Set defaults in PracticeVisit constructor.
 * @param array $attributes
 */
public function __construct(array $attributes = []) {
    // Set default date to today
    $attributes['date'] = date('Y-m-d');

    // Set default round if there is an active round
    $activeRound = \App\Models\Round::activeRound();
    if (isset($activeRound->id)) {
        $attributes['round_id'] = $activeRound->id;
    }
    
    // Check if current user has tpd role.
    $user = auth()->user();
    if ($user && $user->hasRole('tpd')) {
        // set tpd id to current user. 
        $attributes['tpd_id'] = $user->id;
    }

    // Pass new attributes to parent constructor to complete initialisation.
    parent::__construct($attributes);
}
``

