#Add button to data row of Uxcrud datatable
See insight > Clients > button

            Actions::create()
                ->insertBefore('delete', Action::create('anonymise', 'actions.anonymise'))
            ,

#Add button to DataTable header
To add a button next to the Export button of a datatable add the following code to the UxcrudController.

````
public function dataTableOptions() {
    $this->dataTableOptions = parent::dataTableOptions();

    $this->dataTableOptions->customButtons =
        // Single button
        DataTableButton::create('E-mail')
            ->icon('fas fa-envelope')
        ;

        // Dropdown button
        DataTableButton::create('E-mail')
            ->icon('fas fa-envelope')
                ->addButtons(
                    [
                    DataTableButton::create('Applicant')
                        ->icon('fas fa-user'),
                    DataTableButton::create('TPD')
                        ->icon('fas fa-user-md'),
                    ]
                )
        ;
    return $this->dataTableOptions;
}
````
