


From your PHP method return the following:
````php
return response()->json(
    JsonToast::create()
        ->header("Your header")
        ->body("Your detailed messages")
        ->status('success')
);
````
