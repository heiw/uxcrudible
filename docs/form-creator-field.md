Uxcrud Controller class:

Endsure that add model id on fields cache is set to true to enable pulling through of data stored in json for the Form Creator Field. 
````    
public static $addModelIdOnFieldsCache = true;
````

##Customise options for Form Fields

You can add options for a specific form field by extending the ````setFormCreatorField```` method. 

`````
public function setFormCreatorField($fieldDefinition)
``````
