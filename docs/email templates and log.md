# Email Templates

Email templates can be created and edited at /admin/settings#emailtemplate. 
After creation their name can only be changed by system administrators.

## Translations
Translatable features are offered for all facets of Email Templates. The 
recipients preferred language (stored in user.locale) is used when sending emails.

## Usage
To send an email create an email template by specifying it's name, adding data and the list of recipient(s) and calling send().


    // Create email template from name
    $emailTemplate = EmailTemplate::create('tpd_practice_visit_sign_off')
        // add data to template 
        ->data($this->emailTemplateViewData())
    ;
    // Get list of users or single user
    $recipients = [auth()->user(), $otherUser];
    $emailTemplate->addRecipients($recipients)
        // Send message
        ->send();

## Available data
A model's data is automatically made available by calling the 
function ->emailTemplateViewData(). In the Email Template 
these are the available using translatable syntax :name.

E.g. Dear :user, you have the following roles: :user.roles
In the above :user and :user.roles are automatically replaced. If not entry is 
found the label is shown.

To limit load only fields shown on index are included by default, you can change 
this using the $view parameter.

To make additional information available extend the function

    /**
     * Make field values available for inclusion in email template.
     * They can be includes with :key.
     * E.g. Dear :user, you have the following roles: :user.roles
     * To limit load only fields shown on index are included by default.
     * @param null $key
     * @param array $data
     * @param string $view Change to Permission::VIEW to include all fields.
     * @return array|null
     * @throws \Throwable
     */
    public function emailTemplateViewData($key = null, &$data = [], 
            $view = Permission::INDEX) {
        $data = parent::emailTemplateViewData($key, $data, $view);
        return $data;
    }

## Signature
A site wide signature can be included by creating a snippet named: ````email_snippet_signature````.

# Email logs
Emails are automatically logged in email_log at /admin/settings#emaillog. Here 
you can filter them by sender, recipient, email template, success etc.

Please note that success only indicates successful sending of the message. It 
does not guarantee delivery of the message.  

# Upgrading
To upgrade from previous versions run:  
````php artisan db:seed --class=Heiw\Uxcrudible\Seeds\ModelTableSeeder````
