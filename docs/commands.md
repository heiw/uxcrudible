````
php artisan uxcrud:make-model Test --from-uxcrud-controller --translatable
````

To create a migration with translation use:
````
php artisan uxcrud:make-migration create_scheme_table --translatable
````

````
php artisan uxcrud:make-migration create_snippet_table --from-uxcrud-controller --translatable
````

For example:
````
php artisan uxcrud:make-migration create_sessional_commitment_table --from-uxcrud-controller
````

Also see make-migration.md
