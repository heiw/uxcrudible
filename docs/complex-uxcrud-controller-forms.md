Use select expression on fields and `selectUxcrudFields($this)` to avoid ambiguous columns when joining tables in a UxcrudController's builder method.

Specify select expression to avoid ambiguous columns on the FormField: 
`->selectExpression('review.date')`

Add `->selectUxcrudFields($this)` to the `builder()` method to ensure that the required fields are queried.

````
    public function fields($model = null)
    {
        $fields = [
            // Make fields available for searching.
            Text::create('search_client_name')
                ->hideSearchable()
                ->selectExpression(DB::raw("CONCAT(client.title, ' ', client.firstname, ' ', client.lastname, ' ', client.gmc_number)"))
            ,

            // Start of regular fields
            Date::create('date', __('Date'))
                // Specify select expression to avoid ambiguous columns. 
                ->selectExpression('review.date')
            ,

        ];
        return $fields;
    }

    public function builder(): \Illuminate\Database\Eloquent\Builder {
        // Link record and client for search
        $builder = parent::builder()
            ->selectUxcrudFields($this)
            ->leftJoin('record', 'record.id', '=', 'review.record_id')
            ->leftJoin('client', 'client.id', '=', 'record.client_id')
        ;
        return $builder;
    }

}
````

Note this implementation avoids the MySQL error; SQLSTATE[23000]: Integrity constraint violation: 1052 Column '###' in field list is ambiguous
