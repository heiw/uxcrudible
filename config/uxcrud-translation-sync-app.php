<?php
/*
 * To run command
 * php artisan uxcrud:translation-sync --options=config/uxcrud-translation-sync-app.php
 */
return [
    'locale' => 'cy',

    'namespace' => 'App',

    // For packages set to true to copy before and after translatable command.
    'copyFirstFile' => false,

    // List of files to export. Ensure the first file is the main file when using copyFirstFile
    'files' => [
        'resources/lang/cy.json' => 0,
        'resources/lang/cy/auth.php' => 1,
        'resources/lang/cy/pagination.php' => 1,
        'resources/lang/cy/passwords.php' => 1,
        'resources/lang/cy/slugs.php' => 0,
        'resources/lang/cy/validation.php' => 1,
    ]
];
