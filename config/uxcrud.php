<?php

return [
    /**
     * The colour of the theme.
     */
    'theme_colour' => 'blue',

    /**
     * Controller namespace
     * Uxcrud will automatically look for a model's matching controllers in the
     * sub-folder specified.
     * This can be altered to your preference, it will need a trailing slash
     * e.g. 'Uxcrud\\'
     */
    'controller_namespace' => 'Uxcrud\\',

    /**
     * Model namespace
     * Uxcrud will automatically look for a controler's matching model in the
     * sub-folder specified.
     * This can be altered to your preference, it will need a trailing slash
     * e.g. 'Models\\'
     */
    'model_namespace' => 'Models\\',

    /**
     * Policy namespace
     * Uxcrud will automatically look for the policy in the sub-folder specified.
     * This can be altered to your preference, it will need a trailing slash
     * e.g. 'Policies\\'
     */
    'policy_namespace' => 'Policies\\',

    /**
     * Number of items per page to load for remote data for filters.
     */
    'remote_data_page_length' => 25,

    /**
     * Application Locale Configuration
     * The available locales that will be provided.
     * The fallback locale is the first locale listed.
     * locale code
     *      name    Localised name
     *      icon    Flag icon
     *      host    Host for locale
     */
    'locales' => [
        'en' => [
            'name' => 'English',
            'icon' => 'flag-icon-gb',
            'host' => env('HOST_EN')
        ],
        'cy' => [
            'name' => 'Cymraeg',
            'icon' => 'flag-icon-wales',
            'host' => env('HOST_CY')
        ]
    ],

    /**
     * Number of seconds after which to show the session timeout dialog.
     */
    'session_timeout_dialog' => 30 * 60, //env('SESSION_LIFETIME') * .8 * 60,

    /**
     * Number of seconds to show the dialog timer for before locking.
     */
    'session_timeout_dialog_showtime' => 30,

    /**
     * Set to false to not allow withdrawing from privacy statement.
     */
    'gdpr_allow_withdraw' => true,

    /**
     * Id of the Email Template that allows for free text entry.
     */
    'email_template_free_template_id' => 1
];
