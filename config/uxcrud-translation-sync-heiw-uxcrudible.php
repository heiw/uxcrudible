<?php
/*
 * To run command
 * php artisan uxcrud:translation-sync --options=config/uxcrud-translation-sync-heiw-uxcrudible.php
 */
return [
    'locale' => 'cy',

    'namespace' => 'Heiw.Uxcrudible',

    // For packages set to true to copy before and after translatable command.
    'copyFirstFile' => true,

    // List of files to export. Ensure the first file is the main file when using copyFirstFile
    'files' => [
        'packages/heiw/uxcrudible/src/resources/lang/cy.json' => 0,
        'packages/heiw/uxcrudible/src/resources/lang/cy/slugs.php' => 1,
        'packages/heiw/uxcrudible/src/resources/lang/cy/uxcrud.php' => 1,
        'packages/heiw/uxcrudible/src/assets/js/lang/cy.js' => 0
    ]
];
