# HEIW Core Package

Core functionality for HEIW applications. Including:

+ User 
+ Role
+ ...

# Documentation

Additional documentation can be found in **uxcrudible/docs/** folder.

## Installation

### Install laravel

Install Laravel 8 as normal.

```shell
composer create-project --prefer-dist laravel/laravel blog
```

### Configure DB connection

Edit your connections settings in `.env`

### Add default authentication

Run the following command in the terminal:

```shell
composer require laravel/ui
php artisan ui vue --auth
npm install
npm run dev
```

If you have an error Error: Cannot find module 'webpack/lib/rules/DescriptionDataMatcherRulePlugin' run `npm i vue-loader` then re-run `npm run dev`

### Create config file

Create a config file for Uxcrud in config called **uxcrud.php**

```php
<?php

return [
    /**
     * The colour of the theme.
     */
    'theme_colour' => 'blue',

    /**
     * Controller namespace
     * Uxcrud will automatically look for a model's matching controllers in the
     * sub-folder specified.
     * This can be altered to your preference, it will need a trailing slash
     * e.g. 'Uxcrud\\'
     */
    'controller_namespace' => 'Uxcrud\\',

    /**
     * Model namespace
     * Uxcrud will automatically look for a controler's matching model in the
     * sub-folder specified.
     * This can be altered to your preference, it will need a trailing slash
     * e.g. 'Models\\'
     */
    'model_namespace' => 'Models\\',

    /**
     * Policy namespace
     * Uxcrud will automatically look for the policy in the sub-folder specified.
     * This can be altered to your preference, it will need a trailing slash
     * e.g. 'Policies\\'
     */
    'policy_namespace' => 'Policies\\',

    /**
     * Number of items per page to load for remote data for filters.
     */
    'remote_data_page_length' => 25,

    /**
     * Application Locale Configuration
     * The available locales that will be provided.
     * The fallback locale is the first locale listed.
     * locale code
     *      name    Localised name
     *      icon    Flag icon
     *      host    Host for locale
     */
    'locales' => [
        'en' => [
            'name' => 'English',
            'icon' => 'flag-icon-gb',
            'host' => env('HOST_EN')
        ],
        'cy' => [
            'name' => 'Cymraeg',
            'icon' => 'flag-icon-wales',
            'host' => env('HOST_CY')
        ]
    ],

    /**
     * Number of seconds after which to show the session timeout dialog.
     */
    'session_timeout_dialog' => 30 * 60, //env('SESSION_LIFETIME') * .8 * 60,

    /**
     * Number of seconds to show the dialog timer for before locking.
     */
    'session_timeout_dialog_showtime' => 30,

    /**
     * Set to false to not allow withdrawing from privacy statement.
     */
    'gdpr_allow_withdraw' => true,

    /**
     * Id of the Email Template that allows for free text entry.
     */
    'email_template_free_template_id' => 1
];
```

### Require Uxcrud

```shell
composer require heiw/uxcrudible
```

### Setup Laravel

You can now setup Laravel to use Uxcrud in the app.

In `routes\web.php` add

```php
use Heiw\Uxcrudible\Facades\AuthTranslation;
```

and replace

```php
Auth::routes();
```

with

```php
AuthTranslation::routes(['register' => false]);
```

Delete the default users migration file from database\migrations:

`database\migrations\2014_10_12_000000_create_users_table.php`

### Add the HEIW packages (optional)

The HEIW NHS package is optional, it can be required by running:

```shell
composer require heiw/nhs
```

### Add package service providers

Add the package service providers to `config/app.php`:

```php
/*
 * Package Service Providers...
 */
\Heiw\Uxcrudible\UxcrudibleServiceProvider::class,
``` 

### Add middleware

Add page permissions to `app/Http/Kernel.php`

```php
protected $routeMiddleware = [
    // :: add to the bottom of list of middle ware
    'pagePermission' => \Heiw\Uxcrudible\Middleware\PagePermissionMiddleware::class
];
```

## Extend User from Uxcrudible

To use Uxcrud you will need to create a `User` class in `App/User.php` which extends `\Heiw\Uxcrudible\Models\User`

```php
<?php

namespace App;

class User extends \Heiw\Uxcrudible\Models\User
{

}
```

Also edit any existing `User` in `App/Models/User.php` to extend the above user

```php
// ...
class User extends \App\User
// ...
```

### Create User Policy

Ensure the default UserPolicy in created `app\Policies\UserPolicy.php` to call UxcrudUserPolicy.

```php
<?php

namespace App\Policies;

use Heiw\Uxcrudible\Policies\UxcrudUserPolicy;

class UserPolicy extends UxcrudUserPolicy
{

}
```

### Copy public assets

To copy the public assets to the correct place, please run the following command after installation.

```shell
php artisan vendor:publish --tag=public --force
```

### Enable translatable

To enable run

```shell
php artisan vendor:publish --tag=translatable --force
```

To configure follow instructions in /vendor/heiw/uxcrudible/docs/translatable.md

### Disable strict

In `config/database.php` change the mysql connection `strict`  to `false`:

```php
    'connections' => [
           ::
        'mysql' => [
                ::
            'strict' => false
```

### Generate default tables

For a completely new database run

```shell
php artisan migrate:refresh --seed
php artisan db:seed --class=Heiw\Uxcrudible\Seeds\DatabaseSeeder
```

For upgrading an existing database run the following two separate commands.

```shell
php artisan migrate
php artisan db:seed --class=Heiw\Uxcrudible\Seeds\DatabaseSeeder
```

Once the user table has been seeded you can either reset your password or create your own account.

```shell
php artisan tinker
```

In the Tinker console

```php
$users = \App\Models\User::all(['id', 'lastname']);
dump($users);
```

Once you have identified your account the password can be set:

```php
$yourAccount = \App\Models\User::find(7); // where 7 is your account id from the dump
$yourAccount->password = \Illuminate\Support\Facades\Hash::make('password');
$yourAccount->save();
$yourAccount->refresh();
dd($yourAccount); // Your account will now show the 'password' hash
```

### Log in

All being well you can now log in!

To force a login open your browser to your app host name /admin/settings.
