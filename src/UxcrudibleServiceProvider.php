<?php

namespace Heiw\Uxcrudible;

use Dotenv\Parser;
use Heiw\Uxcrudible\Models\Model;
use Heiw\Uxcrudible\Models\Role;
use Heiw\Uxcrudible\Policies\UxcrudModelPolicy;
use Heiw\Uxcrudible\Services\Menu;
use Heiw\Uxcrudible\Uxcrud\ChangePassword;
use Heiw\Uxcrudible\Uxcrud\File;
use Heiw\Uxcrudible\Uxcrud\Gdpr;
use Heiw\Uxcrudible\Listeners\EmailSentSubscriber;
use Heiw\Uxcrudible\Uxcrud\Page;
use Heiw\Uxcrudible\Uxcrud\UnlockSession;
use Heiw\Uxcrudible\Uxcrud\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;


class UxcrudibleServiceProvider extends ServiceProvider
{
    public function __construct($app) {
        parent::__construct($app);
        if (!class_exists('Maatwebsite\Excel\ExcelServiceProvider')) {
            // TODO MK Also check other requirements from https://docs.laravel-excel.com/3.1/getting-started/installation.html
            require_once(__DIR__ . '/../vendor/maatwebsite/excel/src/ExcelServiceProvider.php');
        }
        if (!class_exists('Astrotomic\Translatable\TranslatableServiceProvider')) {
            require_once(__DIR__ . '/../vendor/astrotomic/laravel-translatable/src/Translatable/TranslatableServiceProvider.php');
        }
        if (!class_exists('Lab404\Impersonate\ImpersonateServiceProvider')) {
            require_once(__DIR__ . '/../vendor/lab404/laravel-impersonate/src/ImpersonateServiceProvider.php');
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton(Menu::class, function ($app) {
            return new Menu();
        });

        Event::subscribe(EmailSentSubscriber::class);

        //$this->app->register('\Maatwebsite\Excel\ExcelServiceProvider::class');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {

        $this->publishes([
            __DIR__ . '/assets' => public_path('vendor/heiw/uxcrud'),
        ], 'public');

        // Merge site custom settings
        $this->mergeConfigFrom(
            config_path() . '/uxcrud.php', 'uxcrud'
        );

        // Merge default settings
        $this->mergeConfigFrom(
            __DIR__.'/../config/uxcrud.php', 'uxcrud'
        );

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', 'uxcrud');
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'uxcrud');
        $this->publishes([
            __DIR__ . '/resources/views' => resource_path('views/vendor/uxcrud/'),
        ]);


        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\CreateColours::class,
                Commands\UxcrudControllerMakeCommand::class,
                Commands\UxcrudModelMakeCommand::class,
                Commands\UxcrudMigrationMakeCommand::class,
                Commands\UxcrudSeederMakeCommand::class,
                Commands\UxcrudTranslationSyncCommand::class,
                Commands\UxcrudTranslationSyncFileCommand::class,
            ]);
        }

        \Heiw\Uxcrudible\Classes\Locale::setLocaleFromHost();
        self::registerRoutes();

        $this->registerModelPolicies();
        $this->registerPolicies();
    }

    // TODO MK replace with call to database.
    public static function getModels() {
        if (Schema::hasTable('model')) {
            return Model::orderBy('namespace')->orderBy('name')->get();
        } else {
            return collect();
        }
    }

    public static function registerRoutes() {
        Route::resourceVerbs([
            'create' => Str::slug(__('uxcrud::slugs.create')),
            'edit' => Str::slug(__('uxcrud::slugs.edit'))
        ]);


        // TODO MK Fix hack and move catch any route to after Laravel includes web.php
        @include_once(base_path() . '/packages/heiw/forms/src/routes/web.php');
        @include_once(base_path() . '/packages/heiw/surveys/src/routes/web.php');
        @include_once(base_path() . '/routes/web.php');


        //Auth::routes();

//        View::composer('*', function ($view) {

        $menu = new Menu();
        try {
            foreach ($menu->pages() as $page) {
                $middleware = ['web'];
                if (!$page->public) {
                    $middleware[] = 'auth';
                }

                Route::get(
                    $page->route(),
                    'Heiw\Uxcrudible\AccessController@page',
                    ['currentPage' => $page]
                )->middleware($middleware);

            }
        } catch(QueryException $exception) {

        }
//        });

//        $locale = resolve(\Heiw\Uxcrudible\Classes\Locale::class);
//        $locale->pushLocale('en');

        foreach (self::getModels() as $model) {
            $class = $model->getControllerNamespaced(false);

//            Route::resource(
//                $class::getRoute(),
//                $class
//            )->middleware(['web', 'auth']);

            if (!class_exists($class)) {
                // Generate warning
                $warning = "  Class '$class' not found in Model database  ";
                $spaces = str_repeat(' ', strlen($warning));
                (new ConsoleOutput())->write("<fg=black;bg=yellow>\n$spaces\n$warning\n$spaces\n</>");
                Log::warning(trim($warning));
            } else {
                $class::addRoutes($model);
                Route::resource(
                    $class::getAjaxRoute(),
                    $class
                )->middleware(['web', 'auth']);

                // Restore of soft delete route
                Route::post(
                    $class::getAjaxRoute() . '/{' . $model->slug() . '}/' . Str::slug(__('uxcrud::slugs.restore')),
                    $class . '@restore'
                )->middleware(['web', 'auth'])->name($model->slug() . "." . Str::slug(__('uxcrud::slugs.restore')));

                // Force delete route of previously soft deleted item
                Route::post(
                    $class::getAjaxRoute() . '/{' . $model->slug() . '}/' . Str::slug(__('uxcrud::slugs.force delete')),
                    $class . '@forceDelete'
                )->middleware(['web', 'auth'])->name($model->slug() . "." . Str::slug(__('uxcrud::slugs.force delete')));

                Route::get(
                    $class::getExportRoute() . '/{type}',
                    $class . '@export'
                )->middleware(['web', 'auth'])->name($model->slug() . "." . Str::slug(__('uxcrud::slugs.export')));

                Route::get(
                    $class::getAjaxRoute() . '/' . Str::slug(__('uxcrud::slugs.remote-data')) . '/{model}',
                    $class . '@remoteData'
                )->middleware(['web', 'auth'])->name($model->slug() . "." . Str::slug(__('uxcrud::slugs.remote-data')));

                if (isset($class::$rowReorder) &&  $class::$rowReorder) {
                    Route::post(
                        $class::getRowReorderRoute(),
                        $class . '@rowReorder'
                    )->middleware(['web', 'auth'])->name($model->slug() . "." . Str::slug(__('uxcrud::slugs.row-reorder')));
                }

//                // Add custom routes for controller.
            }
        }

//        $routes = [];
//        foreach (\Route::getRoutes()->getIterator() as $route){
//            $routes[] = $route->uri;
//        }
//        dd($routes);

//        $locale->revertLocale();


        // Add user impersonation route
        Route::post(User::getAjaxRoute() . '/{user}/' . Str::slug(__('uxcrud::slugs.impersonate')), User::class . '@impersonate')
            ->name('user.impersonate')
            ->middleware(['web', 'auth']);

        // Reset password
        Route::get(ChangePassword::getAjaxRoute() . '/{user}/', ChangePassword::class . '@edit')
            ->name('user.change-password-edit')
            ->middleware(['web', 'auth']);
        // Reset password
        Route::patch(ChangePassword::getAjaxRoute() . '/{user}/', ChangePassword::class . '@update')
            ->name('user.change-password-update')
            ->middleware(['web', 'auth']);

        // End impersonation route
        Route::post(User::getAjaxRoute() . '/' . Str::slug(__('uxcrud::slugs.end-impersonation')), User::class . '@endImpersonation')
            ->name('user.end-impersonation')
            ->middleware(['web', 'auth']);

        // Extend sesssion
        Route::get(Str::slug(__('uxcrud::slugs.extend-session')), UnlockSession::class . '@extendSession')
            ->name('extendSession')
            ->middleware(['web', 'auth']);

        // Add unlock routes
        Route::get(Str::slug(__('uxcrud::slugs.unlock')), UnlockSession::class . '@showUnlockForm')
            ->name('unlock-form')
            ->middleware(['web']);
        Route::post(Str::slug(__('uxcrud::slugs.unlock')), UnlockSession::class . '@unlock')
            ->name('unlock')
            ->middleware(['web']);
        Route::post(Str::slug(__('uxcrud::slugs.csrfToken')), UnlockSession::class . '@csrfToken')
            ->name('csrf-token')
            ->middleware(['web']);

        // GDPR
        Route::get(Str::slug(__('uxcrud::slugs.gdpr-view')), Gdpr::class . '@view')
            ->name('gdpr.view')
            ->middleware(['web', 'auth']);
        Route::post(Str::slug(__('uxcrud::slugs.gdpr-agree')), Gdpr::class . '@agree')
            ->name('gdpr.agree')
            ->middleware(['web', 'auth']);

        // Only include withdraw route if option set.
        if (config('uxcrud.gdpr_allow_withdraw')) {
            Route::post(Str::slug(__('uxcrud::slugs.gdpr-withdraw')), Gdpr::class . '@withdraw')
                ->name('gdpr.withdraw')
                ->middleware(['web', 'auth']);
        }

        Route::any(
            '/{any}',
            'Heiw\Uxcrudible\AccessController@page'
        )
            ->where('any', '.*')
            ->middleware(['web', 'auth', 'pagePermission']);

    }

    public function registerModelPolicies() {
        foreach (self::getModels() as $model) {
            $class = $model->getModelNamespaced(false);
            $policy = $model->getPolicyNamespaced(false);
            if ($class != '-') {
                if (class_exists($policy)) {
                    $this->policies[$class] = $policy;
                } else {
                    $this->policies[$class] = UxcrudModelPolicy::class;
                }
            }
        }
    }
}
