<?php

namespace Heiw\Uxcrudible\Middleware;

use Closure;
use Heiw\Uxcrudible\Services\Menu;
use Illuminate\Routing\RouteCollection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class PagePermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $routes = \Route::getRoutes();
//        dd($routes);
        $response = $next($request);

        //if (\Route::)
        $menu = resolve(Menu::class);
        $currentPage = $menu->currentPage();

      //  abort_unless($currentPage, 404, __('Page not found'));
        //$request->route()->setParameter('currentPage', $currentPage->id);

        return $response;
    }
}
