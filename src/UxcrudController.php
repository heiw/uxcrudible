<?php

namespace Heiw\Uxcrudible;

use Heiw\Uxcrudible\Classes\DataTableButton;
use Heiw\Uxcrudible\Classes\FormModal;
use Heiw\Uxcrudible\Classes\JsonForm;
use Heiw\Uxcrudible\Classes\JsonModal;
use Heiw\Uxcrudible\Classes\JsonToast;
use Heiw\Uxcrudible\Form\Fields\DataTable;
use Heiw\Uxcrudible\Form\Fields\File;
use Heiw\Uxcrudible\Form\Fields\Files;
use Heiw\Uxcrudible\Form\Fields\Page;
use Heiw\Uxcrudible\Form\Fields\Pages;
use Heiw\Uxcrudible\Form\FormController;
use Heiw\Uxcrudible\Models\Permission;
use Heiw\Uxcrudible\Services\Menu;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Heiw\Uxcrudible\Traits\DataTableController;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Heiw\Form;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationRuleParser;
use Illuminate\Validation\Validator;

// TODO MK Authentication

abstract class UxcrudController extends FormController
{
    use CachedObjectValue, DataTableController;

    const ABILITY_VIEW = 'show';
    const ABILITY_UPDATE = 'update';
    const ABILITY_CREATE = 'create';
    const ABILITY_DELETE = 'delete';

    public static $model = null;

    /**
     * @var bool Set to true to add the model id to the fields cache name.
     * Use this when customising fields in fields based on model values.
     */
    protected static $addModelIdOnFieldsCache = false;

    /**
     * Allow searching of fields
     * @var bool True if fields can be searched
     */
    public static $search = true;

    /**
     * Set to true to close modal on background click or when esc key pressed.
     * Defaults to false where only close buttons close the modal.
     * @var bool
     */
    public static $closeOnBackground = false;

    public $modalOptions = [
        "keyboard" => true,
        "backdrop" => true
    ];

    public static $label = null;
    public static $labels = null;

    /**
     * Set to overwrite route.
     * If not set defaults to $table. Do not include leading or trailing slash.
     * @var null
     */
    public static $route = null;

    // LEGACY CODE..?!
    /**
     * @var string
     */
    //protected $modelName;

    public function __construct()
    {
        $this->middleware('auth');

        $this->closeOnBackground(static::$closeOnBackground);
    }

    /**
     * Close modal when clicking on background or when esc key pressed.
     * @param bool $close
     */
    public function closeOnBackground($close = true) {
        $this->modalOptions['keyboard'] = $close;
        if ($close) {
            $this->modalOptions['backdrop'] = true;
        } else {
            $this->modalOptions['backdrop'] = 'static';
        }
    }

    // Split loading model and values?
    public static function getModel($id = null) {
        // Check if static::$model is defined and if not give hint to developer.
        if (is_null(static::$model)) {
            throw new \Exception('static::$model is missing on ' . static::class);
        }
        $class = static::$model;
        if (!is_null($id)) {
            $model = new $class();
            $model = $model->findOrFail($id);
            return $model;
        } else {
            return new $class();
        }
    }

    public static function initFromModelName($model) {
        $controllerClass = str_replace(config('uxcrud.model_namespace'), config('uxcrud.controller_namespace'), $model);
        return new $controllerClass;
    }


    /**
     * Get the localised route URI for the model.
     *
     * @return string   Route with leading slash but without trailing slash.
     */
    public static function getRoute() {
        $uri = [Str::slug(__('uxcrud::slugs.admin'))];

        // If no route set default to table name.
        if (empty(static::$route)) {
            $uri[] = Str::slug(__(self::getClassName()));
        } else {
            $uri[] = Str::slug(__(static::$route));
        }
        return '/' . implode('/', $uri);
    }

    public static function getAjaxRoute() {
//        $locale = resolve(\Heiw\Uxcrudible\Classes\Locale::class);
//        $locale->pushLocale('en');
        $route = '/ajax' . static::getRoute();
//        $locale->revertLocale();
        return $route;

    }

    public static function getId() {
        return "dt" . str_replace("/", "-", static::getRoute());// . "-" . Str::random();
    }

    public static function getModelTable() {
        $model = self::getModel();
        return $model->getTable();
    }

    public static function getClassName() {
        return Str::kebab(class_basename(get_called_class()));

    }

    /**
     * Get the name of the controller
     * @param int $plural   Returns singular if set to 1 or -1 otherwise
     * returns plural.
     * @return string|string[]|null
     */
    public static function getLabel($plural = 1) {
        switch(abs($plural)) {
            case 1:
                if (!is_null(static::$label)) {
                    return static::$label;
                } elseif (self::getModelTable()) {
                    return str_replace('_', ' ', self::getModelTable());
                }
                break;
            default:
                if (!is_null(static::$labels)) {
                    return static::$labels;
                } else {
                    return Str::plural(str_replace('_', ' ', self::getModelTable()));
                }
                break;
        }
        return null;
    }

    /**
     * Alias for getLabel
     * @param int $plural   Returns singular if set to 1 or -1 otherwise
     * returns plural.
     * @return string|null
     */
    public static function getLabels($plural = 0) {
        return self::getLabel($plural);
    }

    /**
     * @param $ability
     * @return string
     */
    public static function getButtonLabel($ability) {
        return $ability->button . " " . __(self::getLabel());
    }

    /**
     * Override to return forced filtered entries.
     * When overriding remember to call `->whereFilters($this)`
     * @return Builder
     */
    public function builder() : \Illuminate\Database\Eloquent\Builder {
        //dd(request());
        $model = self::getModel();
        $builder = $model::whereFilters($this)
        ;
        if ($model->hasTranslationAttribute($model::getOrderBy())) {
            $builder->orderByTranslation($model::getOrderBy());
        } else {
            $builder->orderBy($model::getOrderBy());
        }

        return $builder;
    }

//    /**
//     * Override to limit access to specific object.
//     * @param UxcrudModel $object
//     * @param string $permission
//     * @return bool
//     */
//    public static function canAccessOrAbort($object, $permission = null) {
//        return true;
//    }

    /**
     * @param  mixed $ability   Action permission (create/update)
     * @param $object           object Original object
     * @param $request          Request object
     * @param null $messages
     * @return array            Array of rules
     * @param bool $includeWarningMessages  Include data-warning messages when set to true.
     * @return array
     */
    public function getValidationRules($ability, $object, $request, &$messages = null, $includeWarningMessages = false) {
        $rules = [];
        $locales = $object->translationLocales();
        $parentClass = null;
        $parentModel = null;
        if (request('parentModel')) {
            $parentClass = request('parentModel')['name'];
            $parentModel = $parentClass::getModel()->findOrFail(value(request('parentModel')['id']));
        }
        $pages = [];
        if ($request->has('uxcrud-page')) {
            $pages = [$request->get('uxcrud-page')];
        }

        $fields = $this->getFields($ability, true, $pages, $object);
        //$fields = $this->preProcessFields($fields, $object);
        foreach($fields  as $field) {
            if ((isset($field->model) && $field->model == $parentClass) ||
                (isset($field->validation) && !$field->isReadOnly($ability))) {
                $field->getValidationRules($ability, $object, $field, $locales, $request, $rules, $messages, $includeWarningMessages);
            }
        }
        return $rules;
    }

    public function fireFormFieldBeforeEvent($ability, $model, &$attributes, &$rules, &$messages) {
        $pages = [];
        foreach($this->getFields($ability, true, $pages, $model) as $field) {
            $field->onBeforeEvent($ability, $model, $attributes, $rules, $messages);
        }
    }

    public function fireFormFieldAfterEvent($ability, $original, $processed, $results = null) {
        $pages = [];
        foreach($this->getFields($ability, true, $pages, $processed) as $field) {
            $field->onAfterEvent($ability, $original, $processed, $results);
        }
    }

    /**
     * Get fields that may be filtered
     * @param int $ability
     * @return array
     */
    public function getFilters($ability = \Heiw\Uxcrudible\Form\Fields\Permission::READ) {
        $filters = [];
        foreach($this->getFields($ability, true) as $field) {
            if ($field->filter) {
                $filters[$field->name] = $field;
            }
        }
        return $filters;
    }

    /**
     * Get fields that can be searched
     * @param int $ability
     * @return array
     */
    public function getSearchable($ability = \Heiw\Uxcrudible\Form\Fields\Permission::READ) {
        $searchable = [];
        foreach($this->getFields($ability, true) as $field) {
            if ($field->search) {
                $searchable[$field->name] = $field;
            }
        }
        return $searchable;
    }

//    public function ajaxColumnNames($ability = \Heiw\Uxcrudible\Form\Fields\Permission::READ) {
//        $columns = array_column($this->getFields($ability, true), 'name');
//        //$columns[] = 'actions';
//        return $columns;
//    }

    protected function addOrder($builder, $model) {
        if (request()->has('order')) {
            // Remove default order
            $builder->getQuery()->orders = null;
            $fields = $this->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true, [], $model);
            // Add orders defined by request
            foreach (request()->get('order') as $order) {
                $orderByColumn = $fields[$order['column']]->name;
                if ($model->hasTranslationAttribute($orderByColumn)) {
                    $builder->orderByTranslation($orderByColumn, $order['dir']);
                } else {
                    $builder->orderBy($orderByColumn, $order['dir']);
                }
            }
        }
        return $builder;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function index() {
        $this->authorize('read', static::$model);

        if (request('parentModel') && empty(request('parentModel')['id'])) {
            $result = [
                "draw" => request()->get('draw'),
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => ""
            ];
        } else {
            // Set locale if provided
            if (request('locale')) {
                $locale = resolve(\Heiw\Uxcrudible\Classes\Locale::class);
                $locale->pushLocale(request('locale'));
            }

            $model = self::getModel();
            $builder = $this->builder();
            $totalCount = $builder->getQuery()->getCountForPagination();

            $builder
                ->skip(request()->get('start'))
                ->take(request()->get('length'));
            $builder = $this->addOrder($builder, $model);
            $entries = $builder->get();
            $sql = $builder->toSql();

            $parentModel = null;
            $parentController = null;

            //$datatableId = $this->setParent($parentModel, $parentController);
            $parentInfo = $this->getDataTableParentInfo();
            $items = [];
            foreach ($entries as $entry) {
                $item = null;
                foreach ($this->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true, [], $entry) as $field) {
                    $item[] = view($field->getBlade("uxcrud::forms.fields.show."), [
                        'displayValue' => $field->extractDisplayValue($entry),
                        'field' => $field,
                        'entry' => $entry,
                        'model' => $model,
                        'controller' => $this,
                        'dtRefreshId' => $parentInfo->dataTableId,
                        'parentModel' => $parentInfo->model
                    ])->render();
                }
                $items[] = $item;
            }

            $result = [
                "draw" => request()->get('draw'),
                "recordsTotal" => $totalCount,
                "recordsFiltered" => $totalCount,
                "data" => $items
            ];

            if (request('locale')) {
                $locale->revertLocale();
            }
        }

        return response()->json($result);
//        $controller = $this;
  //      return view("uxcrud::admin.index", compact(['controller', 'model', 'entries']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function create() {
        $this->authorize('create', static::$model);

        /* @var CrudModel */
        $model = self::getModel();
        //static::canAccessOrAbort($model, 'create');

        $modal = $this->getFormModal($model, self::ABILITY_CREATE);
        return response()->json($modal->jsonModal());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store() {
        $model = self::getModel();
        $this->authorize('create', static::$model);
        $request = request();
        $messages = [];
        $rules = $this->getValidationRules(\Heiw\Uxcrudible\Form\Fields\Permission::CREATE, $model, $request, $messages);
        $attributes = request()->validate(
            $rules,
            $messages
        );

        // MK removed otherwise Translation doesn't store on create. See Snippets
        // Only include attributes that have a field.
        //$attributes = array_intersect_key($validatedAttributes, $rules);

        // TODO  MK Check why Communication Log > Date is not stored
        $original = null;
        $this->fireFormFieldBeforeEvent(\Heiw\Uxcrudible\Form\Fields\Permission::CREATE, $original, $attributes, $rules, $messages);
        $model = $model::create($attributes);
        $this->updateRelations($model, $request);
        $this->fireFormFieldAfterEvent(\Heiw\Uxcrudible\Form\Fields\Permission::CREATE, $original, $model);

        return response()->json(
            JsonToast::create()
                ->status('success')
                ->data($model->id)
        );
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function show($id) {
        $model = self::getModel($id);
        $entry = $model;
        $this->authorize('read', $entry);

        $modal = $this->getFormModal($model, self::ABILITY_VIEW);
        return response()->json($modal->jsonModal());
    }
/*
 * // TODO MK Remove specific blades that are no longer used.
    protected function getViewModal($model) {
        $controller = $this;
        $entry = $model;

        $parentModel = null;
        $parentController = null;
        $dtRefreshId = $this->setParent($parentModel, $parentController);

        $modal = new JsonModal();
        $modal->class = 'uxcrud-view-modal';
        $modal->id = "view-" . Str::kebab($controller->getLabel()) . "-" . $model->id;
        $modal->header = __('uxcrud::uxcrud.view') .  " " . __($controller->getLabel());

        foreach ($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::VIEW) as $field) {
            $displayValue = $field->extractDisplayValue($entry);
            $modal->body .= view("uxcrud::forms.fields.show.line",
                compact(['controller', 'model', 'entry', 'displayValue', 'field', 'parentModel', 'modal', 'dtRefreshId', 'parentModel']))->render();
        }

        // Add buttons to last field
        $fields = $this->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::READ);
        $field = end($fields);
        if ($field->name == 'actions') {
            $modal->footer = view($field->getBlade('uxcrud::forms.fields.show.'),
                compact('controller', 'model', 'entry', 'field', 'dtRefreshId', 'parentModel'))->render();
        }

        $modal->size = 'xxl';
        return $modal;
    }
*/
    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function edit($id) {
        $model = self::getModel($id);
        $this->authorize('update', $model);

        $modal = $this->getFormModal($model, self::ABILITY_UPDATE);
        return response()->json($modal->jsonModal());
    }

    /**
     *
     * @param FormModal $formModal
     * @return FormModal
     */
    public function modalButtons($formModal) {
        if (!$formModal->viewOnly) {
            $formModal->addModalButton(
                DataTableButton::create($this->getButtonLabel($formModal->ability), 'fas fa-check')
                    ->type('submit')
                    ->addDataAttribute('dt-refresh', $formModal->dataTableParentInfo->dataTableId)
                    ->className('ajax-action-uxcrud-submit btn btn-success m-1')
                    ->addRawAttribute($formModal->dataTableParentInfo->dataset)
            );
        }
        return $formModal;
    }

    /**
     * Return action with label, button, permission and isUpdate record.
     * @param $action
     * @return object
     */
    public function getActionInfo($action) {
        if (is_numeric($action)) {
            switch($action) {
                case \Heiw\Uxcrudible\Form\Fields\Permission::UPDATE:
                    $action = self::ABILITY_UPDATE;
                    break;
                case \Heiw\Uxcrudible\Form\Fields\Permission::CREATE:
                    $action = self::ABILITY_CREATE;
                    break;
                case \Heiw\Uxcrudible\Form\Fields\Permission::INDEX:
                case \Heiw\Uxcrudible\Form\Fields\Permission::VIEW:
                case \Heiw\Uxcrudible\Form\Fields\Permission::READ:
                    $action = self::ABILITY_VIEW;
                    break;
                case \Heiw\Uxcrudible\Form\Fields\Permission::DELETE:
                case \Heiw\Uxcrudible\Form\Fields\Permission::RESTORE:
                case \Heiw\Uxcrudible\Form\Fields\Permission::FORCE_DELETE:
                    $action = self::ABILITY_DELETE;
                    break;
            }
        }
        switch ($action) {
            case self::ABILITY_VIEW:
                return (object)[
                    "action" => self::ABILITY_VIEW,
                    "label" => __('uxcrud::uxcrud.view'),
                    //"button" => __('uxcrud::uxcrud.create new'),
                    "permission" => \Heiw\Uxcrudible\Form\Fields\Permission::VIEW,
                    "isUpdate" => false
                ];
                break;
            case self::ABILITY_CREATE:
                return (object)[
                    "action" => self::ABILITY_CREATE,
                    "label" => __('uxcrud::uxcrud.create new'),
                    "button" => __('uxcrud::uxcrud.create new'),
                    "permission" => \Heiw\Uxcrudible\Form\Fields\Permission::CREATE,
                    "isUpdate" => false
                ];
                break;
            case self::ABILITY_UPDATE:
                return (object)[
                    "action" => self::ABILITY_UPDATE,
                    "label" => __('uxcrud::uxcrud.update'),
                    "button" => __('uxcrud::uxcrud.save'),
                    "permission" => \Heiw\Uxcrudible\Form\Fields\Permission::UPDATE,
                    "isUpdate" => true
                ];
                break;
        }
    }

    /**
     * Return FormModal in modal for model
     * @param $model            \Heiw\Uxcrudible\Models\Model to show modal for.
     * @param $action           int action to perform
     * @param bool|UxcrudController $controller
     *                          true (default): Use form based on controller details.
     *                          false: Do not use a controller and return modal without form.
     *                          UxcrudController: Use controller passed to function.
     * @param null $fields If null fields from controller will be used. Provide values to override.
     * @return FormModal Returns JsonModal
     */
    public function getFormModal($model, $action, $controller = true, $fields = null) {
        if ($controller === true) {
            $controller = $this;
        }

        $formModal = new FormModal($model, $action, $controller);
        $formModal->processController();
        if (!is_null($fields)) {
            $formModal->fields($fields);
        }
        return $formModal;
    }

    /**
     * Return edit form in modal for model
     * Included for backwards compatibility.
     * @param $model            \Heiw\Uxcrudible\Models\Model to show modal for.
     * @param $action           int action to perform
     * @param null $fields      If null fields from controller will be used. Provide values to override.
     * @param bool|UxcrudController $controller
     *                          true (default): Use form based on controller details.
     *                          false: Do not use a controller and return modal without form.
     *                          UxcrudController: Use controller passed to function.
     * @return JsonModal        Returns JsonModal
     * @throws \Throwable
     */
    public function getEditModal($model, $action, $controller = true, $fields = null) {
        $formModal = $this->getFormModal($model, $action, $controller, $fields);
        return $formModal->jsonModal();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update($id) {
        $success = 'success';
        $model = self::getModel($id);
        $this->authorize('update', $model);

        $request = request();
        $messages = [];
        $rules = $this->getValidationRules(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, $model, $request, $messages);
        $attributes = $request->validate($rules, $messages);
        $original = clone($model);
        $this->fireFormFieldBeforeEvent(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, $model, $attributes, $rules, $messages);
        $model->update($attributes);
        $this->updateRelations($model, $request);
        $this->fireFormFieldAfterEvent(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, $original, $model);

//        // Validate after storing values.
//        $rules = $this->getValidationRules(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, $model, $request, $messages);
//
//        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules, $messages);
//
//        //$attributes = $request->validate($rules, $messages);
//        if ($validator->fails()) {
//            $success = 'false';
//        } else {
//            $success = 'success';
//        }
//        // Try storing changes
//        try {
//            $this->fireFormFieldBeforeEvent(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, $model, $attributes, $rules, $messages);
//            $model->update($validator->getData());
//            $this->updateRelations($model, $request);
//            $this->fireFormFieldAfterEvent(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, $original, $model);
//        } catch(\Exception $exception) {
//            $success = 'error';
//        }
//
//        $validator->validate();

        return response()->json(
            JsonToast::create()
                ->status($success)
        );
    }

    /**
     * @param UxcrudModel $model
     * @param Request $request
     */
    protected function updateRelations(UxcrudModel $model, Request $request) {
        // Get all fields from form including groups
        $fields = array_combine(
            array_column($this->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, true, [], $model), 'name'),
            $this->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, true, [], $model)
        );

        // Get validation rules
        $messages = [];
        $rules = $this->getValidationRules(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, $model, $request, $messages);

        // Get all relations for the model
        $relations = $model::getAllRelations();

        // If there are files send with the form check for fields of File class.
        if (count($request->allFiles()) > 0) {
            $fileFields = $this->getFieldsOfType(File::class, true);
            $fileFields = array_combine(array_column($fileFields, 'name'), $fileFields);
            $relations = array_merge($relations, $fileFields);
        }

        // Ignore datatable fields as they are already stored.
        $dataTableFields = array_column($this->getFieldsOfType(DataTable::class, true), 'name');
        foreach($dataTableFields as $dataTableField) {
            if (isset($relations[Str::camel($dataTableField)])) {
                unset($relations[Str::camel($dataTableField)]);
            }
        }

        // Use only those relations that are specified in validation rules.
        $updateRelations = array_intersect_ukey($relations, $rules, function($keyA, $keyB) {
            $keyA = Str::camel(trim($keyA, '[].*'));
            $keyB = Str::camel(trim($keyB, '[].*'));

            if ($keyA == $keyB) {
                return 0;
            } else if ($keyA > $keyB) {
                return 1;
            } else {
                return -1;
            }
        });

        // Run sync on all identified relations.
        foreach($updateRelations as $relation => $type) {
            $relation = trim($relation, '[]');
            // TODO MK verify that sync works for all possible relations.

            $field = null;
            if (isset($fields[Str::snake($relation)])) {
                $field = $fields[Str::snake($relation)];
            } elseif (isset($fields[Str::snake($relation).'[]'])) {
                $field = $fields[Str::snake($relation).'[]'];
            }
            if (is_null($field) || !$field->updateRelations($model, $request, $relation, $type)) {
                switch ($type) {
                    case BelongsTo::class:
                        $model->{$relation}()->associate($request->{Str::snake($relation)});
                        break;
                    case HasMany::class:
                        break;
                    case BelongsToMany::class:
                    default:
                        $model->{$relation}()->sync($request->{Str::snake($relation)});
                        break;
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id) {
        $model = self::getModel($id);
        $this->authorize('delete', $model);
        $original = clone($model);
        $this->fireFormFieldBeforeEvent(\Heiw\Uxcrudible\Form\Fields\Permission::DELETE, $model, $attributes, $rules, $messages);
        $result = $model->delete();
        $this->fireFormFieldAfterEvent(\Heiw\Uxcrudible\Form\Fields\Permission::DELETE, $original, $model, $result);


        return response()->json(
            JsonToast::create()
                ->body($model->prettyClassName() . " `" . $model->summary() . "` " . __("has been deleted."))
                ->status('success')
        );
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore($id) {
        $model = self::getModel();
        $model = $model::onlyTrashed()->findOrFail($id);
        $this->authorize('restore', $model);

        $original = clone($model);
        $this->fireFormFieldBeforeEvent(\Heiw\Uxcrudible\Form\Fields\Permission::RESTORE, $model, $attributes, $rules, $messages);
        $result = $model->restore();
        $this->fireFormFieldAfterEvent(\Heiw\Uxcrudible\Form\Fields\Permission::RESTORE, $original, $model, $result);


        return response()->json(
            JsonToast::create()
                ->body($model->prettyClassName() . " `" . $model->summary() . "` " . __("has been restored."))
                ->status('success')
        );
    }

    /**
     * Force the removal of the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function forceDelete($id) {
        $model = self::getModel();
        $model = $model::onlyTrashed()->findOrFail($id);
        $this->authorize('forceDelete', $model);

        $original = clone($model);
        $this->fireFormFieldBeforeEvent(\Heiw\Uxcrudible\Form\Fields\Permission::FORCE_DELETE, $model, $attributes, $rules, $messages);
        $result = $model->forceDelete();
        $this->fireFormFieldAfterEvent(\Heiw\Uxcrudible\Form\Fields\Permission::FORCE_DELETE, $original, $model, $result);

        return response()->json(
            JsonToast::create()
                ->body($model->prettyClassName() . " `" . $model->summary() . "` " . __("has been permanently deleted."))
                ->status('success')
        );
    }

    /**
     * @param int $view
     * @param bool $includeGroups Set to true to include grouped fields directly.
     * @param string|string[] $pages Id or id's of the pages to include. All pages if set to null.
     * @param UxcrudModel null $model Optional modal to use for pre processing.
     * @return mixed
     */
    public function getFields($view = \Heiw\Uxcrudible\Form\Fields\Permission::READ, $includeGroups = false, $pages = [], $model = null) {
        $modelId = '';
        // Get cached generic fields.
        if (static::$addModelIdOnFieldsCache && !is_null($model)) {
            $modelId = $model->id;
        }
        $fields = self::cacheResult("getFields{$view}-{$includeGroups}-{$modelId}", function() use ($view, $includeGroups, $pages, $model) {
            if (is_string($pages)) {
                $pages = [$pages];
            }
            $fields = [];
            foreach ($this->fields($model) as $field) {
                if (isset($field->permissions[$view]) && $field->permissions[$view] !== false) {
                    if ($includeGroups && $field->isGroup) {
                        if (empty($pages) || get_class($field) !== Page::class || (get_class($field) === Page::class  && in_array($field->slug(), $pages))) {
                            $fields = array_merge($fields, $field->getFields($view, $includeGroups, $pages, $model));
                        }
                    } else {
                        $fields[] = $field;
                    }
                }
            }
            return $fields;
        });
        // If model passed pre process fields.
        if (!is_null($model)) {
            $fields = $this->auditFieldsForModel($view, $fields, $model);
        }
        return $fields;
    }

    /**
     * Override to customise fields based on $model class.
     * @param int $view
     * @param $fields
     * @param $model
     * @return mixed
     */
    public function auditFieldsForModel($view, $fields, $model) {
        if ($user = auth()->user()) {
            $fields = $user->auditFields($view, $fields, $model);
        }
        return $fields;
    }

    /**
     * @param string $name
     * @param \Heiw\Uxcrudible\Form\Fields\Permission $view
     * @param bool $includeGroups   Set to true to include grouped fields directly.
     * @return mixed
     */
    public function findField($name, $view = \Heiw\Uxcrudible\Form\Fields\Permission::READ, $includeGroups = true, $model = null) {
        $cacheName = "findField" . $name . $view . $includeGroups;
        if (!is_null($model)) {
            $cacheName .= get_class($model);
            if (isset($model->id)) {
                $cacheName .= $model->id;
            }
        }
        return self::cacheResult($cacheName, function() use ($name, $view, $includeGroups, $model) {
            $fields = $this->getFields($view, $includeGroups, [], $model);
            foreach($fields as $field) {
                // Exact match return
                if ($field->name == $name) {
                    return $field;
                }
            }
            return false;
        });
    }

    /*
     * Override to add routes for this controller.
     * @param \Heiw\Uxcrudible\Models\Model $model
     */
    public static function addRoutes(\Heiw\Uxcrudible\Models\Model $model) {

    }
}

