<!-- UxCRUD navigation.blade -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar" >
{{--    <!-- Login Check -->--}}
{{--    @if (Auth::check())--}}

{{--        <!-- User profile -->--}}
        <div class="user-profile" style="background-image: url('{{ asset('vendor/heiw/uxcrud/colors/trianglify-' . \Heiw\Uxcrudible\Classes\Colours::themeColour() . '.svg') }}');">
{{--            <!-- User profile text-->--}}
            <div class="profile-text">
{{--                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown"--}}
{{--                                         role="button" aria-haspopup="true" aria-expanded="true"--}}
{{--                                         aria-label="{{ __("Show user profile") }}">{{ Auth::user()->name }}</a>--}}
{{--                <ul class="dropdown-menu dropdown-user animated flipInY">--}}
{{--                    @yield('user-dropdown')--}}
{{--                </ul>--}}
            </div>
        </div>
{{--    @endif--}}
{{--        <!-- End User profile text-->--}}
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">{{ __("Main Links") }}</li>
                <li>
                    <a class="waves-effect waves-dark" href="/">
                        <i class="fas fa-shapes"></i>
                        <span class="hide-menu">{{ __("Dashboard") }}</span>
                    </a>
                </li>

                @foreach($menu->children($menu->rootPage()) as $child)
                    @if ($child->id != $child->parent_id)
                        <li class="nav-item {{ $child->ifActive() }}">
                            <a class="{{ $child->ifChildren('has-arrow', 'nav-link') }} waves-effect waves-dark focus-highlight" href="{{ $child->uri() }}"
                                {{ $child->ifChildren('role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"') }}>
                                <div class="flex-menu">
                                    <i class="{{ $child->icon }} fa-fw"></i>&nbsp;
                                    <div class="hide-menu" style="white-space: normal;">{{ __($child->name) }}</div>
                                </div>
                            </a>
                            @include('uxcrud::subnavigation')
                        </li>
                    @endif
                @endforeach
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer">
        @if (Auth :: check())
            <!-- item--><a href="{{ route('logout') }}" class="link focus-highlight" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-toggle="tooltip" aria-label="{{ __("Logout") }}" title="{{ __("Logout") }}">
                <i class="fas fa-power-off"></i>
            </a>
        @endif
    </div>

    <!-- End Bottom points-->
</aside>

