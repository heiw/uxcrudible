<ul aria-expanded="false" class="collapse">
@foreach($menu->children($child) as $grandChild)
    <li class="subnav {{ $grandChild->ifActive() }}">
        <a class="dropdown-item {{ $grandChild->ifActive() }} focus-highlight" href="{{ $grandChild->uri() }}"><i class="{{ $grandChild->icon }} fa-fw"></i> {{ $grandChild->name }}</a>
    </li>

    {{--<a class="{{ $child->ifChildren('has-arrow') }} waves-effect waves-dark" href="{{ $child->uri() }}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
        {{--<i class="{{ $child->icon }}"></i>--}}
        {{--<span class="hide-menu">{{ __($child->name) }}</span>--}}
    {{--</a>--}}
@endforeach
</ul>
