@inject('menu', '\Heiw\Uxcrudible\Services\Menu')
<!DOCTYPE html>
<html lang="{{ \App::getLocale() }}">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    @if(isset($currentPage) && $currentPage)
        @foreach(config('uxcrud.locales') as $code => $locale)
            @if ($currentPage->translate($code))
                <link rel="alternate" hreflang="{{ $code }}" href="{{ env('uri_scheme') }}{{ config('uxcrud.locales')[$code]['host'] }}{{ $currentPage->translate($code)->path }}" />
            @endif
        @endforeach
    @endif

    <title>@yield('title') | {{  __(Heiw\Uxcrudible\Models\SiteConfig::get('app_name')) }}</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- select2 -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.15.1/css/all.css">

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/v/dt/dt-1.10.24/b-1.7.0/b-colvis-1.7.0/cr-1.5.3/fc-3.3.2/fh-3.1.8/kt-2.6.1/r-2.2.7/rg-1.1.2/rr-1.2.7/sl-1.3.3/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.css"/>

    <!-- Material Design CSS -->
    <link href="{{ asset('vendor/heiw/uxcrud/css/style.css' . Heiw\Uxcrudible\Helpers::versionCache()) }}" rel="stylesheet">
    <!-- You can change the theme colors from currentPage->setColour('colour') or uxcrud settings-->
    <link href="{{ asset('vendor/heiw/uxcrud/colors/' . \Heiw\Uxcrudible\Classes\Colours::themeColour() . '.css' . Heiw\Uxcrudible\Helpers::versionCache()) }}" id="theme" rel="stylesheet">
    <link href="{{ asset('vendor/heiw/uxcrud/colors/uxcrud-buttons.css' . Heiw\Uxcrudible\Helpers::versionCache()) }}" id="theme" rel="stylesheet">

    <!-- Default uxcrudible styles -->
    <link href="{{ asset('vendor/heiw/uxcrud/css/uxcrudible.css' . Heiw\Uxcrudible\Helpers::versionCache()) }}" rel="stylesheet">
    <link href="{{ asset('vendor/heiw/uxcrud/css/css-chart.css' . Heiw\Uxcrudible\Helpers::versionCache()) }}" rel="stylesheet">

    <!-- Site specific styles -->
    @if (file_exists(public_path('css/uxcrudible-site.css')))
        <link href="{{ asset('css/uxcrudible-site.css' . Heiw\Uxcrudible\Helpers::versionCache()) }}" rel="stylesheet">
    @endif

    @if(isset($currentPage) && $currentPage)
        @foreach($currentPage->getCss() as $css)
            <link href="{{ asset($css . Heiw\Uxcrudible\Helpers::versionCache()) }}" rel="stylesheet" type="text/css">
        @endforeach

        {!! $currentPage->getRawCss() !!}
    @endif

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    @if (file_exists(public_path('/images/apple-touch-icon.png')))<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/images/apple-touch-icon.png') }}">@endif
    @if (file_exists(public_path('/images/favicon-32x32.png')))<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/images/favicon-32x32.png') }}">@endif
    @if (file_exists(public_path('/images/favicon-16x16.png')))<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/images/favicon-16x16.png') }}">@endif
    @if (file_exists(public_path('/images/site.webmanifest')))<link rel="manifest" href="{{ asset('/images/site.webmanifest') }}">@endif
</head>

<body class="@if (Auth::check()){{ Auth::user()->allRolesWithPrefix() }} @endif{{ \Heiw\Uxcrudible\Classes\Colours::themeColour() }} fix-header fix-sidebar card-no-border"
@if (Auth()->check() && Heiw\Uxcrudible\Uxcrud\UnlockSession::hasSessionTimeout())
    data-session-timeout="{{ Heiw\Uxcrudible\Uxcrud\UnlockSession::getTimeout() }}"
    data-session-dialog-showtime="{{ Heiw\Uxcrudible\Uxcrud\UnlockSession::getDialogShowtime() }}"
    data-session-unlock="{{ Illuminate\Support\Str::slug(__("uxcrud::slugs.unlock")) }}"
    data-session-extend="{{ Illuminate\Support\Str::slug(__("uxcrud::slugs.extend-session")) }}"
@endif
>
<!-- Skip to main content link (Accessibility) -->
<a href="#main-content" class="skip-link" tabindex="1">Skip to main content</a>

<!-- Main wrapper - style you can find in pages.scss -->
<div id="main-wrapper">

    <!-- Topbar header - style you can find in pages.scss -->
    <header class="topbar">
        @include('uxcrud::header')
    </header>

    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    @include('uxcrud::navigation')
    <!-- Page wrapper with left margin  -->
    <div class="page-wrapper">

        <!-- Container fluid  -->
        <div class="container-fluid">

            <!-- Bread crumb and right sidebar toggle -->
            <div class="row page-titles">
                <div class="align-self-center">
                    <h1 class="text-themecolor m-b-0 m-t-0">{{ @$currentPage->name }}</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            @foreach($menu->breadCrumbs() as $page)
                                <li class="breadcrumb-item {{ $page->ifActive() }}">
                                    <a href="{{ $page->uri() }}" class="focus-highlight">{{ $page->summary() }}</a>
                                </li>
                            @endforeach

                            @if (@$controller)
                                <li class="breadcrumb-item active" aria-current="page"><a href="{{ $controller->getRoute() }}">{{ __($controller->getLabel()) }}</a></li>
                                @yield('page_breadcrumb')
                            @endif
                        </ol>
                    </nav>
                </div>
                <div class="col-md-7 col-4 align-self-center">

                </div>
            </div>

            <div class="outdated-browser-warning alert alert-warning hide">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <i class="fa fa-times"></i> </button>
                <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> {{ __('Outdated browser detected') }}</h3> {!! Heiw\Uxcrudible\Models\Snippet::get('outdated_browser_alert') !!}
            </div>

            @if(Heiw\Uxcrudible\Models\Gdpr::isGdprActive())
                @include('uxcrud::gdpr')
            @else
                <!-- Page Content -->
                <div id="main-content" tabindex="-1">
                    @yield('content')
                </div>
            @endif
        </div>

        <!-- footer -->
{{--        <footer class="footer">--}}
{{--            <small>© 2019 HEIW. Theme by wrappixel.com</small>--}}
{{--        </footer>--}}

    </div>
</div>
<footer class="footer">
    <small>© 2019 HEIW. Theme by wrappixel.com</small>
</footer>

            <!-- ============================================================== -->
<!-- All jQuery -->
<!-- ============================================================== -->

<!-- Bootstrap tether Core JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" crossorigin="anonymous"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!-- slimscrollbar scrollbar JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
<!--Wave Effects -->
<script src="//cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.js"></script>
<!--Menu sidebar -->
<script src="{{ asset('vendor/heiw/uxcrud/js/sidebarmenu.js' . Heiw\Uxcrudible\Helpers::versionCache()) }}"></script>
<!--stickey kit -->
<script src="//cdnjs.cloudflare.com/ajax/libs/sticky-kit/1.1.3/sticky-kit.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>
<!-- Bootstrap Toast notifications -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/v/dt/dt-1.10.24/b-1.7.0/b-colvis-1.7.0/cr-1.5.3/fc-3.3.2/fh-3.1.8/kt-2.6.1/r-2.2.7/rg-1.1.2/rr-1.2.7/sl-1.3.3/datatables.min.js"></script>

<!-- TODO MK Only include when used -->
<!-- CKEditor -->
<script src="//cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>

<!-- Dropzone -->
<script src="{{ asset('vendor/heiw/uxcrud/js/dropzone.js' . Heiw\Uxcrudible\Helpers::versionCache()) }}"></script>

<!--Custom JavaScript -->
<script src="{{ asset('vendor/heiw/uxcrud/js/custom.min.js' . Heiw\Uxcrudible\Helpers::versionCache()) }}"></script>


<script src="{{ asset('vendor/heiw/uxcrud/js/uxcrudible.js' . Heiw\Uxcrudible\Helpers::versionCache()) }}"></script>
<script src="{{ asset('vendor/heiw/uxcrud/js/lang/' . App::getLocale() . '.js' . Heiw\Uxcrudible\Helpers::versionCache()) }}"></script>

<!-- Site specific script -->
@if (file_exists(public_path('js/uxcrudible-site.js')))
    <script src="{{ asset('js/uxcrudible-site.js' . Heiw\Uxcrudible\Helpers::versionCache()) }}"></script>
@endif

@if(isset($currentPage) && $currentPage)
    @foreach($currentPage->getJs() as $js)
        <script src="{{ asset($js . Heiw\Uxcrudible\Helpers::versionCache()) }}"></script>
    @endforeach

    {!! $currentPage->getRawJs() !!}
@endif

@yield('script')
<script type="text/javascript">
    @yield('inline-script')
</script>
</body>

</html>
