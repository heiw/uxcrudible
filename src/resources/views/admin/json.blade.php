@foreach ($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::READ) as $field)
    <td class="{{ $field->name }}">
        @if (!empty($field->filter))
            @includeIf($field->getBlade("heiw.form::filters."), [
                'value' => @(old($field->name, $field->extractFilterValue())),
            ])
        @endif
    </td>
@endforeach