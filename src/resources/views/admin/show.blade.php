@foreach ($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::VIEW)  as $field)
    @php
        $displayValue = $field->extractDisplayValue($entry);
    @endphp
    <div class="row row-{{ $field->name }}">
        <label class="col-sm-11 col-md-3"><i class="{{ $field->icon }}"></i>&nbsp; {!! __($field->getLabel()) !!}</label>
        <div class="col-sm-12 col-md-9 {{ $field->name }}">
            @include($field->getBlade("uxcrud::forms.fields.show."))
        </div>
    </div>
@endforeach
