@if ($ability->isUpdate)
    @method('PATCH')
@endif
@csrf

{{--@foreach ($controller->getFields($ability->permission) as $field)--}}
    {{--@include("uxcrud::forms.fields.edit.line")--}}
{{--@endforeach--}}