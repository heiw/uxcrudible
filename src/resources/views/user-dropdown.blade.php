@extends('uxcrud::user-dropdown-custom')

@section('user-dropdown')
    <li class="uxcrud-username">
        <div class="dw-user-box">
            <div class="u-text">
                <h4>{{ Auth::user()->summary() }}</h4>
                {{--                                        <p class="text-muted">{{ Auth::user()->email }}</p><a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a>--}}
            </div>
        </div>
    </li>

    @parent

    @php($user = auth()->user())
    @can('update', $user)
        <li class="uxrud-update">
            <a href="{{ Heiw\Uxcrudible\Uxcrud\User::getAjaxRoute() }}/{{ $user->id }}/{{ Illuminate\Support\Str::slug(__('uxcrud::slugs.edit')) }}"
               class="modal-action focus-highlight">
                <i class="far fa-user fa-fw"></i> {{ __('Edit profile') }}
            </a>
        </li>
        <li class="uxrud-change-password">
            <a href="{{ Heiw\Uxcrudible\Uxcrud\ChangePassword::getAjaxRoute() }}/{{ $user->id }}/"
               class="modal-action focus-highlight">
                <i class="fas fa-key fa-flip-horizontal fa-fw"></i> {{ __('Change password') }}
            </a>
        </li>
        @else
    @endcan
    <li class="uxrud-gdpr">
        <a href="{{ route('gdpr.view') }}" class="modal-action focus-highlight">
            <i class="fas fa-user-shield fa-fw"></i> {{ __('Privacy policy') }}
        </a>
    </li>

    <li role="separator" class="divider"></li>
    @impersonating
    <li class="uxrud-end-impersonation">
        <a href="{{ route('user.end-impersonation') }}" onclick="event.preventDefault(); document.getElementById('end-impersonating').submit();" class="focus-highlight">
            <i class="fas fa-theater-masks fa-fw"></i> {{ __('End impersonating') }}
        </a>
        <form id="end-impersonating" action="{{ route('user.end-impersonation') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
    @endImpersonating
    @if (Auth()->check() && Heiw\Uxcrudible\Uxcrud\UnlockSession::hasSessionTimeout())
        <li  class="uxrud-suspend-session">
            <a href="#" id="suspend-session" class="modal-action focus-highlight">
                <i class="far fa-moon fa-flip-horizontal  fa-fw"></i> {{ __('Suspend session') }}
            </a>
        </li>
    @endif
    <li class="uxrud-logout">
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="focus-highlight">
            <i class="fas fa-power-off fa-fw"></i> {{ __('Logout') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
@endsection

