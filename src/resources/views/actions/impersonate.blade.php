@can('impersonate', auth()->user(), $entry)
    <form method="POST" action="{{ Heiw\Uxcrudible\Uxcrud\User::getAjaxRoute() }}/{{ $entry->id }}/{{ Illuminate\Support\Str::slug(__('uxcrud::slugs.impersonate')) }}">
        @csrf
        <div class="field">
            <div class="control">
                <button title="{{ __('Impersonate') }} {{ $entry->summary() }}" type="submit" class="btn btn-outline-info m-1 float-right toast-action"
                    data-confirm-alert="info"
                    data-confirm-button-label="{{ __('Impersonate') }}" data-confirm-button-icon="fas fa-theater-masks"
                    data-confirm="{{ __('Are you sure you want to impersonate `:name`?', ["name" => $entry->summary()]) }}"
                ><i class="fas fa-theater-masks"></i><span class="label-text"> {{ __('Impersonate user') }}</span></button>
            </div>
        </div>
    </form>

@endcan
