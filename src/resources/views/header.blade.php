<!-- UxCRUD header.blade -->
<nav class="navbar top-navbar navbar-expand-md navbar-light">
    <!-- ============================================================== -->
    <!-- Logo -->
    <!-- ============================================================== -->
    <div class="navbar-header">
        <a class="navbar-brand text-white focus-highlight" href="/" title="{{ __(Heiw\Uxcrudible\Models\SiteConfig::get('app_name')) }}"><i class="{{ Heiw\Uxcrudible\Models\SiteConfig::get('app_icon') }}"></i>
            <span>{{ __(Heiw\Uxcrudible\Models\SiteConfig::get('app_abbr')) }}</span></a>
    </div>
    <!-- ============================================================== -->
    <!-- End Logo -->
    <!-- ============================================================== -->


    <div class="navbar-collapse">
        <!-- ============================================================== -->
        <!-- toggle and nav items -->
        <!-- ============================================================== -->
        <ul class="navbar-nav mr-auto mt-md-0">
        @if (Auth::check())
            <!-- This is  -->
            <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark focus-highlight" href="javascript:void(0)" aria-label="{{ __("Show or hide navigation") }}"><i class="fas fa-bars"></i></a> </li>
            <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark focus-highlight" href="javascript:void(0);" aria-label="{{ __("Show or hide sidebar") }}"><i class="fas fa-bars"></i></a> </li>
        @endif
        </ul>
        <!-- ============================================================== -->
        <!-- User profile and search -->
        <!-- ============================================================== -->

        <ul class="navbar-nav my-lg-0 uxcrud-user-profile">
            <!-- ============================================================== -->
            <!-- End Messages -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item float-right">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
            <!-- ============================================================== -->
                <!-- Comment -->
                <!-- ============================================================== -->
{{--            --}}
{{--                <li class="nav-item dropdown">--}}
{{--                    <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="{{ __("Show messages") }}"> <i class="far fa-envelope"></i>--}}
{{--                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>--}}
{{--                    </a>--}}
{{--                    <div class="dropdown-menu dropdown-menu-right mailbox scale-up">--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <div class="drop-title">Notifications</div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <div class="message-center">--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span> </div>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>Event today</h5> <span class="mail-desc">Just a reminder that you have event</span> <span class="time">9:10 AM</span> </div>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>Settings</h5> <span class="mail-desc">You can customize this template as you want</span> <span class="time">9:08 AM</span> </div>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>--}}
{{--                                        <div class="mail-contnet">--}}
{{--                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
                <!-- ============================================================== -->
                <!-- End Comment -->
                <!-- ============================================================== -->

                <li class="nav-item dropdown" aria-pressed="false" aria-label="{{ __('Show user menu') }}" >
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark menu-block" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="{{ __('Show user menu') }}"><i class="fas fa-user"></i></a>
                    <div id="dropDownMenu" class="focus-highlight dropdown-menu dropdown-menu-right scale-up">
                        <ul class="dropdown-user">
                            @include('uxcrud::user-dropdown')
                            @yield('user-dropdown')
                        </ul>
                    </div>
                </li>

{{--            ORIGINAL--}}
{{--            <li class="nav-item dropdown">--}}
{{--                <a tabindex="0" class="nav-link dropdown-toggle text-muted waves-effect waves-dark menu-block" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="{{ __('Show user menu') }}"><i class="fas fa-user"></i></a>--}}
{{--                <div role="menu" id="dropdown-menus" class="dropdown-menu dropdown-menu-right scale-up">--}}
{{--                    <ul class="dropdown-user">--}}
{{--                        @include('uxcrud::user-dropdown')--}}
{{--                        @yield('user-dropdown')--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </li>--}}
            @endguest

            <!-- ============================================================== -->
            <!-- Language -->
            <!-- ============================================================== -->

            <li class="nav-item dropdown" aria-pressed="false" aria-label="{{ __('Languages drop down menu') }}">
                @php
                    $currentPage = $menu->currentPage();
                    $locales = config('uxcrud.locales');
                    $currentLocale = $locales[App::getLocale()];
                @endphp
                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark menu-block" href="{{ @request()->path() }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="{{ __("Current language :language", ["language" => __($currentLocale['name'])]) }}"><i class="flag-icon {{ $currentLocale['icon'] }}"></i></a>
                <div id="dropDownMenuLang" class="focus-highlight dropdown-menu dropdown-menu-right scale-up">
                    @if(isset($currentPage) && $currentPage)
                        @foreach($currentPage->translations as $id => $translation)
                            <a class="dropdown-item" href="{{ env('URI_SCHEME') }}{{ $locales[$translation->locale]['host'] }}{{ $translation->path }}"><i class="flag-icon {{ $locales[$translation->locale]['icon'] }}"></i>{{ $locales[$translation->locale]['name'] }}</a>
                        @endforeach
                    @endif
                </div>
            </li>

{{--            <li class="nav-item dropdown">--}}
{{--                @php--}}
{{--                    $currentPage = $menu->currentPage();--}}
{{--                    $locales = config('uxcrud.locales');--}}
{{--                    $currentLocale = $locales[App::getLocale()];--}}
{{--                @endphp--}}
{{--                <div tabindex="0" role="button" class="nav-link dropdown-toggle text-muted waves-effect waves-dark focus-highlight"--}}
{{--                     href="{{ @request()->path() }}"--}}
{{--                     data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"--}}
{{--                     aria-label="{{ __("Current language :language", ["language" => __($currentLocale['name'])]) }}"><i class="flag-icon {{ $currentLocale['icon'] }}"--}}
{{--                    ></i></div>--}}
{{--                <div class="dropdown-menu dropdown-menu-right scale-up dropdown-menus" id="dropdown-menus">--}}

{{--                    @if(isset($currentPage) && $currentPage)--}}
{{--                        @foreach($currentPage->translations as $id => $translation)--}}
{{--                            <a class="dropdown-item" href="{{ env('URI_SCHEME') }}{{ $locales[$translation->locale]['host'] }}{{ $translation->path }}"><i class="flag-icon {{ $locales[$translation->locale]['icon'] }}"></i>{{ $locales[$translation->locale]['name'] }}</a>--}}
{{--                        @endforeach--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            </li>--}}
        </ul>
    </div>
</nav>
