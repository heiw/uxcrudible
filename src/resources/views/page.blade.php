@extends('uxcrud::app')

@section('title')
    @if($currentPage && !empty($currentPage->title))
        {{ __($currentPage->title) }}
    @endif
@endsection

@section('content')
    @php($user = auth()->user())
    @if($currentPage && $currentPage->content)
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        {!! $currentPage->content !!}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($currentPage && $currentPage->models->isNotEmpty())
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav fragment-select nav-tabs customtab" role="tablist">
                        @foreach($currentPage->models as $model)
                            @if($user && $user->hasModelPermission('read', $model->getModelNamespaced(false)))
                                <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#{{ $model->slug() }}" data-model="{{ $model->slug() }}" role="tab" title="{{ $model->display }}">
                                        <span class="hidden-sm-up"><i class="{{ $model->icon() }}"></i></span> <span class="hidden-xs-down"><i class="{{ $model->icon() }}"></i> {{ $model->display }}</span></a>
                                </li>
                            @endif
                        @endforeach
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach($currentPage->models as $model)
                            @if($user && $user->hasModelPermission('read', $model->getModelNamespaced(false)))
                                <div class="tab-pane" id="{{ $model->slug() }}" role="tabpanel">
                                    <div class="p-20">
                                        @include('uxcrud::datatable')
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

{{--     Show card link for page children if exists --}}
    @if ($currentPage && $currentPage->children->isNotEmpty())
        @inject('menu', '\Heiw\Uxcrudible\Services\Menu')
        @php($count = $currentPage->children->count())
        @php($i = 0)
{{--        <div class="d-flex row-eq-height flex-wrap">--}}
        <div class="flex-container">   <!-- style.css -->
            @foreach($menu->children($currentPage) as $key => $child)
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-3 col-xs-6 float-left">
                    <a href="{{ $child->uri() }}" class="card-icon card-body card card-jump-panel card-inverse card-{{ \Heiw\Uxcrudible\Classes\Colours::get($i, $count) }} hover-inverse">
                        <div style="font-size: medium">
                            <i class="{{ $child->icon }}"></i>
                            {{ $child->name }}
                        </div>
                    </a>
                </div>
                @php($i++)
            @endforeach
        </div>
{{--        </div>--}}
    @endif

    @parent
@endsection
