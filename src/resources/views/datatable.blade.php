@php
    if(!isset($controller)) {
        $controller = $model->getControllerInstance();
    }
    $instance = $model->getModelInstance();
@endphp

@if (!$controller)
    <div class="has-alert alert alert-danger">Class `{{ $model->getControllerNamespaced() }}` not found</div>
@else
    @can('create', $instance)
        <a href="{{ $controller->getAjaxRoute() }}/{{ Illuminate\Support\Str::slug(__('uxcrud::slugs.create')) }}"
           class="modal-action btn btn-outline-info m-1 uxcrud-create-{{ Illuminate\Support\Str::kebab($instance->prettyClassName()) }}"
           role="button"><i
                class="far fa-plus-square"></i>&nbsp;<span class="first-capital">{{ __('uxcrud::uxcrud.create new') }} {{ __($controller->getLabel()) }}</span>
        </a>
    @endcan
    <table id="{{ $controller->getId() }}" class="display data-table" style="width:100%"
           data-ajax-url="{{ $controller->getAjaxRoute() }}"
           data-row-reorder-url="{{ $controller->getRowReorderRoute() }}"
           data-export-url="{{ $controller->getExportRoute() }}"
           data-columns="{{ json_encode(
               $controller->dataTableColumnNames(
                   \Heiw\Uxcrudible\Form\Fields\Permission::INDEX
               )
           ) }}"
           data-data-table-options="{{ $controller->dataTableOptionsJSON() }}"
           data-order="{{ json_encode($controller->dataTableDefaultSort()) }}"

    >
        <thead>
        <tr>
            @foreach ($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true) as $dt_field)
                <th class="{{ $dt_field->name }} focus-highlight">
                    @if(get_class($dt_field) == Heiw\Uxcrudible\Form\Fields\Actions::class)
                        <div class="float-right">
                            <div class="btn-group uxcrud-datatable-select-locale" role="group" aria-label="{{ __("Select language") }}">
                                @if($instance && $instance->hasTranslationLocale()) @foreach($instance->translationLocales() as $code => $locale)
                                    <label class="btn btn-radio btn-outline-info @if($locale['active']) active @endif" title="{{ $locale['name'] }}">
                                       <input type="radio" name="locale" value="{{ $code }}" autocomplete="off" @if($locale['active']) checked @endif> {{ $code }}
                                    </label>
                                @endforeach @endif
                            </div>
                        </div>
                    @endif
                    @if($dt_field->icon)<i class="{{ $dt_field->icon }}"></i> @endif{!! __($dt_field->getIndexLabel()) !!}
                </th>
            @endforeach
        </tr>
        <tr class="uxcrud-filters">
            @foreach ($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true) as $dt_field)
            <td class="{{ $dt_field->name }} focus-within-highlight">
                    @if (!empty($dt_field->filter))
                        @includeIf($dt_field->getBlade("uxcrud::forms.filters."), [
                            'value' => @(old($dt_field->name, $dt_field->extractFilterValue())),
                            'field' => $dt_field,
                            'model' => $instance,
                            'controller' => $controller,
                        ])
                    @endif
                </td>
            @endforeach
        </tr>
        </thead>
        <tbody>
            <tr><td colspan="{{ count($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true)) }}"><i class="fas fa-spinner fa-spin"></i> {{ __('Loading data') }}</td></tr>
        </tbody>
        <tfoot>
        <tr>
            @foreach ($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true) as $dt_field)
                <th class="{{ $dt_field->name }}">@if($dt_field->icon)<i class="{{ $dt_field->icon }}"></i> @endif{!! __($dt_field->getIndexLabel()) !!}</th>
            @endforeach
        </tr>
        </tfoot>
    </table>
@endif
