@if(isset($parentModel) && get_class($parentModel) == $field->model)
    <input type="hidden" name="{{ $field->name }}" value="{{ $parentModel->id }}" readonly/>
@else
    <select class="uxcrud-filter filter-{{ $field->name }} select2 form-control"
        aria-label="{{ __('Filter :nameOfTable table by :nameOfFilter', ['nameOfFilter' => $field->getPrettyName(), 'nameOfTable' => $model->prettyClassName()]) }}"
        name="{{ $field->name }}" {{ ($field->multiple ? 'multiple' : '') }}
        @if($field->remoteData) data-remote-data="true" @endif
    >
        @if(!$field->remoteData)
            @foreach($field->filterOptions($model) as $option)
                <option value="{{ $option->{$field->valueName} }}"
                    @if ($field->multiple && isset($value) && (in_array((int)$option->id, $value) || in_array((int)$option->id, $field->defaultFilterValue)) ||
                        (!$field->multiple && (($option->id === @$value) || ($option->id === $field->defaultFilterValue))))
                            {{' selected '}}
                    @endif
                    @if(method_exists($option, trim($field->labelName, '()')))
                        >{{  @call_user_func(array($option, trim($field->labelName, '()'))) }}</option>
                    @else
                        >{{ $option->{$field->labelName} }}</option>
                    @endif
            @endforeach
        @endif
    </select>
@endif
