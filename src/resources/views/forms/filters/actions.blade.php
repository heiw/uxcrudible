<div class="d-flex justify-content-center">
    <button class="uxcrud-apply-filter btn btn-outline-info m-1" type="button" aria-label="{{ __("Apply filter") }}"><i class="fas fa-search"></i></button>

    <button class="uxcrud-clear-filter btn btn-outline-info m-1" type="button" aria-label="{{ __("Clear filter") }}">
        <span class="fa-stack fa-xs">
            <i class="fas fa-filter fa-stack-1x"></i>
            <i class="fas fa-times fa-stack-1x fac-lower-right"></i>
        </span>
    </button>

    <select name="uxcrud-trashed-filter" class="uxcrud-filter uxcrud-trashed-filter form-control" aria-label="{{ __("Show items") }}">
        <option value="">{{ __("Active") }}</option>
        <option value="only">{{ __("Deleted") }}</option>
        <option value="with">{{ __("All") }}</option>
    </select>
</div>