@if(!isset($field->filter) || !method_exists($field->filter, 'filterOptions'))
    <input aria-label="{{ __('Filter :nameOfTable by :nameOfFilter', ['nameOfFilter' => $field->getPrettyName(), 'nameOfTable' => $model->prettyClassName()]) }}" name="{{ $field->name }}"
           class="uxcrud-filter filter-{{ $field->name }} form-control" type="search"
           placeholder="{{ __('uxcrud::uxcrud.search') }} {{ __($field->label) }}"
           value="{{ $field->defaultFilterValue }}"/>
@else
    <select class="uxcrud-filter {{ $field->filter->name }} select2 form-control"
            aria-label="{{ __('Filter :nameOfTable by :nameOfFilter', ['nameOfFilter' => $field->getPrettyName(), 'nameOfTable' => $model->prettyClassName()]) }}"
            name="{{ $field->filter->name }}" {{ ($field->filter->multiple ? 'multiple' : '') }}>
        @foreach($field->filter->filterOptions($model) as $option)
            <option value="{{ $option->{$field->filter->valueName} }}"
            @if (
                (!$field->filter->multiple && $option->id == $field->defaultFilterValue))
                {{' selected '}}
                    @endif >{{ $option->{$field->filter->labelName} }}</option>
        @endforeach
    </select>
@endif
