<div class="input-date-range input-group">
    <input aria-label="{{ __('Filter :nameOfTable by :nameOfFilter', ['nameOfFilter' => $field->getPrettyName(), 'nameOfTable' => $model->prettyClassName()]) }}"
       name="{{ $field->name }}-data[]"
       class="uxcrud-filter uxcrud-filter-single filter-{{ $field->name }} form-control" type="date"
       placeholder="{{ __('uxcrud::uxcrud.search') }} {{ __($field->label) }}"
       value="{{ $field->defaultFilterValue }}"/>

    <div class="input-group-append uxcrud-filter-multi">
        <span class="input-group-text btn-outline-info b-0 ">{{ __('to') }}</span>
    </div>

    <input aria-label="{{ __('Filter :nameOfTable by :nameOfFilter', ['nameOfFilter' => $field->getPrettyName(), 'nameOfTable' => $model->prettyClassName()]) }}"
       name="{{ $field->name }}-data[]"
       class="uxcrud-filter-multi uxcrud-filter filter-{{ $field->name }} form-control" type="date"
       placeholder="{{ __('uxcrud::uxcrud.search') }} {{ __($field->label) }}"
       value="{{ $field->defaultFilterValue }}"/>

    <div class="input-group-append uxcrud-date-icon input-group">
        <span class="input-group-text btn-outline-info"></span>
        <select class="uxcrud-filter uxcrud-filter-date {{ $field->filter->name }} form-control btn-outline-info"
                aria-label="{{ __('Filter :nameOfTable by :nameOfFilter', ['nameOfFilter' => $field->getPrettyName(), 'nameOfTable' => $model->prettyClassName()]) }}"
                name="{{ $field->filter->name }}" {{ ($field->filter->multiple ? 'multiple' : '') }}>
            @foreach($field->filter->filterOptions($model) as $option)
                <option value="{{ $option->{$field->filter->valueName} }}"
                @if (
                    (!$field->filter->multiple && $option->id == $field->defaultFilterValue))
                    {{' selected '}}
                        @endif >{{ $option->{$field->filter->labelName} }}</option>
            @endforeach
        </select>
    </div>


</div>

