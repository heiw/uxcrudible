<select class="uxcrud-filter {{ $field->filter->name }} select2 form-control"
    aria-label="{{ __('Filter :nameOfTable by :nameOfFilter', ['nameOfFilter' => $field->getPrettyName(), 'nameOfTable' => $model->prettyClassName()]) }}"
    name="{{ $field->filter->name }}" {{ ($field->filter->multiple ? 'multiple' : '') }}>
    @foreach($field->filter->filterOptions($model) as $option)
        <option value="{{ $option->{$field->filter->valueName} }}"
        @if (
            (!$field->filter->multiple && $option->id === $field->defaultFilterValue))
            {{' selected '}}
                @endif >
            {{ $option->{$field->filter->labelName} }}
        </option>
    @endforeach
</select>
