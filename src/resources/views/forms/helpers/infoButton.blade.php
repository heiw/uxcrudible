@if ($html)
<span id="{{ $id }}" class="hide">
    {!! $content !!}
</span>
@endif
<a class="{{ $classes }} input-group-text" @if($html)data-modal-content-id="{{ $id }}"@else data-modal-content="{{ $content }}" @endif title="{{ $title }}"><i class="{{ $icon }} fa-fw"></i></a>
