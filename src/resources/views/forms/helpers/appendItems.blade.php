@if(!empty($field->appendItems))
    <div class="input-group-append">
        @foreach($field->appendItems as $item)
            {!! $item !!}
        @endforeach
    </div>
@endif
