<table class="uxcrud-matrix uxcrud-show">
    <thead><th>{{ $field->labelY() }} \ {{ $field->labelX()}}</th>

    @foreach($field->optionsX($model) as $labelX)
        <th>{{ $labelX->name }}</th>
    @endforeach
    </thead>

    <tbody>
    @include($field->getEntryTypeBlade("uxcrud::forms.fields.show.matrix."))
    </tbody>
</table>