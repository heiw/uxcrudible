@if($displayValue)
    <ul>
    @foreach($displayValue as $file)
        <li><a href="{{ $field->getDownloadRoute() }}/{{ $file->id }} "><i class="fas fa-download"></i> {{ $file->name }}</a>
        </li>
    @endforeach
    </ul>
@endif