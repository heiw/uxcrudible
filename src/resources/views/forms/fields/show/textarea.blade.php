@if ($field->condensed)
<div class="limit-view">
    <div class="long-content-condensed">
        @if(!is_null($displayValue)){{ $displayValue }}@endif
    </div>
</div>
@else
    @if(!is_null($displayValue)){{ $displayValue }}@endif
@endif