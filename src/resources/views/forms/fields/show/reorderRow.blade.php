@if(isset($groupField) && get_class($groupField) === Heiw\Uxcrudible\Form\Fields\HasManyPivot::class)
{{ $displayValue }}
@else
<span class="uxcrud-row-reorder" data-id="{{ $entry->id }}" data-value="{{ $displayValue }}">@if(!is_null($displayValue)){{ $displayValue }}@endif</span>
@endif