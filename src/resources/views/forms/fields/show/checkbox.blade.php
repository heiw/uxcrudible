@if($displayValue)
    <i class="fas fa-check text-success"></i>
@else
    <i class="fas fa-square text-light"></i>
@endif
