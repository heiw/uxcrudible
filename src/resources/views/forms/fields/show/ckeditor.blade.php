@if ($field->condensed)
    <div class="limit-view">
        <div class="long-content-condensed">
            {!! $displayValue !!}
        </div>
    </div>
@else
    {!! $displayValue !!}
@endif