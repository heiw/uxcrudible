@php
    $locale = resolve(\Heiw\Uxcrudible\Classes\Locale::class);
    $locale->pushLocale($locale->localeFromHost());
@endphp
<div class="d-flex justify-content-center">
    @foreach($field->actions() as $action)
        @if($entry && $entry->id && !$entry->trashed())
            @switch($action->name)
                @case('update')
                    @can('update', $model)
                        <a href="{{ $controller->getAjaxRoute() }}/{{ $entry->id }}/{{ Illuminate\Support\Str::slug(__('uxcrud::slugs.edit')) }}"
                           data-dt-refresh="{{ $dtRefreshId }}"
                           @if ($parentModel)
                               data-parent-model[id]="{{ $parentModel->id }}"
                               data-parent-model[name]="{{ get_class($parentModel) }}"
                           @endif
                           data-locale="{{ $locale->getOriginalLocale() }}"
                           title="{{ __('uxcrud::uxcrud.edit') }} {{ $entry->summary() }}"
                           class="uxcrud-btn-update modal-action btn btn-outline-info m-1 float-right"
                           role="button"><i
                                    class="fas fa-pencil-alt"></i><span class="label-text"> {{ __('uxcrud::uxcrud.edit') }}</span></a>
                    @endcan
                @break

                @case('read')
                    @can('read', $model)
                        <a title="{{ __('uxcrud::uxcrud.view') }} {{ $entry->summary() }}"
                           href="{{ $controller->getAjaxRoute() }}/{{ $entry->id }}"
                           data-locale="{{ $locale->getOriginalLocale() }}"
                           class="uxcrud-btn-view modal-action btn btn-outline-info m-1 float-right"
                           role="button"><i
                                    class="far fa-eye"></i><span class="label-text"> {{ __('uxcrud::uxcrud.view') }}</span></a>
                    @endcan
                @break

                @case('delete')
                    @can('delete', $model)
                        <form method="POST" action="{{ $controller->getAjaxRoute() }}/{{ $entry->id }}" class="uxcrud-frm-delete">
                            @method('DELETE')
                            @csrf

                            <div class="field">
                                <div class="control">
                                    <button title="{{ __('uxcrud::uxcrud.delete') }} {{ $entry->summary() }}" type="submit" class="uxcrud-btn-delete btn btn-outline-danger m-1 float-right toast-action"
                                            data-dt-refresh="{{ $dtRefreshId }}"
                                            data-confirm="{{ __('Are you sure you want to delete the :model `:name`?', ["model" => $model->prettyClassName(), "name" => $entry->summary()]) }}"><i
                                                class="fas fa-times-circle"></i><span class="label-text"> {{ __('uxcrud::uxcrud.delete') }}</span></button>
                                </div>
                            </div>
                        </form>
                    @endcan
                @break
            @endswitch
        @endif

        @if($entry && $entry->id && $entry->trashed())
            @switch($action->name)
                @case('restore')
                    @if($entry->trashed())
                        @can('restore', $model)
                            <form class="" method="POST" action="{{ $controller->getAjaxRoute() }}/{{ $entry->id }}/{{ \Illuminate\Support\Str::slug(__('uxcrud::slugs.restore')) }}" class="uxcrud-frm-restore">
                                @csrf

                                <div class="field">
                                    <div class="control">

                                        <button title="{{ __('uxcrud::uxcrud.restore') }} {{ $entry->summary() }}" type="submit" class="uxcrud-btn-restore btn btn-outline-warning m-1 float-right toast-action"
                                                data-dt-refresh="{{ $dtRefreshId }}"
                                                data-confirm="{{ __('Are you sure you want to restore the :model `:name`?', ["model" => $model->prettyClassName(), "name" => $entry->summary()]) }}"
                                                data-confirm-alert="warning"
                                        ><i class="fas fa-trash-restore-alt"></i>
                                            <span class="label-text"> {{ __('uxcrud::uxcrud.restore') }}</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @else
                            <div title="{{ __(':name has been soft deleted', ['name' => $entry->summary() ]) }}" class="uxcrud-btn-deleted m-1 float-right"
                            ><i class="fas fa-ban"></i>
                            </div>

                        @endcan
                    @endif
                @break

                @case('force-delete')
                    @if($entry->trashed())
                        @can('force-delete', $model)
                            <form method="POST" action="{{ $controller->getAjaxRoute() }}/{{ $entry->id }}/{{ \Illuminate\Support\Str::slug(__('uxcrud::slugs.force delete')) }}" class="uxcrud-frm-force-delete">
                                @csrf
                                <div class="field">
                                    <div class="control">
                                        <button title="{{ __('uxcrud::uxcrud.force delete') }} {{ $entry->summary() }}" type="submit" class="uxcrud-btn-force-delete btn btn-outline-danger m-1 float-right toast-action"
                                                data-dt-refresh="{{ $dtRefreshId }}"
                                                data-confirm="{{ __('Are you sure you want to permanently delete the :model `:name`?', ["model" => $model->prettyClassName(), "name" => $entry->summary()]) }}"><i
                                                    class="fas fa-exclamation-triangle"></i><span class="label-text"> {{ __('uxcrud::uxcrud.force delete') }}</span></button>
                                    </div>
                                </div>
                            </form>
                        @endcan
                    @endif
                @break
            @endswitch
        @endif
        @if (!is_null($action->getBlade()))
            @includeIf($action->getBlade())
        @endif
    @endforeach
</div>
@php
    $locale->revertLocale();
@endphp
