<ul><!-- hasMany -->
@if ($displayValue)
    @if (!is_string($displayValue) && (is_array($displayValue) || get_class($displayValue) === \Illuminate\Database\Eloquent\Collection::class))
        @foreach($displayValue as $option)
                <li>{{ $option->{$field->labelName} }}</li>
        @endforeach
    @else
        {{ $displayValue }}
    @endif
@endif
</ul>