@foreach($field->optionsY($model) as $labelY)
    <tr>
        <th><i class="{{ $labelY->icon() }}"></i> {{ $labelY->name }}</th>
        @foreach($field->optionsX($model) as $labelX)
            <td>
                @if(isset($displayValue[$labelX->id][$labelY->id]))
                    <i class="fas fa-check text-success"></i>
                @else
                    <i class="fas fa-square text-light"></i>
                @endif
            </td>
        @endforeach
    </tr>
@endforeach
