@if($displayValue)
@foreach($displayValue as $file)
{{ $file->name }} [{{ @url($field->getDownloadRoute()) }}/{{ $file->id }}]
@endforeach
@endif