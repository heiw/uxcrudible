<label class="{{ $field->getGridLabelClasses(3, 9) }} control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
<div class="{{ $field->getGridFieldClasses(9, 3) }} float-right">
    <div class="input-group">
        @include('uxcrud::forms.helpers.prependItems')
<input id="{{ $field->name }}" type="date" class="form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
        name="{{ $field->name }}" placeholder="{{ $field->label }}" value="{{ $value }}" {!!  $field->getAttributes() !!}>
        @include('uxcrud::forms.helpers.appendItems')
    </div>
</div>
