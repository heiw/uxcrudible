@if(!$field->isHidden($ability->permission))
    <div class="{{ $field->name }} {{ $field->classes() }}" {!!  $field->getDataAttributes() !!}>
        @include($field->getBlade("uxcrud::forms.fields.edit."), [
            'error' => (isset($errors)) ? $errors->first($field->name) : '',
            'value' => old($field->name, $field->extractValue($model)),
            'readonly' => ($viewOnly || $field->isReadOnly($ability->permission)) ? 'readonly=readonly' : ''
        ])
        <div class="col-12 form-control-feedback"><div class="danger">{{ (isset($errors)) ? $errors->first($field->name) : '' }}</div><div class="warning"></div></div>
    </div>
@endif