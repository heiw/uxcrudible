@if ($model->hasTranslationAttribute($field->name))
    <div class="{{ $field->getGridFieldClasses(9, 2) }} @if($field->labelRight) order-sm-1 @endif">
        @foreach($model->translationLocalesField($field->name) as $code => $locale)
            <div id="{{ $field->name }}" class="input-group locale locale-{{ $code }}">
                @include('uxcrud::forms.helpers.prependItems')
                <input id="{{ $code }}-{{ $field->name }}" aria-label="{{ $code}} {{ __($field->getLabel()) }}" type="hidden" class="form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
                name="{{ $code }}[{{ $field->name }}]" placeholder="{{ $field->label }}" value="{{ $field->extractValue($model, $code) }}"
                       data-locale="{{ $code }}"
                    {!!  $field->getAttributes() !!}>
                @include('uxcrud::forms.helpers.appendItems')
            </div>
        @endforeach
    </div>
@else
    <div class="{{ $field->getGridFieldClasses(9, 2) }} @if($field->labelRight) order-sm-1 @endif float-right">
        <div class="input-group">
            @include('uxcrud::forms.helpers.prependItems')
            <input id="{{ $field->name }}" type="{{ $field->type }}" class="form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
            name="{{ $field->name }}" placeholder="{{ $field->label }}" value="{{ $value }}"
                {!!  $field->getAttributes() !!}>
            @include('uxcrud::forms.helpers.appendItems')
        </div>
    </div>
@endif
