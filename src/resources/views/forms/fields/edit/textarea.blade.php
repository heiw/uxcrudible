@if ($model->hasTranslationAttribute($field->name))
    <label class="{{ $field->getGridLabelClasses(3, 12) }} @if($field->labelRight) order-sm-2 @endif control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
    <div class="{{ $field->getGridFieldClasses(9, 12) }}  @if($field->labelRight) order-sm-1 @endif">
        @foreach($model->translationLocalesField($field->name) as $code => $locale)
            <div id="{{ $field->name }}" class="input-group locale locale-{{ $code }}">
                @include('uxcrud::forms.helpers.prependItems')
                <div class="input-group-prepend">
                    <span class="input-group-text" title="{{ __(':language :label', ["label" => $field->label, "language" => $locale['name']]) }}">{{ $code }}</span>
                </div>
                <textarea id="{{ $code }}-{{ $field->name }}" aria-label="{{ $code}} {{ __($field->getLabel()) }}" type="text" rows="{{ $field->rows }}" class="form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
                name="{{ $code }}[{{ $field->name }}]" placeholder="{{ $field->label }}"
                {!!  $field->getAttributes() !!}>{{ $field->extractValue($model, $code) }}</textarea>
                @include('uxcrud::forms.helpers.appendItems')
            </div>
        @endforeach
    </div>
@else
    <label class="{{ $field->getGridLabelClasses(3, 12) }}  @if($field->labelRight) order-sm-2 @endif control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
    <div class="{{ $field->getGridFieldClasses(9, 12) }}  @if($field->labelRight) order-sm-1 @endif float-right">
        <div class="input-group">
            @include('uxcrud::forms.helpers.prependItems')
            <textarea id="{{ $field->name }}" type="text" rows="{{ $field->rows }}" class="form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
            name="{{ $field->name }}" placeholder="{{ $field->label }}" {!!  $field->getAttributes() !!}>{{ $value }}</textarea>
            @include('uxcrud::forms.helpers.appendItems')
        </div>
    </div>
@endif
