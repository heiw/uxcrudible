<label class="{{ $field->getGridLabelClasses(3, 10) }} @if($field->labelRight) order-sm-2 @endif control-label"
       for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
<div id="{{ $field->name }}"
     class="multi-fields {{ $field->getGridFieldClasses(9, 2) }} @if($field->labelRight) order-sm-1 @endif">
    @if ($model->hasTranslationAttribute($field->name))
        @foreach($field->extractValue($model, $code) as $v)
            @foreach($model->translationLocalesField($field->name) as $code => $locale)
                <div class="multi-field-group input-group locale locale-{{ $code }}">
                    @include('uxcrud::forms.helpers.prependItems')
                    <input aria-label="{{ __($field->getLabel()) }}" id="{{ $field->name }}-{{ $code }}-{{ $key }}" type="{{ $field->type }}"
                           class="form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
                           name="{{ $code }}[{{ $key }}][{{ $field->name }}]" placeholder="{{ $field->label }}"
                           value="{{ $v }}"
                           data-locale="{{ $code }}"
                            {!!  $field->getAttributes() !!}>
                    <div class="input-group-append">
                        <span class="input-group-text"
                              title="{{ __(':language :label', ["label" => $field->label, "language" => $locale['name']]) }}">{{ $code }}</span>
                        @if (!$readonly)
                            <button class="input-group-text uxcrud-add-multi-field text-success"
                                    title="{{ __('Add :label field', ["label" => $field->label]) }}"><i
                                        class="fas fa-plus-square"></i></button>
                            <button class="input-group-text uxcrud-remove-multi-field text-danger"
                                    title="{{ __('Remove :label field', ["label" => $field->label]) }}"><i
                                        class="fas fa-minus-square"></i></button>
                        @endif
                    </div>
                    @include('uxcrud::forms.helpers.appendItems')
                </div>
            @endforeach
        @endforeach
    @else
        @foreach(json_decode($value) as $key => $v)
            <div class="multi-field-group input-group">
                @include('uxcrud::forms.helpers.prependItems')
                <input aria-label="{{ __($field->getLabel()) }}" id="{{ $field->name }}-{{ $key }}" type="{{ $field->type }}"
                       class="form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
                       name="{{ $field->name }}[{{ $key }}]" placeholder="{{ $field->label }}" value="{{ $v }}"
                        {!!  $field->getAttributes() !!}>
                @if (!$readonly)
                    <div class="input-group-append">
                        <button class="input-group-text uxcrud-add-multi-field text-success"
                                title="{{ __('Add :label field', ["label" => $field->label]) }}"><i
                                    class="fas fa-plus-square"></i></button>
                        <button class="input-group-text uxcrud-remove-multi-field text-danger"
                                title="{{ __('Remove :label field', ["label" => $field->label]) }}"><i
                                    class="fas fa-minus-square"></i></button>
                    </div>
                @endif
                @include('uxcrud::forms.helpers.appendItems')
            </div>
        @endforeach
    @endif
</div>
