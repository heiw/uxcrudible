<label class="@if($field->labelLong) col-sm-11 @else col-sm-3 @endif control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
@if(isset($field->icon))
    <div class="@if($field->labelLong) col-sm-1 @else col-sm-9 @endif ">
        @include('uxcrud::forms.helpers.prependItems')
    </div>
@endif

@if ($model->hasTranslationAttribute($field->name))
    @foreach($model->translationLocalesField($field->name) as $code => $locale)
        <div class="input-group locale locale-{{ $code }}">
            <div class="input-group-prepend">
                <span class="input-group-text" title="{{ __(':language :label', ["label" => $field->label, "language" => $locale['name']]) }}">{{ $code }}</span>
            </div>
            <div class="col-sm-12">
{!! $field->extractValue($model, $code) !!}
            </div>
        </div>
    @endforeach
@else
    <div class="col-sm-12">
{!! $value !!}
    </div>
@endif