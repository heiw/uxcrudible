@if(isset($parentModel) && get_class($parentModel) == $field->model)
    <input type="hidden" name="{{ $field->name }}" value="{{ $parentModel->id }}" readonly/>
    <input type="hidden" name="parentModel[id]" value="{{ $parentModel->id }}" readonly/>
    <input type="hidden" name="parentModel[name]" value="{{ get_class($parentModel) }}" readonly/>
@endif
@if($viewOnly || $field->isReadOnly($ability->permission) || !isset($parentModel) || get_class($parentModel) !== $field->model)
    <label class="{{ $field->getGridLabelClasses(3, 9) }} @if($field->labelRight) order-sm-2 @endif control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
    <div class="{{ $field->getGridFieldClasses(9, 3) }} @if($field->labelRight) order-sm-1 @endif">
        <div class="input-group">
            @include('uxcrud::forms.helpers.prependItems')
            <div id="{{ $field->name }}" class="input-group-select" style="flex: 1 1 auto;">
                @foreach($field->options($model) as $option)
                    <input class="with-gap form-control" type="radio" value="{{ $option->{$field->valueName} }}"
                           name="{{ $field->name }}"
                           id="{{ $field->name }}-{{ $option->{$field->valueName} }}"
                        @if ($field->multiple && isset($value) && $value->contains($field->valueName, $option->{$field->valueName}) ||
                            (!$field->multiple && $option->{$field->valueName} === @$value))
                                {{' checked '}}
                        @else
                            @if($viewOnly || $field->isReadOnly($ability->permission))
                               disabled
                            @endif
                        @endif
                        {!!  $field->getAttributes() !!}
                    >
                    <label for="{{ $field->name }}-{{ $option->{$field->valueName} }}">
                        @if(method_exists($option, trim($field->labelName, '()')))
                            {{  @call_user_func(array($option, trim($field->labelName, '()'))) }}
                        @else
                            {{ $option->{$field->labelName} }}
                        @endif
                    </label>
                @endforeach
            </div>
            @include('uxcrud::forms.helpers.appendItems')
        </div>
    </div>
@endif
