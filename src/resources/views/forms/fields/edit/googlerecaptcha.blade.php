<label class="{{ $field->getGridLabelClasses(3, 9) }} control-label" for="{{ $field->name }}">{{ __($field->getLabel()) }}</label>
<div class="{{ $field->getGridFieldClasses(9, 3) }}">
    <div class='g-recaptcha' data-sitekey='{{ env('RECAPTCHA_SITE_KEY') }}' {!!  $field->getAttributes() !!}></div>
</div>
