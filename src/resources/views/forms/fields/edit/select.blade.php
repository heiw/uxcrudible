@if(isset($parentModel) && get_class($parentModel) == $field->model)
    <input type="hidden" name="{{ $field->name }}" value="{{ $parentModel->id }}" readonly/>
    <input type="hidden" name="parentModel[id]" value="{{ $parentModel->id }}" readonly/>
    <input type="hidden" name="parentModel[name]" value="{{ get_class($parentModel) }}" readonly/>
@endif
@if($viewOnly || $field->isReadOnly($ability->permission) || !isset($parentModel) || get_class($parentModel) !== $field->model)
    <label class="{{ $field->getGridLabelClasses(3, 10) }} @if($field->labelRight) order-sm-2 @endif control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
    <div class="{{ $field->getGridFieldClasses(9, 2) }} @if($field->labelRight) order-sm-1 @endif">
        <div class="input-group">
            @include('uxcrud::forms.helpers.prependItems')
            @if($viewOnly || $field->isReadOnly($ability->permission))
                @if ($field->multiple)
                        <ul class='form-control readonly uxcrud-multiple-select-items'>
                        @php($items = $field->extractDisplayValue($model))
                        @if (!is_string($items))
                            @foreach($items as $item)
                                <li class="uxcrud-select-item">{{ $item->summary() }}</li>
                            @endforeach
                        @else
                            <li class="uxcrud-select-item">{{ $items }}</li>
                        @endif
                        </ul>
                @else
                    <div id="{{ $field->name }}" class="form-control readonly" data-value="{{ $value }}">{{ $field->extractDisplayValue($model) }}</div>
                @endif
            @else
                <div class="input-group-select focus-drop-highlight" style="flex: 1 1 auto;">
                    <select class="select2 form-control {{ ($error ? 'is-invalid' : '') }} focus-drop-highlight"
                        name="{{ $field->name }}"
                        id="{{ $field->name }}"
                        {{ ($field->multiple ? 'multiple' : '') }} {{ $readonly }}
                        {!!  $field->getAttributes() !!}>
                        @if (!is_null($field->emptyOptionLabel) || !is_null($field->emptyOptionValue))
                            <option value="{{ $field->emptyOptionValue }}">{{ $field->emptyOptionLabel }}</option>
                        @endif
                        @foreach($field->options($model) as $option)
                            <option value="{{ $option->{$field->valueName} }}"
                                @if ($field->multiple && isset($value) && $value->contains($field->valueName, $option->{$field->valueName}) ||
                                    (!$field->multiple && $option->{$field->valueName} === @$value))
                                        {{' selected '}}
                                @endif
                                @if(method_exists($option, trim($field->labelName, '()')))
                                    >{{  @call_user_func(array($option, trim($field->labelName, '()'))) }}</option>
                                @else
{{--                                    if (isset($option->{$field->labelName}))--}}
                                    >{{ $option->{$field->labelName} }}</option>
{{--                                @else--}}
{{--                                    {{ __('Unknown option') }}--}}
                                @endif
                        @endforeach
                    </select>
                </div>
            @endif
            @include('uxcrud::forms.helpers.appendItems')
        </div>
    </div>
@endif
