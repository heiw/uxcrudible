<label class="{{ $field->getGridLabelClasses(3, 9) }} control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
<div class="{{ $field->getGridFieldClasses(9, 3) }} float-right">
    <div data-name="{{ $field->name }}" class="input-group">
        @include('uxcrud::forms.helpers.prependItems')
        <input id="{{ $field->name }}_date" aria-label="{{ __($field->getLabel()) }}" type="date" class="form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
        name="{{ $field->name }}[date]" placeholder="{{ $field->label }}" value="{{ $field->extractDate($value) }}" {!!  $field->getAttributes() !!}>

        <input id="{{ $field->name }}_time" aria-label="{{ __($field->getLabel()) }}" type="time" class="form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
        name="{{ $field->name }}[time]" placeholder="{{ $field->label }}" value="{{ $field->extractTime($value, 'H:i') }}" {!!  $field->getAttributes() !!}>
        @include('uxcrud::forms.helpers.appendItems')
    </div>
</div>
