<!-- Form Page tabs -->
<ul class="nav fragment-select nav-tabs customtab uxcrud-form-tab" role="tablist"
    @if($field->defaultTab) data-default-tab="{{ $field->defaultTab }}"@endif>
    @foreach($field->fields as $groupField)
        @if(!$groupField->isHidden($ability->permission))
            <li class="nav-item">
                <a class="nav-link show" data-toggle="tab" id="{{ $groupField->slug() }}-nav-link" href="#{{ $groupField->slug() }}" role="tab" title="{{ __($groupField->label) }}">
                    <span class="hidden-sm-up"><i class="{{ $groupField->icon }}"></i></span> <span class="hidden-xs-down"><i class="{{ $groupField->icon }}"></i> {{ __($groupField->label) }}</span>
                </a>
            </li>
        @endif
    @endforeach
</ul>

<!-- Form Page panes -->
<div class="tab-content">
    @foreach($field->fields as $groupField)
        @if(!$groupField->isHidden($ability->permission))
            <div class="tab-pane" id="{{ $groupField->slug() }}" role="tabpanel">
                <div class="p-20">
                    @includeIf("uxcrud::forms.fields.edit.line",
                        ["field" => $groupField, "parentField" => $field]
                    )
                </div>
                {!! $groupField->formNavigation($ability) !!}
            </div>
        @endif
    @endforeach
</div>
