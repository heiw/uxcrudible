<label class="{{ $field->getGridLabelClasses(3, 9) }} control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
<div class="{{ $field->getGridFieldClasses(9, 3) }}">
    <div class="input-group">
        @include('uxcrud::forms.helpers.prependItems')
<div class="input-group-prepend uxcrud-icon-preview" title="{{ __('icon preview') }}">
            <span class="input-group-text"><i class="{{ $value }}"></i></span>
        </div>
        <input id="{{ $field->name }}" type="text" class="uxcrud-icon-field form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
        name="{{ $field->name }}" placeholder="{{ $field->label }}" value="{{ $value }}" {!!  $field->getAttributes() !!}>
        @include('uxcrud::forms.helpers.appendItems')
    </div>
</div>
