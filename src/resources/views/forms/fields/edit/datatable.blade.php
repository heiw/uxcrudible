{{--@include('uxcrud::datatable', [--}}
    {{--'model' => $field->newModel(),--}}
    {{--'controller' => $field->controller(),--}}
    {{--'parentModel' => $model,--}}
    {{--'parentController' => $controller,--}}
{{--])--}}

@php
    $parentModel = $model;
    $parentController = $controller;
    $model = $field->newModel();
    $controller = $field->controller();

    $instance = $model->getModelInstance();
@endphp
@if(!$viewOnly)
    @includeIf("uxcrud::forms.fields.edit." . $field->getClass() . ".createButton")
@endif
<div class="col-12">
    <table id="{{ $parentController->getId() }}-{{ $controller->getId() }}" class="display data-table {{ $parentController->getId() }}-{{ $controller->getId() }}" style="width:100%"
        {{--data-ajax-url="{{ $parentController->getAjaxRoute() }}/datatable/{{ $parentModel->id }}/{{ $field->name }}"--}}
        data-ajax-url="{{ $controller->getAjaxRoute() }}"
        data-row-reorder-url="{{ $controller->getRowReorderRoute() }}"
        data-export-url="{{ $controller->getExportRoute() }}"
        data-parent-model[name]="{{ $parentController::$model }}"
        data-parent-model[id]="{{ $parentModel->id }}"
        data-columns="{{ json_encode(
            $controller->dataTableColumnNames(
                \Heiw\Uxcrudible\Form\Fields\Permission::INDEX
            )
        ) }}"
        data-data-table-options="{{ $controller->dataTableOptionsJSON() }}"
        data-order="{{ json_encode($controller->dataTableDefaultSort()) }}"
        {!!  $field->getAttributes() !!}
    >
        <thead>
        <tr>
            @foreach ($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true) as $dt_field)
                <th class="{{ $dt_field->name }} focus-highlight">
                    @if(get_class($dt_field) == Heiw\Uxcrudible\Form\Fields\Actions::class)
                        <div class="float-right">
                            <div class="btn-group uxcrud-datatable-select-locale" role="group" aria-label="{{ __("Select language") }}">
                                @if($instance->hasTranslationLocale()) @foreach($instance->translationLocales() as $code => $locale)
                                    <label class="btn btn-radio btn-outline-info @if($locale['active']) active @endif" title="{{ $locale['name'] }}">
                                        <input type="radio" name="locale" value="{{ $code }}" autocomplete="off" @if($locale['active']) checked @endif> {{ $code }}
                                    </label>

                                @endforeach @endif
                            </div>
                        </div>
                    @endif
                @if($dt_field->icon)<i class="{{ $dt_field->icon }}"></i> @endif{!! __($dt_field->getIndexLabel()) !!}</th>
            @endforeach
        </tr>
        <tr class="uxcrud-filters">
            @foreach ($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true) as $dt_field)
                <td class="{{ $dt_field->name }}">
                    @if (!empty($dt_field->filter) || $dt_field->getBlade("uxcrud::forms.filters.") == "uxcrud::forms.filters.select")
                        @includeIf($dt_field->getBlade("uxcrud::forms.filters."), [
                            'value' => @(old($dt_field->name, $dt_field->extractFilterValue())),
                            'field' => $dt_field,
                            'model' => $instance,
                            'controller' => $controller,
                            'parentModel' => $parentModel,
                            'parentController' => $parentController
                        ])
                    @endif
                </td>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <tr><td colspan="{{ count($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true)) }}"><i class="fas fa-spinner fa-spin"></i> {{ __('Loading data') }}</td></tr>
        </tbody>
        <tfoot>
        <tr>
            @foreach ($controller->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true) as $dt_field)
                <th class="{{ $dt_field->name }}">@if($dt_field->icon)<i class="{{ $dt_field->icon }}"></i> @endif{!! __($dt_field->getIndexLabel()) !!}</th>
            @endforeach
        </tr>
        </tfoot>
    </table>
</div>
