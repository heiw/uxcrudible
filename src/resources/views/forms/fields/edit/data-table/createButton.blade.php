@if(auth()->user()->hasModelPermission('create', $controller::$model))
    <div class="col-12" id="{{ $field->name }}">
        <a href="{{ $controller->getAjaxRoute() }}/{{ Illuminate\Support\Str::slug(__('uxcrud::slugs.create')) }}"
           data-parent-model[name]="{{ $parentController::$model }}"
           data-parent-model[id]="{{ $parentModel->id }}"
           class="modal-action btn btn-outline-info m-1 uxcrud-create-{{ $field->getClass() }}" role="button"><i
                    class="far fa-plus-square"></i> {{ __('uxcrud::uxcrud.create new') }} {{ __($controller->getLabel()) }}
        </a>
    </div>
@endif
