<label class="{{ $field->getGridLabelClasses(3, 10) }} @if($field->labelRight) order-sm-2 @endif control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
<div class="{{ $field->getGridFieldClasses(9, 2) }} @if($field->labelRight) order-sm-1 @endif float-right">
    <div class="input-group">
        @include('uxcrud::forms.helpers.prependItems')
        <input id="{{ $field->name }}" type="number" class="form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
        name="{{ $field->name }}" placeholder="{{ $field->label }}" value="{{ $value }}"
        {!!  $field->getAttributes() !!}>
        @include('uxcrud::forms.helpers.appendItems')
    </div>
</div>
