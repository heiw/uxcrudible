<table class="uxcrud-matrix uxcrud-edit table-striped table-hover">
    <thead><th class="col-sm-3">{{ __($field->labelY()) }} \ {{ __($field->labelX()) }}</th>

    @foreach($field->optionsX($model) as $labelX)
        <th class="{{ \Illuminate\Support\Str::kebab($labelX->name) }}">{{ __($labelX->name) }}</th>
    @endforeach
    </thead>

    <tbody>
    @include($field->getEntryTypeBlade("uxcrud::forms.fields.edit.matrix."))
    </tbody>
</table>