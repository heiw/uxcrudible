@foreach($field->optionsY($model) as $labelY)
    <tr class="{{ $labelY->name }}">
        <th scope="col" class="p-2">{{ __(\Illuminate\Support\Str::snake($labelY->name, ' ')) }}</th>
        @foreach($field->optionsX($model) as $labelX)
            <td scope="col" class="{{ \Illuminate\Support\Str::kebab($labelX->name) }} p-2 m-0 text-center">
                <div class="checkbox checkbox-success">
                    <input aria-label="{{ $field->labelX() }}: {{ $labelX->name }} {{ $field->labelY() }}: {{ $labelY->name }}" id="{{ \Illuminate\Support\Str::kebab($field->name) }}-{{ $labelX->id }}-{{ $labelY->id }}" data-size="small" name="{{ $field->name }}[{{ $labelX->id }}][{{ $labelY->id }}]" type="checkbox" @if(isset($value[$labelX->id][$labelY->id])) checked @endif  {{ $readonly }}/>
                    <label for="{{ \Illuminate\Support\Str::kebab($field->name) }}-{{ $labelX->id }}-{{ $labelY->id }}" class="p-0 m-0">
                        <span class="hide">{{ $field->labelX() }}: {{ $labelX->name }} {{ $field->labelY() }}: {{ $labelY->name }}</span>
                    </label>
                </div>

            </td>
        @endforeach
    </tr>
@endforeach
