@if(auth()->user()->hasModelPermission('create', $controller::$model))
    <div class="col-12" id="{{ $field->name }}">
            <div class="input-group">
                @include('uxcrud::forms.helpers.prependItems')
<input id="{{ $field->name }}" aria-label="{{ $field->label }}" type="file" class="dropzone form-control {{ ($error ? 'is-invalid' : '') }}" {{ $readonly }}
                name="{{ $field->name }}[]" multiple>    </div>
    </div>
@endif
