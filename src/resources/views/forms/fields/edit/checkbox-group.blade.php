@if ($field->getLabel())
<label class="{{ $field->getGridLabelClasses(3, 10) }} control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
@endif
@foreach($field->getCheckboxes($model) as $key => $checkbox)
    <div class="uxcrud-line row col-12">
        <div class="col-sm-1">{!! $field->getKey($key) !!}</div>
        <span class="{{ $field->getGridLabelClasses(3, 9) }} control-label @if($field->labelRight) order-sm-2 @endif">{!! __($checkbox->label) !!}</span>
        <div class="{{ $field->getGridFieldClasses(8, 2) }} @if($field->labelRight) order-sm-1 @endif">
            <div class="input-group">
                @include('uxcrud::forms.helpers.prependItems')
                <div class="input-group-prepend p-2">
                    <input id="{{ $checkbox->name }}" data-size="small" name="{{ $checkbox->name }}" placeholder="{{ $field->name }}" type="checkbox" @if($checkbox->value == 1) checked @endif  {{ $readonly }} {{ $checkbox->readonly }} {!!  $field->getAttributes() !!}/>
                    <label for="{{ $checkbox->name }}" class="p-0 m-0"><span class="hide">{{ $checkbox->label }}</span></label>
                </div>
                @include('uxcrud::forms.helpers.appendItems')
            </div>
        </div>
        <div class="col-12 form-control-feedback"><div class="danger">{{ $errors->first($checkbox->name) }}</div><div class="warning"></div></div>
    </div>
@endforeach
