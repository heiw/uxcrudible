@if(isset($parentModel) && get_class($parentModel) == $field->model)
    <input type="hidden" name="{{ $field->name }}" value="{{ $parentModel->id }}" readonly/>
    <input type="hidden" name="parentModel[id]" value="{{ $parentModel->id }}" readonly/>
    <input type="hidden" name="parentModel[name]" value="{{ get_class($parentModel) }}" readonly/>
@endif
@if($viewOnly || $field->isReadOnly($ability->permission) || !isset($parentModel) || get_class($parentModel) !== $field->model)
    <label class="{{ $field->getGridLabelClasses(3, 9) }} control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
    <div class="{{ $field->getGridLabelClasses(9, 3) }}">
        <div class="input-group">
            @include('uxcrud::forms.helpers.prependItems')
</div>
    </div>
    <div id="has-many-pivot-root-{{ $field->name }}" class="row has-many-pivot-root" data-pivot-modal="{!! htmlentities(json_encode($field->getPivotModal($model, $controller))) !!}">
        <input type="hidden" class="pivot-values" id="{{ $field->name }}" name="{{ $field->name }}"/>
        <div class="col-6">
            <div class="col-sm-12"><label>{{ __('Available') }} {!! __($field->getLabel()) !!}</label></div>
            <table id="{{ $field->name }}-{{ $controller->getId() }}-source" class="display data-table has-many-pivot has-many-pivot-source {{ $field->name }}-{{ $controller->getId() }}-source"
                   data-columns="{{ json_encode($field->dataTableSourceColumnNames()) }}"
                   data-data-table-options="{{ $field->dataTableOptionsJSON($field::SOURCE) }}"
                    {!!  $field->getAttributes() !!}
            >
                <thead>
                <tr>
                    @foreach ($field->getFieldsSource() as $dt_field)
                        <th class="{{ $dt_field->name }} focus-highlight">
                            @if($dt_field->icon)<i class="{{ $dt_field->icon }}"></i> @endif{{ __($dt_field->getLabel()) }}</th>
                    @endforeach
                </thead>
                <tbody>
                @foreach($field->optionsSource($model, $value) as $option)
                    @if (get_class($option) === $field->model())
                        <tr data-id="{{ $option->id }}">
                            @foreach ($field->getFieldsSource() as $dt_field)
                                <td>
                                    @include($dt_field->getBlade("uxcrud::forms.fields.show."), [
                                    'displayValue' => $dt_field->extractDisplayValue($option),
                                    'field' => $dt_field,
                                    'entry' => $option,
                                    'model' => $model,
                                    'controller' => $controller,
                                    'parentModel' => $parentModel
                                    ])
                                </td>
                            @endforeach
                        </tr>
                    @endif
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    @foreach ($field->getFieldsSource() as $dt_field)
                        <th class="{{ $dt_field->name }}">@if($dt_field->icon)<i class="{{ $dt_field->icon }}"></i> @endif{{ __($dt_field->label) }}</th>
                    @endforeach
                </tr>
                </tfoot>
            </table>
        </div>

{{--        <div class="col-1">--}}
{{--        </div>--}}

        <div class="col-6">
            <div class="col-sm-12"><label>{{ __('Selected') }} {!! __($field->getLabel()) !!}</label></div>
            <table id="{{ $field->name }}-{{ $controller->getId() }}-selection" class="display data-table has-many-pivot has-many-pivot-selection {{ $field->name }}-{{ $controller->getId() }}-source"
                   data-columns="{{ json_encode($field->dataTableSelectionColumnNames()) }}"
                   data-data-table-options="{{ $field->dataTableOptionsJSON($field::SELECTION) }}"
                   data-order="{{ json_encode($field->dataTableDefaultSort()) }}"
                   data-pivot-fields="{{ $field->pivotFieldsJSON() }}"
                    {!!  $field->getAttributes() !!}
            >
                <thead>
                <tr>
                    @foreach ($field->getFieldsSelection() as $dt_field)
                        <th class="{{ $dt_field->name }} focus-highlight">
                            @if($dt_field->icon)<i class="{{ $dt_field->icon }}"></i> @endif{{ __($dt_field->getLabel()) }}</th>
                    @endforeach
                </thead>
                <tbody>
                    @foreach($field->optionsSelection($model, $value) as $option)
                    @if (get_class($option) === $field->model())
                    <tr data-id="{{ $option->id }}">
                        @foreach ($field->getFieldsSelection() as $dt_field)
                            <td>
                                @include($dt_field->getBlade("uxcrud::forms.fields.show."), [
                                'displayValue' => $dt_field->extractDisplayValue($option),
                                'field' => $dt_field,
                                'entry' => $option,
                                'model' => $model,
                                'controller' => $controller,
                                'parentModel' => $parentModel
                                ])
                            </td>
                        @endforeach
                    </tr>
                    @endif
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    @foreach ($field->getFieldsSelection() as $dt_field)
                        <th class="{{ $dt_field->name }}">@if($dt_field->icon)<i class="{{ $dt_field->icon }}"></i> @endif{{ __($dt_field->label) }}</th>
                    @endforeach
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endif
