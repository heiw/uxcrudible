@if ($values)
    @if ($field->condensed)
        <div class="limit-view">
            <div class="long-content-condensed">
    @endif
<ul class="uxcrud-hash-locate alert alert-warning">
    @foreach($values as $key => $message)
        <li><a href="#{{ $key }}">{{ \Heiw\Uxcrudible\Helpers::to_sentence_case($key) }}: {{ $message[0] }} <i class="fas fa-highlighter"></i></a></li>
    @endforeach
</ul>
    @if ($field->condensed)
            </div>
        </div>
    @endif
@endif