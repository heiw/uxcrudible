<label class="{{ $field->getGridLabelClasses(12, 12) }}" {!!  $field->getAttributes() !!}>{!! __($field->getLabel()) !!}
@if(!empty($field->prependItems) || !empty($field->appendItems))
    <div class="float-right">
        @include('uxcrud::forms.helpers.prependItems')
        @include('uxcrud::forms.helpers.appendItems')
    </div>
@endif
</label>
