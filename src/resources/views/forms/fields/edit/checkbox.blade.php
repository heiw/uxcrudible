<?php
// Generate unique qualifier for id. This is needed as multiple fields may have
// the name in different modals that are open at the same time. E.g. DataTable
// relations that pop up in new modal. This is only required for checkboxes as
// they use javascript for the fancy ticks.
use Illuminate\Support\Str;

$unique = Str::random(8);
?>
<label class="{{ $field->getGridLabelClasses(3, 10) }} control-label @if($field->labelRight) order-sm-2 @endif" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
<div class="{{ $field->getGridFieldClasses(9, 2) }} @if($field->labelRight) order-sm-1 @endif">
    <div class="input-group">
        @include('uxcrud::forms.helpers.prependItems')
        <div class="input-group-prepend p-2 uxcrud-checkbox focus-within-highlight">
            <input id="{{ $unique }}-{{ $field->name }}" class="form-control" data-size="small" name="{{ $field->name }}" placeholder="{{ $field->name }}" type="checkbox" @if($value == 1) checked @endif  {{ $readonly }} {!!  $field->getAttributes() !!}/>
            <label for="{{ $unique }}-{{ $field->name }}" class="p-0 m-0"><span class="hide">{{ $field->label }}</span></label>
        </div>
        @include('uxcrud::forms.helpers.appendItems')
    </div>
</div>
