@foreach($field->fields as $groupField)
    @includeIf("uxcrud::forms.fields.edit.line",
        ["field" => $groupField, "parentField" => $field]
    )
@endforeach