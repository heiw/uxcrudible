@if(isset($parentModel) && get_class($parentModel) == $field->model)
    <input type="hidden" name="{{ $field->name }}" value="{{ $parentModel->id }}" readonly/>
    <input type="hidden" name="parentModel[id]" value="{{ $parentModel->id }}" readonly/>
    <input type="hidden" name="parentModel[name]" value="{{ get_class($parentModel) }}" readonly/>
@else
    <label class="{{ $field->getGridLabelClasses(3, 9) }} control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>
    <div class="{{ $field->getGridFieldClasses(9, 3) }}">
        <div class="input-group">
            @include('uxcrud::forms.helpers.prependItems')
@if($viewOnly || $field->isReadOnly($ability->permission))
                <div class="form-control readonly">{{ $field->extractDisplayValue($model) }}</div>
            @else
                <div class="input-group-select" style="flex: 1 1 auto;">
                    <select class="select2linked form-control {{ ($error ? 'is-invalid' : '') }}"
                            name="{{ $field->name }}"
                            id="{{ $field->name }}"
                            data-linked-to="{{ $field->linkedTo }}"
                            data-linked-name="{{ $field->linkedName }}"
                            {{ ($field->multiple ? 'multiple' : '') }} {{ $readonly }}
                            {!!  $field->getAttributes() !!}>
                        @if (!is_null($field->emptyOptionLabel) || !is_null($field->emptyOptionValue))
                            <option value="{{ $field->emptyOptionValue }}">{{ $field->emptyOptionLabel }}</option>
                        @endif
                        @foreach($field->options($model) as $option)
                    {{--</select>@dd($option)--}}
                        @php
                        @endphp
                            <option value="{{ $option->{$field->valueName} }}"
                            @if(isset($option->{$field->linkedName}))
                                data-link-id="{{ $option->{$field->linkedName} }}"
                            @endif
                            @if ($field->multiple && isset($value) && $value->contains($field->valueName, $option->{$field->valueName}) ||
                                (!$field->multiple && $option->{$field->valueName} == @$value))
                                {{' selected '}}
                                    @endif
                                @if(method_exists($option, trim($field->labelName, '()')))
                                    >{{  @call_user_func(array($option, trim($field->labelName, '()'))) }}</option>
                                @else
                                    >{{ $option->{$field->labelName} }}</option>
                                @endif
                        @endforeach
                    </select>
                </div>
            @endif
        </div>
    </div>
@endif
