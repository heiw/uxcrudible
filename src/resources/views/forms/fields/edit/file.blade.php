<label class="{{ $field->getGridLabelClasses(3, 9) }} control-label" for="{{ $field->name }}">{!! __($field->getLabel()) !!}</label>

@if(auth()->user()->hasModelPermission('create', $controller::$model))
    <div class="{{ $field->getGridFieldClasses(9, 3) }}">
        <div class="input-group">
            @include('uxcrud::forms.helpers.prependItems')
            @php($file = $field->getFile($model))
            <div class="form-control uxcrud-file-or-upload p-0">
                @if (!$readonly)
                <input id="{{ $field->name }}" aria-label="{{ $field->label }}" type="file"
                       class="dropzone {{ ($error ? 'is-invalid' : '') }} {{ ($file ? 'hide': '') }}" {{ $readonly }}
                        name="{{ $field->name }}" {!!  $field->getAttributes() !!}>
                @endif
                @if($file)
                    <a href="{{ $field->getDownloadRoute() }}/{{ $file->id }} " class="{{ ($file ? '': 'hide') }}"><i class="fas fa-download"></i> {{ $file->name }}</a>
                @endif
            </div>
            @if($file && !$readonly)
                <div class="input-group-append">
                    <a title="{{ __('Replace :file', ['file' => ($file) ? $file->summary() : '']) }}"
                       class="uxcrud-replace-file-upload btn btn-outline-info float-right"
                       role="button"><i class="fas fa-exchange-alt"></i></a>
{{--                        @include('uxcrud::forms.fields.show.actions', [--}}
{{--                            'entry' => $file,--}}
{{--                            'dtRefreshId' => null,--}}
{{--                            'controller' => Heiw\Uxcrudible\Helpers::get_controller_from_model_name($file),--}}
{{--                        ])--}}
                </div>
            @endif
            @include('uxcrud::forms.helpers.appendItems')
        </div>
    </div>
@endif

