<div class="row">
    <div class="col-12">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav fragment-select nav-tabs customtab" role="tablist">
                @php($random = "-" . bin2hex(random_bytes(2)))
                @foreach(Heiw\Uxcrudible\Models\Gdpr::getActiveAndPrevious() as $gdpr)
                    <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#{{ $gdpr->slug() }}{{ $random }}" data-model="{{ $gdpr->slug() }}{{ $random }}" role="tab" title="{{ __($gdpr->name) }}">
                            <span class="hidden-sm-up"><i class="{{ $gdpr->icon() }}"></i></span> <span class="hidden-xs-down"><i class="{{ $gdpr->icon() }}"></i> {{ __($gdpr->name) }}</span></a>
                    </li>
                @endforeach
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                @foreach(Heiw\Uxcrudible\Models\Gdpr::getActiveAndPrevious() as $gdpr)
                    <div class="tab-pane" id="{{ $gdpr->slug() }}{{ $random }}" role="tabpanel">
                        <div class="p-20">
                            {!! $gdpr->content !!}
                            @if(!is_null($gdpr->sign_date))
                                <div class="row">
                                    <div class="col-sm">
                                    {{ __('Signed on :date', ['date' => date('d/m/Y', strtotime($gdpr->sign_date))]) }}
                                    </div>
                                    <div class="col-sm">
                                    @if ($gdpr->active && config('uxcrud.gdpr_allow_withdraw'))
                                        <form id="gdpr-withdraw" action="{{ route('gdpr.withdraw') }}" method="POST"
                                              class="modal-form-embedded"
                                        >
                                            @csrf
                                            <input type="hidden" name="gdpr_id" value="{{ $gdpr->id }}"/>
                                            <button type="submit" class="ajax-action-uxcrud-submit btn btn-danger float-right"
                                            data-confirm="{{ __('Are you sure you want to withdraw from the privacy agreement?') }}"><i class="fas fa-minus-circle"></i> {{ __('Withdraw agreement') }}</button>
                                        </form>
                                    @endif
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-sm">
                                    <form id="gdpr-agree" action="{{ route('gdpr.agree') }}" method="POST"
                                    class="modal-form-embedded">
                                        @csrf
                                        <input type="hidden" name="gdpr_id" value="{{ $gdpr->id }}"/>
                                        <button type="submit" class="ajax-action-uxcrud-submit btn btn-info float-right" data-dismiss="modal"><i class="far fa-handshake"></i> {{ __('I agree') }}</button>
                                    </form>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
