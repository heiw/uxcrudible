<?php
return array(
    'checked' => 'Checked',
    'create new' => 'Create new',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'force delete' => 'Force delete',
    'restore' => 'Restore',
    'save' => 'Save',
    'search' => 'Search',
    'unchecked' => 'Unchecked',
    'update' => 'Update',
    'view' => 'View',
);
