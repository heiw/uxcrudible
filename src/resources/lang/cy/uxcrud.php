<?php
return array (
  'checked' => 'Gwiriwyd',
  'create new' => 'Creu newydd',
  'delete' => 'Dileu',
  'edit' => 'Golygu',
  'force delete' => 'Gorfodi dileu',
  'restore' => 'Adfer',
  'save' => 'Arbed',
  'search' => 'Chwilio',
  'unchecked' => 'Dirwystr',
  'update' => 'Dirwystr',
  'view' => 'Golwg',
);
