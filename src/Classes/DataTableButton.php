<?php

namespace Heiw\Uxcrudible\Classes;

use App\Policies\PracticeVisitPolicy;
use Illuminate\Support\Str;

/**
 * Options for DataTables
 * Class DataTableOptions
 */
class DataTableButton implements \JsonSerializable
{
    public $autoClose = true;
    public $extend;

    /**
     * @var string JS function to call on activation of the button.
     */
    public $onAction;
//    public $uxcrudAction;
    public $name = null;
    public $icon = null;
    public $className = 'btn btn-outline-info m-1';
    public $buttons = [];
    public $fade = true;
    public $dataAttributes = [];
    public $rawAttributes = [];

    public $element = 'button';
    public $type = 'button';

    public function __construct($name, $icon = null, $className = null) {
        $this->name($name);
        if (!is_null($icon)) {
            $this->icon($icon);
        }
        if (!is_null($className)) {
            $this->className($className);
        }
        return $this;
    }

    public static function create(...$arguments)
    {
        $field = new static(...$arguments);
        return $field;
    }

    private static function removeEmptyExceptFalseOrZeroValues($array) {
        foreach($array as $key => $value) {
            if (empty($value) && $value != 0 && $value !== false) {
                unset($array[$key]);
            } elseif (is_array($value)) {
                $array[$key] = self::removeEmptyExceptFalseOrZeroValues($value);
            }
        }
        return $array;
    }

    /**
     * Serialize $text by combining $text and $label
     * @return mixed|void
     */
    public function jsonSerialize()
    {
        $results = (array)($this);
        $results['text'] = "<i class=\"$this->icon\"></i> <span class='label-text'>$this->name</span>";
        unset($results['icon']);
        //unset($results['label']);

        // Not all properties are used for all uses of button, therefore
        // remove all entries that are empty (except 0 or false).
        $results = self::removeEmptyExceptFalseOrZeroValues($results);
//        foreach($results as $key => $result) {
//            if (empty($result) && !$result == 0 && !$result == false) {
//                unset($results[$key]);
//            }
//        }

//        // Only include data attributes when set.
//        if (count($this->dataAttributes) === 0) {
//            unset($results['dataAttributes']);
//        }
//        // Only include buttons when set.
//        if (count($this->buttons) === 0) {
//            unset($results['buttons']);
//        }
        return $results;
    }

    public function toString() {
        $result = null;
        $rawAttributes = implode(' ', $this->rawAttributes);
        $dataAttributes = null;
        foreach($this->dataAttributes as $key => $value) {
            $dataAttributes .= " $key=\"" . addslashes($value) . "\"";
        }
        $result .= "<{$this->element} type='{$this->type}' class='{$this->className}' $dataAttributes {$rawAttributes}><i class='{$this->icon}'></i> <span class='label-text'>{$this->name}</span></{$this->element}>";
        return $result;
    }

    /**
     * Append classname
     * @param null $className
     * @return $this
     */
    public function addClassName($className = null) {
        $this->className .= ' ' . trim($className);
        return $this;
    }

    /**
     * Set classname
     * @param null $className
     * @return $this
     */
    public function className($className = null) {
        $this->className = $className;
        return $this;
    }

    /**
     * Set element
     * @param null $element
     * @return $this
     */
    public function element($element = null) {
        $this->element = $element;
        return $this;
    }

    /**
     * Set type
     * @param null $type
     * @return $this
     */
    public function type($type = null) {
        $this->type = $type;
        return $this;
    }

    /**
     * Set button name
     * @param string $name
     * @return $this
     */
    public function name($name = null) {
        $this->name = $name;
        return $this;
    }

    /**
     * Set rawAttributes rawAttributes
     * @param string $rawAttribute
     * @return $this
     */
    public function addRawAttribute($rawAttribute = null) {
        $this->rawAttributes[] = $rawAttribute;
        return $this;
    }

    /**
     * Set JS function to call when button is pressed.
     *     onAction(e, dt, node, config, params)
     * @param string $onAction
     * @return $this
     */
    public function onAction($onAction = null) {
        $this->onAction = $onAction;
        return $this;
    }

//    /**
//     * Set JS function to call when button is pressed.
//     *     uxcrudAction(e, dt, node, config, params)
//     * @param string $uxcrudAction
//     * @return $this
//     */
//    public function uxcrudAction($uxcrudAction = null) {
//        $this->uxcrudAction = $uxcrudAction;
//        return $this;
//    }

    /**
     * Set button icon
     * @param string $icon
     * @return $this
     */
    public function icon($icon = null) {
        $this->icon = $icon;
        return $this;
    }

    /**
     * Set button autoClose
     * @param null $autoClose
     * @return $this
     */
    public function autoClose($autoClose = null) {
        $this->autoClose = $autoClose;
        return $this;
    }

    /**
     * Set button fade
     * @param null $fade
     * @return $this
     */
    public function fade($fade = true) {
        $this->fade = $fade;
        return $this;
    }

    /**
     * @param string $extend
     * @return $this
     */
    public function extend($extend = 'collection') {
        $this->extend = $extend;
        return $this;
    }


    /**
     * @param $key
     * @param $dataAttribute
     * @return $this
     */
    public function addDataAttribute($key, $dataAttribute) {
        $this->dataAttributes[self::prependDataDashToString($key)] = $dataAttribute;
        return $this;
    }

    /**
     * Reset or set all data attributes.
     * @param array $dataAttributes
     * @return $this
     */
    public function clearDataAttributes($dataAttributes = []) {
        $this->dataAttributes = self::prependDataDashToKeys($dataAttributes);
        return $this;
    }

    /**
     * Add data- to string or array keys if not present.
     * @param array $dataAttributes
     * @return array
     */
    protected static function prependDataDashToKeys($dataAttributes = []) {
        if (is_array($dataAttributes)) {
            foreach ($dataAttributes as $key => $value) {
                if (!Str::startsWith($key, ['data-'])) {
                    $dataAttributes['data-' . $key] = $value;
                    unset($dataAttributes[$key]);
                }
            }
        }
        return $dataAttributes;
    }

    /**
     * Add data- to string if not present.
     * @param string $string
     * @return string
     */
    protected static function prependDataDashToString($string) {
        if (!Str::startsWith($string, ['data-'])) {
            return 'data-' . $string;
        }
        return $string;
    }

    /**
     * Turn button into collection button and add $buttons.
     * @param array|DataTableButton $buttons
     * @return $this
     */
    public function addButtons($buttons) {
        $this->extend = 'collection';
        if (!is_array($buttons)) {
            $buttons->addClassName('w-100');
            $this->buttons[] = $buttons;
        } else {
            foreach($buttons as $button) {
                $button->addClassName('w-100');
            }
            $this->buttons = $this->buttons + $buttons;
        }
        return $this;
    }

}
