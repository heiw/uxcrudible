<?php

namespace Heiw\Uxcrudible\Classes;

/**
 * Source: http://stackoverflow.com/questions/254514/php-and-enumerations
 * Class BasicEnum
 * @package Heiw\Uxcrudible\Classes
 */
abstract class BasicEnum
{
    private static $constCacheArray = null;

    private static function getConstants() {
        if (self::$constCacheArray == null) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new \ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    public static function getValues() {
        $constants = self::getConstants();
        return array_combine(array_values($constants), array_values($constants));
    }

    public static function getValue($key) {
        $constants = self::getConstants();
        return array_search($key, (array)$constants);
    }

    public static function getKeys() {
        $constants = self::getConstants();
        return array_combine(array_keys($constants), array_keys($constants));
    }

    public static function getArray() {
        $constants = self::getConstants();
        return array_flip($constants);
    }

    public static function getArrayFlip() {
        $constants = self::getConstants();
        return $constants;
    }

    public static function isValidName($name, $strict = false) {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    public static function isValidValue($value, $strict = true) {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict);
    }


    /**
     * Get a random key.
     * @return mixed
     */
    public static function getRandomKey() {
        $keys = static::getKeys();
        return $keys[array_rand($keys)];
    }

    /**
     * Get a random value.
     * @return mixed
     */
    public static function getRandomValue() {
        $values = static::getValues();
        return $values[array_rand($values)];
    }
}