<?php

namespace Heiw\Uxcrudible\Classes;

/**
 * Class JsonModal
 * Template for Modal form passed as JSON.
 */
class JsonModal {
    /**
     * @var string
     */
    public $id;

    /**
     * @var string  danger|success|warning|error
     */
    public $result;

    /**
     * @var string
     */
    public $colour;

    /**
     * @var string
     */
    public $class;

    /**
     * @var string
     */
    public $header;

    /**
     * @var string
     */
    public $footer;

    /**
     * @var string
     */
    public $body;

    /**
     * @var boolean
     */
    public $closeButton = true;

    /**
     * @var array of string
     */
    public $buttons = [];

    /**
     * @var string size of modal.
     */
    public $size = 'xl';

    /**
     * @var array Data attribute name value pairs.
     */
    public $dataAttributes = [];

    /**
     * @var JsonForm
     */
    public $form = null;


    public $type = 'modal';

    public static function create(...$arguments)
    {
        $field = new static(...$arguments);
        return $field;
    }

    /**
     * Status of the notification, also sets the colour.
     * @param $status   string  danger|success|warning|error|refresh
     * @return $this
     */
    public function status($status) {
        $this->result = $status;
        $this->colour = $status;
        return $this;
    }

    /**
     * @param string $header   Header of modal notification.
     * @return $this
     */
    public function header($header) {
        $this->header = $header;
        return $this;
    }

    /**
     * @param string $body  Main message of modal.
     * @return $this
     */
    public function body($body) {
        $this->body = $body;
        return $this;
    }

    /**
     * @param string $footer  Footer message of modal.
     * @return $this
     */
    public function footer($footer) {
        $this->footer = $footer;
        return $this;
    }

    /**
     * @param string $size  size of the modal, can be sm, lg, xl, xxl.
     * @return $this
     */
    public function size($size) {
        $this->size = $size;
        return $this;
    }

    /**
     * @param string $closeButton  Label for close button. Set to null for no close button.
     * @return $this
     */
    public function closeButton($closeButton) {
        $this->closeButton = $closeButton;
        return $this;
    }

    /**
     * @param string $colour  Main message of modal.
     * @return $this
     */
    public function colour($colour) {
        $this->colour = $colour;
        return $this;
    }

    function addForm(JsonForm $form) {
        $this->form = $form;
        return $this;
    }

    public function toHTML() {
        $result = "";
        if (!is_null($this->form)) {
            $this->form->class .= " modal-form-embedded";
            $result .= "<form ";
            foreach((array)$this->form as $param => $value) {
                if ($param == "data") {
                    foreach($value as $key => $val) {
                        $result .= " data-$key='$val'";
                    }
                } else {
                    $result .= " $param='$value'";
                }
            }
            $result .= ">";
        }
        $result .= "<div class='modal-header embedded'>" . $this->header . "</div>" .
            "<div class='modal-body embedded'>" . $this->body . "</div>" .
            "<div class='modal-footer embedded'>" . implode($this->buttons) . "</div>";
        if (!is_null($this->form)) {
            $result .= "</form>";
        }
        return $result;
    }

    function addDataAttributes($dataAttributes) {
        $this->dataAttributes = array_merge($this->dataAttributes, $dataAttributes);
        return $this;
    }

    function addDataAttribute($name, $value) {
        $this->dataAttributes[$name] = $value;
        return $this;
    }
}
