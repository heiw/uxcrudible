<?php

namespace Heiw\Uxcrudible\Classes;

/**
 * Class EmailTemplateRecipient contains name and email pair used for addressing
 * email by EmailTemplate.
 * @package Heiw\Uxcrudible\Classes
 */
class EmailTemplateRecipient {
    public $email;
    public $name;

    public function __construct($email, $name) {
        $this->email($email);
        $this->name($name);
        return $this;
    }

    public static function create(...$arguments)
    {
        $field = new static(...$arguments);
        return $field;
    }

    /**
     * Set name
     * @param null $name
     * @return $this
     */
    public function name($name = null) {
        $this->name = $name;
        return $this;
    }

    /**
     * Set email address
     * @param null $email
     * @return $this
     */
    public function email($email = null) {
        $this->email = $email;
        return $this;
    }


}
