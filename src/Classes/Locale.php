<?php

namespace Heiw\Uxcrudible\Classes;

use Heiw\Uxcrudible\Helpers;
use Illuminate\Support\Facades\App;

/**
 * Class Locale
 */
class Locale
{
    /**
     * @var string array Original locale
     */
    private $originalLocales = [];

    /**
     * Get the locale based on the host.
     * @return \Illuminate\Config\Repository|mixed|string
     */
    public static function localeFromHost() {
        switch (Helpers::get_host()) {
            case env('HOST_CY'):
                return 'cy';
                break;
            default:
                $uri = explode(".", request()->getHost());
                $tld = end($uri);
                switch ($tld) {
                    case 'lleol':
                    case 'cymru':
                        return 'cy';
                        break;
                }
                break;
        }
        // Default to primary locale (first in locales array in config/translatable.php)
        if (is_array(config('translatable.locales'))) {
            return config('translatable.locales')[0];
        } else {
            return config('app.locale');
        }
    }

    /**
     * Set locale based on host value
     */
    public static function setLocaleFromHost() {
        App::setLocale(self::localeFromHost());
    }

    /**
     * Retrieve the fallback locale.
     * @return int|mixed|string
     */
    public static function fallbackLocale() {
        return array_key_first(config('uxcrud.locales'));
    }

    /**
     * Set locale whilst keeping current locale to revert to.
     * @param string $locale    Locale to set
     */
    public function pushLocale($locale) {
        array_push($this->originalLocales, App::getLocale());
        App::setLocale($locale);
    }

    /**
     * Alias for pushLocale
     * @param $locale   Locale to set
     */
    public function addLocale($locale) {
        $this->pushLocale($locale);
    }

    /**
     * Revert the locale to the previous state.
     */
    public function popLocale() {
        App::setLocale(array_pop($this->originalLocales));
    }

    /**
     * Alias for popLocale
     */
    public function revertLocale() {
        $this->popLocale();
    }

    public function getLocale() {
        return App::getLocale();
    }

    public function getOriginalLocale() {
        return end($this->originalLocales);
    }
}
