<?php

namespace Heiw\Uxcrudible\Classes;

/**
 * Class JsonToast
 * Template for Toast message passed as JSON.
 * Success messages disappear automatically. All other messages will need to be
 * manually dismissed.
 */
class JsonToast {
    /**
     * @var string  danger|success|warning|error
     */
    public $status;

    /**
     * @var string
     */
    public $header;

    /**
     * @var string
     */
    public $body;

    /**
     * @var string
     */
    public $data;

    /**
     * @var string
     */
    public $redirect;

    public $type = 'toast';

    /**
     * Status of the notification.
     * @param $status   string  danger|success|warning|error|refresh
     * @return $this
     */
    public function status($status) {
        $this->status = $status;
        return $this;
    }

    /**
     * @param string $header   Header of toast notification.
     * @return $this
     */
    public function header($header) {
        $this->header = $header;
        return $this;
    }

    /**
     * @param string $body  Main message of notification.
     * @return $this
     */
    public function body($body) {
        $this->body = $body;
        return $this;
    }

    /**
     * @param string $data  Main message of notification.
     * @return $this
     */
    public function data($data) {
        $this->data = $data;
        return $this;
    }

    /**
     * @param string $redirect  URI to redirect to.
     * @return $this
     */
    public function redirect($redirect) {
        $this->redirect = $redirect;
        return $this;
    }

    public static function create(...$arguments)
    {
        $field = new static(...$arguments);
        return $field;
    }

    /**
     * JsonToast constructor.
     * @param string $body Main message of notification.
     * @param $status   string  danger|success|warning|error
     * @param string $header Header of toast notification.
     * @param null $data
     * @param null $redirect
     */
    public function __construct(
        $body = null,
        $status = null,
        $header = null,
        $data = null,
        $redirect = null
    ) {
        $this->header = $header;
        $this->body= $body;
        $this->status= $status;
        $this->data= $data;
        $this->redirect = $redirect;
        return $this;
    }
}
