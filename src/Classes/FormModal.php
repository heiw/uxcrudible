<?php

/****
 * TODO MK Move FormModal functionality from UxcrudController->getEditModal this
 * separate class.
 */

namespace Heiw\Uxcrudible\Classes;

use Heiw\Uxcrudible\Form\Fields\DataTable;
use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\UxcrudController;
use Illuminate\Support\Str;

/**
 * Class FormModal
 * Create a Form Modal to view, create or edit
 */
class FormModal {
    /**
     * @var \Heiw\Uxcrudible\Models\Model
     */
    public $model;

    /**
     * Defaults to action label + controller label + model summary.
     * @var string
     */
    public $title = null;

    /**
     * @var int
     */
    public $action;

    public $fields;
    public $ability;

    public $viewOnly = true;

    /*
     * @var DataTableButtons[]
     */
    public $modalButtons = [];

    /**
     * @var string When set to null use default label set to override.
     */
    public $closeButtonLabel = null;

    /**
     * @var DataTableParentOptions
     */
    public $dataTableParentInfo;

    /**
     * @var bool|UxcrudController
     * false: Do not use a controller and return modal without form.
     * UxcrudController: Use controller passed to function.
     */
    public $controller;

    /**
     * @param $model            \Heiw\Uxcrudible\Models\Model to show modal for.
     * @param $action           int action to perform
     * @param bool|UxcrudController $controller
     *                          false: Do not use a controller and return modal without form.
     *                          UxcrudController: Use controller passed to function.
     */
    public function __construct($model, $action, $controller) {
        $this->model($model);
        $this->action($action);
        $this->controller($controller);
        return $this;
    }

    public static function create(...$arguments)
    {
        $field = new static(...$arguments);
        return $field;
    }

    public function model($model) {
        $this->model = $model;
        return $this;
    }

    /**
     * Defaults to action label + controller label + model summary.
     * Overwrite to change.
     * @param string $title    Title to use for modal.
     * @return $this
     */
    public function title($title) {
        $this->title = $title;
        return $this;
    }

    public function action($action) {
        $this->action = $action;
        return $this;
    }

    public function viewOnly($viewOnly = true) {
        $this->viewOnly = $viewOnly;
        return $this;
    }

    public function controller($controller) {
        $this->controller = $controller;
    }

    /**
     * Set modal properties based on controller.
     * @return $this
     */
    public function processController() {
        $this->ability($this->controller->getActionInfo($this->action));

        $this->dataTableParentInfo($this->controller->getDataTableParentInfo());
        $this->fields($this->controller->getFields($this->ability->permission, false, [], $this->model));

        // Allow controller to set modal buttons.
        $this->controller->modalButtons($this);
        return $this;
    }

    public function fields($fields) {
        $this->fields = $fields;
        return $this;
    }

    /**
     * When set to null use default label set to override.
     * @param string|null $closeButtonLabel
     * @return $this
     */
    public function closeButtonLabel($closeButtonLabel = null) {
        $this->closeButtonLabel = $closeButtonLabel;
        return $this;
    }

    public function ability($ability) {
        $this->ability = $ability;
        $this->viewOnly = ($this->ability->permission == \Heiw\Uxcrudible\Form\Fields\Permission::VIEW);
        return $this;
    }

    /**
     * Clears existing modal buttons and add provided modal buttons if set.
     * @param DataTableButton|DataTableButton[] $modalButtons
     * @return $this
     */
    public function resetModalButtons($modalButtons = []) {
        $this->modalButtons = [];
        if (!empty($modalButtons)) {
            $this->addModalButtons($modalButtons);
        }
        return $this;
    }

    /**
     * Alias for addModalButtons
     * @param DataTableButton $modalButton
     * @return $this
     */
    public function addModalButton($modalButton) {
        return $this->addModalButtons($modalButton);
    }

    /**
     * Add modal button(s) to modal.
     * @param DataTableButton|DataTableButton[] $modalButtons
     * @return $this
     */
    public function addModalButtons($modalButtons) {
        if (!is_array($modalButtons)) {
            $modalButtons = [$modalButtons];
        }
        $this->modalButtons = array_merge($this->modalButtons, $modalButtons);
        return $this;
    }

    /**
     * Set the dataTableParentInfo
     * @param $dataTableParentInfo
     * @return $this
     */
    public function dataTableParentInfo($dataTableParentInfo) {
        $this->dataTableParentInfo = $dataTableParentInfo;

        // On create action add the details of the parent record to the modal
        // title
        if ($this->ability->action === UxcrudController::ABILITY_CREATE && is_null($this->title)) {
            if (isset($this->dataTableParentInfo->model)) {
                $this->title($this->ability->label . " " .
                    __($this->controller->getLabel()) . " &raquo; " .
                    $this->dataTableParentInfo->model->summary()
                );
            }
        }

        return $this;
    }

    public function jsonModal() {
        $modal = new JsonModal();
        $modal->addDataAttributes($this->controller->modalOptions);

        if($this->controller) {
            $modal->addForm(new JsonForm(
                rtrim(__($this->controller->getAjaxRoute()) . "/" . $this->model->id, "/"),
                "uxcrudible-form-" . Str::kebab($this->ability->action) . "-" . Str::kebab($this->controller->getClassName()),
                "uxcrudible-form " . Str::kebab($this->ability->action) . " " . Str::kebab($this->controller->getClassName())
            ));
            $modal->id = trim(Str::kebab($this->ability->action) . "-" . Str::kebab($this->controller->getLabel()) . "-" . $this->model->id, '-');
        } else {
            $modal->id = trim(Str::kebab($this->ability->action) . "-" . $this->model->id, '-');
        }

        // Set locale if provided
        if (request('locale')) {
            $localeStore = resolve(\Heiw\Uxcrudible\Classes\Locale::class);
            $localeStore->pushLocale(request('locale'));
        }

        $locales = $this->model->translationLocales();
//        request()->session()->put('active-edit-locale', 'all');
        $sessionActiveEditLocale = request()->session()->get('active-edit-locale');

        $modal->class = "uxcrud-edit-modal uxcrud-{$this->action}";
        $modal->body = view('uxcrud::admin.edit', ['ability' => $this->ability])->render();

        $modal->header = "";
        if ($locales) {
            $modal->header .= "<div class='float-right'>
                            <div class='btn-group uxcrud-modal-select-locale' role='group' aria-label='" . Helpers::escapeQuotes(__('Select language')) . "'
                            data-locales='" . json_encode(array_keys($locales)) . "'
                            >";
            foreach($locales as $code => $locale) {
                if( (is_null($sessionActiveEditLocale) && $locale['active']) || $sessionActiveEditLocale == $locale) {
                    $active = "active";
                    $checked = "checked";
                    $modal->class .= " locale-{$code}";
                } else {
                    $active = "";
                    $checked = "";
                }
                if ($sessionActiveEditLocale == 'all') {
                    $modal->class .= " locale-{$code}";
                }

                $modal->header .= "<label class='btn btn-radio btn-outline-info $active title='{$locale['name']}'>
                                        <input type='radio' name='locale' value='{$code}' autocomplete='off' {$checked}> {$code}
                                    </label>";
            }
            if (count($locales) > 1) {
                if ($sessionActiveEditLocale == 'all') {
                    $active = "active";
                    $checked = "checked";

                } else {
                    $active = "";
                    $checked = "";
                }
                $modal->header .= "<label class='btn btn-radio btn-outline-info $active title='" . Helpers::escapeQuotes(__('all')) . "'>
                                        <input type='radio' name='locale' value='all' autocomplete='off' {$checked}> <i class='fas fa-globe'></i> " . __('all') . "
                                    </label>";
            }
            $modal->header .="</div></div>";
        }

        // Restore locale if it was previously provided.
        if (request('locale')) {
            $localeStore->revertLocale();
        }

        if (!is_null($this->title)) {
            $modal->header .= $this->title;
        } elseif ($this->controller) {
            $modal->header .= $this->ability->label . " " . __($this->controller->getLabel()) . " " . $this->model->summary();
        }

        foreach($this->modalButtons as $button) {
            if ($button) {
                $modal->buttons[] = $button->toString();
            }
        }

        //$this->modalButtons($this->controller->modalButtons())

        foreach ($this->fields as $field) {
            $modal->body .= view("uxcrud::forms.fields.edit.line",
                [
                    'controller' => $this->controller,
                    'model' => $this->model,
                    'ability' => $this->ability,
                    'field' => $field,
                    'parentModel' => $this->dataTableParentInfo->model,
                    'modal' => $modal,
                    'viewOnly' => $this->viewOnly
                ]
            )->render();
        }

        $modal->footer = view('uxcrud::admin.errors')->render() . $modal->footer;
        $modal->size = 'xxl';

        if (!is_null($this->closeButtonLabel)) {
            $modal->closeButton = $this->closeButtonLabel;
        }

        return $modal;
    }
}
