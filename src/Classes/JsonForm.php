<?php

namespace Heiw\Uxcrudible\Classes;

/**
 * Template for Form element to send back as JSON.
 * Class JsonForm
 */
class JsonForm {
    /**
     * @var string
     */
    public $action;

    /**
     * @var string
     */
    public $class;

    /**
     * @var string
     */
    public $method;
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $enctype;

    public $data = [];

    public function __construct(
        $action,
        $id,
        $class = 'uxcrudible-form',
        $method = 'POST',
        $enctype = 'multipart/form-data'
    ) {
        $this->action = $action;
        $this->id = $id;
        $this->class = $class;
        $this->method = $method;
        $this->enctype = $enctype;
    }

    public function onSuccess($function) {
        $this->data["onSuccess"] = $function;
        return $this;
    }

    public function onError($function) {
        $this->data["onError"] = $function;
        return $this;
    }
}