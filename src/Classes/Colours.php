<?php
namespace Heiw\Uxcrudible\Classes;

use App\Models\Post;
use Heiw\Uxcrudible\Services\Menu;

/**
 * Class Colours
 * Definition of colours used by Material theme
 * @package WalesDeanery\Jam\Classes
 */
class Colours
{
    /**
     * @const array
     */
    private static $colours = array(
        'red',
        'pink',
        'purple',
        'indigo',
        'blue',
        'cyan',
        'teal',
        'green',
        'yellow',
        'orange',
    );

    /**
     * Returns different colour based on total number of colours
     * @param $i
     * @param $total
     * @return string Name of the matched colour
     */
    public static function get($i, $total) {
        return self::$colours[$i % count(self::$colours)];
    }

    public static function themeColour() {
        $colour = "blue";
        $menu = resolve(Menu::class);
        $page = $menu->currentPage();
        if (app('impersonate')->isImpersonating()) {
            $colour = "orange";
        } else {
            if (!empty($page->colour)) {
                $colour = $page->colour;
            } else {
                if (config('uxcrud.theme_colour')) {
                    $colour = config('uxcrud.theme_colour');
                }
            }
        }
        return "uxcrud-$colour";
    }
}