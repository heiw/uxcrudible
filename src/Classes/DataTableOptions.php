<?php

namespace Heiw\Uxcrudible\Classes;

use Illuminate\Support\Str;

/**
 * Options for DataTables
 * Class DataTableOptions
 */
class DataTableOptions
{
    /**
     * @var array boolean
     */
    public $exports = [
        'csv' => true,
        'excel' => false,
//        'pdf' => true,
    ];

    public $search = true;

    public $filters = true;

    public $customButtons = [];

    /**
     * Disable all exports
     * @return $this
     */
    public function noExports($allowExports = false) {
        foreach($this->exports as $key => $export) {
            $this->exports[$key] = $allowExports;
        }
        return $this;
    }

    /**
     * Disable Excel export
     * @param bool $allowExport
     * @return $this
     */
    public function noExcelExport($allowExport = false) {
        $this->exports['excel'] = $allowExport;
        return $this;
    }

    /**
     * Disable CSV export
     * @param bool $allowExport
     * @return $this
     */
    public function noCSVExport($allowExport = false) {
        $this->exports['csv'] = $allowExport;
        return $this;
    }

    /**
     * Disable search
     * @param false $search
     * @return $this
     */
    public function noSearch($search = false) {
        $this->search = $search;
        return $this;
    }

    /**
     * Disable filters
     * @param false $filters
     * @return $this
     */
    public function noFilters($filters = false) {
        $this->filters = $filters;
        return $this;
    }

    /**
     * Set the column to use for row orders.
     * @param $column
     * @return $this
     */
    public function rowReorder($column) {
        if (is_bool($column)) {
                $this->rowReorder = $column;
        } else {
            $this->rowReorder = ['dataSrc' => null];
        }
        return $this;
    }

    public function hasRowReorder() {
        return (!isset($this->rowReorder) || $this->rowReorder !== false);
    }
}
