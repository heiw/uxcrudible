<?php

namespace Heiw\Uxcrudible\Classes;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UxcrudModelMigration extends Migration
{
    public static function models() {
        return [];
    }
    public static function modelTranslations() {
        return [];
    }

    public function upModel() {
        if (Schema::hasTable('model') && static::models()) {
            DB::table('model')->insertOrIgnore(static::models());
        }
        if (Schema::hasTable('model_translation') && static::modelTranslations()) {
            DB::table('model_translation')->insertOrIgnore(static::modelTranslations());
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function downModel()
    {
        if (Schema::hasTable('model') && static::models()) {
            DB::table('model')->whereIn('id', array_column(static::models(), 'id'))->delete();
        }
        if (Schema::hasTable('model_translation') && static::modelTranslations()) {
            DB::table('model_translation')->whereIn('model_id', array_column(static::models(), 'id'))->delete();
        }
    }
}
