<?php

namespace Heiw\Uxcrudible\Classes;


/**
 * Class ProgressStage
 */
class ProgressStage
{
    public $key;
    public $label;
    public $icon;
    public $card;

    public function __construct($label = null, $icon = null, $card = null) {
        $this->label($label);
        $this->icon($icon);
        $this->card($card);
        return $this;
    }

    public static function create(...$arguments)
    {
        $field = new static(...$arguments);
        return $field;
    }

    public function label($label) {
        $this->label = $label;
        return $this;
    }

    public function icon($icon) {
        $this->icon = $icon;
        return $this;
    }

    public function card($card) {
        $this->card = $card;
        return $this;
    }
}
