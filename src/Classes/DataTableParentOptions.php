<?php

namespace Heiw\Uxcrudible\Classes;

use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\UxcrudModel;

class DataTableParentOptions {
    public $dataTableId = null;
    /**
     * @var UxcrudController
     */
    public $controller = null;
    /**
     * @var UxcrudModel
     */
    public $model = null;

    public $dataset = null;
}
