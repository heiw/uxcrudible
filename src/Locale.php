<?php

namespace Heiw\Uxcrudible;

use Closure;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uri = explode(".", request()->getHost());
        $tld = end($uri);
        switch ($tld) {
            case 'lleol':
            case 'cymru':
                app()->setLocale('cy');
                break;
        }

        return $next($request);
    }
}