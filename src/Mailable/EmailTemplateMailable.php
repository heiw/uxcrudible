<?php

namespace Heiw\Uxcrudible\Mailable;

use Heiw\Uxcrudible\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailTemplateMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('uxcrud::emails.emailTemplate')
        ;
    }
}
