<?php

namespace Heiw\Uxcrudible\Models;

use Heiw\Uxcrudible\Services\Menu;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class File extends UxcrudModel implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable, CachedObjectValue;

    public const FOLDER = 1;
    public const FILE = 2;

    protected $table = 'file';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-file';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'store',
        'is_folder',
        'parent_id',
        'uploaded_by_id',
    ];

    protected static function boot(): void
    {
        parent::boot();
        parent::observe(FileObserver::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function files(): \Illuminate\Database\Eloquent\Relations\hasMany {
        return $this->hasMany(
            File::class,
            'parent_id',
            'id'
        );
    }

    public function parent() {
        return $this->belongsTo(File::class, 'parent_id');
    }


    /**
     * Overwrite function to add additional attributes on create
     * @param $attributes   [] array containing default attributes
     * @param $model        Model parent model
     * @param $filename     string Filename of location in store.
     * @param $file         File Full file
     * @return mixed
     */
    public function setAttributesBeforeCreate($attributes, $model, $filename, $file) {
        $attributes['store'] = $filename;
        $attributes['name'] = $file->getClientOriginalName();
        return $attributes;
    }

//    public function uri() {
//        $menu = resolve(Menu::class);
//        return $menu->uri($this->id);
//    }
}

class FileObserver {
    public function deleting($file):void {
        // Don't delete file on soft delete, on delete file from store on forced delete.
        if ($file->isForceDeleting()) {
            Storage::delete($file->store);
        }
    }
}
