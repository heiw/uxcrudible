<?php

namespace Heiw\Uxcrudible\Models;

use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Heiw\Uxcrudible\Classes\EmailTemplateRecipient;
use Heiw\Uxcrudible\Classes\JsonModal;
use Heiw\Uxcrudible\Classes\JsonToast;
use Heiw\Uxcrudible\Classes\Locale;
use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\Uxcrud\EmailTemplateSend;
use Heiw\Uxcrudible\Mailable\EmailTemplateMailable;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class EmailTemplate extends UxcrudModel implements TranslatableContract
{
    use Translatable;

    protected $table = 'email_template';
    protected static $orderBy = 'name';
    public static $icon = 'far fa-newspaper';

    /**
     * @var bool Allow user who normally doesn't have permission to sent an eamil to sent and email.
     */
    private $overWriteSendPermission = false;

    /**
     * @var array Data records from which recipient details can be extracted.
     */
    private $recipients = [];

    /**
     * @var array Data to enable in Email Template.
     */
    private $data = [];

    /**
     * @var bool Set to true to email sent report
     */
    private $sentReport = false;

    public $translatedAttributes = [
        'subject', 'content', 'files'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'include_snippet_signature', 'priority'
    ];

    public function overWriteSendPermission($overWriteSendPermission = true) {
        $this->overWriteSendPermission = $overWriteSendPermission;
        return $this;
    }

    /**
     * @param int|string $template Id or name of template to load.
     * @return EmailTemplate
     */
    public static function getByNameOrID($template) {
        if (is_int($template)) {
            return new EmailTemplate($template);
        } else {
            return EmailTemplate::where('name', '=', $template)->first();
        }
    }

    /**
     * Add record that includes recipients data to use in email template.
     * @param User[]|User|string[]|string Array of records that contain users to
     * send email to.
     * It is recommended to use User objects as these will automatically link the
     * user record to the email log.
     * Plain email addresses in strings may be used but are not recommended.
     * @return $this
     */
    public function addRecipients($recipients) {
        if (is_array($recipients)) {
            $this->recipients = array_merge($this->recipients, $recipients);
        } elseif (is_object($recipients) && (get_class($recipients) === Collection::class)) {
            foreach($recipients as $recipient) {
                $this->recipients[] = $recipient;
            }
        } else {
            $this->recipients = [$recipients];
        }
        return $this;
    }

    public function clearRecipients() {
        $this->recipients = [];
        return $this;
    }

    /**
     * Enable to create sent report to current user after ending of email.
     * @param bool $sentReport
     * @return $this
     */
    public function sentReport($sentReport = true) {
        $this->sentReport = $sentReport;
        return $this;
    }

    /**
     * @param Model|Model[] Model or array of models to make available in template.
     * @return $this
     */
    public function data($data) {
        if (!is_array($data)) {
            $data[] = $data;
        }
        $this->data = array_merge($this->data, $data);
        return $this;
    }

    /**
     * @param File|File[] File or array of Files to attach to the email.
     * @return $this
     */
    public function attach($attachments) {
        if (!is_array($attachments)) {
            $attachments[] = $attachments;
        }
        $this->$attachments = array_merge($this->$attachments, $attachments);
        return $this;
    }

    /**
     * Send the message
     * @param UxcrudController $uxcrudController
     * @return array
     */
    public function send($uxcrudController = null) {
        $sentResultErrors = [];
        $sentResultSuccess = [];
        $locale = new Locale();

        $sentReportData = [
            'sent_errors' => 0,
            'sent_results' => null,
            'email_template' => $this->name,
        ];

        $content = null;

        // Create empty email template in case there are no recipients.
        $template = new EmailTemplateMailable();
        $template->with([
            'log' => [
                // If logged in add user_id
                'sender_id' => (auth()->user()) ? auth()->user()->id : null,
                'email_template_id' => $this->id,
            ]
        ]);

        foreach ($this->recipients as $recipient) {
            // Default to current recipient record as recipient user.
            $recipientUsers = [$recipient];

            if (method_exists($uxcrudController, 'mapEmailTemplateRecordToUsers')) {
                $recipientUsers = $uxcrudController->mapEmailTemplateRecordToUsers($recipient);
            }

            // A record for a recipient can have more than one user to recieve and email.
            foreach ($recipientUsers as $recipientUser) {
                if (is_string($recipientUser)) {
                    $toEmails = [$recipientUser];
                    $recipientId = null;
                } else {
                    $recipientId = $recipientUser->id;

                    /*
                     * TODO MK Figure out a way to clear To email addresses so creating
                     * of template can be done once for a recipient instead for each
                     * individual email.
                     */
                    $toEmails = [
                        EmailTemplateRecipient::create($recipientUser->email, $recipientUser->summary())
                    ];
                    if (!is_null($recipientUser->email_alternative)) {
                        $toEmails[] = EmailTemplateRecipient::create($recipientUser->email_alternative,
                            $recipientUser->summary());
                    }

                    // Set locale for user.
                    if (method_exists($recipientUser, 'preferredLocale')) {
                        $locale->pushLocale($recipientUser->preferredLocale());
                    }
                }

                foreach ($toEmails as $toEmail) {
                    $emailSignature = Snippet::get('email_snippet_signature');
                    $template = new EmailTemplateMailable();

                    $template->with([
                        'data' => $this->data,
                        'includeSnippetSignature' => $this->include_snippet_signature,
                        'emailSignature' => $emailSignature,

                        'log' => [
                            'recipient_id' => $recipientId,
                            // If logged in add user_id
                            'sender_id' => (auth()->user()) ? auth()->user()->id : null,
                            'email_template_id' => $this->id,
                        ]
                    ]);

                    if (is_int($this->priority)) {
                        $template->priority($this->priority);
                    }

                    $data = $this->data;

                    // Merge current recipient's data if not plain email address.
                    if  (!is_string($recipient)) {
                        $data = array_merge(
                            $data,
                            $recipient->emailTemplateViewData('recipient')
                        );
                        $templateViewData = $recipient->emailTemplateViewData('recipient');
                    } else {
                        $templateViewData = $recipient;
                    }

                    // Include overview of available data in short keys
                    $data['email_template_short_keys'] = Helpers::implode_different_last(array_keys($data), true);

                    // Replace any data in content.
                    $content = __($this->content, $data);


                    // If recipientUsers differs from recipient add it's emailTemplateViewData
                    if (!is_string($recipientUser) && !is_null($recipientUser) && $recipientUser !== $recipient) {
                        $templateViewData = array_merge($recipientUser->emailTemplateViewData(), $templateViewData);
                    }

                    $template
                        // Replace any data in subject and add to message.
                        ->subject(__($this->subject, $data))
                        ->with(['content' => $content])
                        // Add recipient data for use in blade.
                        ->with(['recipient' => $templateViewData]);

                    if (isset($toEmail->email)) {
                        $toEmailAddress = $toEmail->email;
                    } else {
                        $toEmailAddress = $toEmail;
                    }

                    try {
                        $mail = Mail::to($toEmail)
                            ->send($template);
                        $sentResultSuccess[] = $toEmailAddress;
                    } catch (\Exception $exception) {
                        // Set email
                        $emailLog = new EmailLog();
                        $emailLog->sender_id = $template->viewData['log']['sender_id'];
                        $emailLog->recipient_id = $template->viewData['log']['recipient_id'];
                        $emailLog->email_template_id = $template->viewData['log']['email_template_id'];
                        $emailLog->content = $content;
                        $emailLog->addresses = json_encode($toEmail);
                        $emailLog->success = false;
                        $emailLog->date = now();
                        $emailLog->errors = json_encode([
                            'code' => $exception->getCode(),
                            'message' => $exception->getMessage(),
                        ]);
                        $emailLog->save();
                        $sentResultErrors[$exception->getCode()][] = [
                            'email' => $toEmailAddress,
                            'message' => $exception->getMessage()
                        ];
                        $sentReportData['sent_errors']++;
                    }
                }
            }

            // Revert locale for user
            if (method_exists($recipient, 'preferredLocale')) {
                $locale->popLocale();
            }
        }


        if (count($sentResultErrors) > 0) {
            $sentReportData['sent_results'] .= '<h4>' . __('Sent errors') . '</h4><ul>';
            foreach ($sentResultErrors as $errorCode => $error) {
                $sentReportData['sent_results'] .= "<li>" . __('Error code: :code', ['code' => $errorCode]) . "<ul>";
                foreach ($error as $details) {
                    $sentReportData['sent_results'] .= "<li>{$details['email']}: {$details['message']}</li>";
                }
                $sentReportData['sent_results'] .= "</ul></li>";
            }
            $sentReportData['sent_results'] .= '</ul>';
        }
        if (count($sentResultSuccess) > 0) {
            $sentReportData['sent_results'] .= '<h4>' . __('Sent without errors') . '</h4><ul><li>';
            $sentReportData['sent_results'] .= implode('</li><li>', $sentResultSuccess);
            $sentReportData['sent_results'] .= '</li></ul>';
        }
        $sentReportData['email_name'] = $this->name;
        $sentReportData['email_subject'] = $template->subject;
        $sentReportData['email_content'] = $this->content;
        $sentReportData['sent_results'] .= "</ul>";

        if ($this->sentReport && auth()->user()) {
            $sendReportEmailTemplate =  EmailTemplate::getByNameOrID('delivery_report');
            if ($sendReportEmailTemplate) {
                $sendReportEmailTemplate
                    ->data($sentReportData)
                    ->addRecipients(auth()->user())
                ;
                $sendReportEmailTemplate->send();
            } else {
                $sentReportData['sent_errors'] .= "<p>Error: Delivery report template 'delivery_report' not found</p>";
            }
        }
        return $sentReportData;
    }

    public function initialiseFreeText($attributes) {
        $request = request();
        if ($this->id === config('uxcrud.email_template_free_template_id')) {
            $this->fill($attributes);
        }
        return $this;
    }

    /**
     * Send email to a group of users.
     * @param Builder $builder
     * @param UxcrudController $uxcrudController Uxcrud Controller of the
     * datatable that's sending the email.
     * @return JsonModal|JsonToast
     * @throws ValidationException
     */
    public static function sendToGroup($builder, $uxcrudController) {
        $model = new \Heiw\Uxcrudible\Models\EmailTemplateSend();
        $totalCount = $builder->getQuery()->getCountForPagination();
        $controller = new EmailTemplateSend();

        $messages = [];
        $rules = $controller->getValidationRules(\Heiw\Uxcrudible\Form\Fields\Permission::CREATE, $model, request(), $messages);
        $attributes = request()->validate($rules, $messages);

        $entries = $builder->get();

        $template = null;
        $sentResults = null;
        $template = \Heiw\Uxcrudible\Models\EmailTemplate::find(request()->get('email_template_id'));

        // If template doesn't exist throw exception.
        if (is_null($template)) {
            throw ValidationException::withMessages([
                "email_template_id" => [__("Email template not found")]
            ]);
        }

        // Load free text data if set
        if ($template->id == config('uxcrud.email_template_free_template_id')) {
            $template->fill($attributes);
        }

        $template
            ->sentReport(request()->get('sent_report'))
            ->addRecipients($entries)
            // Initialise free text as last in chain.
            ->initialiseFreeText($attributes);
        $sentResults = $template->send($uxcrudController);

        $summary = __('Email :template send to :number users',
            ['template' => $template->summary(), 'subject' => $template->subject, 'number' => $totalCount]);
        if ($sentResults) {
            $response = JsonModal::create()
                ->status('success')
                ->header($summary)
                ->body(Helpers::implode_with_keys($sentResults, ": ", "<br/>"));
        } else {
            $response = JsonToast::create()
                ->status('success')
                ->header(__('Email send'))
                ->body($summary);
        }
        if ($sentResults['sent_errors'] !== 0) {
            $response->status('danger');
        }
        return $response;
    }
}

