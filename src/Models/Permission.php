<?php

namespace Heiw\Uxcrudible\Models;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Permission extends UxcrudModel implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'permission';
    protected static $orderBy = 'id';
    public static $icon = 'fas fa-key';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'display'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(
            Role::class,
            'model_permission_role',
            'permission_id',
            'role_id'
        );
    }

    public function allRoles() {
        return Role::orderBy(Role::getOrderBy())->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function models(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(
            Permission::class,
            'model_permission_role',
            'permission_id',
            'model_id'
        );
    }

    public function allModels() {
        return Model::orderBy(Model::getOrderBy())->get();
    }

}
