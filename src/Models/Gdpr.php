<?php

namespace Heiw\Uxcrudible\Models;

use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Heiw\Uxcrudible\Classes\Locale;
use Heiw\Uxcrudible\Services\Menu;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class Gdpr extends UxcrudModel implements
    TranslatableContract
{
    use Translatable;

    protected $table = 'gdpr';
    protected static $orderBy = 'date';
    public static $icon = 'fas fa-shield-alt';

    public $translatedAttributes = ['name', 'content'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'content',
        'notes',
        'compulsory',
        'active',
        'date',
    ];

    public function summary() {
        return $this->name;
    }

    public static function boot() {
        parent::boot();

        // Hook into the saved event which will fire after storing the data.
        self::saved(function ($model) {
            self::singleActiveGdpr($model);
        });
    }

    public function icon() {
        if ($this->active) {
            return 'fas fa-shield-alt';
        }
        return 'fas fa-history';
    }

    public function slug() {
        return Str::slug($this->name);
    }

    public static function singleActiveGdpr($model) {
        // If GDPR is active de-active all other GDPR items
        if ($model->active == 1) {
            DB::table('gdpr')
                // only update active posts
                ->where('active', 1)
                // leave current post active
                ->where('id', '<>', $model->id)
                // de-active
                ->update(['active' => 0])
            ;
        }
    }

    /**
     * Get the active GDPR statement
     */
    public static function getActive() {
        return Gdpr::where('active', '=', 1)
            ->first();
    }

    public function allGdpr() {
        return Gdpr::orderBy(Gdpr::getOrderBy())->get();
    }

    /**
     * Get active GDPR statement and any previously signed statements.
     * @return mixed
     */
    public static function getActiveAndPrevious() {
        return Gdpr::
            leftJoin('gdpr_user', function($join) {
                $join->on('gdpr_user.gdpr_id', '=', 'gdpr.id');
                $join->on('gdpr_user.user_id', '=', DB::raw(auth()->user()->id));
            })
            ->where('active', '=', 1)
            ->orWhereNotNull('gdpr_user.sign_date')
            ->orderBy('active', 'DESC')
            ->orderBy('date', 'DESC')
            ->get();
    }

    /**
     * Check whether GDPR agreement has been signed by any users.
     * @return bool
     */
    public function isSigned() {
        return DB::table('gdpr_user')
            ->whereGdprId($this->id)
            ->count() > 0;
    }

    /**
     * Check whether GDPR agreement is active/compulsory
     * @return bool
     */
    public static function isGdprActive() {
        $user = auth()->user();
        if ($user) {
            $active = self::getActive();
            if ($active && $user->activeGdpr->count() == 0) {
                return true;
            }
        }
        // No user, not active or already signed.
        return false;
    }
}

