<?php

namespace Heiw\Uxcrudible\Models;

use Heiw\Uxcrudible\UxcrudModel;

class SiteConfig extends UxcrudModel
{
    protected $table = 'site_config';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-sliders-h';

    protected $fillable = ['name', 'value', 'description'];

    public static function get($name, $default = null) {
        $result = SiteConfig::where('name', $name)->first();
        if ($result) {
            return $result->value;
        } else {
            return $default;
        }
    }
}
