<?php

namespace Heiw\Uxcrudible\Models;

class ModelTranslation extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'model_translation';

    public $timestamps = false;
    protected $fillable = ['display'];
}