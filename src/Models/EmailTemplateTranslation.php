<?php

namespace Heiw\Uxcrudible\Models;

class EmailTemplateTranslation extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'email_template_translation';

    public $timestamps = false;
    protected $fillable = [
        'subject', 'content', 'files'
    ];
}