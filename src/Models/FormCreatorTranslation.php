<?php

namespace Heiw\Uxcrudible\Models;

class FormCreatorTranslation extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'form_creator_translation';

    public $timestamps = false;
    protected $guarded = ['id'];

}
