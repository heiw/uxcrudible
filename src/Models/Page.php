<?php

namespace Heiw\Uxcrudible\Models;

use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Heiw\Uxcrudible\Classes\Locale;
use Heiw\Uxcrudible\Services\Menu;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class Page extends UxcrudModel implements
    AuthenticatableContract,
    AuthorizableContract,
    TranslatableContract
{
    use Authenticatable, Authorizable, CachedObjectValue, Translatable;

    public const ACCESS_PRIVATE_ONLY = 0;
    public const ACCESS_PUBLIC = 1;
    public const ACCESS_PUBLIC_ONLY = 2;

    protected $table = 'page';
    protected static $orderBy = 'tree_order';
    public static $icon = 'fas fa-book-open';

    protected $css = [];
    protected $js = [];

    protected $rawCss = [];
    protected $rawJs = [];

    protected $colour = null;

    public $translatedAttributes = ['path', 'name', 'slug', 'title', 'description', 'content'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tree_order',
        'path',
        'display_order',
        'name',
        'slug',
        'page_type',
        'title',
        'description',
        'content',
        'icon',
        'public',
        'parent_id',
        'children'
    ];

    public function summary() {
        return $this->name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function snippets(): \Illuminate\Database\Eloquent\Relations\hasMany {
        return $this->hasMany(
            Snippet::class,
            'page_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function children(): \Illuminate\Database\Eloquent\Relations\hasMany {
        return $this->hasMany(
            Page::class,
            'parent_id',
            'id'
        );
    }

    public function hasChildren() {
        return ($this->children()->count() > 0);
    }

    /**
     * Return label (defaults to "children") if page has children.
     * If no children it returns null.
     * @param string $onTrue
     * @return string|null
     */
    public function ifChildren($onTrue = "children", $onFalse = null) {
        if ($this->hasChildren()) {
            return $onTrue;
        } else {
            return $onFalse;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(Role::class)
            ->orderBy(Role::getOrderBy());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function models(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        $result = $this->belongsToMany(Model::class)
            ->withPivot('order')
            ->orderBy('order')
            ->orderBy(Model::getOrderBy());
        return $result;
    }

    public function parent() {
        return $this->belongsTo(Page::class, 'parent_id');
    }

    public function isActive() {
        $menu = resolve(Menu::class);
        return $menu->isActive($this);
    }

    /**
     * Returns label (defaults to "active") when page or child page is active
     * or returns null when not active.
     * @param string $onTrue
     * @param null $onFalse
     * @return string|null
     */
    public function ifActive($onTrue = "active", $onFalse = null) {
        if ($this->isActive()) {
            return $onTrue;
        } else {
            return $onFalse;
        }
    }

    /**
     * @param bool $fallbackLocale   If set to true use fallback locale instead of current locale.
     * @return mixed
     */
    public function uri($fallbackLocale = false) {
        if ($fallbackLocale) {
            if (!is_null($this->translate(Locale::fallbackLocale()))) {
                return $this->translate(Locale::fallbackLocale())->path;
            }
        } else {
            return $this->path;
        }
        return null;
    }

    public function route() {
        return trim($this->uri(), '/');
    }

    public function setColour($colour) {
        $this->colour = $colour;
        return $this;
    }

    public function addCss($file)
    {
        $this->css[] = $file;
        return $this;
    }

    public function addRawCss($css)
    {
        $this->rawCss[] = $css;
        return $this;
    }

    public function getCss() {
        return self::cacheResult("getCss", function() {
            $file = 'css' . $this->uri(true) . '.css';
            if (file_exists(public_path($file))) {
                $this->addCss($file);
            }
            return $this->css;
        });
    }

    public function getBlade() {
        return self::cacheResult("getBlade", function() {
            $blade = str_replace('/', '.', $this->uri(true));
            if(View::exists($blade)) {
                return $blade;
            } else {
                return 'uxcrud::page';
            }
        });
    }

    public function getRawCss() {
        if (count($this->rawCss) > 0) {
            return '<style>' . implode('</style><style>', $this->rawCss) . '</style>';
        }
        return null;
    }

    public function addJS($file)
    {
        $this->js[] = $file;
        $this->js = array_unique($this->js);
        return $this;
    }

    public function addRawJS($js)
    {
        $this->rawJs[] = $js;
        return $this;
    }

    public function getJs() {
        return self::cacheResult("getJs", function() {
            $file = 'js' . $this->uri(true) . '.js';
            if (file_exists(public_path($file))) {
                $this->addJs($file);
            }
            return $this->js;
        });
    }

    public function getRawJs() {
        if (count($this->rawJs) > 0) {
            return '<script type="text/javascript">' . implode('</script><script type="text/javascript">', $this->rawJs) . '</script>';
        }
        return null;
    }

    private function allPageTranslations() {
        $pages = DB::table('page_translation')
            ->addSelect('*')
            ->addSelect('page_translation.id AS pageTranslationId')
            ->join('page', 'page_translation.page_id', '=', 'page.id')
            ->groupBy('page.id', 'page_translation.locale')
            ->orderBy('tree_order')
            ->get();

        $results = (object)[
            // Page objects by id
            'pageId' => [],
            // Translations by id.
            'id' => [],
        ];

        foreach ($pages as $page) {
            if (!isset($results->locales[$page->locale])) {
                $results->locales[$page->locale] = [];
            }
            $results->locales[$page->locale][$page->page_id] = $page;
            $results->pageTranslationId[$page->pageTranslationId] = $page;
        }
        return $results;
    }

    /**
     * Update tree order of pages.
     * Display_order is pre-padded with 0 to allow for correct sorting as alphanumeric.
     * TODO MK Remove limit of maximum path depth of 4 pages.
     */
    public static function updateOrder() {
        DB::statement("
            UPDATE `page` p, (
                SELECT DISTINCT
                    p0.id,
                    CONCAT('/',
                        TRIM(BOTH '/' FROM CONCAT(
                            IFNULL(LPAD(p3.display_order, 4, 0), ''), '/',
                            IFNULL(LPAD(p2.display_order, 4, 0), ''), '/',
                            IFNULL(LPAD(p1.display_order, 4, 0), ''), '/',
                            IFNULL(LPAD(p0.display_order, 4, 0), ''), '/'
                            )
                        )
                    ) AS tree_order
                FROM
                    (SELECT * FROM page) AS p0
                    LEFT JOIN page AS p1 ON p1.id = p0.parent_id AND p1.id <> p1.parent_id
                        LEFT JOIN page AS p2 ON p2.id = p1.parent_id AND p2.id <> p2.parent_id
                            LEFT JOIN page AS p3 ON p3.id = p2.parent_id AND p3.id <> p3.parent_id
               # WHERE p0.id = p.id
            ) AS tree_order
            SET p.tree_order = tree_order.tree_order WHERE p.id = tree_order.id AND p.id > 0
        ");
    }

    private static function parentPath($pageTranslations, $page) {
        if (empty($page->parent_id) ||
            ($page->page_id == $page->parent_id) ||
            !isset($pageTranslations->locales[$page->locale][$page->parent_id])
            ) {
            return $page->slug;
        }
        $parent = $pageTranslations->locales[$page->locale][$page->parent_id];
        $parentSlug= self::parentPath($pageTranslations, $parent);
        if ($parentSlug != "/") $parentSlug .= "/"; //$parentSlug;
        return $parentSlug . $page->slug;
    }

    /**
     * Update paths
     */
    public static function updatePaths() {
        $sql = null;
        $allPageTranslations = (new Page())->allPageTranslations();
        foreach ($allPageTranslations->locales as $locale => $pageTranslations) {
            foreach ($pageTranslations as $pageTranslation) {
                /*$pageTranslation->path = self::parentPath($allPageTranslations, $pageTranslation);
                $sql .= "UPDATE `page_translation` SET `path` = '" . self::parentPath($allPageTranslations, $pageTranslation) . "' WHERE (`id` = '{$pageTranslation->pageTranslationId}');";*/
                $pt = PageTranslation::find($pageTranslation->pageTranslationId);
                if ($pt) {
                    $pt->path = self::parentPath($allPageTranslations, $pageTranslation);
                    $pt->save();
                }
            }
        }

        //DB::select(DB::raw($sql));
// TODO MK Fix stuff
//        PageTranslation::updateOrCreate($pageTranslationPaths);

        return true;
    }
}

