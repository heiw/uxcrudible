<?php

namespace Heiw\Uxcrudible\Models;

class SnippetTranslation extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'snippet_translation';

    public $timestamps = false;
    protected $fillable = [
        'content'
    ];
}