<?php

namespace Heiw\Uxcrudible\Models;

use Astrotomic\Translatable\Translatable;
use Heiw\Uxcrudible\UxcrudModel;

class Snippet extends UxcrudModel
{
    use Translatable;

    protected $table = 'snippet';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-cut';

    public $translatedAttributes = ['content'];

    protected $fillable = ['name', 'content', 'page_id'];

    public function page() {
        return $this->belongsTo(Page::class, 'page_id');
    }

    public static function get($name, $default = null) {
        $result = Snippet::where('name', $name)->first();
        if ($result) {
            return $result->content;
        } else {
            return $default;
        }
    }
}
