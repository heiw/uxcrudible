<?php

namespace Heiw\Uxcrudible\Models;

class GdprTranslation extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'gdpr_translation';

    public $timestamps = false;
    protected $fillable = ['name', 'content'];
}