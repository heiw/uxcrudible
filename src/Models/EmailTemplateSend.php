<?php

namespace Heiw\Uxcrudible\Models;

use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Heiw\Uxcrudible\Classes\Locale;
use Heiw\Uxcrudible\Mailable\EmailTemplateMailable;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class EmailTemplateSend extends EmailTemplate
{
    //use Translatable;

    protected $table = 'email_template';
    protected static $orderBy = 'name';
    public static $icon = 'far fa-newspaper';

    public function emailTemplate() {
        return $this->belongsTo(EmailTemplate::class);
    }

    public function allEmailTemplates() {
        $results = EmailTemplateSend::orderBy(EmailTemplateSend::getOrderBy())->get();
        return $results;
    }
}

