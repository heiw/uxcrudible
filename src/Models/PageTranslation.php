<?php

namespace Heiw\Uxcrudible\Models;

class PageTranslation extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'page_translation';

    public $timestamps = false;
    protected $fillable = ['path', 'name', 'slug', 'title', 'description', 'content'];
}