<?php

namespace Heiw\Uxcrudible\Models;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class EmailLog extends UxcrudModel
{
    protected $table = 'email_log';
    public static $icon = 'fas fa-archive';

    protected $fillable = [
    ];

    protected static $orderBy = 'date';

    public function sender() : BelongsTo {
        return $this->belongsTo(User::class);
    }

    public function recipient() : BelongsTo {
        return $this->belongsTo(User::class);
    }

    public function emailTemplate() : BelongsTo {
        return $this->belongsTo(EmailTemplate::class);
    }

}