<?php

namespace Heiw\Uxcrudible\Models;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Collection;

class Role extends UxcrudModel implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'role';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-theater-masks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'icon', 'display', 'hierarchy', 'users[]', 'model_permission_role[]'
    ];

    public function modelPermissionRole() : HasMany {
        return $this->hasMany(ModelPermissionRole::class);
    }

    /**
     * @return BelongsToMany
     */
    public function users() : BelongsToMany {
        return $this->belongsToMany(User::class)->orderBy(User::getOrderBy());
    }

    /**
     * @return BelongsToMany
     */
    public function models(): BelongsToMany {
        //dd($this, $this->id);
        return $this->belongsToMany(
            Model::class,
            'model_permission_role'
        );
    }

    public function allModels() {
        return Model::orderBy(Model::getOrderBy())->get();
    }

    /**
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany {
        return $this->belongsToMany(
            Permission::class,
            'model_permission_role',
            'role_id',
            'permission_id'
        );
    }

    public function allPermissions() {
        return Permission::orderBy(Permission::getOrderBy())->get();
    }
}
