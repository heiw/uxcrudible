<?php

namespace Heiw\Uxcrudible\Models;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Str;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Model extends UxcrudModel implements
    AuthenticatableContract,
    AuthorizableContract,
    TranslatableContract

{
    use Authenticatable, Authorizable, Translatable;

    public $translatedAttributes = ['display'];

    protected $table = 'model';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-database';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'namespace'
    ];

    /**
     * Return dummy Model that is not included in the database
     * @param $fullyQualifiedName
     * @return Model
     */
    public static function databaseLessModel($fullyQualifiedName) {
        $model = new Model();
        if (strpos($fullyQualifiedName, config('uxcrud.model_namespace')) !== false) {
            $parts = explode(config('uxcrud.model_namespace'), $fullyQualifiedName);
        } else {
            $parts = explode(config('uxcrud.controller_namespace'), $fullyQualifiedName);
        }

        $model->namespace = $parts[0];
        $model->name = $parts[1];
        return $model;
    }

    public function summary() {
        return "{$this->namespace}...\\{$this->name}";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(
            Role::class,
            'model_permission_role',
            'model_id',
            'role_id'
        );
    }

    public function allRoles() {
        return Role::orderBy(Role::getOrderBy())->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(
            Permission::class,
            'model_permission_role',
            'model_id',
            'permission_id'
        );
    }

    public function allPermissions() {
        return Permission::orderBy(Permission::getOrderBy())->get();
    }

    /**
     * Return name as slug
     */
    public function slug() {
        return Str::slug($this->name);
    }

    public function icon() {
        $model = $this->getModelNamespaced();
        if (class_exists($model) && isset($model::$icon)) {
            return $model::$icon;
        } else {
            return false;
        }
    }

    public function getAjaxNamespaced($leadingSlash = true) {
        $slash = ($leadingSlash) ? '\\' : '';
        return $slash . 'ajax' . $this->getControllerNamespaced($leadingSlash);
    }

    public function getControllerNamespaced($leadingSlash = true) {
        $slash = ($leadingSlash) ? '\\' : '';
        return $slash . $this->namespace . config('uxcrud.controller_namespace') . $this->name;
    }

    public function getModelNamespaced($leadingSlash = true) {
        $slash = ($leadingSlash) ? '\\' : '';
        return $slash . $this->namespace . config('uxcrud.model_namespace') . $this->name;
    }

    public function getPolicyNamespaced($leadingSlash = true) {
        $slash = ($leadingSlash) ? '\\' : '';
        return $slash . $this->namespace . config('uxcrud.policy_namespace') . $this->name . "Policy";
    }

    public function getControllerInstance() {
        $className = $this->getControllerNamespaced();
        if (class_exists($className)) {
            return new $className;
        } else {
            return false;
        }
    }

    public function getModelInstance() {
        $className = $this->getModelNamespaced();
        if (class_exists($className)) {
            return new $className;
        } else {
            return false;
        }
    }
}
