<?php

namespace Heiw\Uxcrudible\Models;

use Astrotomic\Translatable\Translatable;
use Heiw\Uxcrudible\UxcrudModel;

class FormCreator extends UxcrudModel
{
    use Translatable;

    protected $table = 'form_creator';
    public static $icon = 'fas fa-edit';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'title'
    ];

    protected static $orderBy = 'name';
}
