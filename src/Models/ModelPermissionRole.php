<?php

namespace Heiw\Uxcrudible\Models;

//use Heiw\Uxcrudible\UxcrudModel;

class ModelPermissionRole extends \Illuminate\Database\Eloquent\Model
{
    protected $forceDeleting = true;

    protected $table = 'model_permission_role';
    protected static $orderBy = 'model_id';

    protected static $validationRules = [
        'model_id' => 'required',
        'permission_id' => 'required',
        'role_id' => 'required',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'model_id', 'permission_id', 'role_id'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model() : \Illuminate\Database\Eloquent\Relations\BelongsTo {
        return $this->belongsTo(Model::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function permission() : \Illuminate\Database\Eloquent\Relations\BelongsTo {
        return $this->belongsTo(Permission::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role() : \Illuminate\Database\Eloquent\Relations\BelongsTo {
        return $this->belongsTo(Role::class);
    }

}
