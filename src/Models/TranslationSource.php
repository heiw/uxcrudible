<?php

namespace Heiw\Uxcrudible\Models;

use Heiw\Uxcrudible\UxcrudModel;

class TranslationSource extends UxcrudModel
{
    protected $table = 'translation_source';
    public static $icon = 'fas fa-file';

    protected $fillable = [
        'name', 'use_keys'
    ];

    protected static $orderBy = 'name';

    public function summary() {
        return $this->default;
    }

    public function fileType() {
        return pathinfo($this->name, PATHINFO_EXTENSION);
    }

    public static function firstOrAdd($name, $useKeys) {
        $first = self::where('name', '=', $name)->first();
        if (!$first) {
            $first = new self();
            $first->name = $name;
            $first->use_keys = $useKeys;
            $first->save();
        }
        return $first;
    }
}