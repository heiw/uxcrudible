<?php

namespace Heiw\Uxcrudible\Models;

use Heiw\Uxcrudible\Auth\Notifications\VerifyEmail;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Heiw\Uxcrudible\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class User extends UxcrudModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    HasLocalePreference
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable, MustVerifyEmail,
        CachedObjectValue, \Lab404\Impersonate\Models\Impersonate
    ;

    protected $table = 'user';
    protected static $orderBy = 'lastname';
    public static $icon = 'fas fa-users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'firstname',
        'lastname',
        'email',
        'email_alternative',
        'password',
        'roles[]',
        'locale'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function summary() {
        return $this->title . ' ' . $this->firstname . ' ' . $this->lastname;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(Role::class)->orderBy(Role::getOrderBy());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function gdpr(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(Gdpr::class)->orderBy(Gdpr::getOrderBy());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function activeGdpr(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(Gdpr::class)->where('gdpr.active', '=', '1')->orderBy(Gdpr::getOrderBy());
    }

    public function allRoles() {
        return Role::orderBy(Role::getOrderBy())->get();
    }

    public function allRolesWithPrefix($prefix = 'uxcrud-role-') {
        $string = "";
        foreach($this->roles as $role) {
            $string .= " uxcrud-role-{$role->name}";
        }
        return $string;
    }

    /**
     * Get the user's preferred locale.
     *
     * @return string
     */
    public function preferredLocale()
    {
        if (is_null($this->locale)) {
            // If no locale set return app locale.
            return App::getLocale();
        } else {
            return $this->locale;
        }
    }

    /**
     * @param string|array $roles
     * @return bool
     */
    public function authorizeRoles($roles) {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, __('This action is unauthorized.'));
        }
        return $this->hasRole($roles) ||
            abort(401, __('This action is unauthorized.'));
    }

    /**
     * Check multiple roles
     * @param array $roles
     * @return bool
     */
    public function hasAnyRole($roles) {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role
     * @param string $role
     * @return bool
     */
    public function hasRole($role) {
        return null !== $this->roles()->where('name', $role)->first();
    }

    /**
     * @param string $permissionName
     * @param  string|UxcrudModel  $model
     * @return bool
     */
    public function hasModelPermission(string $permissionName, $model = null) {
        // TODO MK, remove guess work based on URL to determine model for create and index.
        // Investigate overriding core Gate / Policy classes instead.
        if (is_null($model)) {
            $modelName = explode("/", Route::getFacadeRoot()->current()->uri());
            if (last($modelName) == Str::slug(__('uxcrud::slugs.create'))) {
                array_pop($modelName);
            }
            $modelName = Str::camel(last($modelName));
            $model = Model::where('name', $modelName)
                //->orWhere('display', $modelName)
                ->firstOrFail();
            $model = $model->getModelNamespaced(false);

        } elseif(!is_string($model)) {
            $model = get_class($model);
        }

        $modelPermissions = $this->modelPermissions();
        $permissionExists =
            $model::$databaseLessModel ||
            (isset($modelPermissions[$model]) && (isset($modelPermissions[$model][$permissionName])));
        return $permissionExists;
    }

    /**
     * Get all model permissions for current user
     */
    protected function modelPermissions() {
        return self::cacheResult("modelPermissions", function() {
            $results = [];
            $permissions = DB::select("
                SELECT DISTINCT
                    CONCAT(model.`namespace`, 'Models\\\\', model.name) AS model,
                    GROUP_CONCAT(DISTINCT permission.name) AS permissions
                FROM
                    model_permission_role
                        INNER JOIN permission ON permission.id = model_permission_role.permission_id
                        INNER JOIN model ON model.id = model_permission_role.model_id
                        INNER JOIN role_user ON role_user.role_id = model_permission_role.role_id
                WHERE
                    role_user.user_id = :user_id
                GROUP BY model.id, model.`namespace`, model.name
            ", ['user_id' => auth()->user()->id]);

            foreach($permissions as $permission) {
                $keys = explode(',', $permission->permissions);
                $results[$permission->model] = array_fill_keys($keys, true);
            }
            return $results;
        });
    }

    public function outrankedByCurrentUser() {
        $targetMinHierarchy = $this->roles()->pluck('hierarchy')->min();
        $userMinHierarchy = auth()->user()->roles()->pluck('hierarchy')->min();
        return $targetMinHierarchy >= $userMinHierarchy;
    }

    /**
     * Determine whether the user can be impersonated by the current user.
     * @return bool
     */
    public function canBeImpersonated() {
        if (app('impersonate')->isImpersonating()) {
            return false;
        }
        // It's not possible to impersonate a deleted user.
        if ($this->trashed()) {
            return false;
        }
        return $this->outrankedByCurrentUser();
    }

    public function canBeDeleted() {
        return $this->outrankedByCurrentUser();
    }

    public function canEditRoles() {
        return auth()->user()->hasAnyRole(['admin', 'sys_admin']);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    /**
     * Override to customise fields based on $model class.
     * @param int $view
     * @param $fields
     * @param $model
     * @return mixed
     */
    public function auditFields($view, $fields, $model) {
        if (isset($model->translatedAttributes)) {
            $isTranslator = auth()->user()->hasRole('translator');
            if ($isTranslator) {
                foreach ($fields as $field) {
                    if (!in_array($field->name, $model->translatedAttributes)) {
                        $field->readOnly(true);
                    }
                    if (isset($field->fields)) {
                        $field->fields = $this->auditFields($view, $field->fields, $model);
                    }
                }
            }
        }
        return $fields;
    }
}
