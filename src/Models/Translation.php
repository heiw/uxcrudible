<?php

namespace Heiw\Uxcrudible\Models;

use Heiw\Uxcrudible\UxcrudModel;

class Translation extends UxcrudModel
{
    protected $table = 'translation';
    public static $icon = 'fas fa-language';

    protected $fillable = [
        'source',
        'original',
        'translation',
        'locale'
    ];

    protected static $orderBy = 'original';

    public function summary() {
        return $this->default;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function translationSources(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(TranslationSource::class)
            ->withPivot('key')
            ->orderBy(TranslationSource::getOrderBy());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function translation_sources(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->translationSources();
    }

}