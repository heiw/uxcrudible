<?php

namespace Heiw\Uxcrudible\Services;

use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\Models\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Illuminate\Support\Str;
use PHPUnit\TextUI\Help;

class Menu
{
    use CachedObjectValue;

    /**
     * Get page index by uri and page id for current user
     */
    private function pageIndex() {
        return self::cacheResult("pages_" . Auth::id(), function () {
            $results = (object)[
                // Page objects by id
                'id' => [],
                // Pages indexed by uri.
                'uri' => [],
                // Uris indexed by page id.
                'uriById' => [],
                //
                'parentOf' => []
            ];

            if (Auth::check()) {
                $pages = DB::table('page')
                    ->leftJoin('page_role', 'page_role.page_id', '=', 'page.id')
                    ->leftJoin('role_user', 'role_user.role_id', '=', 'page_role.role_id')
                    // Restrict to public pages
                    ->where('public', '=', Page::ACCESS_PUBLIC)
                    // Or private pages where user has access through role
                    ->orWhere(function($query) {
                        $query
                            ->where('public', '=', Page::ACCESS_PRIVATE_ONLY)
                            ->where('role_user.user_id', '=', Auth::id());
                    })
                    // Ignore deleted pages
                    ->whereNull('page.deleted_at')
                    ->groupBy('page.id')
                    ->orderBy('tree_order');
                $pages = $pages
                    ->get();
            } else {
                $pages = DB::table('page')
                    // Page needs the be accessible by public or public only
                    ->where('public', '>=', Page::ACCESS_PUBLIC)
                    ->groupBy('page.id')
                    ->orderBy('tree_order')
                    ->whereNull('page.deleted_at')
                    ->get();
            }

            $pages = Page::hydrate($pages->all());
            foreach ($pages as $page) {
                $uri = $page->path;
                if ($page->modelName) {
                    $uri .= '/' . Str::snake($page->modelName);
                }
                $results->uri[$uri] = $page->id;
                $results->id[$page->id] = $page;
                $results->uriById[$page->id] = $uri;
                $results->parentOf[$page->parent_id][] = $page->id;
            }
            return $results;
        });
    }

    /**
     * @param string|int $uriOrId
     * @return bool|Page
     */
    public function page($uriOrId) {
        if (is_int($uriOrId)) {
            if (isset($this->pageIndex()->id[$uriOrId])) {
                return $this->pageIndex()->id[$uriOrId];
            } else {
                return false;
            }
        } else {
            // Make sure uri starts with leading /.
            if ($uriOrId !== '/') {
                $uriOrId = '/' . $uriOrId;
            }
            if (isset($this->pageIndex()->uri[$uriOrId])) {
                return $this->page($this->pageIndex()->uri[$uriOrId]);
            } else {
                return false;
            }
        }
    }

    public function pages() {
        return $this->pageIndex()->id;
    }

    public function currentPage() {
        // TODO MK strip action from route.
        $uri = request()->path();
        return $this->page($uri);
    }

    public function rootPage() {
        $result = $this->page('/');
        return $result;
    }

    public function uri($id) {
        if (isset($this->pageIndex()->uriById[$id])) {
            return $this->pageIndex()->uriById[$id];
        } else {
            return false;
        }
    }

    /**
     * Retrieve a list of bread crumbs (parents) in an array.
     * @return array|bool[]|Page[]
     */
    public function breadCrumbs() {
        $crumbs = [];
        $page = $this->currentPage();
        if ($page) {
            $crumbs = [$page];
            while ($page && $page->parent_id && $page->parent_id != $page->id) {
                $page = $this->page($page->parent_id);
                // Add parent page at front of crumbs if it exists.
                if ($page) {
                    array_unshift($crumbs, $page);
                }
            }
        }
        return $crumbs;
    }

//    protected function showSubMenu($page) {
//        $html = "";
////        dd($this->pageIndex(), $page);
//  //      return;
//        foreach ($this->children($page) as $child) {
//            $html .= "
//                <li>
//                    <a class='waves-effect waves-dark' href='/'>
//                        <i class='fas fa-shapes'></i>
//                        <span class='hide-menu'>$child->name</span>
//                    </a>
//                    ";
//            dd($child);
//            $html .= $this->children($child);
//            $html .="</li>";
//        }
//        return $html;
//    }
//
    /**
     * @param Page $page
     * @return array
     */
    public function children($page) {
        $children = [];
        if (isset($page->id) && isset($this->pageIndex()->parentOf[$page->id])) {
            foreach ($this->pageIndex()->parentOf[$page->id] as $childId) {
                $child = $this->page($childId);
                // append path to tree_order for cases where display_order is not unique.
                $children[$child->tree_order . $child->path] = $child;
            }
        }
        ksort($children);
        return $children;
    }

    public function isActive($page) {
        return Str::startsWith(request()->path(), $page->route());
    }

    public function show() {
        $page = $this->rootPage();
        $html = $this->showSubMenu($page);
        return $html;
    }

    /**
     * Get all page permissions for current user
     */
    protected function pagePermissions() {
        return self::cacheResult("pagePermissions_" . auth()->user()->id, function () {
            $results = [];
            $permissions = DB::select("
                SELECT DISTINCT

                FROM
                    model_permission_role
                        INNER JOIN permission ON permission.id = model_permission_role.permission_id
                        INNER JOIN model ON model.id = model_permission_role.model_id
                        INNER JOIN role_user ON role_user.role_id = model_permission_role.role_id
                WHERE
                    role_user.user_id = :user_id
                GROUP BY model.id, model.`namespace`, model.name
            ", ['user_id' => auth()->user()->id]);

            foreach ($permissions as $permission) {
                $keys = explode(',', $permission->permissions);
                $results[$permission->model] = array_fill_keys($keys, true);
            }
            return $results;
        });
    }

//    /**
//     * Cached git version.
//     * @param $type
//     * @return mixed
//     */
//    public function gitVersionParameter($type = Helpers::COMMIT_HASH_SHORT) {
//        return self::cacheResult("gitVersion" . $type, function() use ($type) {
//            $helper = new Helpers();
//            return '?v=' . $helper->gitVersion($type);
//        });
//    }

}
