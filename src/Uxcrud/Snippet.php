<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\CKEditor;
use Heiw\Uxcrudible\Form\Fields\HasOne;
use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;

class Snippet extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\Snippet::class;

    public function fields($model = null)
    {
        $fields = [
            HasOne::create('page', __('Page'))
                //->icon(\Heiw\Uxcrudible\Models\Page::$icon)
                ->readOnlyOnEdit(Helpers::userHasRole('sys_admin'))
                ->validation('required')
                ->setModel(\Heiw\Uxcrudible\Models\Page::class)
            ,
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->readOnlyOnEdit(Helpers::userHasRole('sys_admin'))
                ->validation('unique:snippet,name,$id|required')
            ,
            CKEditor::create('content', __('Content'))
                ->icon('fas fa-cut')
                ->validation('required')
            ,
            Actions::create()
        ];
        return $fields;
    }
}
