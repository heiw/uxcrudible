<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Classes\JsonToast;
use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\BelongsToMany;
use Heiw\Uxcrudible\Form\Fields\Checkbox;
use Heiw\Uxcrudible\Form\Fields\CKEditor;
use Heiw\Uxcrudible\Form\Fields\DataTable;
use Heiw\Uxcrudible\Form\Fields\DateTime;
use Heiw\Uxcrudible\Form\Fields\Enum;
use Heiw\Uxcrudible\Form\Fields\HasMany;
use Heiw\Uxcrudible\Form\Fields\HasManyPivot;
use Heiw\Uxcrudible\Form\Fields\HasOne;
use Heiw\Uxcrudible\Form\Fields\Icon;
use Heiw\Uxcrudible\Form\Fields\Number;
use Heiw\Uxcrudible\Form\Fields\Pages;
use Heiw\Uxcrudible\Form\Fields\ReorderPivot;
use Heiw\Uxcrudible\Form\Fields\ReorderRow;
use Heiw\Uxcrudible\Form\Fields\TextArea;
use Heiw\Uxcrudible\Seeds\PageTableSeeder;
use Heiw\Uxcrudible\Services\Menu;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;
use Illuminate\Support\Facades\DB;

class Page extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\Page::class;

    public static $rowReorder = 'display_order';

/*
    public function dataTableOptions() {
        parent::dataTableOptions();
        $this->dataTableOptions->rowReorder('display_order');
        $this->dataTableOptions->rowReorder(true);
    }
*/

    public function fields($model = null)
    {
        $models = new Model();
        $fields = [
            Pages::create()->setFields([
                \Heiw\Uxcrudible\Form\Fields\Page::create('details', __('Page'))->icon('fas fa-bookmark')->setFields([
                    ReorderRow::create('display_order', __('Display order'))
                        ->icon('fas fa-sort-numeric-down')
                        ->validation('required')
                    ,
                    Icon::create('icon', __('Icon'))
                        ->icon('fas fa-info-circle')
                    ,
                    Text::create('name', __('Name'))
                        ->addClass('name')
                        ->icon('fas fa-tag')
                        ->validation('required')
                    ,
                    Text::create('slug', __('Slug'))
                        ->addClass('slug')
                        ->icon('fas fa-link')
                        ->validation('required')
                    ,
                    Text::create('title', __('Title'))
                        ->icon('fas fa-bullhorn')
                    ,
                    TextArea::create('description', __('Description'))
                        ->icon('fas fa-asterisk')
                    ,
                    CKEditor::create('content', __('Content'))
                        ->icon('fas fa-align-left')
                        ->rows(10)
                    ,
                    HasOne::create('parent', __('Parent'))
                        ->icon('fas fa-sitemap')
                        ->validation('nullable')
                        ->setModel(\Heiw\Uxcrudible\Models\Page::class)
                        ->defaultFilterValue(PageTableSeeder::HOME_ID)
                    ,
                    BelongsToMany::create('children', __('Children'))
                        ->icon('far fa-clone fa-flip-horizontal')
                        ->setMethod('children')
                        ->setModel(\Heiw\Uxcrudible\Models\Page::class)
                        ->hideOnCreate()
                        ->hideOnEdit()
                    ,
                    HasMany::create('roles', __('Roles'))
                        ->icon('fas fa-theater-masks')
                        ->labelName('display')
                        ->setModel(\Heiw\Uxcrudible\Models\Role::class)
                        ->addPrependOption('', '-')
                    ,
                    Enum::create('public', __('Public'))
                        ->setEnumOptions(
                            [\Heiw\Uxcrudible\Models\Page::ACCESS_PRIVATE_ONLY => __('Private only'),
                            \Heiw\Uxcrudible\Models\Page::ACCESS_PUBLIC_ONLY => __('Public only'),
                            \Heiw\Uxcrudible\Models\Page::ACCESS_PUBLIC=> __('Public')])
                        ->validation('required')
                        ->icon('fas fa-globe')
                    ,
                    HasManyPivot::create('models', __('Models'))
                        ->pivotFields(
                                [
                                    ReorderRow::create('pivot_order', __('Order'))
                                        ->hideOnEdit()
                                    ,
                                ]
                        )
                        ->icon('fas fa-database')
                        ->setModel(\Heiw\Uxcrudible\Models\Model::class)
                        ->noFilter()
                        ->noSearch()                    ,
                ]),
                \Heiw\Uxcrudible\Form\Fields\Page::create('snippets', __('Snippets'))->icon('fas fa-cut')->setFields([
                    DataTable::create('snippets', __('Snippets'))
                        ->icon('fas fa-cut')
                        ->setModel(\Heiw\Uxcrudible\Models\Snippet::class)
                    //->hideOnIndex()
                    ,
                ]),
            ]),

            Actions::create()

        ];
        return $fields;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store() {
        $result = parent::store();
        \Heiw\Uxcrudible\Models\Page::updatePaths();
        \Heiw\Uxcrudible\Models\Page::updateOrder();
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update($id) {
        $result = parent::update($id);
        \Heiw\Uxcrudible\Models\Page::updatePaths();
        \Heiw\Uxcrudible\Models\Page::updateOrder();
        return $result;
    }

    public function page($uri = null) {
        dd(request());
        $menu = resolve(Menu::class);
        $currentPage = $menu->currentPage();
        abort_unless($currentPage, 404, __('Page not found'));
        return view('uxcrud::page', compact(['currentPage']));
    }

    /**
     * Update the order of the resources in storage.
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function rowReorder() {
        $result = parent::rowReorder();
        \Heiw\Uxcrudible\Models\Page::updatePaths();
        \Heiw\Uxcrudible\Models\Page::updateOrder();

        return $result;
    }
}
