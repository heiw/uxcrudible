<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\Form\Fields\TextArea;
use Heiw\Uxcrudible\UxcrudController;

class FormCreator extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\FormCreator::class;
    public function fields($model = null) {
        $fields = [
            TextArea::create('form', __('Form definition'))
            ,
            Actions::create()
        ];
        return $fields;
    }
}
