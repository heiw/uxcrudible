<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Classes\JsonToast;
use Heiw\Uxcrudible\Form\Fields\Action;
use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\BelongsToMany;
use Heiw\Uxcrudible\Form\Fields\Password;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class UnlockSession extends UxcrudController
{
    use AuthenticatesUsers, Authenticatable;

    public static $model = \Heiw\Uxcrudible\Models\User::class;

    public static function hasSessionTimeout() {
       return config('uxcrud.session_timeout_dialog') > 0;
    }

    public static function getTimeout() {
       return config('uxcrud.session_timeout_dialog');
    }

    public static function getDialogShowtime() {
       return config('uxcrud.session_timeout_dialog_showtime');
    }

    /**
     * UnlockSession constructor.
     * @param Guard $auth
     */
    public function __construct(Guard $auth) {
        //parent::__construct();
        // Allow anonymous access.
//        $this->middleware('auth', ['except' => 'showUnlockForm']);
//        $this->middleware('auth:web');
        $this->closeOnBackground(static::$closeOnBackground);
    }

    public function fields($model = null)
    {
        $fields = [
            Text::create('email', __('Email'))
                ->icon('fas fa-at')
                ->readOnly(auth()->user())
            ,
            Password::create('password', __('Password'))
                ->icon('fas fa-user-secret')
                ->validation('email|unique:user,email,$id|required')
            ,
            Actions::create()
                ->add(Action::create('unlock', 'uxcrud::actions.unlock'))

        ];
        return $fields;
    }


    /**
     * Return action with label, button, permission and isUpdate record.
     * @param $action
     * @return object
     */
    public function getActionInfo($action) {
        $ability = parent::getActionInfo($action);
        $ability->isUpdate = false;
        $ability->label = 'unlock';
        return $ability;
    }

    /**
     * Show the unlock form
     * @param Request $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function showUnlockForm(Request $request) {
        $user = Auth::user();
        if ($user) {
            if (app('impersonate')->isImpersonating()) {
                // Show login for impersonator instead of impersonatee.
                $user = app('impersonate')->getImpersonator();
            }
            $modal = $this->getFormModal($user, self::ABILITY_UPDATE);
        } else {
            $model = self::getModel();
            $modal = $this->getFormModal($model, self::ABILITY_CREATE);
        }
        return $this->setUnlockModal($modal->jsonModal());
    }

    protected function setUnlockModal($modal) {
        $modal->id = 'lock-screen';
        $modal->form->action = '/' . Str::slug(__('uxcrud::slugs.unlock'));
        $modal->form->{"data-on-success"}="SessionTimeoutOnSuccessUnlockSession";
        $modal->closeButton = false;
        $modal->class = 'uxcrud-edit-modal uxcrud-update privacy-backdrop';
        $modal->colour = 'info';
        $modal->header = __('Your :icon :site session has been locked', [
            'icon' => "<i class='" . \Heiw\Uxcrudible\Models\SiteConfig::get('app_icon') . "'></i>",
            'site' => \Heiw\Uxcrudible\Models\SiteConfig::get('app_abbr')
        ]);
        $modal->buttons = [
            '<button id="login-session" type="submit" class="ajax-action-uxcrud-submit btn btn-info"><i class="fas fa-sign-in-alt"></i> Unlock</button>',
        ];
        return response()->json($modal);
    }

    public static function getLabel($plural = 1) {
        switch(abs($plural)) {
            case 1:
                // TODO MK Check whether this needs to be translated.
                return 'Session';
                break;
            default:
                return 'Sessions';
                break;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|JsonResponse|\Illuminate\Http\Response
     */
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function unlock(Request $request) {
        try {
            $response = $this->login($request);
        } catch(ValidationException $ve) {
            return response()->json([
                    'message' => $ve->getMessage(),
                    'errors' => [
                        'password' => [$ve->getMessage()],
                    ],
                ]
            , $ve->status);
        }

        return response()->json(
            JsonToast::create()
                ->status('success')
            ->data([
                'csrf' => csrf_token(),
                'dismiss' => 'modal'
            ] )
        );
    }

    public function csrfToken() {
        return response()->json(
            JsonToast::create()
                ->status('success')
                ->data([
                    'csrf' => csrf_token(),
                    'dismiss' => 'modal'
                ] )
        );
    }

    /**
     * Extend current session.
     * @param Request $request
     * @return JsonResponse
     */
    public function extendSession(Request $request) {
        return response()->json(
            JsonToast::create()
                ->status('success')
                ->body(__('Session extended'))
                ->data([
                    'csrf' => csrf_token(),
                ])
        );
    }

}
