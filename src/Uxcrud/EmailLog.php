<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\Checkbox;
use Heiw\Uxcrudible\Form\Fields\CKEditor;
use Heiw\Uxcrudible\Form\Fields\DateTime;
use Heiw\Uxcrudible\Form\Fields\HasOne;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\Form\Fields\TextArea;
use Heiw\Uxcrudible\UxcrudController;

class EmailLog extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\EmailLog::class;
    public function fields($model = null) {
        $fields = [
            DateTime::create('date', __('Date'))
                ->icon('fas fa-calendar')
                ->defaultSort('desc')
            ,
            HasOne::create('sender', __('Sender'))
                ->icon('fas fa-sign-out-alt')
                ->labelName('summary()')
                ->setModel(\Heiw\Uxcrudible\Models\User::class)
                ->validation('nullable')
            ,
            HasOne::create('recipient', __('Recipient'))
                ->icon('fas fa-sign-in-alt')
                ->labelName('summary()')
                ->setModel(\Heiw\Uxcrudible\Models\User::class)
                ->validation('nullable')
            ,
            HasOne::create('email_template', __('Email Template'))
                ->icon(\Heiw\Uxcrudible\Models\EmailTemplate::$icon)
                ->labelName('summary()')
                ->setModel(\Heiw\Uxcrudible\Models\EmailTemplate::class)
                ->validation('nullable')
            ,
            Checkbox::create('success', __('Success'))
                ->icon('fas fa-check')
            ,
            Text::create('addresses', __('Addresses'))
                ->icon('fas fa-at')
            ,
            Text::create('errors', __('Errors'))
                ->icon('fas fa-exclamation-triangle')
            ,
            CKEditor::create('content', __('Content'))
                ->icon('fas fa-align-left')
            ,
            Actions::create()
        ];
        return $fields;
    }
}
