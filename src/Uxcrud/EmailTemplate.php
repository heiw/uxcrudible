<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\Checkbox;
use Heiw\Uxcrudible\Form\Fields\CKEditor;
use Heiw\Uxcrudible\Form\Fields\Enum;
use Heiw\Uxcrudible\Form\Fields\Files;
use Heiw\Uxcrudible\Form\Fields\Label;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\UxcrudController;

class EmailTemplate extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\EmailTemplate::class;
    public function fields($model = null) {
        $fields = [

            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->readOnlyOnEdit(auth()->user() && !auth()->user()->hasRole('sys_admin'))
                //->readOnlyOnEdit()
                ->validation('unique:email_template,name,$id|required')
            ,
            Text::create('subject', __('Subject'))
                ->icon('fas fa-bullhorn')
            ,
            CKEditor::create('content', __('Content'))
                ->icon('fas fa-align-left')
                ->rows(10)
                ->infoButton(__('Include :email_template_short_keys to see which email template short keys are available',
                    ['email_template_short_keys' => ':email_template_short_keys']), 'Short keys')
            ,
            Checkbox::create('include_snippet_signature', __('Include snippet signature'))
                ->icon('fas fa-file-signature')
            ,
//            Files::create('files', __('Attachments'))
//                ->icon('fas fa-copy')
//            ,

            Enum::create('priority', __('Priority'))
                ->icon('fas fa-exclamation-circle')
                ->setEnumOptions([1 => __('High'), 3 => __('Normal'), 5 => __('Low')])
            ,

            Actions::create()
        ];
        return $fields;
    }
}
