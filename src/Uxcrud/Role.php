<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Models\ModelPermissionRole;
use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\BelongsToMany;
use Heiw\Uxcrudible\Form\Fields\Icon;
use Heiw\Uxcrudible\Form\Fields\Matrix;
use Heiw\Uxcrudible\Form\Fields\Number;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;

class Role extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\Role::class;

    public function fields($model = null)
    {
        $fields = [
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->validation('required')
            ,
            Text::create('description', __('Description'))
                ->icon('fas fa-asterisk')
                ->validation('nullable')
            ,
            Text::create('display', __('Display'))
                ->icon('fas fa-tv')
                ->validation('nullable')
            ,
            Number::create('hierarchy', __('Hierarchy'))
                ->icon('fas fa-sitemap')
                ->validation('nullable')
            ,
            Icon::create('icon', __('Icon'))
                ->icon('fas fa-info-circle')
                ->validation('nullable')
            ,
            // TODO MK Replace with datatable with checkboxes?
//            BelongsToMany::create('users', __('Users'))
//                ->icon('fas fa-users')
//                ->labelName('summary()')
//                ->setModel(\Heiw\Uxcrudible\Models\User::class)
//                ->hideOnIndex()
//                ->validation('nullable')
//            ,
            Matrix::create('model_permission_role', __('Permissions for roles'))
                ->icon('fas fa-shield-alt')
                ->setModels(
                    \Heiw\Uxcrudible\Models\Role::class,
                    \Heiw\Uxcrudible\Models\Permission::class,
                    \Heiw\Uxcrudible\Models\Model::class,
                    ModelPermissionRole::class)
                //->setOptions('all')
                ->setQuickSelect()
                ->noSorting()
                ->noSearch()
                ->hideOnIndex()
                ->validation('nullable')
            ,
            Actions::create()
        ];
        return $fields;
    }
}
