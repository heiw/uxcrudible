<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\BelongsToMany;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;

class Permission extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\Permission::class;

    public function fields($model = null)
    {
        $fields = [
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->validation('required')
            ,
            Text::create('display', __('Display'))
                ->icon('fas fa-tv')
                ->validation('required')
            ,
            Actions::create()

        ];
        return $fields;
    }
}
