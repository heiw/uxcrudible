<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\BelongsToMany;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;

class Model extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\Model::class;

    public function fields($model = null)
    {
        $fields = [
            Text::create('namespace', __('Namespace'))
                ->icon('fas fa-shapes')
                ->validation('required')
            ,
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->validation('required')
            ,
            Text::create('display', __('Display'))
                ->icon('fas fa-dragon')
                ->validation('required')
            ,
            Actions::create()
        ];
        return $fields;
    }
}
