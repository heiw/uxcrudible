<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\Checkbox;
use Heiw\Uxcrudible\Form\Fields\CKEditor;
use Heiw\Uxcrudible\Form\Fields\Enum;
use Heiw\Uxcrudible\Form\Fields\Files;
use Heiw\Uxcrudible\Form\Fields\Group;
use Heiw\Uxcrudible\Form\Fields\HasOne;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\UxcrudController;

class EmailTemplateSend extends EmailTemplate
{
    const ABILITY_SEND_EMAIL = 'send-email';

    public static $model = \Heiw\Uxcrudible\Models\EmailTemplateSend::class;
    public function fields($model = null) {
        if (!is_null($model)) {
            // Enable sent report by default when form is shown.
            if (is_null($model->sent_report)) {
                $model->sent_report = true;
            }
        }

        $emailTemplateFields = parent::fields($model);

        // Remove the name field.
        $key = array_search('name', array_column($emailTemplateFields, 'name'));
        unset($emailTemplateFields[$key]);

        $fields = [
            HasOne::create('email_template', __('Email template'))
                ->icon(\Heiw\Uxcrudible\Models\EmailTemplateSend::$icon)
                ->validation('required')
                ->setMethod('emailTemplates')
                ->setModel(\Heiw\Uxcrudible\Models\EmailTemplateSend::class)
                ->setEmptyOption()
                // TODO MK  Get id from configuration or DB?!?
                ->dataShowDetail(config('uxcrud.email_template_free_template_id'), 'free_template')
            ,
            Checkbox::create('sent_report', __('Email sent report'))
            ,
            Group::create('free_template')
                ->setFields(
                    $emailTemplateFields
                )
        ];

        return $fields;
    }

    public function getActionInfo($action) {
        if ($action == self::ABILITY_SEND_EMAIL) {
            $actionInfo = parent::getActionInfo(self::ABILITY_CREATE);
            $actionInfo->label = __('Send email');
            $actionInfo->button = __('Send email');
        } else {
            $actionInfo = parent::getActionInfo($action);
        }
        return $actionInfo;
    }


}
