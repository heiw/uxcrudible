<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Classes\JsonToast;
use Heiw\Uxcrudible\Form\Fields\Action;
use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\BelongsToMany;
use Heiw\Uxcrudible\Form\Fields\Checkbox;
use Heiw\Uxcrudible\Form\Fields\Group;
use Heiw\Uxcrudible\Form\Fields\Password;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePassword extends User
{
    public static $model = \Heiw\Uxcrudible\Models\User::class;

    public function fields($model = null)
    {
        $fields = [
            Password::create('password', __('Current password'))
                ->icon('fas fa-user-secret')
                ->validation(['required', function ($attribute, $value, $fail) {
                    if (!Hash::check($value, Auth::user()->password)) {
                        return $fail(__('The current password is incorrect.'));
                    }
                }])
                ->noSearch()
            ,
            Password::create('new_password', __('New password'))
                ->icon('fas fa-asterisk')
                ->validation('required|string|min:8|confirmed')
                ->noSearch()
            ,
            Password::create('new_password_confirmation', __('Repeat new password'))
                ->icon('fas fa-equals')
                ->validation('required')
                ->noSearch()
            ,

            Actions::create()
        ];
        return $fields;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function edit($id) {
        $model = self::getModel($id);

        $modal = $this->getFormModal($model, self::ABILITY_UPDATE);
        $modal->header = __('Change password for :username', ['username' => auth()->user()->summary()]);
        return response()->json($modal->jsonModal());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update($id) {
        $model = self::getModel($id);

        $request = request();
        $messages = [];
        $rules = $this->getValidationRules(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, $model, $request, $messages);
        $attributes = $request->validate($rules, $messages);
        $original = clone($model);
        //$attributes
        $this->fireFormFieldBeforeEvent(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, $model, $attributes, $rules, $messages);
        $attributes['password'] = Hash::make($attributes['new_password']);
        $model->update($attributes);
        $this->fireFormFieldAfterEvent(\Heiw\Uxcrudible\Form\Fields\Permission::UPDATE, $original, $model);

        return response()->json(
            JsonToast::create()
                ->status('success')
                ->header(__('Your password has been updated'))
                ->body('')
        );
    }
}
