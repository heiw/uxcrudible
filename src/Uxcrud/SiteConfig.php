<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;

class SiteConfig extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\SiteConfig::class;

    public function fields($model = null)
    {
        $fields = [
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->readOnlyOnEdit(auth()->user() && !auth()->user()->hasRole('sys_admin'))
                ->validation('unique:site_config,name,$id|required')
            ,
            Text::create('value', __('Value'))
                ->icon('far fa-gem')
                ->validation('required')
            ,
            Text::create('description', __('Description'))
                ->icon('fas fa-asterisk')
            ,
            Actions::create()
        ];
        return $fields;
    }
}
