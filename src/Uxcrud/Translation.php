<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\BelongsToMany;
use Heiw\Uxcrudible\Form\Fields\Enum;
use Heiw\Uxcrudible\Form\Fields\HasMany;
use Heiw\Uxcrudible\Form\Fields\ReadOnly;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\Form\Fields\TextArea;
use Heiw\Uxcrudible\Models\TranslationSource;
use Heiw\Uxcrudible\UxcrudController;

class Translation extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\Translation::class;
    public function fields($model = null) {
        $fields = [
            BelongsToMany::create('translationSources', __('Source'))
                ->setModel(TranslationSource::class)
                ->noSearch(true)
                //->setName('translation_sources')
            ,
            TextArea::create('original', __('Original'))
                ->readOnly()
            ,
            TextArea::create('translation', __('Translation'))
            ,
            Enum::create('locale', __('Locale'))
                ->readOnly()
            ,
            Actions::create()
        ];
        return $fields;
    }
}
