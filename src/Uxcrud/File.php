<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\BelongsToMany;
use Heiw\Uxcrudible\Form\Fields\Checkbox;
use Heiw\Uxcrudible\Form\Fields\FileDownload;
use Heiw\Uxcrudible\Form\Fields\HasMany;
use Heiw\Uxcrudible\Form\Fields\HasOne;
use Heiw\Uxcrudible\Form\Fields\Icon;
use Heiw\Uxcrudible\Form\Fields\TextArea;
use Heiw\Uxcrudible\Traits\DataTableController;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class File extends UxcrudController
{
    use DataTableController;

    public static $model = \Heiw\Uxcrudible\Models\File::class;

    public function fields($model = null)
    {
        $fields = [
            FileDownload::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->validation('required')
            ,
//            Text::create('uri')
//                ->icon('fas fa-link')
//                ->validation('required')
//            ,
//            Checkbox::create('is_folder')
//                ->icon('fas fa-folder-open')
//            ,
//            HasOne::create('parent')
//                ->icon('fas fa-folder')
//                ->nullable()
//                ->setModel(\Heiw\Uxcrudible\Models\File::class)
//            ,
            Actions::create()
        ];
        return $fields;
    }

    /**
     * Download route for the file
     * @return string
     */
    public static function getDownloadRoute() {
        return '/' . Str::slug(__('uxcrud::slugs.download'));
    }

    /**
     * Add route for download of file
     * @param Model $model
     */
    public static function addRoutes(\Heiw\Uxcrudible\Models\Model $model) {
        // Force delete route of previously soft deleted item
        Route::get(
            static::getDownloadRoute() . '/{' . Str::kebab(class_basename(static::class)) . '}',
            $model->getControllerNamespaced(false) . '@downloadFile'
        )->middleware(['web', 'auth']);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function downloadFile($id) {
        $model = self::getModel($id);
        $entry = $model;
        $this->authorize('downloadFile', $entry);
        $this->authorize('read', $entry);

        // Using Storage::download gives an error on Azure. 502 - Web server received an invalid response while acting as a gateway or proxy server.
        //There is a problem with the page you are looking for, and it cannot be displayed. When the Web server (while acting as a gateway or proxy) contacted the upstream content server, it received an invalid response from the content server.
        // $result = Storage::download($model->store, $model->name);

        // Working replacement code:
        $result = Storage::get($model->store, $model->name);

        $response = Response($result, 200, [
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="' . $model->name . '"',
        ]);

        return $response;
    }

    public function dataTableOptions() {
        parent::dataTableOptions();
        //$this->dataTableOptions->noExports();
        return $this->dataTableOptions;
    }

    public function fireFormFieldBeforeEvent($ability, $model, &$attributes, &$rules, &$messages) {
        parent::fireFormFieldBeforeEvent($ability, $model, $attributes, $rules,
            $messages);
        if ($ability === Permission::ABILITY_CREATE) {
            $attributes['uploaded_by_id'] = auth()->user()->id;
        } else {
            unset($attributes['uploaded_by_id']);
        }
    }
}
