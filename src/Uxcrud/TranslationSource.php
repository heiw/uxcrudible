<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\Enum;
use Heiw\Uxcrudible\Form\Fields\ReadOnly;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\Form\Fields\TextArea;
use Heiw\Uxcrudible\UxcrudController;

class TranslationSource extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\Translation::class;
    public function fields($model = null) {
        $fields = [
            Text::create('name', __('Name'))
                ->readOnly()
            ,
            Actions::create()
        ];
        return $fields;
    }
}
