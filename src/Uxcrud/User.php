<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Classes\DataTableButton;
use Heiw\Uxcrudible\Classes\JsonToast;
use Heiw\Uxcrudible\Form\Fields\Action;
use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\BelongsToMany;
use Heiw\Uxcrudible\Form\Fields\Checkbox;
use Heiw\Uxcrudible\Form\Fields\Enum;
use Heiw\Uxcrudible\Form\Fields\Group;
use Heiw\Uxcrudible\Form\Fields\Password;
use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\Models\EmailTemplate;
use Heiw\Uxcrudible\Traits\DataTableSendEmailTemplateUxcrud;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class User extends UxcrudController
{
    use DataTableSendEmailTemplateUxcrud;

    public static $model = \Heiw\Uxcrudible\Models\User::class;

    public static $freeTextEmailTemplate = true;

    public function fields($model = null)
    {
        // Create array with locales are keys and locale name as values for Enum.
        $languages = array_combine(array_keys(config('uxcrud.locales')), array_column(config('uxcrud.locales'), 'name'));

        // If user is creating themselves use site locale as user's locale default. Not set for admin's create users as
        // there won't be a relation between site locale and user.
        if (!is_null($model)) {
            if (is_null(auth()->user()) && is_null($model->locale)) {
                $model->locale = app()->getLocale();
            }
        }

        $fields = [
            Text::create('title', __('Title'))
                ->icon('fas fa-tag')
            ,
            Text::create('firstname', __('First name'))
                ->icon('fas fa-user')
                ->validation('required')
                ->defaultSort('asc', 2)
            ,
            Text::create('lastname', __('Last name'))
                ->icon('fas fa-user-circle')
                ->validation('required')
                ->defaultSort('asc', 1)
            ,
            Text::create('email', __('Email'))
                ->icon('fas fa-at')
                ->validation('email|unique:user,email,$id|required')
            ,
            Text::create('email_alternative', __('Email alternative'))
                ->icon('fas fa-crow')
            ,
            BelongsToMany::create('roles', __('Roles'))
                ->icon('fas fa-theater-masks')
                ->setOptionsMethod('allRoles', 'id', 'description')
                ->readOnly(is_null(auth()->user()) || !auth()->user()->canEditRoles())
                ->noSorting()
            ,
            BelongsToMany::create('gdpr', __('GDPR'))
                ->icon('fas fa-user-shield')
                //->options
                ->setOptionsMethodClass(\Heiw\Uxcrudible\Models\Gdpr::class)
                ->setOptionsMethod('allGdpr', 'id', 'name')
                ->hideOnEdit(is_null(auth()->user()) || auth()->user()->canEditRoles())
                ->noSorting()
            ,
            Enum::create('locale', __('Preferred communication language'))
                ->icon('fas fa-globe')
                ->setEnumOptions($languages)
                ->noSorting(true)
            ,
            Actions::create()
                ->add(Action::create('impersonate', 'uxcrud::actions.impersonate'))

        ];
        return $fields;
    }


    /**
     * Start impersonating the specified user
     * @param $id
     * @return JsonResponse
     */
    public function impersonate($id) {
        if (app('impersonate')->isImpersonating()) {

            return false;
        }
        $impersonatee = \Heiw\Uxcrudible\Models\User::find($id);

        if ($impersonatee->id == $id && $impersonatee->canBeImpersonated()) {
            auth()->user()->impersonate($impersonatee);
            return response()->json(
                JsonToast::create()
                    ->body(__('Impersonating: :name', ['name' => $impersonatee->summary()]))
                    ->status('success')
                    ->redirect('/')
            );
        } else {
            return response()->json(
                JsonToast::create()
                    ->header(__('You can\'t impersonate :name', ['name' => $impersonatee->summary()]))
                    ->body(__('User not found or you do not have the required level to impersonate :name', ['name' => $impersonatee->summary()]))
                    ->status('error')
            );
        }
    }

    /**
     * End impersonating a user
     * @return JsonResponse
     */
    public function endImpersonation() {
        auth()->user()->leaveImpersonation();
        return redirect()->back();
    }

    /**
     * Add send email button to data table.
     * @return \Heiw\Uxcrudible\Classes\DataTableOptions|null
     */
    public function dataTableOptions() {
        $this->dataTableOptions = parent::dataTableOptions();
        if (auth()->user()->can('sendEmailTemplate', static::$model)) {
            $this->dataTableOptions->customButtons =
                DataTableButton::create('E-mail')
                    ->icon('fas fa-envelope')
                    ->onAction('modal-action')
                    ->addDataAttribute('method', 'GET')
                    ->addDataAttribute('dt-refresh', 'dt-admin-user')
                    ->addDataAttribute('href',
                        static::getAjaxRoute() . '/' . Str::kebab(__('uxcrud::slugs.select-email-template')))
            ;
        }
        return $this->dataTableOptions;
    }

    /**
     * Add ajax routes to send email.
     * @param \Heiw\Uxcrudible\Models\Model $model
     */
    public static function addRoutes(\Heiw\Uxcrudible\Models\Model $model) {
        parent::addRoutes($model);

        Route::get(
            static::getAjaxRoute() . '/' . Str::kebab(__('uxcrud::slugs.select-email-template')),
            $model->getControllerNamespaced(false) . '@selectEmailTemplate'
        )->middleware(['web', 'auth']);
        Route::get(
            static::getAjaxRoute() . '/' . Str::kebab(__('uxcrud::slugs.send-email-template')),
            $model->getControllerNamespaced(false) . '@sendEmailTemplate'
        )->middleware(['web', 'auth']);
    }
}
