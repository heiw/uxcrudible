<?php

namespace Heiw\Uxcrudible\Uxcrud;

use Heiw\Uxcrudible\Classes\JsonForm;
use Heiw\Uxcrudible\Classes\JsonModal;
use Heiw\Uxcrudible\Classes\JsonToast;
use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\BelongsToMany;
use Heiw\Uxcrudible\Form\Fields\Checkbox;
use Heiw\Uxcrudible\Form\Fields\CKEditor;
use Heiw\Uxcrudible\Form\Fields\DataTable;
use Heiw\Uxcrudible\Form\Fields\Date;
use Heiw\Uxcrudible\Form\Fields\Enum;
use Heiw\Uxcrudible\Form\Fields\HasMany;
use Heiw\Uxcrudible\Form\Fields\HasOne;
use Heiw\Uxcrudible\Form\Fields\Icon;
use Heiw\Uxcrudible\Form\Fields\Number;
use Heiw\Uxcrudible\Form\Fields\Pages;
use Heiw\Uxcrudible\Form\Fields\TextArea;
use Heiw\Uxcrudible\Services\Menu;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;
use Illuminate\Support\Facades\DB;

class Gdpr extends UxcrudController
{
    public static $model = \Heiw\Uxcrudible\Models\Gdpr::class;

    public function fields($model = null)
    {
        $fields = [
            Text::create('name', __('Name'))
                ->addClass('name')
                ->icon('fas fa-tag')
                ->validation('required')
                //->readOnly(isSigned)
            ,
            CKEditor::create('content', __('Content'))
                ->icon('fas fa-align-left')
                ->rows(10)
            ,
            CKEditor::create('notes', __('Notes'))
                ->icon('fas fa-clipboard')
                ->rows(10)
            ,
//            Checkbox::create('compulsory', __('Compulsory'))
//                ->icon('fas fa-exclamation-circle')
//            ,
            Checkbox::create('active', __('Active'))
                ->icon('fas fa-bolt')
            ,
            Date::create('date', __('Archive date'))
                ->icon('fas fa-calendar-day')
            ,

            Actions::create()

        ];
        return $fields;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function agree()
    {
        $user = auth()->user();
        $gdpr = \Heiw\Uxcrudible\Models\Gdpr::findOrFail(request()->input('gdpr_id'));

        if (!$gdpr->active) {
            return response()->json([
                'message' => __('You can only agree to the active GDPR statement. The selected statement is not active.'),
            ],
                422
            );
        }

        // If user doesn't have GDPR statement add it.
        if ($user->gdpr()->find(['id' => $gdpr->id])) {
            $user->gdpr()->attach($gdpr, ['sign_date' => now()]);
        }

        return response()->json(
            JsonToast::create()
                ->status('refresh')
        );
    }

    public function view() {
        $modal = new JsonModal();
        $modal->header = __('Privacy notifications :user', ['user' => auth()->user()->summary()]);
        $modal->body = view('uxcrud::gdpr')->render();
        $modal->size = 'xxl';
        return response()->json($modal);
    }

    public function withdraw() {
        if (config('uxcrud.gdpr_allow_withdraw')) {
            $user = auth()->user();
            $gdpr = \Heiw\Uxcrudible\Models\Gdpr::findOrFail(request()->input('gdpr_id'));

//        if (!$gdpr->active) {
//            return response()->json([
//                'message' => __('You can only agree to the active GDPR statement. The selected statement is not active.'),
//            ],
//                422
//            );
//        }

            // If user has GDPR statement remove it.
            if ($user->gdpr()->find(['id' => $gdpr->id])) {
                $user->gdpr()->detach($gdpr);
            }

            return response()->json(
                JsonToast::create()
                    ->status('refresh')
            );
        }
        return response()->json(
            JsonToast::create()
                ->status('refresh')
        );
    }


    /**
     * @param  mixed $ability   Action permission (create/update)
     * @param $object           object Original object
     * @param $request          \Illuminate\Http\Request object
     * @param null $messages
     * @return array            Array of rules
     * @param bool $includeWarningMessages  Include data-warning messages when set to true.
     * @return array
     */
    public function getValidationRules($ability, $object, $request, &$messages = null, $includeWarningMessages = false) {
        $rules = parent::getValidationRules($ability, $object, $request,
            $messages, $includeWarningMessages);

        if ($object->isSigned()) {
            $translations = $object->getTranslationsArray();
            foreach($translations as $language => $field) {
                foreach($field as $name => $value) {
                    if ($object->translate($language)->{$name} !== $request->input("$language.$name")) {
                        $rules["$language.$name"] = "in:[0]";
                        $messages["$language.$name.in*"] = __("No changes are permitted to the :name field after users have agreed to it.",
                            ['name' => $name]);
                    }
                }
            }
        }
        return $rules;
    }

    public function auditFieldsForModel($view, $fields, $model) {
        $fields = parent::auditFieldsForModel($view, $fields, $model);
        if ($model->isSigned()) {
            foreach($fields as $id => $field) {
                switch($field->name) {
                    case "name":
                    case "content":
                        //$fields[$id]->canEdit = false;
                        $fields[$id]->readOnlyOnEdit(true);
                    break;
                }
            }
        }
        return $fields;
    }

}
