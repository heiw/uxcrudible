<?php

namespace Heiw\Uxcrudible\Listeners;

use App\Models\PracticeVisit;
use App\Models\User;
use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\Models\EmailLog;
use Illuminate\Mail\Events\MessageSent;

class EmailSentSubscriber {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param MessageSent $event
     */
    public function handleMessageSent($event) {
        $emailLog = new EmailLog();
        if (isset($event->data['log'])) {
            $emailLog->sender_id = $event->data['log']['sender_id'];
            $emailLog->recipient_id = $event->data['log']['recipient_id'];
            $emailLog->email_template_id = $event->data['log']['email_template_id'];
        } else {
            // Get first from to list for system emails.
            $tos = array_keys($event->message->getTo());
            $to = reset($tos);
            if ($to) {
                $user = User::where('email', '=', $to)->first();
                if ($user) {
                    $emailLog->recipient_id = $user->id;
                }
            }
        }
        $emailLog->content =
            nl2br((e($event->message->getHeaders()->toString())))
            . $event->message->getBody();
        $emailLog->addresses = json_encode([
            'to' => $event->message->getTo(),
            'cc' => $event->message->getCc(),
            'bcc' => $event->message->getBcc(),
        ]);
        $emailLog->success = true;
        $emailLog->date = $event->message->getDate();
        $emailLog->save();
        //dump($event->message->toString());
        //$event->message->getBody();

    }

    public function subscribe($events) {
        $events->listen(MessageSent::class, self::class . "@handleMessageSent");
    }
}
