<?php

namespace Heiw\Uxcrudible;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\DataTable;
use Heiw\Uxcrudible\Form\Fields\Permission;
use Heiw\Uxcrudible\Form\Filters\WhereClause;
use Heiw\Uxcrudible\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class UxcrudModel extends Model
{
    use Traits\RelationsManager;
    use SoftDeletes;

    protected static $icon;

    protected static $orderBy;

    protected static $deleteOnAnonymise = false;

    /**
     * Set to true to allow model without requiring entry in Model table in
     * database.
     * No restrictions from the user/role tables are applied. This means
     * that all checks need to be made on the corresponding Policy class.
     * @var bool True:
     */
    public static $databaseLessModel = false;

    public static function getIcon() {
        return static::$icon;
    }

    public static function getOrderBy() {
        return static::$orderBy;
    }

    /**
     * Return summary of entry
     * Defaults to value of order by property. Overwrite function to customise.
     * @return mixed
     */
    public function summary() {
        return $this->{static::$orderBy};
    }

    public function prettyClassName() {
        return __(Str::title(Helpers::from_camel_case(class_basename(get_class($this)))));
    }

    /**
     * Returns true if model can be translated.
     * Note: this doesn't require translations for all locales to be present.
     */
    public function hasTranslationLocale() {
        // Check for translation trait
        return (method_exists($this, 'bootTranslatable'));
    }

    /**
     * Return all available translation locales if model can be translated.
     * If no translation locales defined return default locale.
     * Note: this doesn't require translations for all locales to be present.
     */
    public function translationLocales() {
        $locales = config('uxcrud.locales');
        // Check for translation trait
        if (!self::hasTranslationLocale()) {
            $locale[App::getLocale()] = $locales[App::getLocale()];
            $locale[App::getLocale()]['active'] = true;
            return $locale;
        }
        foreach($locales as $locale => $details) {
            $locales[$locale]['active'] = ($locale == App::getLocale());
        }
        return $locales;
    }

    public function translationLocalesField($fieldName) {
        $locales = config('uxcrud.locales');
        // Check for translation trait

        if (!$this->hasTranslationAttribute($fieldName)) {
            $locale[App::getLocale()] = $locales[App::getLocale()];
            $locale[App::getLocale()]['active'] = true;
            return $locale;
        }
        foreach($locales as $locale => $details) {
            $locales[$locale]['active'] = ($locale == App::getLocale());
        }
        return $locales;

    }

    public function hasTranslationAttribute($key) {
        if (method_exists($this, 'isTranslationAttribute')) {
            return $this->isTranslationAttribute($key);
        }
        return false;
    }

    /**
     * Make field values available for inclusion in email template.
     * They can be includes with :key.
     * E.g. Dear :user, you have the following roles: :user.roles
     * To limit load only fields shown on index are included by default.
     * @param null $key
     * @param array $data
     * @param string $view Change to Permission::VIEW to include all fields.
     * @return array|null
     * @throws \Throwable
     */
    public function emailTemplateViewData($key = null, &$data = [], $view = Permission::INDEX) {
        $controller = Helpers::get_controller_from_model_name(get_class($this));
        if ($controller) {
            $fields = $controller->getFields($view, true);
            if (is_null($key)) {
                $key = Str::snake($this->prettyClassName());
            }
            $data[$key] = $this->summary();
            $key .= ".";
            foreach ($fields as $field) {
                switch (get_class($field)) {
                    case Actions::class:
                        // Don't include actions.
                        break;
                    default:
                        $blade = $field->getBlade("uxcrud::forms.fields.show.");
                        if (View::exists($blade)) {
                            $name = Helpers::stripFromEnd('_id', $field->name);
                            $data[$key . $name] = trim(view($blade, [
                                'displayValue' => $field->extractDisplayValue($this),
                                'field' => $field,
                                'entry' => $this,
                                'model' => $this,
                                'controller' => $controller,
                                'dtRefreshId' => null,
                                'parentModel' => null
                            ])->render());
                        }
                        break;
                }
            }
        }
        return $data;
    }



    /**
     * Scope a query to include soft deleted items based on request.
     * @param $query
     * @param null $request
     * @return mixed
     */
    public function scopeSoftDeleted($query) {
        $withoutTrashed = true;
        if ($value = request()->input('uxcrud-filter')) {
            // Include trashed (only) according to filter settings.
            if (isset($value['uxcrud-trashed-filter'])) {
                switch ($value['uxcrud-trashed-filter']) {
                    case 'with':
                        $query->withTrashed();
                        $withoutTrashed = false;
                        //                            $query->builder->withTrashed();
                        break;
                    case 'only':
                        $query->onlyTrashed();
                        $withoutTrashed = false;
                        //                            $query->builder->onlyTrashed();
                        break;
                }
            }
        }
        if ($withoutTrashed) {
            $query->withoutTrashed();
        }

        return $query;
    }

    /**
     * Custom scope (query builder method) to add filters to query
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param UxcrudController $controller
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereFilters($query, $controller) {
        $request = request();

        $query->softDeleted($query);

        /*
         * TODO MK Check whether functionality below is used anywhere.
         * If not check whether an if filter check should be added to the uxcrud-filter functionaly .
         */
//        foreach($controller->getFilters(Permission::READ) as $filter) {
//            switch ($filter->type) {
//                case "hasMany":
//                case "belongsToMany":
//                    $filterName = trim($filter->name, '[]');
//                    $value = $request->input($filterName);
//                    if (!empty($value)) {
//                        $query->whereHas($filter->method, function($q) use ($value) {
//                            $q->whereIn('id', $value);
//                        }
//                        );
//                    }
//                    break;
//                default:
//                    $value = $request->input($filter->name);
//                    if (!empty($value)) {
//                        if ($this->hasTranslationAttribute($filter->name)) {
//                            $query->whereTranslation($filter->name, 'like', "%$value%");
//                        } else {
//                            $query->where($filter->name, 'like', "%$value%");
//                        }
//                    }
//                    break;
//            }
//        }

        if ($value = $request->input('uxcrud-filter')) {
            $query->where(function($query) use ($value, $controller) {
                foreach($controller->getFields(Permission::READ, true) as $field) {
                    $name = trim($field->name, '[]');

                    if (isset($value[$name])) {
                        switch ($field->type) {
                            case "matrix":
                                break;
                            case "hasMany":
                            case "belongsToMany":
                                $query->where(function($query) use ($field, $name, $value) {
                                    $query->whereHas($name, function($subQuery) use ($field, $name, $value) {
                                        $subQuery->whereIn($field->valueName, $value);
                                    });
                                })->with($name);
                                //$query->with($name)->whereIn('role_id', $value[$name]);
                                break;
                            case "hasOne":
                                $query->where($name, '=', $value[$name]);
                                break;
                            default:
                                $selectExpressions = $field->selectExpressions;
                                $valueKey = $name;
                                // Set value key to name-data if present.
                                if (isset($value["{$name}-data"])) {
                                    $valueKey = "{$name}-data";
                                }
                                // If no select expression set use name.
                                if (empty($selectExpressions)) {
                                    $selectExpressions[] = $name;
                                }
                                foreach($selectExpressions as $selectExpression) {
                                    $method = null;
                                    $operator = null;
                                    // Determine which query type to use based
                                    // on value supplied.
                                    switch($value[$name]) {
                                        case WhereClause::BETWEEN:
                                            $method = "whereBetween";
                                            break;
                                        case WhereClause::NOT_BETWEEN:
                                            $method = "whereNotBetween";
                                            break;
                                        case WhereClause::IN:
                                            $method = "whereIn";
                                            break;
                                        case WhereClause::NOT_IN:
                                            $method = "whereNotIn";
                                            break;
                                        case WhereClause::NOT_NULL:
                                            if (!is_null($value[$valueKey][0])) {
                                                $method = "where";
                                                $operator = "like";
                                                $value[$valueKey] = "%{$value[$valueKey][0]}%";
                                            } else {
                                                // Different parameters on call to whereNull
                                                // compared to standard where[...]
                                                $query->whereNotNull($selectExpression);
                                            }
                                            break;
                                        case WhereClause::NULL:
                                            // Different parameters on call to whereNull
                                            // compared to standard where[...]
                                            $query->whereNull($selectExpression);
                                            break;
                                        default:
                                            $method = "where";
                                            $operator = "like";
                                            $value[$valueKey] = "%{$value[$valueKey]}%";
                                            break;
                                    }
                                    if (!is_null($method)) {
                                        if (is_null($operator)) {
                                            // TODO MK: Check whether need to check for translation field.
                                            $query->$method($selectExpression, $value[$valueKey]);
                                        } else {
                                            if ($this->hasTranslationAttribute($selectExpression)) {
                                                // Correct method name as
                                                // Laravel Translatable uses different
                                                // method names
                                                switch($method) {
                                                    case "where":
                                                        $method = "whereHas";
                                                        break;
                                                }
                                                $query->whereTranslation($selectExpression, $value[$valueKey], null, $method, $operator);
                                            } else {
                                                $query->$method($selectExpression, $operator, $value[$valueKey]);
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
            });
        }

        if (isset($request->input('search')['value'])) {
            $value = $request->input('search')['value'];
            /*
             * TODO MK
             * Check whether the query uses group by. If so:
             *      use $query->having instead of $query->where
             *      Problem: Laravel doesn't support closures on having / orHaving.
             */
            $query->where(function($query) use ($value, $controller) {
                foreach ($controller->getSearchable() as $field) {
                    // Strip array from column name.
                    $name = trim($field->name, '[]');
                    // If not fully qualified column name add table name.
                    if (strpos($name, '.') === false) {
                        $name = "{$this->table}.$name";
                    }

                    $selectExpressions = $field->selectExpressions;
                    if (empty($selectExpressions)) {
                        if (isset($value["{$name}-data"])) {
                            $selectExpressions[] = "{$name}-data";
                        } else {
                            $selectExpressions[] = $name;
                        }
                    }
                    foreach($selectExpressions as $name) {
                        $tableAndField = explode(".", $name);
                        switch ($field->type) {
                            case "hasOne":
                            case "hasMany":
                            case "belongsToMany":
                                // TODO MK allow search on relations.
                                break;
                            case "date":
                                $query->orWhere(DB::raw("DATE_FORMAT(" . $name . ", \"%d/%m/%Y\")"), 'like', "%$value%");
                                //$query->orHavingRaw(DB::raw("DATE_FORMAT(" . $name . ", \"%d/%m/%Y\") LIKE ?"), ["%$value%"]);
                                break;
                            default:
                                switch (get_class($field)) {
                                    case DataTable::class:
                                        break;
                                    default:
                                        if ((count($tableAndField) == 2) && $this->hasTranslationAttribute($tableAndField[1])) {
                                            $query->orWhereTranslation($tableAndField[1], 'like', "%$value%");
                                        } else {
                                            $query->orWhere($name, 'like', "%$value%");
                                            //$query->orHavingRaw("{$name} LIKE ?", ["%$value%"]);
                                        }
                                        break;
                                }
                                break;
                        }
                    }
                }
            });
        }
        return $query;
    }

    /**
     * Set selects for fields from UxcrudController form to query builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param UxcrudController $controller
     * @param string[] $select     Named array of selects that override fields from UxcrudController form.
     *                             ['client_id' => 'client.id AS client_id']
     * @param string[] $exclude     Names of fields to exclude
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSelectUxcrudFields($query, $controller, $select = [], $exclude = []) {
        if (!isset($select['id'])) {
            $select['id'] = $controller::getModelTable() . ".id AS id";
        }
        foreach ($controller->fields() as $column) {
            if (($column->name != "actions")
                && (!empty($column->name))
                && !in_array($column->name, $exclude)
                && !Str::endsWith($column->name, '[]')
                && is_array($column->getSelectExpressions())
            ) {
                $select = array_merge($select, $column->getSelectExpressions());
            }
        }
        $query->select($select);
        return $query;
    }

    /**
     * Add selects for fields to query builder.
     * TODO needs to be thoroughly tested
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param UxcrudController $controller
     * @param string[] $exclude     Names of fields to exclude
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAddSelectsFromFields($query, $controller, $exclude = [], $view = \Heiw\Uxcrudible\Form\Fields\Permission::READ) {
        foreach ($controller->getFields($view, true) as $field) {
            if (!in_array($field->name, $exclude)) {
                $selectExpressions = $field->getSelectExpressions();
                if ($selectExpressions) {
                    $query->addSelect($selectExpressions);
                }
            }
        }

        return $query;
    }

    /**
     * Anonymise model based on GDPR rules.
     * @param array $errors Collection of errors.
     * @param array $retain Array of field names to retain.
     * @return array
     */
    public function anonymise($errors = [], $retain = []) {
        // Get all field names
        $controller = Helpers::get_controller_from_model_name(get_class($this));
        $fields = array_column($controller->getFields(Permission::VIEW, true), 'name');

        // Remove field names we want to keep.
        $fields = array_diff($fields, $retain);

        // Check relations
        $relations = $this->getAllRelations();
        foreach ($relations as $relation => $type) {
            if (!in_array($relation, $retain)) {
                if (isset($this->{$relation})) {
                    foreach ($this->{$relation} as $child) {
                        $child->anonymise($errors);
                    }
                }
                // Delete relation from field array.
                if (($key = array_search($relation, $fields)) !== false) {
                    unset($fields[$key]);
                }
            }
        }

        if (static::$deleteOnAnonymise) {
            // Complete remove record on GDPR anonymisation.
            if (!$this->forceDelete()) {
                $errors[] = __("Can't delete :name :id", ['name' => $this->prettyClassName(), 'id' => $this->id]);
            }
        } else {
            // Set field values to null if they exist.
            foreach ($fields as $field) {
                if (isset($this->{$field})) {
                    $this->{$field} = null;
                }
            }

            // Store anonymised data.
            if (!$this->save()) {
                $errors[] = __("Can't anonymise :name :id",
                    ['name' => __($this->prettyClassName()), 'id' => $this->id]);
            }
        }

        return $errors;
    }
}
