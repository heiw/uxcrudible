<?php

namespace Heiw\Uxcrudible;

// TODO MK Find out why helpers.php isn't loaded.

use Heiw\Uxcrudible\Form\Fields\FormField;
use Heiw\Uxcrudible\Models\SiteConfig;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class Helpers
{
//    use CachedObjectValue;
//
//    const COMMIT_HASH_LONG = '%H';
//    const COMMIT_HASH_SHORT = '%h';
//    const COMMIT_DATE = '%ci';
//    const GIT_COMMIT_VERSION_INFO = [self::COMMIT_HASH_LONG, self::COMMIT_HASH_SHORT, self::COMMIT_DATE];

    /**
     * Convert a string from camelCase.
     * camelCaseString -> Camel Case String
     *
     * @param string $value
     * @param string $delimiter
     * @return string
     */
    public static function from_camel_case($value, $delimiter = ' ') {
        return \Illuminate\Support\Str::snake($value, $delimiter);
    }

    /**
     * Convert a string from kebab or snake case to sentence case.
     * @param $value
     * @return string
     */
    public static function to_sentence_case($value) {
        return Str::title(str_ireplace(["-", "_"], [" ", " "], $value));
    }

    /**
     * Remove string from end of string.
     * @param $strip
     * @param $string
     * @return string|string[]|null
     */
    public static function stripFromEnd($strip, $string) {
        return preg_replace('/' . $strip . '$/', '', $string);
    }

    /**
     * Surround lines with <p></p>.
     * @param $string
     * @return string
     */
    public static function nl2p($string) {
        $array = explode("\n", $string);
        return "<p>" . implode("</p><p>", $array) . "</p>";
    }


    /**
     * Provided the class name of a model it returns the corresponding controller object.
     * @param String $model Classname of model
     * @return UxcrudController mixed
     */
    public static function get_controller_from_model_name($model) {
        if (is_object($model)) {
            $model = get_class($model);
        }
        $controllerClass = str_replace(config('uxcrud.model_namespace'), config('uxcrud.controller_namespace'), $model);
        if ($controllerClass == 'App\User') {
            $controllerClass = 'App\Uxcrud\User';
        }
        if (class_exists($controllerClass)) {
            return new $controllerClass;
        }
        return null;
    }

    /**
     * Join a string with a natural language conjunction at the end.
     * Based on https://gist.github.com/dan-sprog/e01b8712d6538510dd9c
     * @param array $list
     * @param string $conjunction
     * @param string $separator
     * @return mixed|string
     */
    public static function implode_different_last(array $list, $conjunction = 'and', $separator = ', ') {
        $last = array_pop($list);
        if ($list) {
            return implode($separator, $list) . ' ' . $conjunction . ' ' . $last;
        }
        return $last;
    }

    /**
     *
     * @param array $list
     * @param string $glue
     * @param string $lineGlue
     * @return string|null
     */
    public static function implode_with_keys(array $list, $glue = "=", $lineGlue = "&") {
        $result = null;
        foreach($list as $key => $item) {
            $result .= "$key$glue$item$lineGlue";
        }
        return $result;
    }

    /**
     * Check whether a date falls between or on start and end date and dates
     * are not null.
     * @param $needle       Date to check
     * @param $startDate
     * @param $endDate
     * @return bool
     * @throws \Exception
     */
    public static function date_between($needle, $startDate, $endDate) {
        if (is_string($needle)) {
            $needle = new Carbon($needle);
        }
        if (is_string($startDate)) {
            $startDate = new Carbon($startDate . " 00:00:00");
        }
        if (is_string($endDate)) {
            $endDate = new Carbon($endDate . " 23:59:59");
        }
        return (!is_null($startDate) && !is_null($endDate) && $needle >= $startDate && $needle <= $endDate);
    }

    /**
     * Check whether a date falls after the end date and is not null.
     * @param $needle       Date to check
     * @param $endDate
     * @param string $time  Time to add to date string.
     * @return bool
     * @throws \Exception
     */
    public static function date_after($needle, $endDate, $time = null) {
        if (is_string($needle)) {
            $needle = new Carbon($needle);
        }
        if (is_string($endDate)) {
            $endDate = new Carbon($endDate . $time);
        }
        return (!is_null($endDate) && $needle > $endDate);
    }

    /**
     * Check whether a date falls before the start date and is not null.
     * @param  $needle       Date to check
     * @param $startDate
     * @param $startDate
     * @return bool
     * @throws \Exception
     */
    public static function date_before($needle, $startDate) {
        if (is_string($needle)) {
            $needle = new Carbon($needle);
        }
        if (is_string($startDate)) {
            $startDate = new Carbon($startDate . " 00:00:00");
        }
        return (!is_null($startDate) && $needle < $startDate);
    }

    /**
     * Return named array as html element attributes.
     * @param $attributes
     * @return string
     */
    public static function implode_array_to_attributes($attributes) {
        if (empty($attributes)) {
            return null;
        }
        if (!is_array($attributes)) {
            return $attributes;
        }

        $attributePairs = [];
        foreach ($attributes as $key => $value) {
            if (is_int($key)) {
                $attributePairs[] = $value;
            } else {
                $value = htmlspecialchars($value, ENT_QUOTES);
                $attributePairs[] = "{$key}=\"{$value}\"";
            }
        }

        return join(' ', $attributePairs);
    }


    /**
     * Inserts value into array before the key provided.
     * If key isn't found the value is added to the end of the array.
     * @param $array
     * @param $value
     * @param $beforeKey
     * @return array
     */
    public static function array_insert_before_key($array, $value, $key) {
        $result = $array;
        if ($key === false) {
            array_push($result, $value);
        } else {
            if (!is_array($value)) {
                $value = [$value];
            }
            $result = array_merge
            (
                array_slice($result, 0, $key),
                $value,
                array_slice($result, $key, null)
            );
        }
        return $result;
    }

    /**
     * Inserts value into array on the position before the $before value.
     * If $before isn't found the value is added to the end of the array.
     * @param $array
     * @param $value
     * @param $beforeKey
     * @return array
     */
    public static function array_insert_before_value($array, $value, $beforeValue) {
        $key = array_search($beforeValue, $array);
        return self::array_insert_before_key($array, $value, $key);
    }

    /**
     * @param $number
     * @param bool $uppercase
     * @return string
     * Based on https://stackoverflow.com/a/15023547/4559562
     */
    public static function decimal_to_roman($number, $uppercase = true) {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $result = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if($number >= $int) {
                    $number -= $int;
                    $result .= $roman;
                    break;
                }
            }
        }
        if (!$uppercase) {
            $result = strtolower($result);
        }
        return $result;
    }

    public static function decimal_to_alpha($number, $uppercase = true)
    {
        if ($number <= 26) {
            $result = chr($number + 64);
        } else {
            $alpha = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
            );
            $result = '';
            do {
                $number--;
                $limit = floor($number / 26);
                $reminder = $number % 26;
                $result = $alpha[$reminder] . $result;
                $number = $limit;
            } while ($number > 0);
        }
        if (!$uppercase) {
            $result = strtolower($result);
        }
        return $result;
    }

    public static function userHasRole($role) {
        $user = auth()->user();
        if (!$user) {
            return false;
        }
        return !$user->hasRole($role);
    }

    public static function removeColon($string) {
        return str_replace(':', '', $string);
    }

//    /**
//     * Get git version number or commit date.
//     * @param string $type
//     * @return mixed
//     */
//    public function gitVersion($type = self::COMMIT_HASH_SHORT) {
//        if (in_array($type, self::GIT_COMMIT_VERSION_INFO)) {
//            $cmd = "git -C \"" . base_path() . "\\\" log --pretty='$type' -n1 HEAD 2>&1";
//            $gitVersion = exec($cmd, $output, $return_var);
//            return $gitVersion;
//        }
//        return null;
//    }

    public static function versionCache() {
        return '?v=' . SiteConfig::get('app_version');
    }

    public static function get_host() {
        return Str::lower(request()->getHost());
    }

    /**
     * Escape quotes.
     *
     * @param $string   String to
     * @return string|array|null
     */
    public static function escapeQuotes($string)
    {
        return htmlspecialchars($string, ENT_QUOTES);
    }

    /**
     * @param string $needle        Needle to search for
     * @param FormField[] $fields   Array of form fields to search through
     * @param string $key           Key to search for, defaults to name.
     */
    public static function findInFields($needle, $fields, $key = 'name') {
        // Get the values for the column by key name.
        $columns = array_column($fields, $key);

        // Assign the array keys of $fields to the extracted column names.
        $haystack = array_combine(array_keys($fields), $columns);

        $id = array_search($needle, $haystack);
        if ($id !== false) {
            return $fields[$id];
        }
        return false;
    }

}
