<?php

namespace Heiw\Uxcrudible\Seeds;

use Heiw\Uxcrudible\Models\Role;
use Heiw\Uxcrudible\Traits\IdFromNameSeederTrait;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    const SYS_ADMIN = 'sys_admin';
    const ADMIN = 'admin';
    const REPORT = 'report';
    const USER = 'user';
    const TRANSLATOR = 'translator';

    /**
     * Returns id from (constant) name value
     * @param $value
     * @param string $name
     * @return false
     */
    public static function idFromName($value, $name = 'name') {
        $entry = DB::table('role')->where($name, '=', $value)->first();
        if (isset($entry->id)) {
            return $entry->id;
        } else {
            return false;
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('role')->insertOrIgnore([
            [
                'name' => self::SYS_ADMIN,
                'description' => 'System Administrator',
                'hierarchy' => 0,
                'display' => 'System',
                'icon' => 'fas fa-user-shield',
            ],
            [
                'name' => self::ADMIN,
                'description' => 'Administrator',
                'hierarchy' => 1,
                'display' => 'Admin',
                'icon' => 'fas fa-user-cog',
            ],
            [
                'name' => self::REPORT,
                'description' => 'Reporting',
                'hierarchy' => 2,
                'display' => 'Reporting',
                'icon' => 'fas fa-table',
            ],
            [
                'name' => self::USER,
                'description' => 'User',
                'hierarchy' => 4,
                'display' => 'User',
                'icon' => 'fas fa-user',
            ],
            [
                'name' => self::TRANSLATOR,
                'description' => 'Translator',
                'hierarchy' => 4,
                'display' => 'Translator',
                'icon' => 'fas fa-language',
            ],
        ]);
    }
}
