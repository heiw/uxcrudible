<?php

namespace Heiw\Uxcrudible\Seeds;

use Heiw\Uxcrudible\Models\Model;
use Heiw\Uxcrudible\Models\Permission;
use Heiw\Uxcrudible\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModelTableSeeder extends Seeder
{
    const MODEL_ID = 1;
    const PAGE_ID = 2;
    const PERMISSION_ID = 3;
    const ROLE_ID = 4;
    const SITE_CONFIG_ID = 6;
    const USER_ID = 7;
    const FILE_ID = 8;
    const SNIPPET_ID = 9;
    const GDPR_ID = 11;
    const TRANSLATION_ID = 12;
    const TRANSLATION_SOURCE_ID = 13;
    const EMAIL_TEMPLATE_ID = 14;
    const EMAIL_LOG_ID = 15;
    const FORM_CREATOR_ID = 16;

    /**
     * @param array|int $model_ids  Single id or array of ids for models.
     * @param array|int $role_ids Single id or array of ids for roles.
     * @param array $permissions    List of permissions to add.
     */
    public static function addRights($model_ids, $role_ids, $permissions) {
        if (!is_array($role_ids)) {
            $role_ids = [$role_ids];
        }
        foreach($role_ids as $role_id) {
            if (Role::find($role_id)) {
                if (!is_array($model_ids)) {
                    $model_ids = [$model_ids];
                }
                $params = [];
                foreach ($permissions as $permission) {
                    foreach ($model_ids as $model_id) {
                        $params[] =
                            ['model_id' => $model_id, 'permission_id' => $permission, 'role_id' => $role_id];
                    }
                }

                DB::table('model_permission_role')->insertOrIgnore($params);
            }
        }
    }

    public static function addFullRights($model_ids, $role_ids) {
        self::addRights($model_ids, $role_ids,
            [PermissionTableSeeder::idFromName(PermissionTableSeeder::CREATE), PermissionTableSeeder::idFromName(PermissionTableSeeder::READ), PermissionTableSeeder::idFromName(PermissionTableSeeder::UPDATE), PermissionTableSeeder::idFromName(PermissionTableSeeder::DELETE), PermissionTableSeeder::idFromName(PermissionTableSeeder::RESTORE), PermissionTableSeeder::idFromName(PermissionTableSeeder::FORCE_DELETE)]);
    }

    public static function addAdminRights($model_ids, $role_ids) {
        self::addRights($model_ids, $role_ids,
            [PermissionTableSeeder::idFromName(PermissionTableSeeder::CREATE), PermissionTableSeeder::idFromName(PermissionTableSeeder::READ), PermissionTableSeeder::idFromName(PermissionTableSeeder::UPDATE), PermissionTableSeeder::idFromName(PermissionTableSeeder::DELETE)]);
    }

    public static function addFullRightsForSysAdmin($model_ids) {
        self::addFullRights($model_ids, RoleTableSeeder::idFromName(RoleTableSeeder::SYS_ADMIN));
    }

    public static function addReadRightsForTranslator($model_ids) {
        self::addRights($model_ids, RoleTableSeeder::idFromName(RoleTableSeeder::TRANSLATOR),
            [PermissionTableSeeder::idFromName(PermissionTableSeeder::READ), PermissionTableSeeder::idFromName(PermissionTableSeeder::UPDATE)]);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $models = [
            ['id' => self::MODEL_ID,      'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'Model'],
            ['id' => self::PAGE_ID,       'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'Page'],
            ['id' => self::PERMISSION_ID, 'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'Permission'],
            ['id' => self::ROLE_ID,       'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'Role'],
            ['id' => self::SITE_CONFIG_ID,'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'SiteConfig'],
            ['id' => self::USER_ID,       'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'User'],
            ['id' => self::FILE_ID,       'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'File'],
            ['id' => self::SNIPPET_ID,    'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'Snippet'],
            ['id' => self::GDPR_ID,       'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'Gdpr'],
            ['id' => self::TRANSLATION_ID,'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'Translation'],
            ['id' => self::TRANSLATION_SOURCE_ID,'namespace' => 'Heiw\\Uxcrudible\\','name' => 'TranslationSource'],
            ['id' => self::EMAIL_TEMPLATE_ID,'namespace' => 'Heiw\\Uxcrudible\\','name' => 'EmailTemplate'],
            ['id' => self::EMAIL_LOG_ID,    'namespace' => 'Heiw\\Uxcrudible\\','name' => 'EmailLog'],
            ['id' => self::FORM_CREATOR_ID, 'namespace' => 'Heiw\\Uxcrudible\\','name' => 'FormCreator'],
        ];
        include(__DIR__ . '/../../database/migrations/2020_01_07_095216_create_translation_table.php');
        $models = array_merge(
            $models,
            \CreateTranslationTable::models()
        );

        $model_translation = [
            ['model_id' => self::MODEL_ID,              'locale' => 'en', 'display' => __('Model'      , [], 'en')],
            ['model_id' => self::PAGE_ID,               'locale' => 'en', 'display' => __('Page'       , [], 'en')],
            ['model_id' => self::PERMISSION_ID,         'locale' => 'en', 'display' => __('Permission' , [], 'en')],
            ['model_id' => self::ROLE_ID,               'locale' => 'en', 'display' => __('Role'       , [], 'en')],
            ['model_id' => self::SITE_CONFIG_ID,        'locale' => 'en', 'display' => __('Site Config', [], 'en')],
            ['model_id' => self::USER_ID,               'locale' => 'en', 'display' => __('User'       , [], 'en')],
            ['model_id' => self::FILE_ID,               'locale' => 'en', 'display' => __('File'       , [], 'en')],
            ['model_id' => self::SNIPPET_ID,            'locale' => 'en', 'display' => __('Snippet'    , [], 'en')],
            ['model_id' => self::GDPR_ID,               'locale' => 'en', 'display' => __('GDPR'       , [], 'en')],
            ['model_id' => self::TRANSLATION_ID,        'locale' => 'en', 'display' => __('Translation', [], 'en')],
            ['model_id' => self::TRANSLATION_SOURCE_ID, 'locale' => 'en', 'display' => __('Translation Source', [], 'en')],
            ['model_id' => self::EMAIL_TEMPLATE_ID,     'locale' => 'en', 'display' => __('Email Template', [], 'en')],
            ['model_id' => self::EMAIL_LOG_ID,          'locale' => 'en', 'display' => __('Email Log', [], 'en')],
            ['model_id' => self::FORM_CREATOR_ID,       'locale' => 'en', 'display' => __('Form Creator', [], 'en')],

            ['model_id' => self::MODEL_ID,              'locale' => 'cy', 'display' => __('Model',      [], 'cy')],
            ['model_id' => self::PAGE_ID,               'locale' => 'cy', 'display' => __('Page',       [], 'cy')],
            ['model_id' => self::PERMISSION_ID,         'locale' => 'cy', 'display' => __('Permission', [], 'cy')],
            ['model_id' => self::ROLE_ID,               'locale' => 'cy', 'display' => __('Role',       [], 'cy')],
            ['model_id' => self::SITE_CONFIG_ID,        'locale' => 'cy', 'display' => __('Site Config',[], 'cy')],
            ['model_id' => self::USER_ID,               'locale' => 'cy', 'display' => __('User',       [], 'cy')],
            ['model_id' => self::FILE_ID,               'locale' => 'cy', 'display' => __('File',       [], 'cy')],
            ['model_id' => self::SNIPPET_ID,            'locale' => 'cy', 'display' => __('Snippet',    [], 'cy')],
            ['model_id' => self::GDPR_ID,               'locale' => 'cy', 'display' => __('GDPR',       [], 'cy')],
            ['model_id' => self::TRANSLATION_ID,        'locale' => 'cy', 'display' => __('Translation',[], 'cy')],
            ['model_id' => self::TRANSLATION_SOURCE_ID, 'locale' => 'cy', 'display' => __('Translation Source',[], 'cy')],
            ['model_id' => self::EMAIL_TEMPLATE_ID,     'locale' => 'cy', 'display' => __('Email Template', [], 'cy')],
            ['model_id' => self::EMAIL_LOG_ID,          'locale' => 'cy', 'display' => __('Email Log', [], 'cy')],
            ['model_id' => self::FORM_CREATOR_ID,       'locale' => 'cy', 'display' => __('Form Creator', [], 'cy')],
        ];

        $model_translation = array_merge(
            $model_translation,
            \CreateTranslationTable::modelTranslations()
        );

        DB::table('model')->insertOrIgnore($models);
        DB::table('model_translation')->insertOrIgnore($model_translation);

        // Add full rights to all models for sys admin.
        foreach($models as $model) {
            self::addFullRightsForSysAdmin($model['id']);
        }
        foreach([ModelTableSeeder::MODEL_ID,
                 ModelTableSeeder::PAGE_ID,
                 ModelTableSeeder::SNIPPET_ID,
                 ModelTableSeeder::GDPR_ID] as $modelId) {
            self::addReadRightsForTranslator($modelId);
        }
        DB::table('model_permission_role')->insertOrIgnore([
            ['model_id' => ModelTableSeeder::PAGE_ID, 'permission_id' => PermissionTableSeeder::idFromName(PermissionTableSeeder::CREATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::PAGE_ID, 'permission_id' => PermissionTableSeeder::idFromName(PermissionTableSeeder::READ),   'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::PAGE_ID, 'permission_id' => PermissionTableSeeder::idFromName(PermissionTableSeeder::UPDATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::PAGE_ID, 'permission_id' => PermissionTableSeeder::idFromName(PermissionTableSeeder::DELETE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],

            ['model_id' => ModelTableSeeder::USER_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::CREATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::USER_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::READ),   'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::USER_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::UPDATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::USER_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::DELETE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],

            ['model_id' => ModelTableSeeder::FILE_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::CREATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::FILE_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::READ),   'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::FILE_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::UPDATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::FILE_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::DELETE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],

            ['model_id' => ModelTableSeeder::SNIPPET_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::CREATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::SNIPPET_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::READ),   'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::SNIPPET_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::UPDATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::SNIPPET_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::DELETE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],

            ['model_id' => ModelTableSeeder::SITE_CONFIG_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::CREATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::SITE_CONFIG_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::READ),   'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::SITE_CONFIG_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::UPDATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ['model_id' => ModelTableSeeder::SITE_CONFIG_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::DELETE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],

//            ['model_id' => ModelTableSeeder::FORM_CREATOR_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::CREATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
//            ['model_id' => ModelTableSeeder::FORM_CREATOR_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::READ),   'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
//            ['model_id' => ModelTableSeeder::FORM_CREATOR_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::UPDATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
//            ['model_id' => ModelTableSeeder::FORM_CREATOR_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::DELETE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],

            ['model_id' => ModelTableSeeder::MODEL_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::CREATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::USER)],
            ['model_id' => ModelTableSeeder::MODEL_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::READ),   'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::USER)],
            ['model_id' => ModelTableSeeder::MODEL_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::UPDATE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::USER)],
            ['model_id' => ModelTableSeeder::MODEL_ID, PermissionTableSeeder::idFromName(PermissionTableSeeder::DELETE), 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::USER)],
        ]);
    }
}
