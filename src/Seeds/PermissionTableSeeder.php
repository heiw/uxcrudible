<?php

namespace Heiw\Uxcrudible\Seeds;

use Heiw\Uxcrudible\Models\Role;
use Heiw\Uxcrudible\Models\User;
use Heiw\Uxcrudible\Traits\IdFromNameSeederTrait;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    const CREATE = 'create';
    const READ = 'read';
    const UPDATE = 'update';
    const DELETE = 'delete';
    const RESTORE = 'restore';
    const FORCE_DELETE = 'force delete';

    /**
     * Returns id from (constant) name value
     * @param $value
     * @param string $name
     * @return false
     */
    public static function idFromName($value, $name = 'name') {
        $entry = DB::table('permission')->where($name, '=', $value)->first();
        if (isset($entry->id)) {
            return $entry->id;
        } else {
            return false;
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission')->insertOrIgnore([
            ['name' => self::CREATE, 'display' => 'Create'],
            ['name' => self::READ, 'display' => 'View'],
            ['name' => self::UPDATE, 'display' => 'Update'],
            ['name' => self::DELETE, 'display' => 'Delete'],
            ['name' => self::RESTORE, 'display' => 'Restore'],
            ['name' => self::FORCE_DELETE, 'display' => 'Force Delete']
        ]);
/*
        factory(User::class, 50)->create()->each(function ($user) {
            $user->roles()->attach(Role::where('name', 'user')->first());

            // insert relationship here.
        });
*/
        /*
        DB::table('user') ->insertOrIgnore([
            'name' => str_random(10),
            'email' => str_random(10).'@example.com',
            'password' => bcrypt('secret')
        ]);*/
    }


}
