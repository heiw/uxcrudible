<?php

namespace Heiw\Uxcrudible\Seeds;

use App\Models\Scheme;
use App\Models\User;
use App\Models\UserTPD;
use Heiw\Uxcrudible\Models\Page;
use Heiw\Uxcrudible\Models\Role;
use Heiw\Uxcrudible\Seeds\PermissionTableSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Heiw\Uxcrudible\Seeds\RoleTableSeeder;

class EmailTemplateComponentSeeder extends Seeder
{
    const FREE_TEXT_TEMPLATE_ID = 1;
    const DELIVERY_REPORT_ID = 2;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $models = [
//            ['id' => ModelTableSeeder::NEW_PRACTICE_VISIT_REPORT_ID,  'namespace' => 'App\\Reports\\',   'name' => 'NewPracticeVisitReport'],
        ];

        $model_translation = [
//            [	'model_id' => ModelTableSeeder::PRACTICE_VISIT_ID, 'locale' => 'en', 'display' => __('Practice Visit', [], 'en'),         ],

//            [	'model_id' => ModelTableSeeder::PRACTICE_VISIT_ID, 'locale' => 'cy', 'display' => __('Practice Visit', [], 'cy')],
        ];

        DB::table('model')->insertOrIgnore($models);
        DB::table('model_translation')->insertOrIgnore($model_translation);

//        \Heiw\Uxcrudible\Seeds\ModelTableSeeder::addFullRights(ModelTableSeeder::PRACTICE_VISIT_ID, \Heiw\Uxcrudible\Seeds\RoleTableSeeder::idFromName(RoleTableSeeder::SYS_ADMIN));

        $pages = [
//            [	'id' => PageTableSeeder::PRACTICE_VISIT_ID, 	'tree_order' => '/4',   'display_order' => '2', 'page_type' => '',  'icon' => 'fas fa-clinic-medical', 	'parent_id' => '1', 	'public' => '0',],
        ];

        $page_translation = [
//            ['page_id' => PageTableSeeder::PRACTICE_VISIT_ID,    'path' => '/' . Str::kebab(__('slugs.practice-visit', [], 'en')),   'name' => __('Practice Visit', [], 'en'),    'slug' => Str::kebab(__('slugs.practice-visit', [], 'en')),    'title' => __('Practice Visit', [], 'en'),   'description' => '',    'content' => '',    'locale' => 'en'],
//            ['page_id' => PageTableSeeder::PRACTICE_VISIT_ID,    'path' => '/' . Str::kebab(__('slugs.practice-visit', [], 'cy')),   'name' => __('Practice Visit', [], 'cy'),    'slug' => Str::kebab(__('slugs.practice-visit', [], 'cy')),    'title' => __('Practice Visit', [], 'cy'),   'description' => '',    'content' => '',    'locale' => 'cy'],
        ];
        DB::table('page')->insertOrIgnore($pages);
        DB::table('page_translation')->insertOrIgnore($page_translation);

        // Add rights to view homepage.
        DB::table('page_role')->insertOrIgnore([
//            ['page_id' => PageTableSeeder::HOME_ID,  'role_id' => RoleTableSeeder::TRAINING_PROGRAMME_DIRECTOR_ID]
        ]);

        // Add rights to view pages
//        DB::table('page_role')->insertOrIgnore(['page_id' => PageTableSeeder::PRACTICE_VISIT_ID, 'role_id' => RoleTableSeeder::TRAINING_PROGRAMME_DIRECTOR_ID]);

        DB::table('model_page')->insertOrIgnore([
//            ['model_id' => ModelTableSeeder::NEW_PRACTICE_VISIT_REPORT_ID,    'page_id' => PageTableSeeder::PRACTICE_VISIT_ID, 'order' => 3],
        ]);


        $snippet = [
//            ['id' => self::GUIDANCE_SURGICAL_KIT,           'name' => 'guidance_surgical_kit', 'page_id' => PageTableSeeder::PRACTICE_VISIT_ID],
        ];

        $snippet_translation = [
//            ['snippet_id' => self::GUIDANCE_SURGICAL_KIT,             'content' => '<h2>Recommended Dental Foundation Training surgical tray equipment list</h2><ul><li>Scalpel Handle (if not disposable)</li><li>11 and 15 Blades</li><li>Mitchell’s Trimmer</li><li>Some sort of Periosteal Elevator (eg. Howarths or Wards Periosteal Elevator)</li><li>Variety of retractors, Austin’s, Minnesota, Lax</li><li>Toothed Forceps (e.g. Adson’s)</li><li>Non Toothed Forceps (e.g. Adson’s)</li><li>Artery Forceps (Clip or Spencer Wells)</li><li>Needle Holder (Crile Wood Holder)</li><li>Surgical Scissors (Iris)</li><li>Surgical straight handpiece with external irrigation (not an acrylic straight handpiece)</li><li>3’0 Vicryl Rapide Sutures</li></ul><p><strong>All</strong> practices<strong> </strong>should have a variety of luxators, couplands, warwick james and cryers.</p>', 	'locale' => 'en', 	],

//            ['snippet_id' => self::GUIDANCE_SURGICAL_KIT,             'content' => __('<h2>Recommended Dental Foundation Training surgical tray equipment list</h2><ul><li>Scalpel Handle (if not disposable)</li><li>11 and 15 Blades</li><li>Mitchell’s Trimmer</li><li>Some sort of Periosteal Elevator (eg. Howarths or Wards Periosteal Elevator)</li><li>Variety of retractors, Austin’s, Minnesota, Lax</li><li>Toothed Forceps (e.g. Adson’s)</li><li>Non Toothed Forceps (e.g. Adson’s)</li><li>Artery Forceps (Clip or Spencer Wells)</li><li>Needle Holder (Crile Wood Holder)</li><li>Surgical Scissors (Iris)</li><li>Surgical straight handpiece with external irrigation (not an acrylic straight handpiece)</li><li>3’0 Vicryl Rapide Sutures</li></ul><p><strong>All</strong> practices<strong> </strong>should have a variety of luxators, couplands, warwick james and cryers.</p>'), 	'locale' => 'cy', 	],
        ];

        DB::table('snippet')->insertOrIgnore($snippet);
        DB::table('snippet_translation')->insertOrIgnore($snippet_translation);

        $email_template = [
            ['id' => self::FREE_TEXT_TEMPLATE_ID, 'name' => '[free text template]', 'include_snippet_signature' => 1],
            ['id' => self::DELIVERY_REPORT_ID, 'name' => 'delivery_report', 'include_snippet_signature' => 1],
        ];

        $email_template_translation = [
            ['email_template_id' => self::FREE_TEXT_TEMPLATE_ID,   'subject' => '', 'content' => '<p><strong>Dear :recipient,</strong><br><br>', 	'locale' => 'en', 	],
            ['email_template_id' => self::DELIVERY_REPORT_ID,   'subject' => __('Delivery report :email_subject', [], 'en'), 'content' => __('<p>Dear :recipient,&nbsp;</p><p>The following email has been sent.&nbsp;</p><p>Number of errors on sent: :sent_errors</p><p>:sent_results</p><p>Template: :email_name</p><p>Subject: :email_subject</p><p>Content: :email_content</p>', [], 'en'), 	'locale' => 'en', 	],

            ['email_template_id' => self::FREE_TEXT_TEMPLATE_ID,   'subject' => '', 'content' => __('<p><strong>Dear :recipient,</strong><br><br>'), 	'locale' => 'cy', 	],
            ['email_template_id' => self::DELIVERY_REPORT_ID,   'subject' => __('Delivery report :email_subject', [], 'cy'), 'content' => __('<p>Dear :recipient,&nbsp;</p><p>The following email has been sent.&nbsp;</p><p>Number of errors on sent: :sent_errors</p><p>:sent_results</p><p>Template: :email_name</p><p>Subject: :email_subject</p><p>Content: :email_content</p>', [], 'cy'), 	'locale' => 'cy', 	],
        ];

        DB::table('email_template')->insertOrIgnore($email_template);
        DB::table('email_template_translation')->insertOrIgnore($email_template_translation);

        //\Heiw\Uxcrudible\Seeds\ModelTableSeeder::addAdminRights(ModelTableSeeder::USER_TPD_ID, \RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN));

/*
        if (User::where('email', '=', 'tpd@example.com')->count() == 0) {
            // Add test TPD user
            $user = new UserTPD();
            $user->title = 'T';
            $user->firstname = 'P';
            $user->lastname = 'D';
            $user->email = 'tpd@example.com';
            $user->save();
            $role = Role::where('name', '=', 'tpd')->get()->first();
            $user->roles()->attach($role);
            $scheme = Scheme::find(3);
            DB::table('role_scheme_user')->insertOrIgnore([
                ['role_id' => $role->id, 'scheme_id' => 3, 'user_id' => $user->id],
                ['role_id' => $role->id, 'scheme_id' => 5, 'user_id' => $user->id]
            ]);
            $user->save();
        }

        DB::table('practice')
            ->where('address', 'LIKE', '%CF23%')
            ->orWhere('address', 'LIKE', '%CF10%')
            ->orWhere('address', 'LIKE', '%CF5%')
            ->update(['scheme_id' => 5])
        ;

        DB::table('practice')
            ->where('address', 'LIKE', '%CF6%')
            ->update(['scheme_id' => 3])
        ;
        DB::table('practice')
            ->where('address', 'LIKE', '% NP%')
            ->update(['scheme_id' => 2])
        ;

*/



    }
}
