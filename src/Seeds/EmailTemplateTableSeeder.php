<?php

namespace Heiw\Uxcrudible\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailTemplateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $email_template = [
            [
                'id' => 1,
                'name' => '[free text template]',
                'include_snippet_signature' => 1
            ],
            [
                'id' => 2,
                'name' => 'delivery_report',
                'include_snippet_signature' => 1
            ]
        ];

        $email_template_translation = [
            [
                'email_template_id' => 1,
                'subject' => '',
                'content' => '<p>' . __('Dear :recipient', [], 'en') . ',</p>',
                'locale' => 'en'
            ],
            [
                'email_template_id' => 2,
                'subject' => __('Delivery report :email_subject', [], 'en'),
                'content' => '<p>' . __('Dear :recipient', [], 'en') . ',</p><p>' .
                    __('The following email has been sent.', [], 'en') . '</p><p>' .
                    __('Number of errors on sent: :sent_errors', [], 'en') . '</p><p>:sent_results</p><p>' .
                    __('Template: :email_name', [], 'en') . '</p><p>' .
                    __('Subject: :email_subject', [], 'en') . '</p>',
                'locale' => 'en'
            ],
            [
                'email_template_id' => 1,
                'subject' => '',
                'content' => '<p>' . __('Dear :recipient', [], 'cy') . ',</p>',
                'locale' => 'cy'
            ],
            [
                'email_template_id' => 2,
                'subject' => __('Delivery report :email_subject', [], 'cy'),
                'content' => '<p>' . __('Dear :recipient', [], 'cy') . ',</p><p>' .
                    __('The following email has been sent.', [], 'cy') . '</p><p>' .
                    __('Number of errors on sent: :sent_errors', [], 'cy') . '</p><p>:sent_results</p><p>' .
                    __('Template: :email_name', [], 'cy') . '</p><p>' .
                    __('Subject: :email_subject', [], 'cy') . '</p>',
                'locale' => 'cy'
            ],
        ];

        DB::table('email_template')->insertOrIgnore($email_template);
        DB::table('email_template_translation')->insertOrIgnore($email_template_translation);
    }
}
