<?php

namespace Heiw\Uxcrudible\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormCreatorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $form_creator = [

        ];

        DB::table('form_creator')->insert($form_creator);
    }
}
