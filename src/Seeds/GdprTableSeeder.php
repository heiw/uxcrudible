<?php

namespace Heiw\Uxcrudible\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GdprTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $gdpr = [
            ['id' => '1',  'active' => 0, 'date' => date('Y-m-d', strtotime('-1 year'))],
            ['id' => '2',  'active' => 1, 'date' => null],
        ];

        $gdpr_translation = [
            [	'gdpr_id' => '1',  'locale' => 'en', 'name' => 'Archived GDPR Notification', 'content' => 'By using, registering or submitting any information on our site you consent to the collection, use and transfer of your information under the terms of the Privacy Notice.' ],
            [	'gdpr_id' => '2',  'locale' => 'en', 'name' => 'Sample GDPR Notification', 'content' => 'By using, registering or submitting any information on our site you consent to the collection, use and transfer of your information under the terms of the Privacy Notice.' ],

            [	'gdpr_id' => '1',  'locale' => 'cy', 'name' => 'Archived GDPR Notification', 'content' => 'By using, registering or submitting any information on our site you consent to the collection, use and transfer of your information under the terms of the Privacy Notice.' ],
            [	'gdpr_id' => '2',  'locale' => 'cy', 'name' => 'Sample GDPR Notification', 'content' => 'By using, registering or submitting any information on our site you consent to the collection, use and transfer of your information under the terms of the Privacy Notice.' ],
        ];

        DB::table('gdpr')->insertOrIgnore($gdpr);
        DB::table('gdpr_translation')->insertOrIgnore($gdpr_translation);
    }
}
