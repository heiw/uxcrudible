<?php
namespace Heiw\Uxcrudible\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SnippetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $snippet = [
            ['id' => 1, 'name' => 'outdated_browser_alert']
        ];

        $snippet_translation = [
            ['snippet_id' => '1', 'content' => '<p>You are currently using an outdated browser. Although this site will work on IE11 it will not provide the best usability experience. We recommend using a modern browser such as the latest version of <i class=\"fab fa-chrome\"></i> Chrome, <i class=\"fab fa-edge\"></i> Edge or <i class=\"fab fa-firefox-browser\"></i> FireFox. If you don\'t have this on your machine, please contact your IT department and ask them to install it for you.</p>', 	'locale' => 'en', 	],

            ['snippet_id' => '1', 'content' => __('<p>You are currently using an outdated browser. Although this site will work on IE11 it will not provide the best usability experience. We recommend using a modern browser such as the latest version of <i class=\"fab fa-chrome\"></i> Chrome, <i class=\"fab fa-edge\"></i> Edge or <i class=\"fab fa-firefox-browser\"></i> FireFox. If you don\'t have this on your machine, please contact your IT department and ask them to install it for you.</p>', [], 'cy'), 	'locale' => 'cy', 	],
        ];

        DB::table('snippet')->insertOrIgnore($snippet);
        DB::table('snippet_translation')->insertOrIgnore($snippet_translation);
    }
}
