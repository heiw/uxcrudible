<?php

namespace Heiw\Uxcrudible\Seeds;

use Heiw\Uxcrudible\Models\Model;
use Heiw\Uxcrudible\Models\Page;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PageTableSeeder extends Seeder
{
    const HOME_ID = 1;
    const ADMIN_ID = 2;
    const SETTINGS_ID = 3;
    const USERS_ID = 4;
    const LOGIN_ID = 5;

    public static function addModelToPage($modelId, $pageId, $order = null) {
        if (Page::find($pageId) && Model::find($modelId)) {
            DB::table('model_page')->insertOrIgnore([
                ['model_id' => $modelId, 'page_id' => $pageId, 'order' => $order]
            ]);
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $pages = [
            [	'id' => self::HOME_ID, 	'tree_order' => '/1', 	'display_order' => '1', 'page_type' => '', 	'icon' => 'fas fa-home', 	'parent_id' => self::HOME_ID, 	'public' => '0',],
            [	'id' => self::ADMIN_ID, 	'tree_order' => '/9',   'display_order' => '9', 'page_type' => '', 	'icon' => 'fas fa-cogs', 	'parent_id' => self::HOME_ID, 	'public' => '0',],
            [	'id' => self::LOGIN_ID, 	'tree_order' => '/10',  'display_order' => '10', 'page_type' => '', 	'icon' => 'fas fa-sign-in-alt', 	'parent_id' => self::HOME_ID, 	'public' => '2',],
            [	'id' => self::SETTINGS_ID, 	'tree_order' => '/9/9', 'display_order' => '9', 'page_type' => '', 	'icon' => 'fas fa-sliders-h', 	'parent_id' => self::ADMIN_ID, 	'public' => '0',],
            [	'id' => self::USERS_ID, 	'tree_order' => '/9/3', 'display_order' => '3', 'page_type' => '', 	'icon' => 'fas fa-users', 	'parent_id' => self::ADMIN_ID, 	'public' => '0',],
//            [	'id' => '6', 	'tree_order' => '/9/5', 'display_order' => '5', 'page_type' => '', 	'icon' => 'fas fa-file-import', 	'parent_id' => self::ADMIN_ID, 	'public' => '0',],
//            [	'id' => '8', 	'tree_order' => '/5', 	'display_order' => '5', 'page_type' => '', 	'icon' => 'far fa-envelope', 	'parent_id' => self::HOME_ID, 	'public' => '0',],
        ];
        $page_translation = [
            [	'page_id' => self::HOME_ID, 	'path' => '/', 	             'name' => 'Home', 	            'slug' => '/', 	        'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'en'],
            [	'page_id' => self::ADMIN_ID, 	'path' => '/admin', 	     'name' => 'Administration', 	'slug' => 'admin', 	    'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'en'],
            [	'page_id' => self::SETTINGS_ID, 'path' => '/admin/settings', 'name' => 'Settings', 	        'slug' => 'settings', 	'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'en'],
            [	'page_id' => self::USERS_ID, 	'path' => '/admin/users', 	 'name' => 'Users', 	        'slug' => 'users', 	    'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'en'],
            [	'page_id' => self::LOGIN_ID, 	'path' => '/login', 	     'name' => 'Login', 	        'slug' => 'login', 	    'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'en'],

            [	'page_id' => self::HOME_ID, 	'path' => '/', 	             'name' => __('Home', [], 'cy'), 	            'slug' => '/', 	        'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'cy'],
            [	'page_id' => self::ADMIN_ID, 	'path' => '/' . Str::kebab(__('slugs.admin', [], 'cy')), 	     'name' => __('Administration', [], 'cy'), 	'slug' =>  Str::kebab(__('slugs.admin', [], 'cy')), 	    'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'cy'],
            [	'page_id' => self::SETTINGS_ID, 'path' => '/' . Str::kebab(__('slugs.admin', [], 'cy')) . '/' . Str::kebab(__('slugs.settings', [], 'cy')), 'name' => __('Settings', [], 'cy'), 	        'slug' => Str::kebab(__('slugs.settings', [], 'cy')), 	'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'cy'],
            [	'page_id' => self::USERS_ID, 	'path' => '/' . Str::kebab(__('slugs.admin', [], 'cy')) . '/' . Str::kebab(__('slugs.users', [], 'cy')), 	 'name' => __('Users', [], 'cy'), 	        'slug' => Str::kebab(__('slugs.users', [], 'cy')), 	    'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'cy'],
            [	'page_id' => self::LOGIN_ID, 	'path' => '/' . Str::kebab(__('slugs.login', [], 'cy')), 	     'name' => __('Login', [], 'cy'), 	        'slug' => Str::kebab(__('slugs.login', [], 'cy')), 	    'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'cy'],
//            [	'page_id' => self::LOGIN_ID, 	'path' => '/admin/import', 	 'name' => 'Import', 	        'slug' => 'import', 	'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'en'],
//            [	'page_id' => '8', 	'path' => '/messages', 	     'name' => 'Messages', 	        'slug' => 'messages', 	'title' => '', 	'description' => '', 	'content' => '', 	'locale' => 'en'],
        ];


        DB::table('page')->insertOrIgnore($pages);
        DB::table('page_translation')->insertOrIgnore($page_translation);

        // Add full rights to all pages for sys admin and admin.
        foreach($pages as $page) {
            DB::table('page_role')->insertOrIgnore([
                ['page_id' => $page['id'],  'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::SYS_ADMIN)],
                ['page_id' => $page['id'],  'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ]);
        }

        // Add read rights to all translatable pages for translator.
        foreach([self::HOME_ID, self::ADMIN_ID, self::SETTINGS_ID] as $pageId) {
            DB::table('page_role')->insertOrIgnore([
                ['page_id' => $pageId,  'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::TRANSLATOR)],
            ]);
        }

        DB::table('model_page')->insertOrIgnore([
            ['model_id' => ModelTableSeeder::USER_ID,           'page_id' => self::USERS_ID,],

            ['model_id' => ModelTableSeeder::EMAIL_TEMPLATE_ID, 'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::EMAIL_LOG_ID,      'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::MODEL_ID,          'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::PAGE_ID,           'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::PERMISSION_ID,     'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::ROLE_ID,           'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::SITE_CONFIG_ID,    'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::TRANSLATION_ID,    'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::FILE_ID,           'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::SNIPPET_ID,        'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::GDPR_ID,           'page_id' => self::SETTINGS_ID,],
            ['model_id' => ModelTableSeeder::FORM_CREATOR_ID,   'page_id' => self::SETTINGS_ID,],
        ]);
    }
}
