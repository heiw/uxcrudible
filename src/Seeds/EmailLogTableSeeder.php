<?php

namespace Heiw\Uxcrudible\Seeds;

use Heiw\Uxcrudible\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailLogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $email_log = [

        ];

        DB::table('email_log')->insertOrIgnore($email_log);
    }
}
