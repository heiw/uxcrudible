<?php

namespace Heiw\Uxcrudible\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiteConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('site_config')->insertOrIgnore([
            [
                'id' => '1',
                'name' => 'admin_email_address',
                'value' => 'no-reply@cardiff.ac.uk',
                'description' => 'Admin From Email Address',
            ],
            [
                'id' => '2',
                'name' => 'admin_reply_to_email_address',
                'value' => 'marijn.kampf@wales.nhs.uk',
                'description' => 'Admin Reply-To Email Address',
            ],
            [
                'id' => '3',
                'name' => 'admin_email_name',
                'value' => 'Marijn Kampf',
                'description' => 'Admin Email Name',
            ],
            [
                'id' => '4',
                'name' => 'email_template_signature',
                'value' => 'Best wishes, Digital Team',
                'description' => 'Email template signature',
            ],
            ['id' => '5', 'name' => 'app_abbr', 'value' => 'Uxcrud', 'description' => 'Application Abbreviation',],
            ['id' => '6', 'name' => 'app_name', 'value' => 'Uxcrudible', 'description' => 'Application Full Name',],
            ['id' => '7', 'name' => 'app_icon', 'value' => 'fas fa-magnet', 'description' => 'Application Icon',],
            ['id' => '8', 'name' => 'app_version', 'value' => date('YmdHis'), 'description' => 'Application Version',],


        ]);
    }
}
