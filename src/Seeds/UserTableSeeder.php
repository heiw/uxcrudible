<?php

namespace Heiw\Uxcrudible\Seeds;

use Heiw\Uxcrudible\Models\Role;
use Heiw\Uxcrudible\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sysAdmins = [
            [
                'title' => 'Mr',
                'firstname' => 'Marijn',
                'lastname' => 'Kampf',
                'email' => 'marijn.kampf@wales.nhs.uk',
            ],
            [
                'title' => 'Mr',
                'firstname' => 'Jay',
                'lastname' => 'Beavan',
                'email' => 'jay.beavan@wales.nhs.uk',
            ],
            [
                'title' => 'Mr',
                'firstname' => 'Gareth',
                'lastname' => 'Peters',
                'email' => 'gareth.peters@wales.nhs.uk',
            ],
            [
                'title' => 'Mr',
                'firstname' => 'Miguel',
                'lastname' => 'Cossio',
                'email' => 'miguel.cossio@wales.nhs.uk',
            ],
            [
                'title' => 'Mr',
                'firstname' => 'Jake',
                'lastname' => 'Plumley',
                'email' => 'jake.plumley@wales.nhs.uk',
            ],
            [
                'title' => 'Mr',
                'firstname' => 'Michael',
                'lastname' => 'Pritchard',
                'email' => 'michael.pritchard3@wales.nhs.uk',
            ],
            [
                'title' => 'Mr',
                'firstname' => 'Farid',
                'lastname' => 'Zouheir',
                'email' => 'farid.zouheir@wales.nhs.uk',
            ],
            [
                'title' => 'Mr',
                'firstname' => 'Shane',
                'lastname' => 'Wilson',
                'email' => 'shane.wilson@wales.nhs.uk',
            ],
        ];
        DB::table('user') ->insertOrIgnore($sysAdmins);

        // Add admin and sys admin roles to users.
        $sysAdmins = User::whereIn('email', array_column($sysAdmins, 'email'))->get();
        $roles = Role::whereIn('name', [RoleTableSeeder::SYS_ADMIN, RoleTableSeeder::ADMIN])->get();
        foreach($sysAdmins as $sysAdmin) {
            $sysAdmin->roles()->syncWithoutDetaching($roles);
        }

        $translators = [
            [
                'title' => 'Mrs',
                'firstname' => 'Tra',
                'lastname' => 'Nslator',
                'email' => 'heiwtestmk02@wales.nhs.uk',
                'password' => Hash::make('secret'),
            ]
        ];

        DB::table('user') ->insertOrIgnore($translators);

        // Add translation role to test user.
        $translators = User::whereIn('email', ['heiwtestmk02@wales.nhs.uk'])->get();
        $roles = Role::whereIn('name', [RoleTableSeeder::TRANSLATOR])->get();
        foreach($translators as $translator) {
            $translator->roles()->syncWithoutDetaching($roles);
        }
    }


}
