<?php

namespace Heiw\Uxcrudible\Seeds;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            SiteConfigTableSeeder::class,
            RoleTableSeeder::class,
            UserTableSeeder::class,
            PermissionTableSeeder::class,
            ModelTableSeeder::class,
            PageTableSeeder::class,
            SnippetTableSeeder::class,
            GdprTableSeeder::class,
            EmailTemplateTableSeeder::class,
            EmailLogTableSeeder::class,
            //TranslationSourceTableSeeder::class,
        ]);

        if (class_exists(\Heiw\Nhs\Seeds\DatabaseSeeder::class)) {
            $this->call([
                \Heiw\Nhs\Seeds\DatabaseSeeder::class
            ]);
        }
    }
}
