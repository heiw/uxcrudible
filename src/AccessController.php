<?php

namespace Heiw\Uxcrudible;


use App\Http\Controllers\Controller;
use App\Models\User;
use Heiw\Uxcrudible\Services\Menu;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class AccessController extends Controller
{

    public function __construct(Request $request) {
        $test = null;
        /*
        $menu = resolve(Menu::class);
        $currentPage = $menu->currentPage();
        if($currentPage->public) {
            $this->middleware(['ability:view,,true' ]);
        }        //dd(Auth::id());
        //dd(Auth::User()->id);
//        $this->middleware([ 'ability:view,,true' ]);
/*
        $authorised_user = User::where('id', Auth::User()->id)->whereHas('role', function ($query) use ($request) {
            $query->whereHas('permission', function ($query) use ($request) {
                $query->where('routes', $request->route('uri'));
                });
        })->firstOrFail();

        if ($authorised_user) {
            $permission = Permission::where('routes', $request->route('uri'))->findOrFail();

            $this->middleware(['ability:' . $authorised_user->role()->name . ',' . $permission->name . ',true']);
        } else {
            // user is not authorised. Do what ever you want
        }*/
    }

    public function page($uri = null) {
        //dd(request());
        $menu = resolve(Menu::class);
        $currentPage = $menu->currentPage();
        abort_unless($currentPage, 404, __('Page not found'));
        return view($currentPage->getBlade(), compact(['currentPage']));
    }
}