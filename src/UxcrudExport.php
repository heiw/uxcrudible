<?php

namespace Heiw\Uxcrudible;

use App\UxcrudModel;
use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\Group;
use Heiw\Uxcrudible\Form\Fields\Header;
use Heiw\Uxcrudible\Models\Page;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class UxcrudExport implements FromArray, WithHeadings, ShouldAutoSize, WithColumnFormatting, WithEvents
{
    use Exportable, RegistersEventListeners;

    /** @var Builder */
    protected $builder;
    protected $model;
    protected $uxcrudController;
    protected $headings = [];

    public function __construct($builder, $model, $uxcrudController) {
        $this->builder = $builder;
        $this->model = $model;
        $this->uxcrudController = $uxcrudController;
    }

    public function array(): array {
        // Create empty list in case there are no entries
        $items = [];
        foreach($this->builder->get() as $entry) {
            $item = null;
            foreach($this->uxcrudController->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::EXPORT, true, [], $entry) as $field) {
                switch(get_class($field)) {
                    case Actions::class:
                    case Header::class:
                        break;
                    default:
                        // Check whether blade exists in export folder.
                        $blade = $field->getBlade("uxcrud::forms.fields.export.");
                        // If not default to blade from show folder.
                        if (!View::exists($blade)) {
                            $blade = $field->getBlade("uxcrud::forms.fields.show.");
                        }
                        $item[] = trim(view($blade, [
                            'displayValue' => $field->extractDisplayValue($entry),
                            'field' => $field,
                            'entry' => $entry,
                            'model' => $this->model,
                            'controller' => $this->uxcrudController,
                            // 'dtRefreshId' => $datatableId,
                            // 'parentModel' => $parentModel
                        ])->render());
                        break;
                }
            }
            $items[] = $item;
        }
        return $items;
    }

    /**
     * Specify headings for export
     * @return array
     */
    public function headings(): array {
        foreach($this->uxcrudController->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::EXPORT, true)  as $field) {
            switch(get_class($field)) {
                case Actions::class:
                case Header::class:
                    break;
                default:
                    $this->headings[] = $field->label;
                    break;
            }
        }
        return $this->headings;
    }

    /**
     * Specify headings for export
     * @return array
     */
    public function columnFormats(): array {
        $columnFormats = [];
        foreach($this->uxcrudController->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::EXPORT, true)  as $field) {
            switch(get_class($field)) {
                case Actions::class:
                case Header::class:
                    break;
                default:
                    if ($field->exportColumnFormat() !== null) {
                        $key = array_search($field->label, $this->headings);
                        $columnFormats[Coordinate::stringFromColumnIndex($key+1)] = $field->exportColumnFormat();
                    }
                    break;
            }
        }
        return $columnFormats;
    }

    public static function afterSheet(AfterSheet $event) {
        $event->sheet->freezePane('A2');
        $sheetRange = 'A1:' . $event->sheet->getHighestColumn() . $event->sheet->getHighestRow();
        $event->sheet->setAutoFilter($sheetRange);
        $alignment = $event->sheet->getStyle($sheetRange)->getAlignment();
        $alignment->setWrapText(true);
        $alignment->setVertical('top');
    }


}
