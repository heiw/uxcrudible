<?php

namespace Heiw\Uxcrudible\Form\Fields;

class HasMany extends Select {
    public $type = 'hasMany';

    public $blades = ['hasMany', 'select'];

    public function __construct($name, $label = null) {

        $this->method = $name;
        parent::__construct($name, $label);

        // Default to allowing multiple entries.
        // To override add setMultiple(false) when defining field.
        $this->setMultiple();

        return $this;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        return null;
    }

    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
/* commenting this out enables viewing in Children[], Roles[] in /admin/settings#page
    public function extractDisplayValue($object) {
        if (is_callable($this->method)) {
            return $this->method;
        } else {
            // Return label as variable from object if present
            if (isset($object->{$this->method()}) && isset($object->{$this->method()}->{$this->labelName})) {
                return $object->{$this->method()}->{$this->labelName};
                // Return value from options by key
            } else {
                $options = $this->options($object);
                $item = $options->firstWhere($this->valueName, $object->{$this->name});
                if ($item) {
                    return $item->{$this->labelName};
                } else {
                    return parent::extractDisplayValue($object);
                }
            }
        }
    }
*/

}