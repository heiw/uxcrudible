<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HasOneLinked extends Select {
    public $type = 'hasOneLinked';

    public $blades = ['hasOneLinked', 'hasOne', 'select'];

    public $linkedName = null;
    public $linkedTo = null;
    //public $linkId = null;

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);

        // Add _id to end of name
        $this->name = $this->name . "_id";

        return $this;
    }

    /**
     * The attribute to link to
     * @param string $name      Name of the attribute
     * @param null $linkedName  Column name to link to. Defaults to $name with _id appended.
     * @return $this
     */
    public function linkTo($name, $linkedName = null) {
        $this->linkedTo = Str::snake($name);
        if (!is_null($linkedName)) {
            $this->linkedName = $linkedName;
        } else {
            $this->linkedName = Str::snake($name) . "_id";
        }

        // Add default empty option
        $this->preOptions[] = (object)[
            $this->valueName => '',
            $this->labelName => '',
            $this->linkedName => ''
        ];

        return $this;
    }
}
