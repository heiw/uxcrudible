<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Classes\BasicEnum;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Nexmo\Insights\Basic;

class Pages extends Page {
    use CachedObjectValue;

    /**
     * @var array List of edit / show blades to review. First entry found is used.
     */
    public $blades = ['pages', 'page', 'group'];

    public $classes = ['uxcrud-pages'];

    /**
     * When set this is the name of the page/tab that will be opened. If set to
     * null the first page/tab will be shown.
     * @var null
     */
    public $defaultTab = null;

    /**
     * Show pages as steps instead of tabs.
     * @param bool $stepped
     * @return $this
     */
    public function stepped($stepped = true) {
        if ($stepped) {
            $this->addClass('uxcrud-steps');
        } else {
            $this->removeClass('uxcrud-steps');
        }
        return $this;
    }

    /**
     * Set default tab to open.
     * @param $id   String id of tab element to open including #.
     * @return $this
     */
    public function defaultTab($id) {
        $this->defaultTab = $id;
        return $this;
    }
}
