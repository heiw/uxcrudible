<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Classes\BasicEnum;
use Heiw\Uxcrudible\Models\Model;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Nexmo\Insights\Basic;

class Group extends FormField {
    use CachedObjectValue;

    /**
     * @var array List of edit / show blades to review. First entry found is used.
     */
    public $blades = ['group'];

    public $fields = [];
    public $isGroup = true;

    public function __construct($name = null, $label = null) {
        if (is_null($name)) $name = 'n' . Str::random();
        parent::__construct($name, $label);
        $this->removeSearch();
        $this->removeClass('row');
        return $this;
    }

    public function prepend(...$fields) {
        if (isset($fields[0]) && count($fields) == 1) {
            $fields = $fields[0];
        }
        foreach($fields as $key => $field) {
            $fields[$key]->parent = $this;
        }
        $this->fields = array_merge($fields, $this->fields);
        return $this;
    }

    public function append(...$fields) {
        if (isset($fields[0]) && is_array($fields[0]) && count($fields) == 1) {
            $fields = $fields[0];
        }
        foreach($fields as $key => $field) {
            $fields[$key]->parent = $this;
        }
        $this->fields = array_merge($this->fields, $fields);
        return $this;
    }

    /**
     * Remove one or more fields from group.
     * @param string|string[] $names Name or array of names of fields to remove
     * @return $this
     */
    public function remove($names) {
        if (!is_array($names)) {
            $names = [$names];
        }
        //
        $namedKeys = array_combine(array_column($this->fields, 'name'), array_keys($this->fields));
        foreach($names as $name) {
            if (isset($namedKeys[$name])) {
                unset($this->fields[$namedKeys[$name]]);
            }
        }
        return $this;
    }

    /**
     * Alias for append($fields)
     * @param $fields
     * @return Group
     */
    public function setFields(...$fields) {
        return $this->append(...$fields);
    }

    public function fields($model = null) {
        return $this->fields;
    }

    /**
     * @param Uxcrudible\Form\Fields\Permission $view
     * @param bool $includeGroups   Set to true to include grouped fields directly.
     * @param string|string[] $pages   Id or id's of the pages to include. All pages if set to null.
     * @return mixed
     */
    public function getFields($view = \Heiw\Uxcrudible\Form\Fields\Permission::READ, $includeGroups = false, $pages = [], $model = null) {
        return self::cacheResult("getFields" . $view . $includeGroups, function() use ($view, $includeGroups, $pages, $model) {
            $fields = [];
            foreach ($this->fields($model) as $field) {
                if (isset($field->permissions[$view]) && $field->permissions[$view] !== false) {
                    if ($includeGroups && $field->isGroup) {
                        if (empty($pages) || get_class($field) !== Page::class || (get_class($field) === Page::class  && in_array($field->slug(), $pages))) {
                            $fields = array_merge($fields, $field->getFields($view, $includeGroups, $pages, $model));
                        }
                    } else {
                        $fields[] = $field;
                    }
                }
            }
            return $fields;
        });
    }

    /***
     * @param string|string[] $classes Fully qualified class string or array of
     * classes of the fields to return.
     * @param bool $includeGroups Set to true to include grouped fields directly.
     * @param null|Model $model Model to which the fields apply to.
     * @return array
     */
    public function getFieldsOfType($classes, $includeGroups = true, $model = null) {
        $fields = [];
        if (is_string($classes)) {
            $classes = [$classes];
        }
        foreach($this->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::READ, $includeGroups, [], $model) as $field) {
            if (in_array(get_class($field), $classes)) {
                $fields[] = $field;
            }

        }
        return $fields;
    }

    /***
     * @param string $name Name to search for.
     * @param bool $includeGroups Set to true to include grouped fields directly.
     * @param string $column    Column to search in, defaults to name.
     * @param null|Model $model Model to which the fields apply to.
     * @return FormField|false  Returns FormField when found or false if not.
     */
    public function getFieldByName(string $name, $includeGroups = true, $model = null, $column = 'name') {
        foreach($this->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::READ, $includeGroups, $model) as $field) {
            if ($field->{$column} == $name) {
                return $field;
            }
        }
        return false;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        // Don't generate migration line for field.
        return null;
    }
}
