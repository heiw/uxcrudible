<?php

namespace Heiw\Uxcrudible\Form\Fields;

class ReorderRow extends Number {
    public $type = 'reorderRow';

    public $blades = ['reorderRow', 'number', 'text'];
    
    public function __construct(...$arguments) {
        parent::__construct(...$arguments);
        $this->defaultSort();
        return $this;
    }
}