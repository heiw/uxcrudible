<?php

namespace Heiw\Uxcrudible\Form\Fields;

class TextArea extends FormField {
    public $rows = 3;
    public $blades = ['textarea', 'text'];
    
    public $condensed = true;


    /**
     * Set number of visible text lines for the field.
     * @param int $rows
     * @return $this
     */
    public function rows($rows) {
        $this->rows = $rows;
        return $this;
    }
    
    /**
     * Set number of visible text lines for the field.
     * @param int $condensed
     * @return $this
     */
    public function condensed($condensed) {
        $this->condensed = $condensed;
        return $this;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        return "\t\t\t\$table->mediumText('{$this->name}')" .
            $this->dataTableColumnNullable() . ";\n";
    }
}