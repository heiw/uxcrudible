<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\UxcrudController;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationRuleParser;

class Likert extends Enum
{
    public $type = 'likert';

    public $reverseScoring = false;

    public $blades = ['likert', 'enum', 'select'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
    }

    /**
     * Reverse scoring for array. Note this is a quick hack and need to be called
     * before ->setEnumOptions(...)
     * @param bool $reverse
     * @return $this
     * @throws \Exception
     */
    public function reverseScoring($reverse = true) {
        if (count($this->preOptions) > 0) {
            throw new \Exception(__METHOD__ . " needs to be called before ->setEnumOptions(...) is.");
        }
        $this->reverseScoring = $reverse;
        return $this;
    }

    public function setEnumOptions($array, $valueName = null, $labelName = null) {
        // Start array at 1 for Likert scores
        if ($this->reverseScoring) {
            $start = count($array);
            $end = 1;
        } else {
            $start = 1;
            $end = count($array);
        }
        $array = array_combine(range($start, $end), array_values($array));
        parent::setEnumOptions($array, $valueName, $labelName);
        return $this;
    }

    public function setFormCreatorField($fieldDefinition) {
        if (isset($fieldDefinition->reverseScoring) && $fieldDefinition->reverseScoring) {
            $this->reverseScoring();
        }
        parent::setFormCreatorField($fieldDefinition);
        return $this;
    }

}
