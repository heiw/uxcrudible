<?php

namespace Heiw\Uxcrudible\Form\Fields;

class CKEditor extends TextArea {
    public $rows = 3;

    public $classes = ['uxcrud-ckeditor', 'uxcrud-line', 'form-group', 'row'];
    public $blades = ['ckeditor', 'textarea', 'text'];
//
//    public function __construct($name, $label = null) {
//        parent::__construct($name, $label);
//
//        // Set to long/wide version as default.
//        // To override and use ->labelLong(false)
//        $this->labelLong();
//
//        return $this;
//    }
}
