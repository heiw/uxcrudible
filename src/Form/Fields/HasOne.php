<?php

namespace Heiw\Uxcrudible\Form\Fields;

class HasOne extends Select {
    public $type = 'hasOne';

    public $blades = ['hasOne', 'select'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);

        // Add _id to end of name
        $this->name = $this->name . "_id";

        return $this;
    }
}