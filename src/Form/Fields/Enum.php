<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\UxcrudController;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationRuleParser;

class Enum extends Select
{
    public $type = 'enum';

    public $blades = ['enum', 'select'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        $this->setModel(null)
            ->noRemoteData()
        ;
    }

    public function setEnumOptions($array, $valueName = null, $labelName = null) {
        if (!is_null($valueName)) {
            $this->valueName = $valueName;
        }
        if (!is_null($labelName)) {
            $this->labelName = $labelName;
        }
        $this->prependOptionArray($array);

        // Add empty please select option if validation rule includes null.
        $rules = ValidationRuleParser::parse($this->validation);
        if (($this->type !== self::TYPE_RADIO) && in_array('Nullable', $rules)) {
            $this->addPrependOption(null, __('-'));
        }
        if (!is_null($this->pleaseSelectLabel)) {
            $this->addPrependOption(null, $this->pleaseSelectLabel);
        }

        return $this;
    }

    /**
     * Add radio button and setEnumOptions from field definition.
     * @param \stdClass $fieldDefinition Field definition from json.
     * @return $this
     */
    public function setFormCreatorField($fieldDefinition) {
        parent::setFormCreatorField($fieldDefinition);
        $locale = resolve(\Heiw\Uxcrudible\Classes\Locale::class);
        if (isset($fieldDefinition->radio) && $fieldDefinition->radio) {
            $this->radio();
        }
        $this->setEnumOptions($fieldDefinition->setEnumOptions->{$locale->getLocale()});
        return $this;
    }


    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractDisplayValue($object) {
        if (is_callable($this->method)) {
            return $this->method;
        } else {
            // Return label as variable from object if present
            if (isset($object->{$this->method()}) && isset($object->{$this->method()}->{$this->labelName})) {
                return $object->{$this->method()}->{$this->labelName};
                // Return value from options by key
            } else {
                $options = $this->options($object);
                $item = $options->firstWhere($this->valueName, '===', $object->{$this->name});
                if ($item) {
                    return $item->{$this->labelName};
                } else {
                    return parent::extractDisplayValue($object);
                }
            }
        }
    }
}
