<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Classes\BasicEnum;
use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Heiw\Uxcrudible\UxcrudController;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Nexmo\Insights\Basic;

class Page extends Group {
    use CachedObjectValue;

    /**
     * @var array List of edit / show blades to review. First entry found is used.
     */
    public $blades = ['page', 'group'];

    public function slug() {
        return Str::slug($this->name);
    }

    public function formNavigation($ability) {
        $buttons = '<div class="row"><div class="col-12">';
        $fieldNames = [];

        $previousKey = null;
        $currentKey = null;
        $nextKey = null;

        // Set previous, current and next keys by looping over fields whilst
        // taking into account hidden fields.
        foreach($this->parent->fields as $key => $field) {
            if(!$field->isHidden($ability->permission)) {
                // Set next key if current key is set and next key hasn't been set yet.
                if (!is_null($currentKey) && is_null($nextKey)) {
                    $nextKey = $key;
                }
                // If field name matches set current key
                if ($field->name == $this->name) {
                    $currentKey = $key;
                }
                // Set previousKey if not the same as the current key
                if (!is_null($currentKey) && is_null($nextKey) && $key !== $currentKey) {
                    $previousKey = $key;
                }
                $fieldNames[$key] = $field->name;
            }
        }

        if (!is_null($previousKey)) {
            $field = $this->parent->fields[$previousKey];
            //$buttons .= $field->isReadOnly($ability);
            switch ($ability->action) {
                case UxcrudController::ABILITY_VIEW:
                case UxcrudController::ABILITY_CREATE:
                    $buttons .= "<a class='btn btn-info m-1 float-left' data-toggle='uxcrud-page' href='#{$field->slug()}' title='" .
                        Helpers::escapeQuotes(__('Return to :pageName', ['pageName' => $field->getLabel()])) .
                        "'><i class='fas fa-angle-left'></i> " . __('Previous') . " {$field->getLabel()} <i class='{$field->icon}'></i> </a>";
                    break;
                default:
                    $buttons .= "<button class='ajax-action-uxcrud-submit btn btn-info m-1 float-left' data-uxcrud-form-page data-uxcrud-continue-to='" . Str::slug($field->name) . "' title='" .
                        Helpers::escapeQuotes(__('Store progress and return to :pageName', ['pageName' => $field->getLabel()])) .
                        "'><i class='fas fa-angle-left'></i> " . __('Save and go back') . "</button>";
                    break;
            }
        }
        if (!is_null($nextKey)) {
            $field = $this->parent->fields[$nextKey];
            switch ($ability->action) {
                case UxcrudController::ABILITY_VIEW:
                case UxcrudController::ABILITY_CREATE:
                    $buttons .= "<a class='btn btn-info m-1 float-right' data-toggle='uxcrud-page' href='#{$field->slug()}' title='" .
                        Helpers::escapeQuotes(__('Continue to :pageName', ['pageName' => $field->getLabel()])) .
                        "'><i class='{$field->icon}'></i> " . __('Next') . " {$field->getLabel()} <i class='fas fa-angle-right'></i> </a>";
                    break;
                default:
                    $buttons .= "<button class='ajax-action-uxcrud-submit btn btn-info m-1 float-right' data-uxcrud-form-page data-uxcrud-continue-to='" . Str::slug($field->name) . "' title='" .
                        Helpers::escapeQuotes(__('Store progress and continue to :pageName',
                            ['pageName' => $field->getLabel()])) .
                        "'> " . __('Save and continue') . " <i class='fas fa-angle-right'></i></button>";
                    break;
            }
        }
        $buttons .= '</div></div>';
        return $buttons;
    }
}
