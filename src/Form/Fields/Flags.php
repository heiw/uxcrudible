<?php

namespace Heiw\Uxcrudible\Form\Fields;

class Flags extends TextArea {
    public $type = 'flags';

    public $blades = ['flags', 'read-only', 'textarea'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        return $this;
    }

    /**
     * Format regular display value using HTML
     * @param $object
     * @param null $locale
     * @return array|mixed|string|null
     * @throws \Throwable
     */
    public function extractValue($object, $locale = null) {
        $values = json_decode(parent::extractValue($object, $locale));
        return view($this->getBlade("uxcrud::forms.fields.edit.extract-value-"), ['values' => $values, 'field' => $this])->render();
    }

    /**
     * Basic value extract from json.
     * @param $object
     * @param null $locale
     * @return mixed|null
     */
    public function extractExportValue($object, $locale = null) {
        $values = json_decode(parent::extractValue($object, $locale), true);
        return $values;
    }
}
