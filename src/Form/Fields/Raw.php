<?php

namespace Heiw\Uxcrudible\Form\Fields;

class Raw extends FormField {
    public $type = 'raw';

    public $blades = ['raw', 'text'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        $this->removeSearch();
        return $this;
    }
}