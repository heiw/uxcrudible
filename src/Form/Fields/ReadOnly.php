<?php

namespace Heiw\Uxcrudible\Form\Fields;

class ReadOnly extends FormField {
    public $type = 'read-only';

    public $blades = ['read-only', 'text'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        $this->removeSearch();
        return $this;
    }
}