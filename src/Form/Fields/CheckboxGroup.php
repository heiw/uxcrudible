<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Helpers;
use Illuminate\Http\Request;

/**
 * Class CheckboxGroup  Group of checkboxes with same settings (including validation).
 * Results are stored in json as string of the selected values.
 * Therefore validation should use required instead of accepted.
 * @package Heiw\Uxcrudible\Form\Fields
 */
class CheckboxGroup extends FormField {
    public $blades = ['checkbox-group', 'checkbox', 'text'];

    public const LOWER_ROMAN = 'lower-roman';
    public const UPPER_ROMAN = 'upper-roman';
    public const DECIMAL = 'decimal';
    public const LOWER_ALPHA = 'lower-alpha';
    public const UPPER_ALPHA = 'upper-alpha';
    public const DISC = 'disc';
    public const NONE = 'none';

    public $checkboxes;
    protected $listStyle = self::DECIMAL;

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);

        $this->filter = new Select($name);
        $this->filter
            ->addPrependOption(1, __('checked'))
            ->addPrependOption(0, __('unchecked'))
            ->addPrependOption(null, __('all'))
            ->setMultiple(false);
    }

    public function checkboxes($checkboxes) {
        $this->checkboxes = $checkboxes;

        // If post method set update the request value to boolean.
        if (!empty($_POST)) {
            $values = [];
            if (is_array($this->checkboxes)) {
                foreach ($this->checkboxes as $key => $value) {
                    $values[$key] = (isset($_POST[$this->name][$key])) ? $value : null;
                }
            }

            request()->merge([$this->name => $values]);
            //
        }

        return $this;
    }

    public function getCheckboxes($object = null) {
        $values = [];
        // Extract values from database.
        if (isset($object->{$this->name})) {
            $values = array_values(json_decode($object->{$this->name}, true));
        }

        $results = [];
        foreach($this->checkboxes as $key => $checkbox) {
            $result = (object)[
                "name" => "{$this->name}[$key]",
                "label" => $checkbox,
                // Use stored string to determine which values are checked.
                "value" => in_array($checkbox, $values, true),
                "readonly" => false
            ];
            $results[$key] = $result;
        }
        return $results;
    }

    public function listStyle($style) {
        $this->listStyle = $style;
        return $this;
    }

    /** Return key for line */
    public function getKey($number) {
        $number+=1;
        switch($this->listStyle) {
            case self::LOWER_ROMAN:
                return Helpers::decimal_to_roman($number, false);
                break;
            case self::UPPER_ROMAN:
                return Helpers::decimal_to_roman($number);

                break;
            case self::LOWER_ALPHA:
                return Helpers::decimal_to_alpha($number, false);

                break;
            case self::UPPER_ALPHA:
                return Helpers::decimal_to_alpha($number);
                break;
            case self::DISC:
                return "&#9679;";
                break;
            case self::NONE:
                return '';
                break;
            case self::DECIMAL:
            default:
                return $number;
                break;
        }
    }


    /**
     * @param mixed $ability Action permission (create/update)
     * @param $object           object Original object
     * @param $field
     * @param $locales
     * @param $request
     * @param $rules
     * @param null $messages
     * @param bool $includeWarningMessages Include data-warning messages when set to true.
     * @return void Array of rules
     */
    public function getValidationRules($ability, $object, $field, $locales, $request, &$rules, &$messages = null, $includeWarningMessages = false) {
        $messageKey = $field->name;
        $count = count($this->getCheckboxes());
        foreach($this->checkboxes as $key => $checkbox) {
            $rules["{$messageKey}.{$key}"] = str_replace('$id', $object->id, $field->validation);
        }
        if (is_array($field->messages)) {
            foreach ($field->messages as $rule => $message) {
                foreach($this->checkboxes as $key => $checkbox) {
                    $messages["{$messageKey}.{$key}.{$rule}"] = str_replace([':key', ':name', ':label'], [$this->getKey($key), "{$this->name}[$key]", "label" => $checkbox], $message);
                }
            }
        }
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        return "        \$table->text('{$this->name}')->nullable();\n";
    }
}