<?php

namespace Heiw\Uxcrudible\Form\Fields;

class MultiDate extends MultiText {
    public $blades = ['multi-date', 'multi-text', 'date'];

    public $type = 'date';
}