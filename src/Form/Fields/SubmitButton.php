<?php

namespace Heiw\Uxcrudible\Form\Fields;

/**
 * Placeholder for submit button in addition to buttons in modal footer.
 * Class SubmitButton
 * @package Heiw\Uxcrudible\Form\Fields
 */
class SubmitButton extends FormField {
    public $type = 'header';

    public $blades = ['submitButton', 'text'];

    public function __construct($name = 'submitButton', $label = null) {
        parent::__construct($name, $label);
        $this->addClass('uxcrud-submit-button')
            ->hideOnIndex()
            ->removeSearch()
        ;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        // Don't generate migration line for field.
        return null;
    }
}