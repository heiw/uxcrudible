<?php

namespace Heiw\Uxcrudible\Form\Fields;

class Label extends FormField {
    public $type = 'label';

    public $blades = ['label', 'text'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        $this->removeSearch();
        $this->hideOnIndex();
        $this->hideOnExport();
        return $this;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        // Don't generate migration line for field.
        return null;
    }
}
