<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Form\Filters\WhereClause;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class DateTime extends Date {
    public $type = 'date';

    public $blades = ['datetime', 'date', 'text'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        if (isset($_POST[$name]['date']) && isset($_POST[$name]['time'])) {
            request()->merge([$name => $_POST[$name]['date'] . " " . $_POST[$name]['time']]);
//            request()->request->remove($name);
//            request()->request->add([$name => $_POST[$name]['date'] . " " . $_POST[$name]['time']]);
        }

        $this->filter = new Select($name);
        $this->filter
            ->addPrependOption(null, __('all'))
            ->addPrependOption(WhereClause::BETWEEN, __('between'))
            ->addPrependOption(WhereClause::NOT_BETWEEN, __('not between'))
            ->addPrependOption(WhereClause::NOT_NULL, __('with dates'))
            ->addPrependOption(WhereClause::NULL, __('without dates'))
            ->setMultiple(false);
    }

    public function exportColumnFormat() {
        return NumberFormat::FORMAT_DATE_DATETIME;
    }

    public function validation($validation, $messages = null) {
        $this->validation = trim('date_format:Y-m-d H:i|' . $validation, '|');
        $this->messages = $messages;
        $this->messages['date_format'] = __('The :name is not a valid date time (dd/mm/yyyy hh:mm)', ['name' => $this->getPrettyName()]);
        return $this;
    }

    public function extractDate($value) {
        return substr($value, 0,10);
    }

    public function extractTime($value, $format = 'H:i:s') {
        $dateTime = new \DateTime($value);
        return $dateTime->format($format);
    }

    /**
     * Overwrite function if value needs to be altered for pivot tables.
     * Ensure user friendly data is cast to database format.
     * @param $value
     * @return mixed
     */
    public function castPivotTableValue($value) {
        $check = \DateTime::createFromFormat('d/m/Y G:i', $value);
        if ($check !== false) {
            $value = $check->format('Y-m-d G:i');
        }

        return $value;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        return "            \$table->dateTime('{$this->name}')" .
            $this->dataTableColumnNullable() . ";\n";
    }
}
