<?php

namespace Heiw\Uxcrudible\Form\Fields;

class Header extends FormField {
    public $type = 'header';

    public $blades = ['header', 'text'];

    public $level = 2;

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        $this->removeSearch();
        return $this;
    }

    public function level($level) {
        $this->level = $level;
        return $this;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        // Don't generate migration line for field.
        return null;
    }
}