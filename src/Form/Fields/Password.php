<?php

namespace Heiw\Uxcrudible\Form\Fields;

class Password extends Text {
    public $type = 'password';

    public $blades = ['password', 'text'];

    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractValue($object, $locale = null) {
		// Don't show value for password
        return null;
    }
}