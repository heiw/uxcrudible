<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\UxcrudController;

class FileDownload extends FormField {
    public $blades = ['fileDownload', 'text'];
}