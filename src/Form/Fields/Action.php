<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Illuminate\Support\Str;

class Action {
    const TYPE_LINK = 'a';
    const TYPE_FORM = 'form';

    public $name = null;
    public $type = self::TYPE_LINK; // Either a or form
    public $label = null;
    public $blade = null;

    public function __construct($name = 'action', $blade = null) {
        $this->name = Str::snake($name);
        $this->blade = $blade;
        return $this;
    }

    public static function create(...$arguments)
    {
        $field = new static(...$arguments);
        return $field;
    }

    public function getBlade() {
        if (view()->exists($this->blade)) {
            return $this->blade;
        }
    }
}