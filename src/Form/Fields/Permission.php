<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Classes\BasicEnum;

abstract class Permission extends BasicEnum {
    const CREATE = 1;
    const READ = 2;
        const INDEX = "2.1";
        const VIEW = "2.2";
        const EXPORT = "2.3";
    const UPDATE = 3;
    const DELETE = 4;
    const RESTORE = 5;
    const FORCE_DELETE = 6;
}
