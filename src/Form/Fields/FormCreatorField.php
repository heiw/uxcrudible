<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Classes\Locale;
use Heiw\Uxcrudible\Models\Model;

class FormCreatorField extends Group {
    /**
     * @var \Heiw\Uxcrudible\Models\FormCreator
     */
    private $formCreator;

    /**
     * Initialise Form Creator and Model object
     * @param \Heiw\Uxcrudible\Models\FormCreator $formCreator  Form creator to use
     * @param $object   Model to extract values from.
     * @return $this
     */
    public function formCreator(\Heiw\Uxcrudible\Models\FormCreator $formCreator, $model) {
        $this->formCreator = $formCreator;

        $formDefinition = json_decode($this->formCreator->form);
        if (is_null($formDefinition)) {
            $this->append(new Label('error', 'Invalid form'));
        } else {
            $locale = resolve(\Heiw\Uxcrudible\Classes\Locale::class);
            foreach ($formDefinition as $name => $fieldDefinition) {
                $field = new $fieldDefinition->class("{$this->name}[{$name}]", $fieldDefinition->label->{$locale->getLocale()});
                $field->setFormCreatorField($fieldDefinition);
                $this->append($field);
            }
        }

        $this->extractValue($model);

        return $this;
    }

    public function extractValue($object, $locale = null) {
        if (isset($object->{$this->name})) {
            $values = json_decode($object->{$this->name}, true);
            if (is_array($values)) {
                foreach ($values as $key => $value) {
                    // Check if the value is a number, if so cast to float or int by
                    // adding 0. This is needed for Select / Enum fields as they
                    // compare on type.
                    if (is_numeric($value)) {
                        $value += 0;
                    }
                    $object->{"$this->name[$key]"} = $value;
                }
            }
            return parent::extractValue($object, $locale);
        }
        return null;
    }

    /**
     * @param Uxcrudible\Form\Fields\Permission $view
     * @param bool $includeGroups   Set to true to include grouped fields directly.
     * @param string|string[] $pages   Id or id's of the pages to include. All pages if set to null.
     * @return mixed
     */
    public function getFields($view = \Heiw\Uxcrudible\Form\Fields\Permission::READ, $includeGroups = false, $pages = [], $model = null) {
        switch($view) {
            // Return current FormCreatorField on create or update.
            case Permission::CREATE:
            case Permission::UPDATE:
                return [$this];
                break;
            default:
                // Return fields contained by FormCreatorField for other views.
                return parent::getFields($view, $includeGroups, $pages, $model);
                break;
        }
    }

    /**
     * Get the value of a field that is within the FormCreatorField based on
     * it's full name.
     * @param $name
     * @param $model
     * @return mixed|null
     */
    public function getSubFieldValue($name, $model) {
        [$array, $key] = explode("[", trim($name, ']'));
        if (isset( $model->{$array}[$key])) {
            return $model->{$array}[$key];
        } else {
            return null;
        }
    }

    /**
     * Set the field values into an attribute before storing.
     * @param string $ability
     * @param Object $model
     * @param $attributes
     * @param $rules
     * @param $messages
     * @return $this|void
     */
    public function onBeforeEvent($ability, $model, &$attributes, &$rules, &$messages) {
        switch($ability) {
            case Permission::CREATE:
//            case Permission::UPDATE:
                $attributes[$this->name] = json_encode($attributes[$this->name]);
                break;
        }

        if (!is_null($model)) {
            $values = json_decode($model->{$this->name}, true);
            if (is_array($values)) {
                foreach ($values as $key => $value) {
                    unset($model->{"$this->name[$key]"});
                }
            }
        }

        parent::onBeforeEvent($ability, $model, $attributes, $rules, $messages);
        return $this;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        // TODO MK Change to json type once all sites are upgraded to DB that
        // supports it.
        return "\t\t\t\$table->mediumText('{$this->name}')" .
            $this->dataTableColumnNullable() . ";\n";
    }
}
