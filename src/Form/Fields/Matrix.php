<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class Matrix extends FormField {
    public $type = 'matrix';

    public $blades = ['matrix'];

    public $modelMatrix = null;

    /**
     * @var string Type to use for the matrix entries
     */
    public $entryType = 'checkbox';
    public $labels = [];
    public $models = [];
    public $columnNames = [];
    public $getValuesMethod = null;
    public $options = [];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        return $this;
    }

    public function setEntryType($entryType) {
        $this->entryType = $entryType;
        return $this;
    }

    public function getEntryTypeBlade($path) {
        if (view()->exists($path . $this->entryType)) {
            return $path . $this->entryType;
        }
        return null;
    }

    /**
     * @param $modelSelf    string current model
     * @param $modelX       string Model for the horizontal axis
     * @param $modelY       string Model for the vertical axis
     * @param $modelMatrix  string Model that defines the three matrix values
     * @return $this
     */
    public function setModels($modelSelf, $modelX, $modelY, $modelMatrix) {
        $this->modelMatrix = $modelMatrix;
        $this->models = [$modelSelf, $modelX, $modelY];
        return $this;
    }

    public function models() {
        return $this->models;
    }

    public function modelMatrix() {
        return $this->modelMatrix;
    }

    public function modelSelf() {
        return $this->models[0];
    }

    public function modelX() {
        return $this->models[1];
    }

    public function modelY() {
        return $this->models[2];
    }

//    /**
//     * @param $methodX   string Method called for the values for the horizontal axis
//     * @param $methodY   string Method called for the values for the vertical axis
//     * @return $this
//     */
//    public function setMethods($methodX, $methodY) {
//        $this->methods = [$methodX, $methodY];
//        return $this;
//    }
//
//    public function methods() {
//        if (empty($this->methods)) {
//            foreach($this->models as $key => $model) {
//                $this->methods[$key] = Str::camel(class_basename($model));
//            }
//        }
//        return $this->methods;
//    }
//
//    public function methodX() {
//        return $this->methods()[0];
//    }
//
//    public function methodY() {
//        return $this->methods()[1];
//    }

    /**
     * @param $optionX   string Method for the options for the horizontal axis
     * @param $optionY   string Method for the options for the vertical axis
     * @return $this
     */
    public function setOptions($optionX, $optionY) {
        $this->options = [$optionX, $optionY];
        return $this;
    }

    /**
     * Retrieve options from the following possible methods
     *      1. $object->{$this->optionsMethod}
     *      2. $model::{'all'.Str::plural($this->model)}
     *      3. $model::entries()
     *      4. $model::orderBy($model::getOrderBy())->get();
     * @return array
     */
    public function options() {
        if (empty($this->options)) {
            foreach($this->models() as $key => $model) {
                // Use method name prepended with all.
                $classBasename = Str::studly(Str::plural(class_basename($model)));
                if (method_exists($this->modelMatrix, 'all' . $classBasename)) {
                    $this->options[$key] = 'all' . $classBasename;
                } elseif (method_exists($this->models[$key], 'entries')) {
                    $this->models[$key] = $this->models[$key]::entries();
                } else {
                    // Call ordered list of all Model::get()
                    $this->options[$key] = function () use ($key) {
                        return $this->models[$key]::orderBy($this->models[$key]::getOrderBy())->get();
                    };
                }
            }
        }
        return $this->options;
    }

    public function optionX() {
        return $this->options()[0];
    }

    public function optionY() {
        return $this->options()[1];
    }

    protected function optionFunctionById($model, $optionKey) {
        if (is_callable($this->options()[$optionKey])) {
            return $this->options()[$optionKey]();
        } else {
            return $model->$this->options()[$optionKey];
        }

    }

    public function optionsX($model) {
        return $this->optionFunctionById($model, 1);
    }

    public function optionsY($model) {
        return $this->optionFunctionById($model, 2);
    }

    /**
     * @param $labelX   string Label for the horizontal axis
     * @param $labelY   string Label for the vertical axis
     * @return $this
     */
    public function setLabels($labelX, $labelY) {
        $this->labels = [$labelX, $labelY];
        return $this;
    }

    public function labels() {
        if (empty($this->labels)) {
            foreach($this->models as $key => $model) {
                $this->labels[$key] = class_basename($model);
            }
        }
        return $this->labels;
    }

    public function labelX() {
        return $this->labels()[0];
    }

    public function labelY() {
        return $this->labels()[1];
    }

    /**
     * @param $columnNameX   string ColumnName for the horizontal axis
     * @param $columnNameY   string ColumnName for the vertical axis
     * @return $this
     */
    public function setColumnNames($columnNameX, $columnNameY) {
        $this->columnNames = [$columnNameX, $columnNameY];
        return $this;
    }

    public function columnNames() {
        if (empty($this->columnNames)) {
            foreach($this->models as $key => $model) {
                $this->columnNames[$key] = Str::snake(Str::singular(class_basename($model))) . "_id";
            }
        }
        return $this->columnNames;
    }

    public function columnNameX() {
        return $this->columnNames()[0];
    }

    public function columnNameY() {
        return $this->columnNames()[1];
    }

    public function setValuesMethod($method) {
        $this->getValuesMethod = $method;
    }

    public function getValuesMethod() {
        if (empty($this->getValuesMethod)) {
            $this->getValuesMethod = class_basename($this->modelMatrix());
        }
        return $this->getValuesMethod;
    }

    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractValue($object, $locale = null) {
        $values = [];
        $columnX = Str::snake(class_basename($this->modelX())) . "_id";
        $columnY = Str::snake(class_basename($this->modelY())) . "_id";
        foreach($object->{$this->getValuesMethod()} as $value) {
            $idX = $value->$columnX;
            $idY = $value->$columnY;
            if (!isset($values[$idX])) {
                $values[$idX] = [];
            }
            $values[$idX][$idY] = $value;
        };
        return $values;
    }

    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractDisplayValue($object) {
        return $this->extractValue($object);
    }

    /**
     * Allow (de)selecting all checkboxes in a row by clicking on the row label.
     * @param bool $enable  Set to false to
     * @return $this
     */
    public function setQuickSelectRow($enable = true) {
        $this->addClass('quick-select-row', !$enable);
        return $this;
    }

    /**
     * Allow (de)selecting all checkboxes in a column by clicking on the row label.
     * @param bool $enable  Set to false to
     * @return $this
     */
    public function setQuickSelectColumn($enable = true) {
        $this->addClass('quick-select-column', !$enable);
        return $this;
    }

    /**
     * Allow (de)selecting all checkboxes by clicking on the row or column label.
     * @param bool $enable  Set to false to
     * @return $this
     */
    public function setQuickSelect($enable = true) {
        $this->setQuickSelectColumn($enable);
        $this->setQuickSelectRow($enable);
        return $this;
    }

    /**
     * Overwrite this function to customise saving of the relationship
     * @param \Heiw\Uxcrudible\UxcrudModel $model
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Relations\Relation[] $relation
     * @param $type
     * @return bool return true if relation has been updated, return false otherwise.
     */
    public function updateRelations(\Heiw\Uxcrudible\UxcrudModel $model, \Illuminate\Http\Request $request, $relation, $type) {
        $objects = [];
        $columns = $this->columnNames();
        if (is_array($request->{Str::snake($relation)})) {
            foreach ($request->{Str::snake($relation)} as $valueX => $valuesY) {
                if (is_array($valuesY)) {
                    foreach ($valuesY as $valueY => $valueObject) {
                        $object = new $this->modelMatrix();
                        $object->{$columns[0]} = $model->id;
                        $object->{$columns[1]} = $valueX;
                        $object->{$columns[2]} = $valueY;
                        $objects[] = $object;
                    }
                }
            }
        }
        $model->{$relation}()->delete();
        $model->{$relation}()->saveMany($objects);
        return true;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        return null;
    }
}