<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Services\Menu;

class GoogleReCaptcha extends FormField {
    public $blades = ['googlerecaptcha', 'text'];

    public function __construct($name = 'google_recaptcha', $label = null) {
        parent::__construct($name, $label);
        if (is_null($label)) {
            $this->label = '';
        }
        $menu = resolve(Menu::class);
        $page = $menu->currentPage();
        if ($page) {
            $page->addJS("https://www.google.com/recaptcha/api.js");
        }

        $messages["{$this->name}.required"] = __("Please check the `I'm not a robot` box");
    }

    /**
     * Verified ReCaptcha code with Google.
     * @param $ability
     * @param $object
     * @param $field
     * @param $locales
     * @param $request
     * @param $rules
     * @param null $messages
     * @param null $includeWarningMessages
     * @return bool|void
     */
    public function getValidationRules($ability, $object, $field, $locales, $request, &$rules, &$messages = null, $includeWarningMessages = null) {
        $response = $request->get('g-recaptcha-response');
        if (!empty($response)) {
            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $content = http_build_query(
                array(
                    'secret' => env('RECAPTCHA_SECRET_KEY'),
                    'response' => $_POST["g-recaptcha-response"]
                )
            );
            $options = array(
                'http' => array(
                    'header' => "Content-Type: application/x-www-form-urlencoded\r\n" .
                        "Content-Length: " . strlen($content) . "\r\n" .
                        "User-Agent:MyAgent/1.0\r\n",
                    'method' => 'POST',
                    'content' => $content
                ),
                'ssl' => [
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ]
            );
            $context = stream_context_create($options);
            $verify = file_get_contents($url, false, $context);
            $captcha_success = json_decode($verify);
        }
        if (!isset($captcha_success) || $captcha_success->success == false) {
            $this->validation('required');
            parent::getValidationRules($ability, $object, $field, $locales, $request,$rules, $messages, $includeWarningMessages);
        } else {
            return true;
        }
    }
}