<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Classes\BasicEnum;
use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Expression;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationRuleParser;
use Illuminate\View\View;
use Nexmo\Insights\Basic;

abstract class FormField {
    const READ_ONLY = 3;

    public $parent = null;
    public $type = 'text';
    public $name = null;
    public $label = null;
    /**
     * @var string  label for on index
     */
    private $indexLabel = null;

    public $labelRight = false;
    public $labelLong = false;

    /**
     * Used to override css classes for label element.
     * @var null|[]|int|string
     */
    protected $labelClasses = null;
    /**
     * Used to override css classes for element that contains the input field.
     * @var null|[]|int|string
     */
    protected $fieldClasses = null;

    protected $markdown = false;
    public $value = null;
    public $icon = null;
    public $prependItems = [];
    public $appendItems = [];
    public $canEdit = true;
    public $classes = ['uxcrud-line', 'form-group', 'row'];
    public $dataAttributes = [];
    public $attributes = [];
    public $isGroup = false;
//    public $canSubmit = true;

    /**
     * Select expression to use if using query in report
     * @var null
     */
    public $selectExpressions = [];

    /**
     * @var array List of edit / show blades to review. First entry found is used.
     */
    public $blades = [];
    public $overrideBladeLocation = false;

    public $filter = true;
    public $sorting = true;
    public $defaultFilterValue = null;
    public $defaultSort = null;

    public $search = true;
    public $validation = 'nullable';
    public $messages = [];

    public $permissions = [
        Permission::CREATE => true,
        Permission::READ => true,
        Permission::INDEX => true,
        Permission::VIEW => true,
        Permission::UPDATE => true,
        Permission::DELETE => true,
        Permission::EXPORT => true
    ];

    public function __construct($name, $label = null) {
        $this->name = Str::snake($name);
        if (!is_null($label)) {
            $this->label = $label;
        } else {
            $this->label = Str::snake($name, ' ');
        }
        if (empty($this->blades)) {
            $this->blades[] = $this->type;
        }
        return $this;
    }

    public static function create(...$arguments)
    {
        $field = new static(...$arguments);
        return $field;
    }

    public function getPrettyName() {
        return Str::title(str_ireplace(["_id", "_"], ["", " "], $this->name));
    }
    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractValue($object, $locale = null) {
        if (is_null($locale) || !$object->hasTranslationAttribute($this->name)) {
            return $object->{$this->name};
        } else {
            if (is_null($object->translate($locale))) {
                return null;
            }
            return $object->translate($locale)->{$this->name};
        }
    }

    /**
     * Move the label to the right
     * @param bool $labelRight
     * @return $this
     */
    public function labelRight($labelRight = true) {
        $this->labelRight = $labelRight;
        return $this;
    }

    /**
     * Use a long label by changing the widths for the bootstrap grid used for
     * the field label and value.
     * @param bool $labelLong
     * @return $this
     */
    public function labelLong($labelLong = true) {
        $this->labelLong = $labelLong;
        return $this;
    }

    /**
     * Alias for setGridElementClasses
     * @param mixed $width      Width of the label (either int value that will
     * be automatically replace with col-sm-# or string with class name. For
     * multiple classes an array of values can be provided.)
     * @param mixed $fieldWidth Width of the field (see width parameter)
     * @return $this
     */
    public function labelWidth($width, $fieldWidth) {
        return $this->setGridElementClasses($width, $fieldWidth);
    }

    /**
     * Set element classes for label and field.
     * @param mixed $labelClasses       Class(es) to use for label element.
     * Either int value that will be automatically replace with col-sm-# or
     * string with class name. For multiple classes an array of values can be
     * provided.
     * @param null|mixed $fieldClasses  Class(es) to use for field element.
     * @return $this
     */
    public function setGridElementClasses($labelClasses, $fieldClasses = null) {
        $this->labelClasses = $labelClasses;
        if (is_null($fieldClasses) && is_numeric($labelClasses)) {
            // If field classes not set base it size on standard grid size.
            $this->fieldClasses = 12 - $labelClasses;
        } else {
            $this->fieldClasses = $fieldClasses;
        }
        return $this;
    }

    /**
     * Retrieve classes for grid container width. Either returns string values
     * provided or if a number formats it according to col-sm-#.
     * @param string $element
     * @return string
     */
    protected function getGridElementClasses($labelSize, $longLabelSize, $element = 'label') {
        $values = $this->{"{$element}Classes"};
        // If value not set default to value passed from template by using either
        // regular label on long label size.
        if (is_null($values)) {
            $values = !$this->labelLong ? $labelSize : $longLabelSize;
        }
        if (!is_array($values)) $values = [$values];
        $result = "";
        foreach($values as $value) {
            if (is_numeric($value)) {
                $result .= " col-sm-$value";
            } else {
                $result .= $value;
            }
        }
        return trim($result);
    }

    /**
     * @return string
     */
    public function getGridLabelClasses($labelSize, $longLabelSize) {
        return $this->getGridElementClasses($labelSize, $longLabelSize, 'label');
    }

    /**
     * @return string
     */
    public function getGridFieldClasses($labelSize, $longLabelSize) {
        return $this->getGridElementClasses($labelSize, $longLabelSize, 'field');
    }

    /**
     * Label to use for index (if different from regular label)
     * @param string $indexLabel
     * @return $this
     */
    public function indexLabel($indexLabel) {
        $this->indexLabel = $indexLabel;
        return $this;
    }

    /**
     * Enable markdown for the field's label
     * @param boolean $markdown Set to true (default) to enabled markdown for this field.
     * @return $this
     */
    public function markdown($markdown = true) {
        $this->markdown = $markdown;
        return $this;
    }

    /**
     * Alias for markdown.
     * @param boolean $markdown Set to true (default) to enabled markdown for this field.
     * @return FormField
     */
    public function parsedown($markdown = true) {
        return $this->markdown($markdown);
    }

    public function getLabel() {
        if ($this->markdown) {
            return parsedown($this->label);
        } else {
            return $this->label;
        }
    }

    /**
     * Return label for index. Either specific index label if present or regular label.
     * @return string|null
     */
    public function getIndexLabel() {
        if (!is_null($this->indexLabel)) {
            return $this->indexLabel;
        } else {
            return $this->getLabel();
        }
    }


    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractDisplayValue($object) {
        return $this->extractValue($object);
    }

    public function extractFilterValue() {
        $request = request()->query(trim($this->name, '[]'));
        return $request;
    }

    public static function queryFilterValue() {
        return request()->query('uxcrud-filter');
    }

    public function getBlade($path) {
        if ($this->overrideBladeLocation !== false) {
            $path = str_replace('uxcrud::', $this->overrideBladeLocation, $path);
        }
        foreach($this->blades as $blade) {
            if (view()->exists($path . $blade)) {
                return $path . $blade;
            }
        }
        // Views not found; include all paths for error message
        return $path . "[" . implode(", ", $this->blades) . "]";
    }

    /**
     * Set override blade location to specify a different blade. If set to null
     * it will look in resources/views/...
     * @param string $path Path to use instead of uxcrud::
     * @return $this
     */
    public function overrideBladeLocation($path = null) {
        $this->overrideBladeLocation = $path;
        return $this;
    }

    public function selectExpression($selectExpression) {
        $this->selectExpressions[] = $selectExpression;
        return $this;
    }

    /**
     * Return select expressions for field.
     * @return array
     */
    public function getSelectExpressions() {
        $selectExpressions = [];
        if (count($this->selectExpressions) > 0) {
            if (!is_string($this->selectExpressions[0]) && get_class($this->selectExpressions[0]) == Expression::class) {
                $selectExpressions[] = DB::raw($this->selectExpressions[0]->getValue() . " AS " . $this->name);
            } else {
                $selectExpressions[] = $this->selectExpressions[0] . " AS " . $this->name;
            }
        } else {
            if (strpos($this->name, '.')) {
                // Force fully qualified names to be included by Laravel.
                $selectExpressions[] ="{$this->name} AS {$this->name}";// . str_replace(".", "_", $this->name);
            } else {
                $selectExpressions[] = $this->name;
            }
        }
        return $selectExpressions;
    }

    /**
     * Return base class of current object
     * @return string
     */
    public function getClass() {
        return Str::kebab(class_basename(get_class($this)));
    }

    public function classes() {
        return trim(implode("-", $this->blades) . ' ' . implode(' ', $this->classes));
    }

    public function validation($validation, $messages = null) {
        $this->validation = $validation;
        $this->messages = $messages;
        return $this;
    }

    /**
     * @param mixed $ability Action permission (create/update)
     * @param $object           object Original object
     * @param $field
     * @param $locales
     * @param $request
     * @param $rules
     * @param null $messages
     * @param bool $includeWarningMessages Include data-warning messages when set to true.
     * @return void Array of rules
     */
    public function getValidationRules($ability, $object, $field, $locales, $request, &$rules, &$messages = null, $includeWarningMessages = false) {
        $messageKey = $field->name;
        $dataWarnings = [];
        if ($includeWarningMessages && isset($this->attributes['data-warning'])) {
            $dataWarnings[$this->attributes['data-warning']] = $this->attributes['data-warning-messages'];
        }

        if ($object->hasTranslationAttribute($field->name)) {
            foreach($locales as $code => $locale) {
                $rules["{$code}.{$field->name}"] = str_replace('$id', $object->id, $field->validation);
                //$messageKey = "{$code}.{$field->name}";
                foreach($dataWarnings as $key => $dataWarning) {
                    $rules["{$code}.{$field->name}"] = str_replace('$id', $object->id, $key);
                }
            }
        } else {
            $rules[$field->name] = str_replace('$id', $object->id, $field->validation);
            foreach($dataWarnings as $key => $dataWarning) {
                $rules[$field->name] = str_replace('$id', $object->id, $key);
            }
        }
        if (is_array($field->messages)) {
            foreach ($field->messages as $rule => $message) {
                $messages["{$messageKey}.{$rule}"] = $message;
            }
            foreach($dataWarnings as $key => $items) {
                $errorMessages = explode('|', $items);
                foreach($errorMessages as $errorMessage) {
                    list($rule, $message) = explode(':', $errorMessage, 2);
                    $messages["{$messageKey}.{$rule}"] = str_replace('$id', $object->id, $message);

                }
            }

        }
    }

    /**
     * Extend function to perform action before completing of event on form.
     * @param $ability  string indicating action: [create, update, delete, forceDelete, restore]
     * @param $model    Object current model
     * @param $attributes
     * @param $rules
     * @param $messages
     */
    public function onBeforeEvent($ability, $model, &$attributes, &$rules, &$messages) {

    }

    /**
     * Extend function to perform action after completing of event on form.
     * @param $ability  string indicating action: [create, update, delete, forceDelete, restore]
     * @param $original   object Original object
     * @param $processed    object Object after processing.
     * @param $results  Object containing results for delete, foraceDelete and restore.
     */
    public function onAfterEvent($ability, $original, $processed, $results) {

    }

    /**
     * Add class(es) to list of classes
     * @param string $classes     Name of class to add
     * @param bool $remove      If set to true remove class if exists.
     * @return $this
     */
    public function addClass($classes, $remove = false) {
        if (is_string($classes)) {
            $classes = explode(' ', $classes);
        }
        foreach($classes as $class) {
            if ($remove) {
                $key = array_search($class, $this->classes);
                if ($key) {
                    unset($this->classes[$key]);
                }
            } else {
                // Add new classes to array
                $this->classes = array_merge($this->classes, $classes);
            }
        }
        return $this;
    }

    /**
     * Remove class(es) from list of classes.
     * @param $classes
     * @return $this
     */
    public function removeClass($classes) {
        $this->addClass($classes, true);
        return $this;
    }

    /**
     * Determine whether class occurs in list of classes
     * @param $class    String of class to search for.
     */
    public function hasClass($class) {
        return in_array($class, $this->classes);
    }

    /**
     * Add attribute to list of attributes of input field
     * @param array $attributeArray     Key value pair of data attribute to add.
     * @return $this
     */
    public function addAttribute($attributeArray) {
        $attributes = [];
        foreach($attributeArray as $key => $value) {
            if (is_int($key)) {
                $attributes[] = $value;
            } else {
                $attributes[$key] = $value;
            }
        }
        $this->attributes = array_merge($this->attributes, $attributes);
        return $this;
    }

    public function addWarningMessage($rules, $messages) {
        $this->addDataAttribute(['warning' => $rules]);
        $this->addDataAttribute(['warning-messages' => $messages]);
        return $this;
    }

    /**
     * Remove attribute from list of attributes.
     * @param $attributeName
     * @return $this
     */
    public function removeAttribute($attributeName) {
        if (isset($this->attributes[$attributeName])) {
            unset($this->attributes[$attributeName]);
        }
        return $this;
    }

    /**
     * Returns data attributes
     * @return string
     */
    public function getAttributes() {
        $result = Helpers::implode_array_to_attributes($this->attributes);
        return $result;
    }

    /**
     * Add dataAttribute to list of dataAttributes to div field
     * @param array $dataAttributeArray     Key value pair of data attribute to add.
     * @return $this
     */
    public function addDataAttribute($dataAttributeArray) {
        $attributes = [];
        foreach($dataAttributeArray as $key => $value) {
            if (is_int($key)) {
                $attributes[] = $value;
            } else {
                if (substr($key, 0, 5) !== 'data-') {
                    $key = "data-$key";
                }
                $attributes[$key] = $value;
            }
        }
        $this->dataAttributes = array_merge($this->dataAttributes, $attributes);
        return $this;
    }

    /**
     * Show detail when input is set to value.
     * Target field is current field name with _detail appended.
     * or current_field_name with value of append.
     * @param $value                    string value to check for.
     * @param null $targetFieldName field name to target, defaults to current name with _detail appended.
     * @param bool $append Set to false to replace entire target field name.
     * Set to true it adds the target field name to the current field name.
     * @param bool $invertResult    Set to true to invert result to match not matching values.
     * @return $this
     */
    public function dataShowDetail($value, $targetFieldName = null, $append = false, $invertResult = false) {
        $dataAttributes = ['show-detail' => $value];
        if (!is_null($targetFieldName)) {
            if ($append) {
                $dataAttributes['show-detail-target'] = $this->name . $targetFieldName;
            } else {
                $dataAttributes['show-detail-target'] = $targetFieldName;
            }
        }
        $this->addDataAttribute($dataAttributes);
        if ($invertResult) {
            $this->dataShowDetailInvert(true);
        }
        return $this;
    }

    /**
     * Set to true to invert result to match not matching values.
     * @param bool $invertResult
     * @return $this
     */
    public function dataShowDetailInvert($invertResult = true) {
        $this->addDataAttribute(['show-detail-invert' => $invertResult]);
        return $this;
    }

    /**
     * Remove dataAttribute from list of dataAttributes.
     * @param $dataAttributeName
     * @return $this
     */
    public function removeDataAttribute($dataAttributeName) {
        if (isset($this->dataAttributes[$dataAttributeName])) {
            unset($this->dataAttributes[$dataAttributeName]);
        }
        return $this;
    }

    /**
     * Returns data attributes
     * @return string
     */
    public function getDataAttributes() {
        $result = Helpers::implode_array_to_attributes($this->dataAttributes);
        return $result;
    }

    /**
     * Add an icon to the field
     * @param string $icon font awesome icon, e.g. fas fa-user
     * @return $this
     */
    public function icon($icon) {
        $this->icon = $icon;
        $this->prependItem(view('uxcrud::forms.helpers.icon', ['field' => $this]));
        return $this;
    }

    /**
     * Set field specific settings from the field definition as specified
     * by the form creator. Use to set options for a particular form type.
     * @param \stdClass $fieldDefinition Field definition from json.
     * @return $this
     */
    public function setFormCreatorField($fieldDefinition) {
        if (isset($fieldDefinition->icon)) {
            $this->icon($fieldDefinition->icon);
        }
        return $this;
    }

    /**
     * Add an item (view or string) before the input field.
     * @param string|\Illuminate\View\View|\Illuminate\Contracts\View $item
     * @return $this
     */
    public function prependItem($item) {
        $this->prependItems[] = $item;
        return $this;
    }

    /**
     * Clear prepend items
     * @return $this
     */
    public function clearPrependItems() {
        $this->prependItems = [];
        return $this;
    }



    /**
     * Add an item after the input field.
     * @param string|\Illuminate\View\View|\Illuminate\Contracts\View $item
     * @return $this
     */
    public function appendItem($item) {
        $this->appendItems[] = $item;
        return $this;
    }

    /**
     * Clear append items
     * @return $this
     */
    public function clearAppendItems() {
        $this->appendItems= [];
        return $this;
    }

    /**
     * Add simple info button to field that is shown on click of info icon.
     * @param $content
     * @param $title
     * @param null $id
     * @param null $classes
     * @param null $icon
     * @param bool $append
     * @return $this
     */
    public function infoButton($content, $title, $id = null, $classes = null, $icon = null, $append = true, $html = false) {
        if (is_null($classes)) {
            $classes = 'btn btn-outline-info show-modal';
        }
        if (is_null($icon)) {
            $icon = 'fas fa-info';
        }
        if (is_null($id)) {
            $id = uniqid('ib');
        }
        $view = view('uxcrud::forms.helpers.infoButton',
            ['field' => $this, 'content' => $content, 'title' => $title, 'classes' => $classes, 'id' => $id, 'icon' => $icon, 'append' => $append, 'html' => $html]);
        if ($append) {
            $this->appendItem($view);
        } else {
            $this->prependItem($view);
        }
        return $this;
    }

    /**
     * Add info button that shows content on click. Content is embedded as hidden element.
     * @param $content
     * @param $title
     * @param null $id
     * @param null $classes
     * @param null $icon
     * @param bool $append
     * @return FormField
     */
    public function infoButtonHTML($content, $title, $id = null, $classes = null, $icon = null, $append = true) {
        return $this->infoButton($content, $title, $id, $classes, $icon, $append, true);
    }

    /**
     * Hide field on read (including index, view and export)
     * @param bool $visibility  Override visibility
     * @return $this
     */
    public function hideOnRead($visibility = false) {
        $this->permissions[Permission::READ] = $visibility;
        $this->hideOnIndex($visibility);
        $this->hideOnView($visibility);
        $this->hideOnExport($visibility);
        return $this;
    }

    /**
     * Set the visibility for the field across all abilities.
     * @param bool $visibility  Override visibility
     * @return $this
     */
    public function visible($visibility = true) {
        $this->hideOnRead($visibility);
        $this->hideOnCreate($visibility);
        $this->hideOnEdit($visibility);
        return $this;
    }

    /**
     * Hide field across all abilities.
     * @param bool $visibility  Override visibility
     * @return $this
     */
    public function hide($visibility = false) {
        $this->visible($visibility);
        return $this;
    }

    /**
     * Hide field across all abilities except for search (read)
     * @param bool $visibility  Override visibility
     * @return $this
     */
    public function hideSearchable() {
        $this->hideOnIndex()
            ->hideOnExport()
            ->hideOnEdit()
            ->hideOnView()
            ->hideOnCreate()
        ;

        return $this;
    }

    /**
     * Hide field on index.
     * @param bool $visibility  Override visibility
     * @return $this
     */
    public function hideOnIndex($visibility = false) {
        $this->permissions[Permission::INDEX] = $visibility;
        return $this;
    }

    /**
     * Hide field on view.
     * @param bool $visibility  Override visibility
     * @return $this
     */
    public function hideOnView($visibility = false) {
        $this->permissions[Permission::VIEW] = $visibility;
        return $this;
    }

    /**
     * Hide field on export.
     * @param bool $visibility  Override visibility
     * @return $this
     */
    public function hideOnExport($visibility = false) {
        $this->permissions[Permission::EXPORT] = $visibility;
        return $this;
    }

    /**
     * Hide field on create.
     * @param bool $visibility  Override visibility
     * @return $this
     */
    public function hideOnCreate($visibility = false) {
        $this->permissions[Permission::CREATE] = $visibility;
        return $this;
    }

    /**
     * Hide field on edit.
     * @param bool $visibility  Override visibility
     * @return $this
     */
    public function hideOnEdit($visibility = false) {
        $this->permissions[Permission::UPDATE] = $visibility;
        return $this;
    }

    /**
     * Check whether field is hidden for permission.
     * @param Permission $ability       Permission to check read only setting for.
     * @return bool
     */
    public function isHidden($ability) {
        return ($this->permissions[$ability] !== true && $this->permissions[$ability] !== self::READ_ONLY);
    }

    /**
     * Make field read only
     * @param bool $readOnly
     * @param bool $defaultVisibility Default visibility
     * @return $this
     */
    public function readOnly($readOnly = true, $defaultVisibility = true) {
        $this->readOnlyOnCreate($readOnly, $defaultVisibility);
        $this->readOnlyOnEdit($readOnly, $defaultVisibility);
        return $this;
    }

    /**
     * Make field read only on create
     * @param bool $readOnly
     * @param bool $defaultVisibility Default visibility
     * @return $this
     */
    public function readOnlyOnCreate($readOnly = true, $defaultVisibility = true) {
        $this->permissions[Permission::CREATE] = ($readOnly) ? self::READ_ONLY : $defaultVisibility;
        return $this;
    }

    /**
     * Make field read only on edit.
     * @param bool $readOnly
     * @param bool $defaultVisibility Default visibility
     * @return $this
     */
    public function readOnlyOnEdit($readOnly = true, $defaultVisibility = true) {
        $this->permissions[Permission::UPDATE] = ($readOnly) ? self::READ_ONLY : $defaultVisibility;
        return $this;
    }

    /**
     * Check whether field is readonly for permission.
     * @param Permission $ability       Permission to check read only setting for.
     * @return bool
     */
    public function isReadOnly($ability) {
        return ($this->permissions[$ability] !== true);
    }

    /**
     * Alias for removeSearch
     * @param bool $canSearch
     * @return FormField
     */
    public function noSearch($canSearch = false) {
        return $this->removeSearch($canSearch);
    }

    /**
     * Remove search option for field.
     * @param bool $canSearch
     * @return $this
     */
    public function removeSearch($canSearch = false) {
        $this->search = $canSearch;
        return $this;
    }

    /**
     * Alias for removeFilter
     * @param bool $canFilter
     * @return FormField
     */
    public function noFilter($canFilter = false) {
        return $this->removeFilter($canFilter);
    }

    /**
     * Remove filter for field.
     * @param bool $canFilter
     * @return $this
     */
    public function removeFilter($canFilter = false) {
        $this->filter = $canFilter;
        return $this;
    }

    /**
     * Sets the default filter value.
     * @param string $value
     * @return $this
     */
    public function defaultFilterValue($value) {
        $this->defaultFilterValue = $value;
        return $this;
    }

    /**
     * Add default sort order and priority to field.
     * @param string $order Either 'asc'|'desc'
     * @param null $priority    Optional priority of field if there are multiple
     * fields to sort by; e.g. initial sort on last name then on first name.
     * @return $this
     */
    public function defaultSort($order = 'asc', $priority = null) {
        $this->defaultSort = (object)[
            'order' => $order,
            'priority' => $priority
        ];
        return $this;
    }


    /**
     * Alias for removeSort from field
     * @param bool $canSort
     * @return FormField
     */
    public function noSorting($canSort = false) {
        return $this->removeSorting($canSort);
    }

    /**
     * Remove sort for field.
     * @param bool $canSort
     * @return $this
     */
    public function removeSorting($canSort = false) {
        $this->sorting = $canSort;
        return $this;
    }

    /**
     * Overwrite this function to customise saving of the relationship
     * @param \Heiw\Uxcrudible\UxcrudModel $model
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Relations\Relation[] $relation
     * @param $type
     * @return bool return true if relation has been updated, return false otherwise.
     */
    public function updateRelations(\Heiw\Uxcrudible\UxcrudModel $model,
        \Illuminate\Http\Request $request,
        $relation,
        $type) {
        return false;
    }

    public function exportColumnFormat() {
        return null;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        return "\t\t\t\$table->string('{$this->name}')" .
            $this->dataTableColumnNullable() . ";\n";
    }

    protected function dataTableColumnNullable() {
        $rules = ValidationRuleParser::parse($this->validation);
        if (in_array('Nullable', $rules) || !in_array('Required', $rules)) {
            return "->nullable()";
        }
        return null;
    }

    /**
     * Overwrite function if value needs to be altered for pivot tabls.
     * Ensures user friendly data is cast to database format.
     * @param $value
     * @return mixed
     */
    public function castPivotTableValue($value) {
        return $value;
    }

    /**
     * Exclude field from being included on submit.
     * @param bool $canSubmit
     * @return $this
     */
 /*   public function noSubmit($canSubmit = false) {
        $this->canSubmit = $canSubmit;
        return $this;
    }*/
}
