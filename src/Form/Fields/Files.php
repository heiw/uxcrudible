<?php

namespace Heiw\Uxcrudible\Form\Fields;

use App\Uxcrud\Attachment;
use Heiw\Uxcrudible\Models\File;
use Heiw\Uxcrudible\Models\Model;
use Heiw\Uxcrudible\UxcrudController;
use Illuminate\Support\Str;
use Symfony\Component\Mime\MimeTypes;

class Files extends DataTable
{
    /*
     * In Laravel's Flysystem integration, "visibility" is an abstraction of
     * file permissions across multiple platforms. Files may either be declared
     * public or private. When a file is declared  public, you are indicating
     * that the file should generally be accessible to others.
     */
    public const PUBLIC = 'public';
    public const PRIVATE = 'private';

    public $blades = ['files', 'datatable', 'select'];

    public $disk = 'local';

    public $visibility = self::PRIVATE;

    public $type = 'files';

    public $storageBase = '';

    public $mimeTypes = [
        // Allow encrypted files
        'application/encrypted',
        // jpg
        'image/jpeg', 'image/pjpeg',
        // bmp
        'image/bmp', 'image/x-bmp', 'image/x-ms-bmp',
        // png
        'image/png',
        // gif
        'image/gif',
        // svg
        'image/svg+xml',
        // svgz
        'image/svg+xml', 'image/svg+xml-compressed',
        // doc
        'application/msword', 'application/vnd.ms-word', 'application/x-msword', 'zz-application/zz-winassoc-doc',
        // docx
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.ms-office',
        // csv
        'text/csv', 'text/x-comma-separated-values', 'text/x-csv',
        // mpt
        'application/vnd.ms-project',
        // pdf
        'application/pdf', 'application/acrobat', 'application/nappdf', 'application/x-pdf', 'image/pdf',
        // ppt
        'application/vnd.ms-powerpoint', 'application/mspowerpoint', 'application/powerpoint', 'application/x-mspowerpoint',
        // pptx
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        // text
        'text/plain',
        // xls
        'application/vnd.ms-excel', 'application/msexcel', 'application/x-msexcel', 'zz-application/zz-winassoc-xls',
        // xlsx
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        // zip
        'application/zip', 'application/x-zip', 'application/x-zip-compressed',
    ];

    /**
     * @var null
     */
    private $getStorageLocation = null;

    //public $blades = ['datatable'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        $this->setModel(\Heiw\Uxcrudible\Models\File::class)
            ->removeSearch()
        ;
    }

    /**
     * Set disk to store uploads
     * @param $disk
     * @return $this
     */
    public function disk($disk) {
        $this->disk = $disk;
        return $this;
    }
    /**
     * Set base location to store uploads
     * @param $location
     * @return $this
     */
    public function storageBase($location) {
        $this->storageBase = $location;
        return $this;
    }

    /**
     * Legacy function to reset the allowed mime types for uploads based on
     * file extension.
     * Use resetMimeTypes when using full mime types directly.
     * @param string|array $fileExtensions  Array of allowed file extensions.
     * @return Files
     */
    public function resetMimes($fileExtensions = []) {
        $this->resetMimeTypes();
        $this->addMimes($fileExtensions);
        return $this;
    }

    /**
     * Legacy function to add mime types for uploads based on file extension.
     * Use resetMimeTypes when using full mime types directly.
     * @param string|array $fileExtensions  Array of allowed file extensions.
     * @return Files
     */
    public function addMimes($fileExtensions = []) {
        if (!is_array($fileExtensions)) {
            $fileExtensions[] = explode(',', $fileExtensions);
        }
        $mimeTypes = new MimeTypes();
        foreach($fileExtensions as $fileExtension) {
            $this->addMimeTypes($mimeTypes->getMimeTypes($fileExtension));
        }
        return $this;
    }

    /**
     * Reset the allowed mime types for uploads.
     * @param array $mimeTypes  Array of allowed mime types.
     * @return Files
     */
    public function resetMimeTypes($mimeTypes = []) {
        $this->mimeTypes = $mimeTypes;
        return $this;
    }

    /**
     * Add allowed mime type(s) for uploads.
     * @param string|array $mimeTypes Either array or comma separated string.
     * @return Files
     */
    public function addMimeTypes($mimeTypes = []) {
        if (!is_array($mimeTypes)) {
            $mimeTypes[] = explode(',', $mimeTypes);
        }
        $this->mimeTypes = array_merge($this->mimeTypes, $mimeTypes);
        return $this;
    }

    public function getValidationRules(
        $ability,
        $object,
        $field,
        $locales,
        $request,
        &$rules,
        &$messages = null,
        $includeWarningMessages = false
    ) {
        // Add mime types to validation.
        $this->validation = trim($this->validation . "|mimetypes:" . implode(",", $this->mimeTypes), '|');
        $this->messages['mimetypes'] = __(':name contains a file type that is not allowed', ['name' => $this->getPrettyName()]);
        // Append .* to field name to accept array of files.
        $fieldName = $field->name;
        $field->name .= ".*";
        parent::getValidationRules($ability, $object, $field, $locales, $request, $rules, $messages,
            $includeWarningMessages);
        // Reset field name
        $field->name = $fieldName;
    }

    /**
     * Function to customise the folder name
     * @param function $function
     * @return $this
     */
    public function storageLocation($function) {
        $this->getStorageLocation = $function;
        return $this;
    }

    /**
     * Get the (sub)folders as a string based on the storage location if set.
     * @param $model
     * @return string
     */
    protected function getFolders($model) {
        if (is_callable($this->getStorageLocation)) {
            $folders = call_user_func($this->getStorageLocation, $model);
        } else {
            $folders = [];
        }
        array_unshift($folders, $this->storageBase);
        $folders = implode("/", $folders);
        return $folders;
    }

    /**
     * Store the file
     * @param $file     File to store
     * @param $folders  string Folder to store the file
     * @param $model    Model
     * @return mixed
     */
    protected function storeFile($file, $folders, $model) {
        $filename = $file->store($folders, $this->disk);
        $fileModel = $this->newModel()->getModelNamespaced();
        $store = new $fileModel();
        $attributes = [];
        $attributes = $store->setAttributesBeforeCreate($attributes, $model, $filename, $file);
        return $store::create($attributes);
    }

    /**
     * @param \Heiw\Uxcrudible\UxcrudModel $model
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Relations\Relation[] $relation
     * @param $type
     * @return bool return true if relation has been updated, return false otherwise.
     */
    public function updateRelations(\Heiw\Uxcrudible\UxcrudModel $model,
        \Illuminate\Http\Request $request,
         $relation,
        $type) {

        $folders = self::getFolders($model);

        $allFiles = $request->allFiles();

        foreach($allFiles as $name) {
            foreach($name as $file) {
                $store = self::storeFile($file, $folders, $model);
            }
        }
        return true;
    }

    public function getDownloadRoute() {
        // TODO MK avoid hardcoded Attachment Uxcrud Controller / route

        $controller = UxcrudController::initFromModelName($this->model);
        return $controller->getDownloadRoute();
        //return '/ajax' . static::getRoute();
    }

}
