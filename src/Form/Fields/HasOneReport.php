<?php

namespace Heiw\Uxcrudible\Form\Fields;

/**
 * Has One relation used in Report (or any custom query created in builder())
 * Pass fully qualified name to $name e.g.
 *            HasOneReport::create('grade.name', __('Grade'))
 *                   ->setModel(Grade::class),
 * Class HasOneReport
 * @package Heiw\Uxcrudible\Form\Fields
 */
class HasOneReport extends HasOne {
    public $type = 'hasOne';

    public $blades = ['hasOne', 'select'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);

        $this->useReportDisplayValue();
        $parts = explode(".", $name);
        if (count($parts) > 0) {
            $this->setName($name)
                ->setMethod($parts[0]);
        }

        return $this;
    }
}