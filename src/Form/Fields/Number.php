<?php

namespace Heiw\Uxcrudible\Form\Fields;

class Number extends FormField {
    public $type = 'number';

    public $blades = ['number', 'text'];
    
    public function __construct(...$arguments) {
        parent::__construct(...$arguments);
        return $this;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        $type = "bigInteger";
        if (isset($this->attributes['step'])) {
            $type = "decimal";
        }
        return "\t\t\t\$table->$type('{$this->name}')" .
            $this->dataTableColumnNullable() . ";\n";
    }

    public function step($step) {
        $this->addAttribute(['step' => $step]);
        return $this;
    }
    
    public function min($min) {
        $this->addAttribute(['min' => $min]);
        return $this;
    }
    
    public function max($max) {
        $this->addAttribute(['max' => $max]);
        return $this;
    }
}