<?php

namespace Heiw\Uxcrudible\Form\Fields;

class BelongsToMany extends HasMany {
    public $type = 'belongsToMany';

    public $blades = ['belongsToMany', 'hasMany', 'select'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        return $this;
    }
}