<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationRuleParser;

class Select extends FormField {
    const TYPE_RADIO = 'radio';
    const TYPE_SELECT = 'select';

    public $type = self::TYPE_SELECT;

    public $model;

    public $emptyOptionLabel = null;
    public $emptyOptionValue = null;

    public $preOptions = array();
    /**
     * @var string of Method to call to retrieve options
     */
    public $optionsMethod;
    /**
     * @var string If not default object set name of class to call method on.
     */
    public $optionsMethodClass = null;
    public $method;
    /**
     * @var null Label to show in drop down with [please select message]
     */
    protected $pleaseSelectLabel = null;

    /**
     * @var null controller to use for options
     */
    //public $controllerForOption = null;

    public $valueName = 'id';
    public $labelName = 'name';

    public $multiple = false;

    public $remoteData = true;

    /**
     * Set to true if display value is selected manually in report query.
     * @var bool
     */
    public $useReportDisplayValue = false;

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);

        // Set default method name
        if (!isset($this->method)) {
            if (isset($this->model)) {
                $this->method = Str::camel($this->model);
            } else {
                $this->method = Str::camel($name);
            }
        }

        if (!isset($this->model)) {
            $this->model = Str::camel($name);
        }

        switch($this->type) {
            case 'hasMany':case 'hasOne':
                break;
            default:
                // Add initial please select label by default.
                $this->pleaseSelectLabel();
                break;
        }

        // Disable sorting as it will sort by id instead of content.
        //$this->noSorting();
        return $this;
    }

    public function type($type) {
        $this->type = $type;
        if ($this->type === self::TYPE_RADIO) {
            $this->blades = Helpers::array_insert_before_value($this->blades, $type,
         self::TYPE_SELECT);
        } else {
            $this->blades = Arr::except($this->blades, self::TYPE_RADIO);
            unset($this->blades[self::TYPE_RADIO]);
        }
        return $this;
    }

    public function radio($enable = true) {
        if ($enable) {
            $this->type(self::TYPE_RADIO);
        } else {
            $this->type(self::TYPE_SELECT);
        }
        $this->pleaseSelectLabel(null);
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setMethod($method) {
        $this->method = $method;
        return $this;
    }

    public function method() {
        if (empty($this->method)) {
            $name = 'all' . Str::camel(Str::plural(class_basename($this->model)));
            if (method_exists($this->model, $name)) {
                $this->method = $name;
            } else {
                $this->method = function() {
                    return $this->model::orderBy($this->model::getOrderBy())->get();
                };
            }
        }
        return $this->method;
    }

    public function setModel($model) {
        $this->model = $model;
        // Use icon from model if no icon is set
        if (!isset($this->icon) && isset($model) && isset($model::$icon)) {
            $this->icon($model::$icon);
        }
        if ($this->labelName == 'name' && isset($model) && isset($model::$orderBy)) {
            $this->labelName = $model::$orderBy;
        }
        $this->removeSorting();
        return $this;
    }

    public function model() {
        return $this->model;
    }

    /**
     * Set the method to call to get available options.
     * @param null $method
     * @param string $valueName
     * @param string $labelName
     * @return $this
     */
    public function setOptionsMethod($method = null, $valueName = 'id', $labelName = 'name') {
        //$this->model = $model;
        $this->optionsMethod = $method;
        $this->valueName = $valueName;
        $this->labelName = $labelName;
        return $this;
    }


    public function setOptionsMethodClass($class) {
        $this->optionsMethodClass = $class;
        return $this;
    }

    public function valueName($valueName = 'value') {
        $this->valueName = $valueName;
        return $this;
    }

    /**
     * Define the value to use for the label.
     * @param string $labelName Field name of value or method to call to
     *                          retrieve display value.
     * @return $this
     */
    public function labelName($labelName = 'name') {
        $this->labelName = $labelName;
        return $this;
    }

/*
    public function noFullOptionsList($fullOptionList = false) {
        $this->fullOptionList = $fullOptionList;
        return $this;
    }
*/
    /**
     * Disable remote data loading of options, and include all options directly.
     * @param bool $remoteData
     * @return $this
     */
    public function noRemoteData($remoteData = false) {
        $this->remoteData = $remoteData;
        return $this;
    }

    /**
     * Retrieve options from the following possible methods
     *      1. $model->{$this->optionsMethodClass}
     *      2. $object->{$this->optionsMethod}
     *      3. $model::{'all'.Str::studly(Str::plural($this->model))}
     *      4. $model::entries()
     *      5. $model::orderBy($model::getOrderBy())->get();
     * @param $object
     * @return array|false|\Illuminate\Support\Collection
     */
    public function optionsBuilderWithFallback($object, $search = null) {
        if (!empty($this->optionsMethod)) {
            $options = collect();
            if (!empty($this->optionsMethodClass)) {
                $model = new $this->optionsMethodClass;
                if (method_exists($model, $this->optionsMethod)) {
                    $options = $model->{$this->optionsMethod}($search);
                }
            } else {
                if (method_exists($object, $this->optionsMethod)) {
                    $options = $object->{$this->optionsMethod}($search);
                }
            }
        } elseif (empty($this->model)) {
            $options = collect();
        } else {
            $model = Str::singular($this->model);
            if (method_exists($model, 'all' . Str::studly(Str::plural($this->model)))) {
                $method = 'all' . Str::plural($this->model);
                $options = $model::$method($search);
            } elseif (method_exists($model, 'entries')) {
                $options = $model::entries($search);
            } elseif (class_exists($model)) {
                $modelObject = new $model();
                if ($modelObject->hasTranslationAttribute($model::getOrderBy())) {
                    $options = $model::orderByTranslation($model::getOrderBy());
                } else {
                    $options = $model::orderBy($model::getOrderBy());
                }
                //$options = $model::orderBy($model::getOrderBy());
            } else {
                $options = new Collection();
            }
            $rules = ValidationRuleParser::parse($this->validation);
            if (in_array('Nullable', $rules)) {
                $this->addPrependOption(null, __('-'));
            }
            if (!is_null($this->pleaseSelectLabel)) {
                $this->addPrependOption(null, $this->pleaseSelectLabel);
            }
        }
        return $options;
    }

    public function setEmptyOption($label = null, $value = null) {
        if (is_null($label)) {
            $label = __('-');
        }
        $this->emptyOptionLabel = $label;
        $this->emptyOptionValue = $value;
        return $this;
    }

    /**
     * Retrieve the options for the drop down
     * @param $object
     * @param null $search
     * @return array|false|Builder[]|Collection|\Illuminate\Support\Collection|mixed
     */
    public function options($object, $search = null) {
        $builder = $this->optionsBuilderWithFallback($object, $search);

        // TODO MK verify the following is required.
        if (get_class($builder) == Builder::class) {
            $options = $builder->get();
        } else {
            $options = $builder;
        }

        if (!empty($this->preOptions)) {
            foreach (array_reverse($this->preOptions) as $key => $option) {
                // Don't use key as it potentially overrides existing option.
                $options->prepend($option);
            }
        }

        //$options = array_combine($options->pluck($this->valueName)->toArray(), $options->toArray());
        return $options;
    }

    public function filterOptions($object) {
        $options = $this->options($object);
        // Add empty default key if not already present
        if (!in_array(null, $options->pluck('id')->toArray())) {
            $options->prepend((object)[$this->valueName => "", $this->labelName => __('-')]);
        }
        return $options;
    }

    public function getControllerForOption(UxcrudController $controller) {
        // Check whether model includes path
        if (strpos($this->model, '\\') !== false) {
            // Return Model's fully qualified path
            return $this->model;
        } else {
            // Replace current class with specified model
            // Get array of fully qualified class for controller
            $classes = explode('\\', get_class($controller));
            // Remove class name from list
            array_pop($classes);

            // Add related model instead
            $classes[] = $this->model;
            return implode('\\', $classes);
        }
    }

//    public function setControllerForOption($controller) {
//        $this->controllerForOption = $controller;
//        return $this;
//    }

    public function setMultiple($multiple = true) {
        // trim [] in all cases to avoid having nested arrays.
        $this->name = trim($this->name, '[]');

        if ($multiple) {
//            // Strip _id from name if present
//            if (Str::endsWith($this->name, '_id')) {
//                $this->name = Str::replaceLast('_id', '', $this->name);
//            }

            // Add array for multiple options.
            $this->name .= '[]';
        }
        $this->multiple = $multiple;
        return $this;
    }

    public function addPrependOption($id, $name) {
        $prepend = [$id => (object)[$this->valueName => $id, $this->labelName => $name]];
        $this->preOptions = $prepend + $this->preOptions;
        return $this;
    }

    public function prependOptionArray($array = []) {
        foreach($array as $key => $item) {
            if (isset($item->{$this->valueName}) &&
                (isset($item->{$this->labelName})
                || method_exists($item, trim($this->labelName, '()'))
                )) {
                $this->preOptions[$item->{$this->valueName}] = (object)$item;
            } else {
                $this->preOptions[$key] = (object)[$this->valueName => $key, $this->labelName => $item];
            }
        }
        return $this;
    }

    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractValue($object, $locale = null) {
        if ($this->multiple) {
            if (method_exists($object, $this->method)) {
                return $object->{$this->method};
            } else {
                return null;
            }
        } else {
            if (isset($object->{$this->name})) {
                return $object->{$this->name};
            } else {
                return null;
            }
        }
    }

    public function extractFilterValue() {
        $values = parent::extractFilterValue();
        return $values;

    }

    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractDisplayValue($object) {
        if (is_callable($object->method)) {
            return $object->method;
        } else {
            $functionName = trim($this->labelName, '()');
            $fromSelectExpression = "{$this->method}_{$this->name}";

            // Check option method class is not static (otherwise it will give an error on index view)
            if (isset($this->optionsMethodClass) && method_exists($this->optionsMethodClass, $functionName)) {
                $methodChecker = new \ReflectionMethod($this->optionsMethodClass, $functionName);
            }
            if (isset($this->optionsMethodClass) && method_exists($this->optionsMethodClass, $functionName) && $methodChecker->isStatic()) {
                return call_user_func(array($this->optionsMethodClass, $functionName));

            // Return label as variable from object if present
            } elseif (isset($object->{$this->method()}) && method_exists($object->{$this->method()}, $functionName)) {
                return call_user_func(array($object->{$this->method}, $functionName));
            } elseif (isset($object->{$this->method()}) && isset($object->{$this->method()}->{$this->labelName})) {
                return $object->{$this->method()}->{$this->labelName};

            // Return label as variable from object using function name without trailing _id if present
            } elseif (isset($object->{$fromSelectExpression})) {
                return $object->{$fromSelectExpression};
            } else {
                return parent::extractDisplayValue($object);
            }
        }
    }

    /**
     * Set the label for the first empty drop down item.
     * @param string $pleaseSelectLabel String to use as label defaults to __('-')
     * if set to null no label will be included.
     * @return $this
     */
    public function pleaseSelectLabel($pleaseSelectLabel = '-') {
        if ($pleaseSelectLabel == '-') {
            $pleaseSelectLabel = __('-');
        }
        $this->pleaseSelectLabel = $pleaseSelectLabel;
        return $this;
    }

    /**
     * Don't include please select label.
     * @param null $pleaseSelectLabel
     * @return Select
     */
    public function noPleaseSelectLabel($pleaseSelectLabel = null) {
        return $this->pleaseSelectLabel($pleaseSelectLabel);
    }

    /**
     * Overwrite this function to customise saving of the relationship
     * @param \Heiw\Uxcrudible\UxcrudModel $model
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Relations\Relation[] $relation
     * @param $type
     * @return bool return true if relation has been updated, return false otherwise.
     */
    public function updateRelations(\Heiw\Uxcrudible\UxcrudModel $model, \Illuminate\Http\Request $request, $relation, $type) {
        // Only run this for HasMany class type
        if ($type != \Illuminate\Database\Eloquent\Relations\HasMany::class) {
            return false;
        }
        // Get all related entries
        $related = $model->{$relation}();
        $localKey = $related->getLocalKeyName();
        $foreignKey = $related->getForeignKeyName();

        // Get id's for current relations.
        $current = $related->pluck($localKey)->toArray();
        // Id's of update
        $new = $request->{Str::snake($relation)};

        // Find records to remove if there are any current entries
        if (count($current) > 0) {
            if (count($new) > 0) {
                $delete = array_diff($current, $new);
            } else {
                $delete = $current;
            }
            // Delete entries that have been removed
            DB::table($related->getRelated()->getTable())
                ->whereIn($localKey, $delete)
                ->update([$foreignKey => null]);
        }
        if (count($new) > 0) {
            // Insert new entries
            DB::table($related->getRelated()->getTable())
                ->whereIn($localKey, $new)
                ->update([$foreignKey => $model->id]);
        }
        return true;
    }

    /**
     * Set to true to use report display value automatically.
     * @param bool $value
     * @return $this
     */
    public function useReportDisplayValue($value = true) {
        $this->useReportDisplayValue = $value;
        return $this;
    }

    /**
     * Default filter value only works if remote data is disabled.
     * @param string $value
     * @return $this|FormField
     */
    public function defaultFilterValue($value) {
        parent::defaultFilterValue($value);
        // Default filter value only works if remote data is disabled.
        $this->noRemoteData();
        return $this;
    }

    /**
     * Return select expressions for field including report display value if set.
     * @return array
     */
    public function getSelectExpressions() {
        $selectExpressions = parent::getSelectExpressions();
        if ($this->useReportDisplayValue) {
            $selectExpressions[] = "{$this->method}.{$this->labelName} AS {$this->method}_{$this->name}";
        }
        return $selectExpressions;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        $migration = null;

        $migration .= "\t\t\t\$table->unsignedBigInteger('{$this->name}')" .
            $this->dataTableColumnNullable() . ";\n";
        $modelName = $this->model();
        if ($modelName) {
            $model = new $modelName();

            $migration .= "\t\t\t\$table->foreign('{$this->name}')
\t\t\t\t->references('id')
\t\t\t\t->on('" . $model->getTable() . "')
\t\t\t\t->onDelete('cascade')
\t\t\t\t->onUpdate('cascade');\n\n";
        }
        return $migration;
    }


}
