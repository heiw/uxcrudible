<?php

namespace Heiw\Uxcrudible\Form\Fields;

class MultiText extends FormField {
    public $blades = ['multi-text', 'text'];

    public $validationSubFields = [];
    public $messagesSubFields = [];

    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractValue($object, $locale = null) {
        $value = parent::extractValue($object, $locale);
        if (empty($value)) {
            $value = json_encode([null]);
        }
        return $value;
    }


    /**
     * @param string $subField Name of the subfield to validate
     * @param string $validation   Validation rules
     * @param null|array $messages    Message to display
     * @return $this
     */
    // TODO MK implement custom messages for sub fields.
    public function validationSubField($validation, $subField = null/*, $messages = null*/) {
        $this->validationSubFields[$subField] = $validation;
//        $this->messagesSubFields[$subField] = $messages;
        return $this;
    }

    /**
     * Basic implementation on adding filters to sub fields.
     * TODO MK Implement sub fields that contain translations
     * TODO MK Implement custom messages for sub fields
     * @param mixed $ability
     * @param object $object
     * @param $field
     * @param $locales
     * @param $request
     * @param $rules
     * @param null $messages
     * @param bool $includeWarningMessages
     */
    public function getValidationRules(
        $ability,
        $object,
        $field,
        $locales,
        $request,
        &$rules,
        &$messages = null,
        $includeWarningMessages = false
    ) {
        parent::getValidationRules($ability, $object, $field, $locales, $request, $rules, $messages,
            $includeWarningMessages);

        $fieldName = $field->name;
        foreach($this->validationSubFields as $subField => $validation) {
            // Add subfield name if present (otherwise results in empty string).
            $name = rtrim(".{$subField}", ".");
            $rules["{$field->name}.*{$name}"] = $validation;
        }
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        // TODO MK If MySQL updated change text column type to json.
        return "\t\t\t\$table->text('{$this->name}')" .
            $this->dataTableColumnNullable() . ";\n";
    }
}
