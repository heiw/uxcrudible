<?php

namespace Heiw\Uxcrudible\Form\Fields;

class Hidden extends FormField {
    public $blades = ['hidden', 'text'];
    public $type = 'hidden';
}
