<?php

namespace Heiw\Uxcrudible\Form\Fields;

class Actions extends FormField {
    public $type = 'actions';

    public $icon = 'fas fa-play-circle';

    public $search = false;

    private $actions = array();

    public function actions() {
        return $this->actions;
    }

    public function __construct($name = 'actions', $label = null) {
        if(is_null($label)) {
            $label = __('actions');
        }
        parent::__construct($name, $label);
        $this
            ->add(Action::create('update'))
            ->add(Action::create('read'))
            ->add(Action::create('delete'))
            ->add(Action::create('restore'))
            ->add(Action::create('force-delete'))
        ;
        return $this;
    }

    /**
     * Add action to list with Action name as key.
     * @param Action $action    Action to add to list
     * @param bool $prepend     Set to true to prepend to existing actions.
     * @return $this
     */
    public function add($action, $prepend = false) {
        $addition = [$action->name => $action];
        if ($prepend) {
            $this->actions = $addition + $this->actions;
        } else {
            $this->actions = $this->actions + $addition;
        }
        return $this;
    }

    /**
     * Add action to beginning of list.
     * @param $action   Action
     * @return $this
     */
    public function prepend($action) {
        $this->add($action, true);
        return $this;
    }

    /**
     * Clear actions and replace with provided actions.
     * @param array $actions
     * @return $this
     */
    public function reset($actions = []) {
        $this->actions = [];
        foreach($actions as $action) {
            $this->add($action);
        }
        return $this;
    }


    public function insertAfter($needle, $action, $prepend = false) {
        $needlePosition = array_search($needle, array_keys($this->actions));
        $offset = 1;
        if ($prepend) {
            $offset = 0;
        }
        if ($needlePosition === false) {
            trigger_error("Needle `$needle` not found in actions.", E_USER_WARNING);
        } else {
            $this->actions =
                array_slice($this->actions, 0, $needlePosition + $offset, true) +
                array($action->name => $action) +
                array_slice($this->actions, $needlePosition + $offset, null, true)
            ;
        }
        return $this;
    }

    public function insertBefore($needle, $action) {
        return $this->insertAfter($needle, $action, true);
    }

    /**
     * Automatically exclude actions from select expression by return false.
     * @return false
     */
    public function getSelectExpressions() {
        return false;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        // Don't generate migration line for field.
        return null;
    }
}
