<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Form\Filters\WhereClause;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class Date extends FormField
{
    public $type = 'date';

    public $blades = ['date', 'text'];

    public function __construct($name, $label = null)
    {
        parent::__construct($name, $label);
        $this->filter = new Select($name);
        $this->filter
            ->addPrependOption(null, __('all'))
            ->addPrependOption(WhereClause::BETWEEN, __('between'))
            ->addPrependOption(WhereClause::NOT_BETWEEN, __('not between'))
            ->addPrependOption(WhereClause::NOT_NULL, __('with dates'))
            ->addPrependOption(WhereClause::NULL, __('without dates'))
            ->setMultiple(false);
    }

    public function exportColumnFormat()
    {
        return NumberFormat::FORMAT_DATE_DDMMYYYY;
    }

    /**
     * Automatically add date as required. If the date is conditionally required use the required_if rule and nullable e.g.:
     * ->validation('required_if:ec_eea_national,0|nullable');
     * @param $validation
     * @param $messages
     * @return $this|Date
     */
    public function validation($validation, $messages = null)
    {
        if ($validation && str_contains($validation, 'required_if')) {
            $this->validation = trim($validation . '|date|');
        } else {
            $this->validation = trim('date|' . $validation, '|');
        }
        $this->messages = $messages;
        $this->messages['date'] = __('The :name is not a valid date (yyyy-mm-dd)', ['name' => $this->getPrettyName()]);
        return $this;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine()
    {
        return "            \$table->date('{$this->name}')" .
            $this->dataTableColumnNullable() . ";\n";
    }
}
