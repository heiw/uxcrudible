<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Classes\DataTableButton;
use Heiw\Uxcrudible\Classes\DataTableOptions;
use Heiw\Uxcrudible\Classes\FormModal;
use Heiw\Uxcrudible\Classes\JsonForm;
use Heiw\Uxcrudible\Classes\JsonModal;
use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\Models\Model;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Heiw\Uxcrudible\Traits\DataTableController;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Support\Arr;

/**
 * Class HasManyPivot
 * Add (optional) pivot fields to selection based on source and destination data tables.
 *
 * Example usage in Uxcrud:
 * ````
 * HasManyPivot::create('models', __('Models'))
 *     ->pivotFields(
 *         [
 *             ReorderRow::create('pivot_order', __('Order'))
 *                 ->hideOnEdit()
 *             ,
 *             DateTime::create('pivot_interview_datetime', __('Date/time'))
 *                 ->icon('fas fa-clock')
 *             ,
 *             Text::create('pivot_text', __('Text'))
 *                 ->icon('fas fa-font')
 *             ,
 *         ]
 *     )
 *     ->icon('fas fa-database')
 *     ->setModel(\Heiw\Uxcrudible\Models\Model::class)
 * ,
 * ````
 *
 * Ensure that Model has the additional fields defined with ->withPivot(...):
 * ````
 * public function models(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
 *     return $this->belongsToMany(Model::class)
 *         ->withPivot(['order', 'interview_datetime', 'text'])
 *         ->orderBy('order')
 *         ->orderBy(Model::getOrderBy());
 * }
 *
 * ````
 * @package Heiw\Uxcrudible\Form\Fields
 */
class HasManyPivot extends HasMany {
    use CachedObjectValue, DataTableController;

    const SOURCE = 1;
    const SELECTION = 2;

    public $type = 'hasManyPivot';

    public $blades = ['hasManyPivot', 'hasMany', 'select'];

    //
    public $fieldsSource = null;
    public $fieldsSelection = null;

    public $pivotFields = [];

    public function fieldsSource($fieldSource) {
        $this->fieldsSource = $fieldSource;
        return $this;
    }

    public function fieldsSelection($fieldSelection) {
        $this->fieldSelection = $fieldSelection;
        return $this;
    }

    public function pivotFields($pivotFields) {
        $this->pivotFields = $pivotFields;
        return $this;
    }


    public function __construct($name, $label = null) {
        parent::__construct($name, $label);

        // We can select multiple but don't want the default name.
        $this->setMultiple(false);

        return $this;
    }

    /**
     * @return DataTableOptions
     */
    public function dataTableOptions() {
        if (is_null($this->dataTableOptions)) {
            $this->dataTableOptions = new DataTableOptions();
        }
        $this->dataTableOptions
            ->noExports()
        ;

        $this->dataTableOptions->rowReorder = true;
        $this->dataTableOptions->ordering = true;

        return $this->dataTableOptions;
    }

    public function pivotFieldsJSON() {
        $pivotFields = [];
        foreach($this->pivotFields as $pivotField) {
            $pivotFields[$pivotField->name] = get_class($pivotField);
        }
        return json_encode($pivotFields);
    }

    /**
     * @return HasManyPivot|null
     */
    public function getFields() {
        if (is_null($this->fieldsSource)) {
            $object = Helpers::get_controller_from_model_name($this->model());
            return array_filter(
                $object->fields(),
                function($item) {
                    return ($item->type !== "actions");
                }
            );
        } elseif (is_callable($this->fieldsSource)) {
            $results = call_user_func($this->fieldsSource);
            return $results;
        } else {
            return $this->fieldsSource;
        }
    }

    /**
     * @return HasManyPivot|null
     */
    public function getFieldsSource($type = self::SOURCE) {
        $fields = $this->getFields();

        $pivotFields = $this->getPivotFields($type);

        $idField = Number::create('id');
        $idField->visible = false;

        return array_merge(
            $pivotFields,
            [
                $idField
            ],
            $fields
        );
    }

    public function getPivotFields($type = self::SOURCE) {
        $pivotFields = $this->pivotFields;
        foreach ($pivotFields as $key => $pivotField) {
            if ($type === self::SOURCE) {
                $pivotFields[$key]->addClass('hidden');
            } else {
                $pivotField->removeClass('hidden');
            }
            $pivotFields[$key]->visible = ($type !== self::SOURCE);
        }
        return $pivotFields;
    }

    /**
     * Get column names for source datatable
     * @return array
     */
    public function dataTableSourceColumnNames() {
        return $this->dataTableColumnFieldNames($this->getFieldsSource(self::SOURCE));
    }

    /**
     * Get column names for selection datatable.
     * @return array
     */
    public function dataTableSelectionColumnNames() {
        return $this->dataTableColumnFieldNames($this->getFieldsSource(self::SELECTION));
    }

    /**
     * @return HasManyPivot|null
     */
    public function getFieldsSelection() {
        return $this->getFieldsSource(self::SELECTION);
    }

    public function dataTableOptionsJSON($type) {
        $this->dataTableOptions();

        return json_encode($this->dataTableOptions);
    }

    public function optionsSource($object, $selected) {
        $selectedIds = Arr::pluck($selected, 'id');

        $options = $this->options($object)->whereNotIn('id', $selectedIds);
        return $options;
    }

    public function optionsSelection($object, $selected) {
        foreach($selected as $item) {
            if ($item->pivot) {
                foreach($item->pivot->getAttributes() as $key => $value) {
                    if (!isset($item->{"pivot_$key"})) {
                        $item->{"pivot_$key"} = $value;
                    }
                }
            };
        }
        return $selected;
    }

    public function pivotValues($selected) {
        $pivots = [];
        foreach($selected as $item) {
            $item->pivot->id = $item->id;
            $pivots[] = $item->pivot;
        }
        return $pivots;
    }

    public function dataTableDefaultSort() {
        $columns = $this->getFieldsSelection();
        return array_values($this->dataTableSortColumns($columns));
    }

    public function rowReorder($rowReorder) {
        static::$rowReorder = $rowReorder;
        return $this;
    }

    public function onBeforeEvent($ability, $model, &$attributes, &$rules, &$messages) {
        parent::onBeforeEvent($ability, $model, $attributes, $rules, $messages);
        $results = [];
        $items = json_decode(request()->input($this->name));

        if (is_array($items)) {
            foreach ($items as $item) {
                if (empty($this->pivotFields)) {
                    array_push($results, $item->id);
                } else {
                    foreach ($this->pivotFields as $pivotField) {
                        // Strip pivot_ from beginning of string
                        $pivotFieldNameOnTable = substr($pivotField->name, strlen('pivot_'));
                        $value = $item->{$pivotField->name};
                        $results[$item->id][$pivotFieldNameOnTable] = $pivotField->castPivotTableValue($value);
                    }
                }
            }
        }

        request()->merge([$this->name => $results]);
    }

    /**
     * Return FormModal in modal for model
     * @param $model            \Heiw\Uxcrudible\Models\Model to show modal for.
     * @param $action           int action to perform
     * @param bool|UxcrudController $controller
     *                          true (default): Use form based on controller details.
     *                          false: Do not use a controller and return modal without form.
     *                          UxcrudController: Use controller passed to function.
     * @param null $fields If null fields from controller will be used. Provide values to override.
     * @return FormModal Returns JsonModal
     */
    public function getFormModal($model, $action, $controller = true, $fields = null) {
        $result = parent::getFormModal($model, $action, $controller, $fields);
        return $result;
    }

    /**
     * @param Model $model
     * @param UxcrudController $controller
     * @return mixed
     */
    public function getPivotModal($model, $controller) {
        $fields = $this->getFields();
        foreach($fields as $key => $field) {
            $fields[$key]->readOnly();
        }

        $pivotFields = $this->getPivotFields(self::SOURCE);

        $idField = Number::create('id')->readOnly();
        $idField->addClass('hide');
        $idField->visible = false;

        $fields = array_merge(
            $pivotFields,
            [
                $idField
            ],
           $fields
        );

        $modal = $controller->getFormModal(
            $model,
            \Heiw\Uxcrudible\Form\Fields\Permission::UPDATE,
            true,
            $fields
        );
        $modal->dataTableParentInfo->dataTableId .= "-" . $this->name;

        $modal->closeButtonLabel(__('Cancel'));

        $modal->addModalButton(
            DataTableButton::create(__('Add'), 'fas fa-plus')
                ->className('btn btn-success uxcrud-add-pivot-with-data')
                ->addRawAttribute('onclick="return HasManyPivotField.popUpFormSubmit(this)"')
        );

        return $modal->jsonModal();
    }
}
