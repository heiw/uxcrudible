<?php

namespace Heiw\Uxcrudible\Form\Fields;

use Heiw\Uxcrudible\Models\Model;
use Heiw\Uxcrudible\Traits\CachedObjectValue;
use Illuminate\Support\Str;

class DataTable extends FormField {
    use CachedObjectValue;

    public $model = null;
    public $multiple = false;
    public $remoteData = true;

    public $blades = ['datatable', 'select'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        $this->addClass('uxcrud-form-datatable');
    }

    public function setModel($model) {
        $this->model = $model;
        return $this;
    }

    /**
     *
     * @return mixed
     */
    public function newModel() {
        return self::cacheResult("model", function() {
            if (is_null($this->model)) {
                $model = \Heiw\Uxcrudible\Models\Model::where('name', $this->name)->firstOrFail();
                // TODO MK check for databaseLessModel?
            } else {
                // Return Model either from database or based on file
                if ($this->model::$databaseLessModel === false) {
                    $model = \Heiw\Uxcrudible\Models\Model::
                    whereRaw('CONCAT(namespace, name) = ?',
                        str_replace(config('uxcrud.model_namespace'), '', $this->model))
                        ->first();
                    if (is_null($model)) {
                        $modelTable = new \Heiw\Uxcrudible\Models\Model();
                        abort(404, __(":model not found in :table table", ['model' => $this->model, 'table' => $modelTable->getTable()]));
                    }
                } else {
                    $model = Model::databaseLessModel($this->model);
                }
            }
            return $model;
        });
    }


    public function controller() {
        $model = $this->newModel();
        $controller = $model->getControllerNamespaced();
        $controller = new $controller();
        //$controller->route = '/datatable/';
        return $controller;
    }

//    public function getClass() {
//        return Str::kebab(class_basename(get_class($this)));
//    }

    /**
     * Relations are saved from DataTable
     * @param \Heiw\Uxcrudible\UxcrudModel $model
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Relations\Relation[] $relation
     * @param $type
     * @return bool return true if relation has been updated, return false otherwise.
     */
    public function updateRelations(\Heiw\Uxcrudible\UxcrudModel $model,
        \Illuminate\Http\Request $request,
        $relation,
        $type) {
        // Return true to indicate that relations have already been stored and
        // do not need to be processed when creating/updating the parent model.
        return true;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        // Don't generate migration line for field.
        return null;
    }
}
