<?php

namespace Heiw\Uxcrudible\Form\Fields;

/**
 * Placeholder for form validation messages.
 * Class Messages
 * @package Heiw\Uxcrudible\Form\Fields
 */
class Messages extends FormField {
    public $type = 'messages';

    public $blades = ['messages', 'text'];

    public function __construct($name = 'messages', $label = null) {
        parent::__construct($name, $label);
        $this->addClass('uxcrud-messages')
            ->hideOnIndex()
            ->removeSearch()
        ;
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        // Don't generate migration line for field.
        return null;
    }
}