<?php

namespace Heiw\Uxcrudible\Form\Fields;

use App\Uxcrud\Attachment;
use Heiw\Uxcrudible\UxcrudController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class File extends Files
{
    /*
     * In Laravel's Flysystem integration, "visibility" is an abstraction of
     * file permissions across multiple platforms. Files may either be declared
     * public or private. When a file is declared  public, you are indicating
     * that the file should generally be accessible to others.
     */
    public const PUBLIC = 'public';
    public const PRIVATE = 'private';

    public $blades = ['file', 'files', 'datatable', 'select'];

    public $disk = 'local';

    public $visibility = self::PRIVATE;

    public $type = 'files';

    public $storageBase = '';

    public $multiple = false;

    public $deleteOnReplacement = false;

    /**
     * @var null
     */
    private $getStorageLocation = null;

    //public $blades = ['datatable'];

    public function __construct($name, $label = null) {
        parent::__construct($name, $label);
        $this->setModel(\Heiw\Uxcrudible\Models\File::class)
            ->removeSearch()
            // Allow field messages to be shown by removing class.
            ->removeClass('uxcrud-form-datatable')
        ;
    }

    /**
     * Set disk to store uploads
     * @param $disk
     * @return $this
     */
    public function disk($disk) {
        $this->disk = $disk;
        return $this;
    }
    /**
     * Set base location to store uploads
     * @param $location
     * @return $this
     */
    public function storageBase($location) {
        $this->storageBase = $location;
        return $this;
    }

    /**
     * Function to customise the folder name
     * @param function $function
     * @return $this
     */
    public function storageLocation($function) {
        $this->getStorageLocation = $function;
        return $this;
    }

    protected function storeFile($file, $folders, $model) {
        $filename = $file->store($folders, $this->disk);
        $exists = Storage::disk($this->disk)->exists($filename);
        if ($exists) {
            $fileModel = $this->newModel()->getModelNamespaced();
            $store = new $fileModel();
            $attributes = [];
            $attributes = $store->setAttributesBeforeCreate($attributes, $model, $filename, $file);
            return $store::create($attributes);
        } else {
            return false;
        }
    }

    /**
     * Get the name of field with _id appended if not present yet.
     */
    public function getNameAppendId() {
        $name = $this->name;
        if (!Str::endsWith($name, '_id')) {
            $name .= '_id';
        }
        return $name;
    }

    /**
     * @param \Heiw\Uxcrudible\UxcrudModel $model
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Relations\Relation[] $relation
     * @param $type
     * @return bool return true if relation has been updated, return false otherwise.
     */
    public function updateRelations(\Heiw\Uxcrudible\UxcrudModel $model,
        \Illuminate\Http\Request $request,
         $relation,
        $type) {

        if (is_callable($this->getStorageLocation)) {
            $folders = call_user_func($this->getStorageLocation, $model);
        } else {
            $folders = [];
        }
        array_unshift($folders, $this->storageBase);
        $folders = implode("/", $folders);

        $allFiles = $request->allFiles();

        foreach($allFiles as $key => $files) {
            if ($key === Str::snake($relation)) {
                // Check whether there are multiple files in array
                if (is_array($files)) {
                    foreach ($files as $file) {
                        $storedFile = $this->storeFile($file, $folders, $model);
                    }
                } else {
                    // or a single file.
                    $storedFile = $this->storeFile($files, $folders, $model);
                    $model->{$this->getNameAppendId()} = $storedFile->id;
                }
            }
        }
        // Save model if files added
        if ($model->isDirty()) {
            $model->save();
        }
        return true;
    }

    /**
     * Extend function to perform action after completing of event on form.
     * @param $ability  string indicating action: [create, update, delete, forceDelete, restore]
     * @param $original   object Original object
     * @param $processed    object Object after processing.
     * @param $results  Object containing results for delete, foraceDelete and restore.
     */
    public function onAfterEvent($ability, $original, $processed, $results) {
        parent::onAfterEvent($ability, $original, $processed, $results);
        if ($this->deleteOnReplacement && $original->{"{$this->name}_id"} !== $processed->{"{$this->name}_id"}) {
            $file = $this->getFile($original);
            if ($file) {
                $file->forceDelete();
            }
        }
    }

    public function getDownloadRoute() {
        // TODO MK avoid hardcoded Attachment Uxcrud Controller / route

        $controller = UxcrudController::initFromModelName($this->model);
        return $controller->getDownloadRoute();
        //return '/ajax' . static::getRoute();
    }

    public function deleteOnReplacement($delete = true) {
        $this->deleteOnReplacement = $delete;
        return $this;
    }

    public function actions() {
        $actions = new Actions();
        return $actions->reset()
            ->add(Action::create('delete'))
            ->add(Action::create('restore'))
            ->add(Action::create('force-delete'))
            ->actions();
    }

    public function getFile($model) {
        return $model->{Str::camel($this->name)};
    }

    public function getValidationRules(
        $ability,
        $object,
        $field,
        $locales,
        $request,
        &$rules,
        &$messages = null,
        $includeWarningMessages = false
    ) {
        parent::getValidationRules($ability, $object, $field, $locales, $request, $rules, $messages,
            $includeWarningMessages);
        // Unset validation for file if already uploaded.
        if (!is_null($object->{$field->name . "_id"})) {
            unset($rules[$field->name]);
        }
    }
}
