<?php

namespace Heiw\Uxcrudible\Form\Fields;

/**
 * Class Checkbox   Checkbox field.
 * When validating if a checkbox must be checked use validation('accepted') instead of required.
 * @package Heiw\Uxcrudible\Form\Fields
 */
class Checkbox extends FormField {
    public $blades = ['checkbox', 'text'];

    public function __construct($name, $label = null) {
        request()->merge([$name =>
            // If post method set (typically on patch or create) update the
            // request value to 0 (false) or 1 (true).
            ((isset($_POST[$name]) && $_POST[$name] !== 0)
                // Used for Unit testing
                || request($name) === 1) ? 1 : 0]);

        parent::__construct($name, $label);

        $this->filter = new Select($name);
        $this->filter
            ->addPrependOption(1, __('yes'))
            ->addPrependOption(0, __('no'))
            ->addPrependOption(null, __('any'))
            ->setMultiple(false);
    }

    /**
     * Get code to create field in migration
     * @return string
     */
    public function getMigrationLine() {
        return "            \$table->boolean('{$this->name}')->nullable();\n";
    }
}