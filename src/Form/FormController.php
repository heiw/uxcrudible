<?php

namespace Heiw\Uxcrudible\Form;

use Heiw\Uxcrudible\Form\Fields\FormField;
use Heiw\Uxcrudible\Models\Model;
use Illuminate\Http\Request;
use App\Http\Controllers;

abstract class FormController extends Controllers\Controller
{
    /**
     * @param null|Model $model Model to which the fields apply to.
     * @return FormField[]
     */
    abstract public function fields($model = null);

    /***
     * @param string|string[] $classes Fully qualified class string or array of
     * classes of the fields to return.
     * @param bool $includeGroups Set to true to include grouped fields directly.
     * @param null|Model $model Model to which the fields apply to.
     * @return array
     */
    public function getFieldsOfType($classes, $includeGroups = true, $model = null) {
        $fields = [];
        if (is_string($classes)) {
            $classes = [$classes];
        }
        foreach($this->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::READ, $includeGroups, [], $model) as $field) {
            if (in_array(get_class($field), $classes)) {
                $fields[] = $field;
            }

        }
        return $fields;
    }

    /***
     * @param string $name Name to search for.
     * @param bool $includeGroups Set to true to include grouped fields directly.
     * @param string $column    Column to search in, defaults to name.
     * @param null|Model $model Model to which the fields apply to.
     * @return FormField|false  Returns FormField when found or false if not.
     */
    public function getFieldByName(string $name, $includeGroups = true, $model = null, $column = 'name') {
        foreach($this->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::READ, $includeGroups, $model) as $field) {
            if ($field->{$column} == $name) {
                return $field;
            }
        }
        return false;
    }
}
