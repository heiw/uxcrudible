<?php

namespace Heiw\Uxcrudible\Form\Filters;

use Heiw\Uxcrudible\Classes\BasicEnum;

final class WhereClause extends BasicEnum
{
    const BETWEEN = '[BETWEEN]';
    const NOT_BETWEEN = '[NOT BETWEEN]';
    const IN = '[IN]';
    const NOT_IN = '[NOT IN]';
    const NULL = '[NULL]';
    const NOT_NULL = '[NOT NULL]';
}
