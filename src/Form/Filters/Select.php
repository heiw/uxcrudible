<?php

namespace Heiw\Form\Filters;

use Heiw\Uxcrudible\Classes\BasicEnum;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Nexmo\Insights\Basic;

abstract class FormFilter {
    public $type = 'text';

    public $name = null;
    public $label = null;
    public $value = null;

    public $canEdit = true;

    public $classes = [];

    public $permissions = [
        Permission::CREATE => true,
        Permission::READ => true,
        Permission::UPDATE => true,
        Permission::DELETE => true
    ];

    public $filter = null;

    public $validation = 'nullable';

    const READ_ONLY = 3;

    /**
     * @var array List of edit / show blades to review. First entry found is used.
     */
    public $blades = [];

    public function __construct($name, $label = null) {
        $this->name = Str::snake($name);
        if (!is_null($label)) {
            $this->label = $label;
        } else {
            $this->label = Str::snake($name, ' ');
        }
        if (empty($this->blades)) {
            $this->blades[] = $this->type;
        }
        return $this;
    }

    public static function create(...$arguments)
    {
        $field = new static(...$arguments);
        return $field;
    }

    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractValue($object, $locale = null) {
        return $object->{$this->name};
    }

    /**
     * Return the display value for the form's entry.
     * @param $object
     * @return mixed
     */
    public function extractDisplayValue($object) {
        return $this->extractValue($object);
    }

    public function getBlade($path) {
        foreach($this->blades as $blade) {
            if (view()->exists($path . $blade)) {
                return $path . $blade;
            }
        }
        return null;
    }

    public function classes() {
        return trim(implode("-", $this->blades) . ' ' . implode(' ', $this->classes));
    }

    public function validation($validation) {
        $this->validation = $validation;
        return $this;
    }

    /**
     * Add class to list of classes
     * @param string $class     Name of class to add
     * @param bool $remove      If set to true remove class if exists.
     */
    public function addClass($class, $remove = false) {
        if ($remove && isset($this->classes[$class])) {
            unset($this->classes[$class]);
        } else {
            $this->classes[] = $class;
        }
    }

    public function hideOnIndex($visibility = false) {
        $this->permissions[Permission::READ] = $visibility;
        return $this;
    }

    public function hideOnView($visibility = false) {
        $this->permissions[Permission::READ] = $visibility;
        return $this;
    }

    public function hideOnCreate($visibility = false) {
        $this->permissions[Permission::CREATE] = $visibility;
        return $this;
    }

    public function hideOnEdit($visibility = false) {
        $this->permissions[Permission::UPDATE] = $visibility;
        return $this;
    }

    /**
     * @param bool $readOnly
     * @param bool $defaultVisibility Default visibility
     * @return $this
     */
    public function readOnlyOnCreate($readOnly = true, $defaultVisibility = true) {
        $this->permissions[Permission::CREATE] = ($readOnly) ? self::READ_ONLY : $defaultVisibility;
        return $this;
    }

    /**
     * @param bool $readOnly
     * @param bool $defaultVisibility Default visibility
     * @return $this
     */
    public function readOnlyOnEdit($readOnly = true, $defaultVisibility = true) {
        $this->permissions[Permission::UPDATE] = ($readOnly) ? self::READ_ONLY : $defaultVisibility;
        return $this;
    }

    public function isReadOnly($ability) {
        return ($this->permissions[$ability] !== true);
    }
}