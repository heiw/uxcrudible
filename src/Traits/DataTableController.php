<?php

namespace Heiw\Uxcrudible\Traits;

use App\Models\Post;
use Heiw\Uxcrudible\Classes\DataTableOptions;
use Heiw\Uxcrudible\Classes\DataTableParentOptions;
use Heiw\Uxcrudible\Classes\JsonToast;
use Heiw\Uxcrudible\Form\Fields\ReorderRow;
use Heiw\Uxcrudible\Helpers;
use Heiw\Uxcrudible\UxcrudExport;
use Illuminate\Database\Eloquent\Relations;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Facades\Excel;


trait DataTableController
{
    /**
     * Make option accessible when creating routes
     * @var null
     */
    public static $rowReorder = false;

    /**
     * @var DataTableOptions
     */
    protected $dataTableOptions = null;

    /**
     * @return DataTableOptions
     */
    public function dataTableOptions()
    {
        if (is_null($this->dataTableOptions)) {
            $this->dataTableOptions = new DataTableOptions();
        }
        $this->dataTableOptions->rowReorder(static::$rowReorder);
        return $this->dataTableOptions;
    }

    public function dataTableOptionsJSON()
    {
        $this->dataTableOptions();
        return json_encode($this->dataTableOptions);
    }

    public static function getExportRoute()
    {
        return '/' . Str::slug(__('uxcrud::slugs.export')) . static::getRoute();
    }

    public static function getRowReorderRoute()
    {
        if (! is_null(static::$rowReorder)) {
            return static::getAjaxRoute() . '/' . Str::slug(__('uxcrud::slugs.row-reorder'));
        } else {
            return null;
        }
    }

    public static function getDataTableAjaxRoute()
    {
        return '/datatable';
    }

    /**
     * Generic function to return the field names used in DataTable columns
     * @param $fields
     * @return array
     */
    public function dataTableColumnFieldNames($fields)
    {
        $columns = [];
        foreach ($fields as $key => $column) {
            $columns[$key] = ["name" => trim($column->name, '[]')];
            if (! $column->sorting) {
                $columns[$key]["orderable"] = $column->sorting;
            }
            if (isset($column->visible)) {
                $columns[$key]["visible"] = $column->visible;
            }
            switch (get_class($column)) {
                case ReorderRow::class:
                    $columns[$key]["type"] = "num";
                    break;
            }
        }
        return $columns;
    }


    /**
     * Retrieve column names and settings for DataTables.
     * @param int $ability
     * @return array
     */
    public function dataTableColumnNames($ability = \Heiw\Uxcrudible\Form\Fields\Permission::READ)
    {
        return $this->dataTableColumnFieldNames($this->getFields($ability, true));
    }

    protected function dataTableSortColumns($columns)
    {
        $order = [];
        foreach ($columns as $key => $column) {
            if (! is_null($column->defaultSort)) {
                // Set priority to order array size unless it's specifically provided.
                $priority = (is_null($column->defaultSort->priority)) ? count($order) : $column->defaultSort->priority;
                $order[$priority] = [
                    // Column number
                    $key
                    ,
                    // Direction of order
                    $column->defaultSort->order
                ];
            }
        }
        if (count($order) > 0) {
            // Sort array by priority (key)
            ksort($order);
        }
        return $order;
    }

    public function dataTableDefaultSort()
    {
        $columns = $this->getFields(\Heiw\Uxcrudible\Form\Fields\Permission::INDEX, true);
        return array_values($this->dataTableSortColumns($columns));
    }

    public function getDataTableParentInfo()
    {
        $parentInfo = new DataTableParentOptions();
        $parentInfo->dataTableId = $this->getId();

        if (request('parentModel') && isset(request('parentModel')['id']) && request('parentModel')['name']) {
            $parentClass = request('parentModel')['name'];
            $parentInfo->model = $parentClass::getModel()->findOrFail(value(request('parentModel')['id']));

            $parentController = str_replace(config('uxcrud.model_namespace'), config('uxcrud.controller_namespace'), request('parentModel')['name']);
            $parentInfo->controller = new $parentController();
            $parentInfo->dataTableId = $parentInfo->controller->getId() . "-" . $this->getId();

            if (! is_null($parentInfo->model)) {
                $parentInfo->dataset .= " data-parent-model[name]=\"" . $parentInfo->controller::$model . "\"";
                $parentInfo->dataset .= " data-parent-model[id]=\"" . $parentInfo->model->id . "\"";
            }
        }

        if (request('dtRefresh') != null) {
            $datatableId = request('dtRefresh');
        }

        return $parentInfo;
    }

    /**
     * Export the resource specified
     * @return mixed
     */
    public function export()
    {
        set_time_limit(360);
        $model = self::getModel();
        $this->authorize('export', $model);
        //static::canAccessOrAbort($model, 'export');
        $builder = $this->builder();
        $totalCount = $builder->count();
        $builder = $this->addOrder($builder, $model);

        $export = new UxcrudExport($builder, $model, $this);
        
        $type = '';
        $content_type = '';

        switch (request('type')) {
            case 'csv':
                $type = '.csv';
                $content_type = 'text/csv';
                break;
            default:
                $type = '.xlsx';
                $content_type = 'application/vnd.ms-excel';
                break;
        }

        // Returning download directly gives an error on Azure.
        // 502 - Web server received an invalid response while acting as a gateway or proxy server.
        //   There is a problem with the page you are looking for, and it cannot be displayed. When the Web server (while acting as a gateway or proxy) contacted the upstream content server, it received an invalid response from the content server.
        // Workaround is to get result and return it as an octet-stream.
        $result = Excel::download($export, $model->prettyClassName() . $type);
        return $result;

        /*if (App::environment('local')) {
            return $result;
        }

        $content_type = 'application/vnd.ms-excel';

        $response = Response($result, 200, [
            'Content-Type'        => $content_type,
            'Content-Disposition' => 'attachment; filename="' . $model->prettyClassName() . $type . '"',
        ]);

        return $response;*/
    }

    public function remoteData($fieldName)
    {
        $pageLength = config('uxcrud.remote_data_page_length');
        $more = false;
        $parentModel = self::getModel();
        // Ensure user can view current model
        $this->authorize('export', $parentModel);

        $field = $this->findField($fieldName);
        $modelName = $field->model();
        if (class_exists($modelName)) {
            $model = $modelName::getModel();
        } else {
            $model = $parentModel;
        }

        $builder = $field->optionsBuilderWithFallback($model, request()->get('search'));
        if (get_class($builder) == Builder::class) {
            $controller = Helpers::get_controller_from_model_name($model);

            $builder
                ->whereFilters($controller);

            $skip = (request()->get('page') - 1) * $pageLength;
            $totalCount = $builder->count();

            $builder
                ->skip($skip)
                ->take($pageLength);
            $builder = $this->addOrder($builder, $model);
            $entries = $builder->get();
            $more = ($totalCount > ($skip + $pageLength));


            // Add empty default key to first page of results
            if (request()->get('page') == 1) {
                $entries->prepend((object)['id' => "", $field->labelName => __('-')]);
            }
        } else {
            $entries = $builder;
        }


        //$options = $field->options($model, request()->get('search'));


        $results = [
            "results"    => [],
            "pagination" => [
                "more" => $more
            ]
        ];
        // TODO Fix dynamic loading of results
        foreach ($entries as $option) {
            $result = [];
            if (isset($option->id)) {
                $result['id'] = $option->id;
            }
            if (method_exists($option, trim($field->labelName, '()'))) {
                $result['text'] = call_user_func([$option, trim($field->labelName, '()')]);
            } else {
                $result['text'] = $option->{$field->labelName};
            }
            if ($field->useReportDisplayValue && $result['text'] != __('-')) {
                $result['id'] = $result['text'];
            }
            $results["results"][] = $result;
        }

//        $builder = $this->builder();
//        $totalCount = $builder->count();
//
////        $builder
////            ->skip(request()->get('start'))
////            ->take(request()->get('length'));
//        $builder = $this->addOrder($builder, $model);
//
//        $entries = ['results' => $builder->get()];
        return response()->json($results);
    }

    /**
     * Update the order of the resources in storage.
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function rowReorder()
    {
        $items = request()->input('values');
        if (count($items) > 0) {
            $model = self::getModel($items[0]['id']);
            $this->authorize('update', $model);
            //static::canAccessOrAbort($model, 'update');
            foreach ($items as $item) {
                DB::table($model->getTable())
                    ->where('id', $item['id'])
                    ->update([static::$rowReorder => $item['value']]);
            }
        }

        return response()->json(
            JsonToast::create()
                ->status('success')
        );
    }
}
