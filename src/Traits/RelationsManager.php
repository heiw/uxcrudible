<?php

namespace Heiw\Uxcrudible\Traits;

use Illuminate\Database\Eloquent\Relations;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;


trait RelationsManager
{
    protected static $relationsList = [];

    protected static $relationsInitialized = false;

    protected static $relationClasses = [
        Relations\BelongsTo::class,
        Relations\BelongsToMany::class,
        Relations\HasMany::class,
        Relations\HasManyThrough::class,
        Relations\HasOne::class,
        Relations\MorphMany::class,
        Relations\MorphOne::class,
        Relations\MorphOneOrMany::class,
        Relations\MorphPivot::class,
        Relations\MorphTo::class,
        Relations\MorphToMany::class,

    ];

    /**
     * @param null $type
     * @return array
     */
    public static function getAllRelations($type = null): array {
        if (!self::$relationsInitialized) {
            self::initAllRelations();
        }

        if (!is_null($type)) {
            $relations = array();
            foreach(self::$relationsList as $name => $relationType) {
                if ($type == $relationType) {
                    $relations[$name] = $relationType;
                }
            }
            return $relations;
        } else {
            return self::$relationsList;
        }
    }

    protected static function initAllRelations() {
        self::$relationsInitialized = true;

        $reflect = new \ReflectionClass(static::class);

        foreach ($reflect->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            /** @var ReflectionMethod $method */
            if ($method->hasReturnType() && in_array($method->getReturnType()->getName(), self::$relationClasses)) {
                self::$relationsList[$method->getName()] = $method->getReturnType()->getName();
            }
        }
    }
/*
    public static function relationsByName($type) {
        if (!self::$relationsInitialized) {
            self::initAllRelations();
        }

        $relations = static::getAllRelations($type);

    }
*/
    public static function withAll(): Builder {
        $relations = Arr::flatten(static::getAllRelations());

        return $relations ? self::with($relations) : self::query();
    }
}
