<?php

namespace Heiw\Uxcrudible\Traits;

use Heiw\Uxcrudible\Classes\DataTableButton;
use Heiw\Uxcrudible\Classes\EmailTemplateRecipient;
use Heiw\Uxcrudible\Models\EmailTemplate;
use Heiw\Uxcrudible\Models\User;
use Heiw\Uxcrudible\Uxcrud\EmailTemplateSend;
use Illuminate\Support\Str;

trait DataTableSendEmailTemplateUxcrud
{
    /**
     * Override to customise
     * @param null $parameter
     * @return mixed
     */
    public function selectEmailTemplateBuilder($parameter = null) {
        return $this->builder();
    }

    public function selectEmailTemplateTitle($builder, $totalCount, $parameter = null) {
        return __('Send email to :number :user_type', ['number' => $totalCount, 'user_type' => self::getLabel($totalCount)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function selectEmailTemplate($parameter = null) {
        $this->authorize('sendEmailTemplate', static::$model);

        $model = new \Heiw\Uxcrudible\Models\EmailTemplateSend();

        // Filter to selection from data table filters.
        $builder = $this->selectEmailTemplateBuilder($parameter);

        $controller = new EmailTemplateSend();

        $modal = $controller->getFormModal($model, EmailTemplateSend::ABILITY_SEND_EMAIL);

        $totalCount = $builder->getQuery()->getCountForPagination();
        $title = $this->selectEmailTemplateTitle($builder, $totalCount, $parameter);

        $modal->resetModalButtons(
            DataTableButton::create($title, 'fas fa-paper-plane')
                ->addClassName('ajax-action-uxcrud-submit')
                ->addDataAttribute('dt-refresh', $this->getDataTableParentInfo()->dataTableId)
                ->addDataAttribute('dt-with-filters', $this->getDataTableParentInfo()->dataTableId)
                ->addDataAttribute('parameter', $parameter)
        );

        $modal->title = $title;
        $jsonModal = $modal->jsonModal();
        $jsonModal->form->action = static::getAjaxRoute() . '/' . Str::kebab(__('uxcrud::slugs.send-email-template'));
        if (!is_null($parameter)) {
            $jsonModal->form->action .= '/' . $parameter;
        }
        return response()->json($jsonModal);
    }

    /**
     * Use the specified email template to send an email to the users that are
     * filtered in the datatable.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function sendEmailTemplate($parameter = null) {
        $this->authorize('sendEmailTemplate', static::$model);

        if (!is_null($parameter)) {
            $this->parameter = $parameter;
        }
        // Filter to selection from data table filters.
        $builder = $this->selectEmailTemplateBuilder($parameter);
        $response = EmailTemplate::sendToGroup($builder, $this);
        return response()->json($response);
    }

    /**
     * Map EmailTemplate record to users that should receive email.
     * @param $recipient
     * @return \Heiw\Uxcrudible\Models\User[] Array of /Heiw/Uxcrud/Models/User or its
     * descendant.
     */
    public function mapEmailTemplateRecordToUsers($recipient) {
        return [$recipient];
    }
}
