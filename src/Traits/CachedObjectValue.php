<?php

namespace Heiw\Uxcrudible\Traits;

/***
 * Class CachedObjectValue
 *
 * Description: Caches values.
 * Use for results of queries and other lengthy processes that are accessed multiple times in a single request.
 * Example:
 *

use Heiw\Uxcrudible\Traits\CachedObjectValue;

class cached() {
    use CachedObjectValue;

    function cachedMethod() {
        return self::cacheResult("Contract", function() {
            $contracts = $this->Contracts();
            if (count($contracts) > 0) {
                return $contracts[0];
            } else {
                return false;
            }
        });
    }
 }
 *
 *      If value is cached before read it from cache otherwise execute function and store in cache.
 **/
trait CachedObjectValue
{
    private $cache = array();

    protected function cacheResult($name, $function) {
        if (!isset($this->cache[$name])) {
            $this->cache[$name] = $function();
        }
        return $this->cache[$name];
    }

    protected function cacheSetValue($name, $value) {
        $this->cache[$name] = $value;
    }

    protected function cacheGetValue($name) {
        return $this->cache[$name];
    }
}
