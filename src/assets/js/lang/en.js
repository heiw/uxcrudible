if (typeof uxcrudLang === 'undefined') {
    uxcrudLang = [];
}

uxcrudLang['cy'] = {
    'Lock session': 'Lock session',
    'Stay logged in': 'Stay logged in',
    'Your session is about to expire.': 'Your session is about to expire.',
    'Please note:': 'Please note:',
    'Show filters': 'Show filters',
    'Search': 'Search',
    'Upload': 'Upload',
};

