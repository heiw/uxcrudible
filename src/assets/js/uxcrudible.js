if (!String.prototype.camel) {
    /**
     * Convert string to camelCase.
     * @returns {string}
     */
    String.prototype.camel = function () {
        return this.replace(/( |-|_)([a-z])/g, function (g) {
            return g[1].toUpperCase();
        });
    };
}

if (!String.prototype.studly) {
    /**
     * Convert string to StudlyCase.
     * @returns {string}
     */
    String.prototype.studly = function () {
        let result = this.camel();
        result = result.charAt(0).toUpperCase() + result.slice(1);
        return result;
    };
}

if (!String.prototype.kebab) {
    /**
     * Convert string to kebab-case.
     * @returns {string}
     */
    String.prototype.kebab = function () {
        return this.replace(/([a-z])([A-Z])([0-9])/g, '$1-$2')    // get all lowercase letters that are near to uppercase ones
            .replace(/[\s_]+/g, '-')                // replace all spaces and low dash
            .toLowerCase()                          // convert to lower case
    };
}

/**
 * Polyfill
 * Source https://github.com/kevlatus/polyfill-array-includes/blob/master/array-includes.js
 */
if (!Array.prototype.includes) {
    Object.defineProperty(Array.prototype, 'includes', {
        value: function (searchElement, fromIndex) {

            // 1. Let O be ? ToObject(this value).
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }

            var o = Object(this);

            // 2. Let len be ? ToLength(? Get(O, "length")).
            var len = o.length >>> 0;

            // 3. If len is 0, return false.
            if (len === 0) {
                return false;
            }

            // 4. Let n be ? ToInteger(fromIndex).
            //    (If fromIndex is undefined, this step produces the value 0.)
            var n = fromIndex | 0;

            // 5. If n ≥ 0, then
            //  a. Let k be n.
            // 6. Else n < 0,
            //  a. Let k be len + n.
            //  b. If k < 0, let k be 0.
            var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

            function sameValueZero(x, y) {
                return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
            }

            // 7. Repeat, while k < len
            while (k < len) {
                // a. Let elementK be the result of ? Get(O, ! ToString(k)).
                // b. If SameValueZero(searchElement, elementK) is true, return true.
                // c. Increase k by 1.
                if (sameValueZero(o[k], searchElement)) {
                    return true;
                }
                k++;
            }

            // 8. Return false
            return false;
        }
    });
}

/**
 * Polyfill source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith
 */
if (!String.prototype.endsWith) {
    String.prototype.endsWith = function(search, this_len) {
        if (this_len === undefined || this_len > this.length) {
            this_len = this.length;
        }
        return this.substring(this_len - search.length, this_len) === search;
    };
}

/**
 * Polyfill source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isNaN
 * @type {((number: number) => boolean) | (function(*=): boolean)}
 */
Number.isNaN = Number.isNaN || function isNaN(input) {
    return typeof input === 'number' && input !== input;
};

/**
 * Polyfill source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/entries#Pollyfill
 */
if (!Object.entries) {
    Object.entries = function( obj ){
        var ownProps = Object.keys( obj ),
            i = ownProps.length,
            resArray = new Array(i); // preallocate the Array
        while (i--)
            resArray[i] = [ownProps[i], obj[ownProps[i]]];

        return resArray;
    };
}

/**
 * Polyfill source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
 */
if (typeof Object.assign !== 'function') {
    // Must be writable: true, enumerable: false, configurable: true
    Object.defineProperty(Object, "assign", {
        value: function assign(target, varArgs) { // .length of function is 2
            'use strict';
            if (target === null || target === undefined) {
                throw new TypeError('Cannot convert undefined or null to object');
            }

            var to = Object(target);

            for (var index = 1; index < arguments.length; index++) {
                var nextSource = arguments[index];

                if (nextSource !== null && nextSource !== undefined) {
                    for (var nextKey in nextSource) {
                        // Avoid bugs when hasOwnProperty is shadowed
                        if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                            to[nextKey] = nextSource[nextKey];
                        }
                    }
                }
            }
            return to;
        },
        writable: true,
        configurable: true
    });
}

/**
 * Polyfill source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith
 */
if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
        value: function(search, rawPos) {
            var pos = rawPos > 0 ? rawPos|0 : 0;
            return this.substring(pos, pos + search.length) === search;
        }
    });
}

let ckeditors = {};

function addValueToCustomData(customData, name, value) {
    if (name.indexOf('[]') > 0) {
        name = name.replace('[]', '');
        let index = 0;
        if (customData['uxcrud-filter'][name] === undefined) {
            customData['uxcrud-filter'][name] = [];
        } else {
            index = customData['uxcrud-filter'][name].length;
        }

        customData['uxcrud-filter'][name][index] = value;
    } else {
        customData['uxcrud-filter'][name] = value;
    }
    return customData;
}

/***************************************************************************
 * TODO MK Refrector into component similar to model and AJAX.
 * DataTables
 */
function setDateFilterOptions(element) {
    let icon = $(element).prev();
    let showDateFields = null;
    console.log($(element).val());
    switch ($(element).val()) {
        case '[BETWEEN]':
            $(icon)[0].innerHTML = '<i class="fas fa-arrow-right"></i><i class="fas fa-arrow-left"></i>';
            showDateFields = 2;
            break;
        case '[NOT BETWEEN]':
            $(icon)[0].innerHTML = '<i class="fas fa-arrow-left"></i><i class="fas fa-arrow-right"></i>';
            showDateFields = 2;
            break;
        case '[NULL]':
            $(icon)[0].innerHTML = '<i class="far fa-calendar-times"></i>';
            showDateFields = 0;
            break;
        case '[NOT NULL]':
            $(icon)[0].innerHTML = '<i class="far fa-calendar-check"></i>';
            showDateFields = 1;
            break;
        default:
            showDateFields = 0;
            $(icon)[0].innerHTML = '<i class="fas fa-calendar-day"></i>';
            break;
    }

    $(element).closest('.input-date-range').find('.uxcrud-filter-single').toggle(showDateFields > 0);
    $(element).closest('.input-date-range').find('.uxcrud-filter-multi').toggle(showDateFields == 2);
}

let Utils = {
    getMethodFromString: function (string) {
        var scope = window;
        var scopeSplit = string.split('.');
        for (i = 0; i < scopeSplit.length - 1; i++) {
            scope = scope[scopeSplit[i]];

            if (scope == undefined) return;
        }

        return scope[scopeSplit[scopeSplit.length - 1]];
    },

    // Based on https://stackoverflow.com/questions/22783108/convert-js-object-to-form-data/49388446#49388446
    /**
     * Add object to form data and return form data object
     * @param formData  Formdata
     * @param obj
     * @param rootName
     * @param ignoreList
     * @returns FormData object
     */
    addObjectToFormData: function (formData, obj, rootName, ignoreList) {
        function appendFormData(data, root) {
            if (!ignore(root)) {
                root = root || '';
                if (data instanceof File) {
                    formData.append(root, data);
                } else if (Array.isArray(data)) {
                    for (var i = 0; i < data.length; i++) {
                        appendFormData(data[i], root + '[' + i + ']');
                    }
                } else if (typeof data === 'object' && data) {
                    for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                            if (root === '') {
                                appendFormData(data[key], key);
                            } else {
                                appendFormData(data[key], root + '[' + key + ']');
                            }
                        }
                    }
                } else {
                    if (data !== null && typeof data !== 'undefined') {
                        formData.append(root, data);
                    }
                }
            }
        }

        function ignore(root) {
            return Array.isArray(ignoreList)
                && ignoreList.some(function (x) {
                    return x === root;
                });
        }

        appendFormData(obj, rootName);

        return formData;
    }
}

// Make method callable.
window["EmailTemplate"] = {
    /**
     * Process button Action for EmailTemplate
     * @param setup  Button configuration from DataTableButton
     * @param e      Event that triggered the event
     * @param dt     A DataTables API instance for the host DataTable
     * @param node   jQuery instance for the button node that was clicked
     * @param config The button's configuration object
     * @param params The datatable parameters
     */
    showSelectModal: function(setup, e, dt, node, config, params) {
        console.log("EmailTemplate.showSelectModal(setup, e, dt, config, params)");
    }
}


/***************************************************************************
 * DataTable
 * Emulate class to encapsulate DataTables.
 */
let DataTable = {
    constructor: function (id) {
        let dataTable = this;
        this.id = id;
        if ($(id).data('ajax-url') !== undefined) {
            this.ajaxUrl = $(id).data('ajax-url');
        } else {
            this.ajaxUrl = false;
        }
        this.columns = [];
        this.columnsData = $($(id)[0]).data('columns');
        this.actionsColumn = -1;
        this.columnsDefs = [];
        this.buttons = [];

        $(this.columnsData).each(function(key, data) {
            dataTable.columns.push({"name": data});

            // Set classname for each cell.
            let columnsDef = { "targets": key, "className": data.name };

            // Disallow ordering for actions column
            if (data.name == 'actions') {
                columnsDef.orderable = false;
            }

            dataTable.columnsDefs.push(columnsDef)
        });

        this.options = $(id).data('data-table-options');
        this.exportButtons = [];
        // Add Excel download button if set
        if (this.options.exports.excel) {
            this.exportButtons.push({
                text: '<i class="fas fa-file-excel"></i> Excel',
                className: 'btn btn-outline-info m-1 w-100',
                action: function(e, dt, node, config) {
                    DataTable.exportAction('excel', e, dt, node, config)
                }
            })
        }

        // Add CSV download button if set
        if (this.options.exports.csv) {
            this.exportButtons.push({
                text: '<i class="fas fa-file-csv"></i> CSV',
                className: 'btn btn-outline-info m-1 w-100',
                action: function(e, dt, node, config) {
                    DataTable.exportAction('csv', e, dt, node, config)
                }
            })
        }

        // Add collection for download buttons if there is more than one.
        if (this.exportButtons.length > 1) {
            this.exportButtons = {
                autoClose: true,
                extend: 'collection',
                text: '<i class="fas fa-download"></i> ' + __('Export'),
                className: 'btn btn-outline-info m-1',
                buttons: this.exportButtons,
                fade: true
            }
        }

        this.buttons.push(this.exportButtons);

        $(this.options.customButtons).each(function() {
            let setup = this;
            if (typeof setup.onAction !== "undefined") {
                this.action = function (e, dt, node, config) {
                    DataTable.buttonAction(setup, e, dt, node, config);
                }
            }

            dataTable.buttons.push(this);
        });

        return this;
    },

    loadData: function() {
        if ( !$.fn.dataTable.isDataTable( this.id ) ) {
            // Set dataTable variable for use in anonymous functions.
            let dataTable = this;

            let defaultSettings = {};

            if (dataTable.ajaxUrl) {
                defaultSettings = Object.assign(defaultSettings, {
                    //"processing": true,
                    serverSide: true,
                    ajax: {
                        type: "POST",   // Use POST method to avoid 414 Request-URI Too Long errors
                        url: dataTable.ajaxUrl,
                        data: function (customData) {
                            customData['_method'] = "GET";  // Provide Laravel with GET method.
                            customData['parentModel'] = {
                                id: $(dataTable.id).data('parentModel[id]'),
                                name: $(dataTable.id).data('parentModel[name]')
                            };
                            // Read values for filters
                            customData['uxcrud-filter'] = {};
                            $('.uxcrud-filters input', dataTable.id).each(function () {
                                if (!$(this).hasClass('select2-search__field')) {
                                    customData = addValueToCustomData(customData, this.name, this.value);
                                    //customData['uxcrud-filter'][this.name] = this.value;
                                }
                            });
                            $('.uxcrud-filters select', dataTable.id).each(function (index, value) {
                                customData = addValueToCustomData(customData, this.name, $(this).val());
                                //customData['uxcrud-filter'][this.name.replace('[]', '')] = $(this).val();
                            });
                            let locale = $('input[name=locale]:checked', dataTable.id);
                            if (locale.length > 0) {
                                customData['locale'] = locale[0].value;
                            }
                        },
                        beforeSend: function (request) {
                            $(dataTable.id).addClass('loading');

                        },
                        error: function (result) {
                            $(dataTable.id).removeClass('loading');
                            $(dataTable.id).addClass('has-error');
                            if (result.responseJSON !== undefined) {
                                $('tbody', dataTable.id).html('<tr class="error-message"><td  class="alert alert-danger" colspan="' + $('thead tr th', dataTable.id).length + '"><h2>' + result.responseJSON.heading + '</h2>' + result.responseJSON.message + '</td></tr>');
                            }
                        },
                        complete: function () {
                        },
                    },
                    drawCallback: function(settings) {
                        $(settings.nTable).removeClass('loading has-error');
                    },
                });
            }

            defaultSettings = Object.assign(defaultSettings,
            {
                dom:'lB<"dt-filter">r<"dt-scroll-x"t>ip',
                buttons: {
                    dom: {
                        button: {
                            className: ''
                        }
                    },
                    buttons: dataTable.buttons
                },
                columns: dataTable.columnsData,
                columnDefs: dataTable.columnsDefs,
                orderCellsTop: true,
                searchDelay: 500,
                language: {
                    "emptyTable":     __("No data available in table"),
                    "info":           __("Showing _START_ to _END_ of _TOTAL_ entries"),
                    "infoEmpty":      __("Showing 0 to 0 of 0 entries"),
                    "infoFiltered":   __("(filtered from _MAX_ total entries)"),
                    "lengthMenu":     __("Show _MENU_ entries"),
                    "loadingRecords": __("Loading..."),
                    "processing":     __("Processing..."),
                    "search":         __("Search:"),
                    "zeroRecords":    __("No matching records found"),
                    "paginate": {
                        "first":      __("First"),
                        "last":       __("Last"),
                        "next":       __("Next"),
                        "previous":   __("Previous")
                    },
                    "aria": {
                        "sortAscending":  __(": activate to sort column ascending"),
                        "sortDescending": __(": activate to sort column descending")
                    }
                },
            });
console.log(dataTable.options)
            // Merge default settings with dataTable options and initialise.
            let table = $(this.id).DataTable(Object.assign(defaultSettings, dataTable.options));

            table.on('row-reorder', function(e, diff, edit) {
                dataTable.onRowReorder(e, diff, edit, dataTable.id)
            });

            table.on('row-reordered', function(e, diff, edit) {
                dataTable.onRowReordered(e, diff, edit, dataTable.id)
            });

            // Find base filter div
            let filter = $(dataTable.id + "_wrapper div.dt-filter");
            // Add search elements
            filter.html("<div class='input-group'>" +
                "<input type='search' class='form-control dt-search' aria-label='" + __('Search') + "' placeholder='" + __('Search') + "' aria-controls='" + dataTable.id + "'>" +
                "<!--div class='input-group-append'><button class='dt-btn-search btn btn-info' type='button'><i class='fas fa-search'></i></button></div-->" +
                "<div class='input-group-append'><button class='uxcrud-expand-filters btn btn-outline-info' type='button' aria-label='" + __('Show filters') + "'><i class='fas fa-search-plus'></i></button></div>" +
                "</div>"
            );

            // Search function
            var search = $.fn.DataTable.util.throttle(
                function (id, val) {
                    console.log(id + " " + val);
                    $(id).DataTable().search(val).draw();
                },
                1000
            );

            // Search on change of content
            filter.find('.dt-search').on('keyup change', function () {
                console.log("[" +$(this).data('searched') + "] [" + this.value + "]");
                if ($(this).data('searched') != this.value) {
                    console.log('searching');
                    search($(this).attr('aria-controls'), this.value);
                }
                // Store searched value.
                $(this).data('searched', this.value);
            })
        }
        return this;
    },

    refreshDataTable: function(id) {
        // Check whether url is set, otherwise reload fails.
        try {
            // TODO MK find out why sometimes an array is returned and fix below
            if ($(id).DataTable().ajax.url() !== null) {
                $(id).DataTable().ajax.reload(null, false);
            }
        } catch (e) {
            // TODO find out why random error appears first time on button click,
            // but not consecutive times
            // Uncaught TypeError: Cannot set property '_DT_CellIndex' of undefined
        }
    },

    exportAction: function(type, e, dt, node, config) {
        let params = DataTable.lastRequestParameters(dt);
        let table = $(dt.table().container()).find('table');
        window.location.href =
            table.data('export-url') + '/' +
            type + '?' +
            $.param(params);
    },

    /**
     * Process button Action by passing it to method defined in setup.
     * @param setup  Button configuration from DataTableButton
     * @param e      Event that triggered the event
     * @param dt     A DataTables API instance for the host DataTable
     * @param node   jQuery instance for the button node that was clicked
     * @param config The button's configuration object
     */
    buttonAction: function(setup, e, dt, node, config) {
        console.log('buttonAction');
        console.log(setup.onAction);
        let params = DataTable.lastRequestParameters(dt);
        console.log(params);
        // Add data attributes to node to allow Uxcrud's Ajax methods to be
        // called.
        Object.entries(setup.dataAttributes).forEach(function(dataAttribute) {
            let name = dataAttribute[0]
            let value = dataAttribute[1];
            console.log(name,value)
            if (!name.startsWith('data-')) {
                name = 'data-' + name;
            }
            $(node[0]).attr(name, value);
        });

        if (typeof setup.onAction === "string") {
            switch (setup.onAction) {
                case 'modal-action':
                case 'ajax-action':
                    Actions.ajax(node, e, params);
                    break;
                case '.toast-action':
                    Actions.toast(this, e, params);
                    break;
                case '.ajax-action-uxcrud-submit':
                    Actions.uxcrudSubmit(this, e, params);
                    break;

            }
        }

        // Check for on Action Method and run if present.
        let onActionMethod = Utils.getMethodFromString(setup.onAction);
        if (typeof onActionMethod === "function") {
            onActionMethod(setup, e, dt, node, config, params)
        }
    },

    /**
     * Get parameters from last request based on datatable.
     * Called from buttonAction and exportAction.
     * @param dt
     */
    lastRequestParameters: function(dt) {
        // Get parameters from last request
        let params = dt.ajax.params();
        // Remove unused
        delete params.columns;
        delete params.draw;
        delete params.length;
        delete params.start;

        return params;
    },

    onRowReorder: function(e, diff, edit, tableId) {
        if ($(tableId).data('row-reorder-url')) {
            if (diff.length > 0) {
                console.log(diff)
                let formData = new FormData();
                //formData.append('_method', 'PATCH');
                let values = [];
                for (var i = 0, ien = diff.length; i < ien; i++) {
                    // Find element for id
                    let oldData = $('.uxcrud-row-reorder', $('<div>' + diff[i].oldData.join() + '</div>'))[0];
                    // Find element for value
                    let newData = $('.uxcrud-row-reorder', $('<div>' + diff[i].newData.join() + '</div>'))[0];
                    let value = $(newData).data('value');
                    while (values.includes(value)) {
                        value++;
                    }
                    values.push(value);
                    formData.append("values[" + i + "][id]", $(oldData).data('id'));
                    formData.append("values[" + i + "][value]", value);
                }
                // Post changes
                let ajax = Ajax.constructor({
                    url: $(tableId).data('row-reorder-url'),
                    type: 'POST',
                    data: formData,
                });
                // callback to refresh the page 2 seconds after the ajax call. Only on My shortlisted vacancies (reorder)
                // sometimes (intermittently) the DB and page are out os sync. Always refresh the data from the DB.
                $("#dt-admin-trainee-ranking-report .uxcrud-row-reorder").ready(function() {
                    setTimeout(function () {

                        location.reload();
                    }, 2000);
                });
                ajax.call();
            }
            return false;
        }
    },

    onRowReordered: function(e, diff, edit, tableId) {
        if (!$(tableId).data('row-reorder-url')) {
            HasManyPivotField.reorderedRowData($(tableId).closest('.has-many-pivot-root'), false);
        }
    },
};

/***************************************************************************
 * Uxcrud Form
 * Emulate class responsible for handling uxcrud form.
 */
let UxcrudForm = {
    on: function(event, method) {
        if (window['UxcrudFormEvents'] === undefined) {
            window['UxcrudFormEvents'] = [];
        }
        if (window['UxcrudFormEvents'][event] === undefined) {
            window['UxcrudFormEvents'][event] = [];
        }
        window['UxcrudFormEvents'][event].push(method);
        return this;
    },

    onFormInit: function(method) {
        this.on('onFormInit', method);
        return this;
    },

    formInitEvent: function(event, element) {
        if (window['UxcrudFormEvents'] !== undefined && window['UxcrudFormEvents']['onFormInit'] !== undefined) {
            for(index = 0; index < window['UxcrudFormEvents']['onFormInit'].length; index++) {
                window['UxcrudFormEvents']['onFormInit'][index](this, element);
            }
        }
    },

    // Set initial visibility for detail fields.
    detailFormElements: 'div[data-show-detail] .form-control',
   		//div[data-show-detail] select,div[data-show-detail] textarea,div[data-show-detail] input',

    documentReady: function() {
        $(document.body).on('submit', 'form.uxcrudible-form', function() {
            console.log('submitting');
        });

        $(document.body).on('click', '.uxcrud-remove-multi-field', function(e) {
            e.preventDefault();
            let inputGroup = $(this).closest('.multi-field-group');
            if ($(inputGroup).parent().children().length > 1) {
                $(inputGroup).remove();
            } else {
                $(inputGroup).find('input,textarea,select').each(function() {
                    this.value = "";
                });
            }
        });

        $(document.body).on('click', '.uxcrud-add-multi-field', function(e) {
            const regexMatchFirstBrackets = /\[([^\]]*)\]/;
            e.preventDefault();

            // Find all fields and their ids.
            let ids = [];
            let parentField = $(this).closest('.multi-fields');
            // Loop through all multi field groups in the parent field.
            $('.multi-field-group', parentField).each(function() {
                let multiFieldGroup = this;
                let groupId = null;
                $('input,textarea,select', multiFieldGroup).each(function() {
                    let match;
                    if ((match = regexMatchFirstBrackets.exec(this.name)) !== null) {
                        let value = match[1];
                        // Only add to ids array if first of group.
                        if (groupId === null) {
                            // If value is empty default to 0
                            if (!value) {
                                value = 0;
                            }
                            groupId = parseInt(value);

                            // Only add to unique list if value is a number (otherwise
                            // max function will return NaN).
                            if ($.isNumeric(groupId)) {
                                // If id already exists generate random one.
                                if (ids.includes(groupId)) {
                                   // Get the maximum from the ids array and add 1.
                                   groupId = Math.max.apply(null, ids) + 1;
                                }
                                ids.push(value);
                            }
                        }
                    }
                });
            });

            // Create unique id for cloned fields by taking max of ids + 1.
            let newId = Math.max.apply(null, ids) + 1;
            let group = $(this).closest('.multi-field-group');
            let field = $(group).clone();

            $(field).find('input,textarea,select').each(function() {
                // Change name of field to new id.
                this.name = this.name.replace(regexMatchFirstBrackets, '[' + newId + ']');
                this.value = "";
            });
            group.after(field);
        });

        $(document.body).on('click', '.uxcrud-hash-locate a', function(e) {
            let hash = this.hash.replace(/\./g, '_');
            let target = $(hash);
            document.getElementById(hash.replace('#', '')).scrollIntoView();
            if (target.length === 0) {
                location.replace(UxcrudForm.dotsToBrackets(this.hash))
            }
            if (!$(target).is(":visible")) {
                let tabId = $(target).closest('.tab-pane')[0].id;
                $('a[href="#' + tabId + '"]').first().click();
            }
        });

        $(document.body).on('click', '.limit-view', function(e) {
            e.stopImmediatePropagation();
            var editWysiwyg = this.children[0];
            if (editWysiwyg.style.maxHeight == 'none') {
                $(this).removeClass('expanded');
                editWysiwyg.style.maxHeight = '';
            } else {
                $(this).addClass('expanded');
                editWysiwyg.style.maxHeight = 'none';
            }
        });

        $(document.body).on('click', '.uxcrud-replace-file-upload', function(e) {
            e.stopImmediatePropagation();
            $(this).closest('.input-group').find('.form-control').children().toggleClass('hide');
        });

        $(document.body).on('click', '.fragment-select a[data-toggle="tab"]', function(e) {
            let model = $(this).data('model');
            if (model) {
                let modelId = '#' + model;
                location.replace(modelId);
            }

            let dataTables = [];
            $(this.hash + " table.data-table").each(function() {
                // Add copy of datatable to array.
                dataTables.push(
                    Object.assign({}, DataTable.constructor('#' + this.id))
                        .loadData()
                );
            });
        });

        // $(document.body).on('click', 'table.has-many-pivot tr', function() {
        //     $(this).toggleClass('selected');
        // });

        // On double click of pivot table (de)select item by moving to other table.
        $(document.body).on('dblclick', 'table.has-many-pivot tr', function() {
            HasManyPivotField.moveDataTableRow($(this))
        });

    },

    /**
     * Toggle the visibility of the target element depending on value.
     * @param element
     */
    toggleTargetDetail: function (element) {
        let options = $(element).closest('div[data-show-detail]');
        let value = $(options).attr('data-show-detail');
        let elementValue = (element.type == "checkbox") ? element.checked : element.value;
        // When form is read only no value will be found.
        if (elementValue === undefined) {
            // Get value from data attribute.
            elementValue = $(element).data('value');
        }
        let target = $(options).attr('data-show-detail-target');
        let targetField;
        if (typeof target !== typeof undefined) {
            targetField = $(options).attr('data-show-detail-target');
        } else {
            targetField = element.name + "_detail";
        }
        let show = (elementValue == value);
        if ($(options).attr('data-show-detail-invert')) {
            show = !show;
        }
        $('.uxcrud-line.' + targetField).toggle(show);
    },

    /**
     * Initialise form fields on modal pop ups.
     * @param element
     */
    initFormFields: function (element) {
        /**
         * Initialise select 2 drop downs.
         */
        $('.select2', element).each(function () {
            let options = {
                dropdownParent: $(this).parent()
            };
            if ($(this).data('remote-data')) {
                let url =
                    options = {
                        ajax: {
                            delay: 250,
                            url: $(this).closest('table').data('ajax-url') + '/remote-data/' + $(this).attr('name'),
                            data: function (params) {
                                var query = {
                                    search: {value: params.term},
                                    page: params.page || 1
                                }
                                // Query parameters will be ?search=[term]&page=[page]
                                return query;
                            },
                            dataType: 'json'
                        }
                    }
            }
            $(this).select2(
                options
            );
            $(this).trigger('loaded');
        });

        /**
         * Initialise linked select 2 drop downs.
         */
        $('.select2linked', element).each(function () {
            let target = this;
            // Find linked select field
            // or input field linked field is ready only/parent through datatable
            // or div when field is made read only from Uxcrud $fields
            let linkedSelect = $("select[name='" + this.dataset.linkedName + "'],input[type='hidden'][name='" + this.dataset.linkedName + "'],div#" + this.dataset.linkedName + "", $(this).closest('form')).get(0);

            // If (readonly) div then set value from data-value attribute.
            if (linkedSelect.tagName == "DIV") {
                linkedSelect.value = $(linkedSelect).data('value');
            }

            // Only update target when select2linked are not linking to each other.
            if (target.name != $(linkedSelect).data('linked-name') || (linkedSelect.name != $(target).data('linked-name'))) {
                // Reset target select2 if value of source select changes
                $(linkedSelect).on('change', function (e, fromLinked) {
                    console.log(fromLinked);
                    // Avoid endless stack of calling changed on linked drop downs.
                    if (!fromLinked) {
                        //$(target).val(null).trigger('change', [true]);
                    }
                });
            }

            $(this).select2({
                dropdownParent: $(this).parent(),
                templateResult: function (data, container) {
                    if (data.element) {
                        // Hide elements that don't match
                        if (linkedSelect.value &&
                            data.element.dataset.linkId !== undefined &&
                            data.text !== "-"
                        ) {
                            // Allow for multiple ids to match
                            let matchTo = data.element.dataset.linkId.split(',');
                            // Compare value cast to string to match type returned by split.
                            if (matchTo.indexOf(linkedSelect.value.toString()) === -1) {
                                // Hide container if no match found.
                                $(container).addClass('hidden');
                            }
                        }
                    }
                    return data.text;
                }
            });
        });

        /**
         * Assign aria-label to select2-search__fields
         */
        $('.select2-search__field', element).each(function() {
            $(this).attr('aria-label', __('Search'));
        });

        $(UxcrudForm.detailFormElements).each(function() {
            UxcrudForm.toggleTargetDetail(this);
        });

        $('table.data-table', element).each(function () {
            let dt = DataTable.constructor('#' + this.id);
            // Load data for datatable if not in a page group.
            if ($('#' + this.id).closest('.tab-pane').length === 0) {
                dt.loadData();
            }

        });

        $('.uxcrud-ckeditor textarea', element).each(function () {
            let textarea = this;
            ClassicEditor
                .create(this)
                .then(function (newEditor) {
                    newEditor.isReadOnly = textarea.readOnly;

                    ckeditors[textarea.id] = newEditor;

                    /**
                     * Assign aria-label to ck-hidden
                     */
                    $('.ck-hidden', element).each(function() {
                        $(this).attr('aria-label', __('Upload'));
                    });
                })
                .catch(function (error) {
                    console.error(error);
                });
        });

        // A default tab can be supplied in the data-default-tab value. Select
        // fragments that contain the data attribute.
        var fragmentSelect = $('.nav-tabs.fragment-select[data-default-tab]', element);
        if (fragmentSelect.length > 0) {
            $(fragmentSelect.each(function() {
                // Set the default tab by clicking it.
                $(".fragment-select a[href='" + $(this).data('default-tab') + "']", element).click();
            }));
        } else {
            // Check whether hash in url matches tab name
            fragmentSelect = $('.nav-tabs.fragment-select a[href="#' + window.location.hash.split('#')[1] + '"]', element);
            if (window.location.hash.match('#') && fragmentSelect.length > 0) {
                $(".fragment-select a[href='" + fragmentSelect[0].hash + "']", element).click();
            } else {
                // No other options therefor select first tab.
                $('.fragment-select a[data-toggle="tab"]', element).first().click();
            }
        }

        if (window['initFormField'] !== undefined) {
            console.log(typeof window['initFormField'])
            if (typeof window['initFormField'] == "function") {
                window['initFormField'](this, element)
            } else if (typeof window['initFormField'] === "array") {

            }
        }

        UxcrudForm.formInitEvent(this, element);

        UxcrudForm.validateWarnings(element);
    },

    locateAlertFeedback: function(element, addClass, removeClass) {
        let line = $(element).closest('.uxcrud-line');
        if (addClass) {
            $(line).addClass(addClass);
        }
        if (removeClass) {
            $(line).removeClass(removeClass);
        }
        let feedback = $('.form-control-feedback .warning', line);
        if (feedback.length === 0) {
            feedback = $('.form-control-feedback .warning', $(element).closest('.form-group')).first();
        }
        return feedback;
    },

    addAlert: function(element, form, ruleName, message, defaultMessage, overwrite) {
        if (overwrite === undefined) {
            overwrite = true;
        }
        if (message === undefined) {
            message = defaultMessage;
        }
        let feedback = UxcrudForm.locateAlertFeedback(element, 'has-warning');
        if (overwrite) {
            feedback.html(message);
        } else {
            if (feedback.html() !== '<div>' + message + '</div>') {
                feedback.html('<div>' + feedback.html() + message + '</div>');
            }
        }

        // Locate or create warning area in uxcrud-messages.
        let messages = $('.uxcrud-messages .warning', form);
        if (messages.length === 0) {
            messages = UxcrudForm.resetMessages(form, 'warning');
        }
        $(messages).addClass('mt-3 messages alert alert-warning')

        // Locate or create list
        let ul = $('ul', messages);
        if (ul.length === 0) {
            ul = $(document.createElement('ul'));
            messages.html(__('Please note:'))
            messages.append(ul);
        }

        // Locate or create list item.
        let li = $('li.' + element.id, messages);
        if (li.length === 0) {
//            console.log(rules);
            li = $(document.createElement('li')).addClass(element.id + " " + ruleName);
            $('ul', messages).append(li);
        }

        // Set link and message.
        let link = $(document.createElement('a')).attr('href', '#' + element.id);
        link.html(UxcrudForm.validationFieldName(element) + ": " + message + " <i class='fas fa-highlighter'></i>");
        $(li).empty().append(link);
    },

    clearAlert: function(element, form) {
        let feedback = UxcrudForm.locateAlertFeedback(element, null, 'has-warning has-error').html('');

        // Remove from warning area in uxcrud-messages.
        $('.uxcrud-messages .warning li.' + element.id, form).remove();

        // If no other messages clear warning.
        if ($('.uxcrud-messages .warning li').length === 0) {
            $('.uxcrud-messages .warning').remove();
        }
    },

    resetMessages: function(form, type) {
        // Check for messages element
        let messages = $('.uxcrud-messages .' + type, form);
        // If not exists create messages element
        if (messages.length == 0) {
            // Check for parent element
            let uxcrudMessages = $('.uxcrud-messages', form);
            // If not exists create messages parent element
            if (uxcrudMessages.length == 0) {
                uxcrudMessages = $(document.createElement('div'))
                    .addClass('uxcrud-messages uxcrud-hash-locate');
                $('.modal-body,.modal-form-embedded', form).append(uxcrudMessages);
            }

            messages = $(document.createElement('div'))
                .addClass(type);
            $(uxcrudMessages).append(messages);
        } else {
            // Clear messages
            messages.removeClass('alert alert-' + type)
                .html('');
        }
        return messages;
    },

    validateWarnings: function(form) {
        UxcrudForm.resetMessages(form, 'warning');

        $('*[data-warning]', form).each(function() {
            // Add check to un change
            $(this).on('change', function() {
               UxcrudForm.validateField(this, form);
            });
            // Validate existing value.
            UxcrudForm.validateField(this, form);
        })
    },

    validateField: function(element, form) {
        let rules = UxcrudForm.validationRuleParser($(element).attr('data-warning'), $(element).attr('data-warning-messages'));

        UxcrudForm.clearAlert(element, form);
        if (rules.after) {
            if (new Date(element.value) < new Date(rules.after)) {
                UxcrudForm.addAlert(element, form, 'after', rules.messages.after, 'The ' + UxcrudForm.validationFieldName(element) + ' should be a date after ' + rules.after);
            }
        }
        if (rules.before) {
            if (new Date(element.value) > new Date(rules.before)) {
                UxcrudForm.addAlert(element, form, 'before', rules.messages.before, 'The ' + UxcrudForm.validationFieldName(element) + ' should be a date before ' + rules.before);
            }
        }
        if (rules.lte) {
            if (new Number(element.value) > new Number(rules.lte)) {
                UxcrudForm.addAlert(element, form, 'lte', rules.messages.lte, 'The ' + UxcrudForm.validationFieldName(element) + ' should be less than or equal to ' + rules.lte);
            }
        }
        if (rules.gte) {
            if (new Number(element.value) < new Number(rules.gte)) {
                UxcrudForm.addAlert(element, form, 'gte', rules.messages.gte, 'The ' + UxcrudForm.validationFieldName(element) + ' should be greater than or equal to ' + rules.gte);
            }
        }
        if (rules.required || rules.confirmed || rules.accepted) {
            let showAlert = false;
            switch(element.type) {
                case 'checkbox':
                    showAlert = !element.checked;
                    break;
                case 'file':
                    // Find previously uploaded details.
                    link = $(element).closest('.uxcrud-file-or-upload').find('a');
                    showAlert = (element.value === "" && link.length === 0);
                    break;
                default:
                    showAlert = (!element.value);
                    break;
            }
            if (showAlert) {
                let message, defaultMessage;
                if (rules.required !== undefined) {
                    message = rules.messages.required;
                    defaultMessage = 'The ' + UxcrudForm.validationFieldName(element) + ' is expected';
                }
                if (rules.confirmed !== undefined) {
                    message = rules.messages.confirmed;
                    defaultMessage = 'The ' + UxcrudForm.validationFieldName(element) + ' needs to be confirmed';
                }
                if (rules.accepted !== undefined) {
                    message = rules.messages.accepted;
                    defaultMessage = 'The ' + UxcrudForm.validationFieldName(element) + ' needs to be accepted';
                }
                UxcrudForm.addAlert(element, form, 'required', message, defaultMessage);
            }
        }
    },

    validationFieldName: function(element) {
        let name;
        if (element.name !== undefined) {
            name = element.name;
        } else {
            name = $(element).attr('data-name');
        }
        return name.replace(/[_\[\]]/g, ' ').trim();

    },

    validationRuleParser: function(string, messages) {
        let rules = { messages: {}};
        $(string.split('|')).each(function() {
            let result = this.split(':');
            if (result.length !== 2) {
                rules[result[0]] = true;
            } else {
                rules[result[0]] = result[1];
            }
        });
        if (messages !== undefined) {
            $(messages.split('|')).each(function () {
                let result = this.split(':');
                rules.messages[result[0]] = result[1];
            });
        }
        return rules;
    },

    dotsToBrackets: function(key) {
        let result = key;
        if (key.indexOf(".") >= 0) {
            result = null;
            let keys = key.split('.');
            for(let i = 0; i < keys.length; i++) {
                if (i === 0) {
                    result = keys[i];
                } else {
                    result += '[' + keys[i] + ']';
                }
            }
        }
        return result;
    },
};

/***************************************************************************
 * Uxcrud HasManyPivotField
 * Emulate class responsible for handling Has Many Pivot Field.
 */
let HasManyPivotField = {
    getFields: function(element) {
        // If moving to selection check for pivot fields definition.
        let pivotFields = false;
        if ($(element).attr('data-pivot-fields')) {
            pivotFields = JSON.parse($(element).attr('data-pivot-fields'));
        }
        return pivotFields;
    },

    moveDataTableRow: function(element) {
        let moveFromElement = $(element).closest('table');
        let moveFromDataTable = $(moveFromElement).DataTable();

        let node = moveFromDataTable.row($(element));

        let moveToClass = ($(moveFromElement).hasClass('has-many-pivot-source')) ? '.has-many-pivot-selection' : '.has-many-pivot-source';
        let pivotRoot = $(element).closest('.has-many-pivot-root');
        let moveToElement = $(moveToClass, pivotRoot);
        let moveToDataTable = $(moveToElement).DataTable();


        let pivotFields = HasManyPivotField.getFields($(moveToElement[0]));

        let moveData = true;

        if (pivotFields) {
            let countPivotFields = HasManyPivotField.processBasedOnPivotFields(pivotFields, node, moveToDataTable);
            if (moveToClass === '.has-many-pivot-selection') {
                let pivotModal = Object.assign({}, pivotRoot.data('pivot-modal'));
                pivotModal.body = $.parseHTML(pivotModal.body);

                if(countPivotFields > 0) {
                    // Show popup with pivot elements that can be edited.
                    Object.entries(moveFromElement.data('columns')).forEach(function(column, index) {
                        $("[name=" + column[1].name + "]", pivotModal.body).val(node.data()[index]);
                        // Match all translations
                        $("[name$='[" + column[1].name + "]']", pivotModal.body).val(node.data()[index]);
                    })(node);

                    pivotModal.dataAttributes['pivot-root-id'] = pivotRoot[0].id;
                    pivotModal.dataAttributes['element-id'] = element.data('id');
                    let modal = Modal.constructor(pivotModal).append();
                    moveData = false;
                    modal.show();
                }
            }
        }

        if (moveData) {
            HasManyPivotField.moveNodeData(pivotRoot, moveToDataTable, node);
        }
    },

    processBasedOnPivotFields: function(pivotFields, node, moveToDataTable) {
        let count = 0;
        Object.entries(pivotFields).forEach(function(pivotField) {
            if (pivotField[1] === "Heiw\\Uxcrudible\\Form\\Fields\\ReorderRow") {
                let reorderRow = pivotField[0];
                node.data()[node.column('pivot_order:name').index()] =
                    moveToDataTable.column('.pivot_order').data().length+1;
            } else {
                count++;
            }
        });

        return count;
    },

    /**
     *
     * @param button
     * @returns {boolean}
     */
    popUpFormSubmit: function(button) {
        let modal = $(button).closest('.uxcrud-edit-modal');

        let elementId = modal.data('element-id');
        //let moveFromDataTableId = modal.data('move-from-data-table-id');

        let hasManyPivotRoot = $('#' + modal.data('pivot-root-id'));

        let moveFromElement = $('.has-many-pivot-source', hasManyPivotRoot);
        let element = $('tr[data-id=' + elementId + ']', moveFromElement)[0];
        let moveToElement = $('.has-many-pivot-selection', hasManyPivotRoot);

        let pivotFields = HasManyPivotField.getFields($(moveToElement[0]));

        let moveFromDataTable = $(moveFromElement).DataTable();
        let moveToDataTable = $(moveToElement).DataTable();
        let node = moveFromDataTable.row($(element));

        let form = $(button).closest('form');
        let inputs = $('input,textarea,select', form);

        // Loop over pivot elements and add to node data.
        Object.entries(pivotFields).forEach(function(pivotField) {
            let type = pivotField[1];
            let name = pivotField[0];
            switch(type) {
                case "Heiw\\Uxcrudible\\Form\\Fields\\DateTime":
                    node.data()[node.column(name + ':name').index()] =
                        $("[name='" + name + "[date]']", form).val() + ' ' +
                        $("[name='" + name + "[time]']", form).val();
                     break;
                case "Heiw\\Uxcrudible\\Form\\Fields\\ReorderRow":
                    // Ignore as already set through hasManyPivotProcessBasedOnPivotFields.
                    break;
                default:
                    node.data()[node.column(name + ':name').index()] =
                        $("[name='" + name + "']", form).val();
                    break;
            }
            console.log(name, type);

        });

        HasManyPivotField.moveNodeData(hasManyPivotRoot, moveToDataTable, node);

        modal.modal('hide');

        // Do not submit form
        return false;
    },

    moveNodeData: function(pivotRoot, moveToDataTable, oldNode) {
        let newNode = moveToDataTable.row.add(oldNode.data());

        oldNode.remove().draw();
        HasManyPivotField.reorderedRowData(pivotRoot.parent(), true);

    },

    /**
     * Set initial order if not set or update order after removal of entry.
     * @param element
     * @param onRemove
     */
    reorderedRowData: function(element, onRemove) {
        let pivotSelection = $('.has-many-pivot-selection', element);

        let pivotFields = HasManyPivotField.getFields(pivotSelection);
        let pivotSelectionDataTable = pivotSelection.DataTable();

        Object.entries(pivotFields).forEach(function(pivotField) {
            if (pivotField[1] === "Heiw\\Uxcrudible\\Form\\Fields\\ReorderRow") {
                let reorderRow = pivotField[0];
                let reorderIndex = pivotSelectionDataTable.column('pivot_order:name').index();

                pivotSelectionDataTable.rows({order: 'current'})
                    .every(function (rowIdx, tableLoop, rowLoop) {
                        let data = this.data();
                        if (onRemove || data[reorderIndex] === "") {
                            data[reorderIndex] = rowLoop + 1;
                            this.data(data);
                            this.invalidate();
                        }
                    }
                );

                pivotSelectionDataTable.draw();
            }
        });
    },
};

/***************************************************************************
 * Ajax
 * Emulate class to create jQuery ajax call and process return.
 */
let Ajax = {
    constructor: function(options) {
        this.SUCCESS = 'success';
        this.ERROR = 'error';
        this.COMPLETE = 'complete';

        this.type = 'POST';
        this.dataType = 'JSON';
        this.enctype = 'application/x-www-form-urlencoded';
        this.url = '';
        this.data = '';
        this.events = [];
        this.modifierKeys = {}

        this.url = options.url || this.url;
        this.data = options.data || this.data;
        this.events = options.events || this.events;
        this.type = options.type || this.type;
        this.modifierKeys = options.modifierKeys || this.modifierKeys;

        if (options.onSuccess !== undefined) {
            this.onSuccess(options.onSuccess);
        }
        if (options.onError!== undefined) {
            this.onError(options.onError);
        }
        if (options.onComplete!== undefined) {
            this.onComplete(options.onComplete);
        }
        return this;
    },

    /**
     * Get value from cookie
     * Source: https://stackoverflow.com/questions/32738763/laravel-csrf-token-mismatch-for-ajax-post-request#answer-45533511
     */
    getCookie: function (name) {
        function escape(s) {
            return s.replace(/([.*+?\^${}()|\[\]\/\\])/g, '\\$1');
        }
        var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;%]*)'));
        return match ? match[1] : null;
    },

    /**
     * Add event to object
     * @param event     The event to add
     * @param method    The method to call
     * @returns {Ajax}
     */
    on: function(event, method) {
        this.events[event] = method;
        return this;
    },

    /**
     * Add on success event to object
     * @param method    The method to call
     * @returns {Ajax}
     */
    onSuccess: function(method) {
        this.on(Ajax.SUCCESS, method);
        return this;
    },

    /**
     * Add on error event to object
     * @param method    The method to call
     * @returns {Ajax}
     */
    onError: function(method) {
        this.on(Ajax.ERROR, method);
        return this;
    },

    /**
     * Add on complete event to object
     * @param method    The method to call
     * @returns {Ajax}
     */
    onComplete: function(method) {
        this.on(Ajax.COMPLETE, method);
        return this;
    },

    triggerEvent: function(event, response, ajaxCaller) {
        if (this.events[event] !== undefined && this.events[event] instanceof Function) {
            return this.events[event](response, ajaxCaller);
        }
    },

    call: function() {
        let ajaxCaller = this;
        return $.ajax({
            headers: {
                //'Accept': 'application/json',
                //'Content-Type': 'application/json',
//                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                'X-XSRF-TOKEN': Ajax.getCookie( 'XSRF-TOKEN' )
            },
            type: this.type,
            enctype:this.enctype,
            url: this.url,
            dataType: this.dataType,
            processData: false,
            contentType: false,
            data: this.data,
            beforeSend: function(jqXHR, settings) {
              	// If _token present remove as sent as X-XSRF-TOKEN in headers.
                try {
                    settings.data.delete('_token');
                } catch(e) {

                }
                return true;
            },
            success: function(response) {
                ajaxCaller.triggerEvent(Ajax.SUCCESS, response, ajaxCaller);
            },
            error: function(response) {
                if (!ajaxCaller.triggerEvent(Ajax.ERROR, response, ajaxCaller)) {
                    let text;
                    if (response.responseJSON !== undefined && response.responseJSON.header !== undefined) {
                        header = response.responseJSON.header;
                    } else {
                        header = response.statusText;
                    }
                    if (response.responseJSON !== undefined && response.responseJSON.message !== undefined) {
                        text = response.responseJSON.message;
                    } else {
                        text = response.status + " " + response.statusText;
                    }
                    $.toast({
                        heading: header,
                        text: text,
                        position: 'top-right',
                        hideAfter: false,
                    });
                }
            },
            complete: function(response) {
                ajaxCaller.triggerEvent(Ajax.COMPLETE, response, ajaxCaller);
                SessionTimeout.reset();
            },
        })
    },

    /**
     * Actions to perform before submitting form.
     * @param form
     */
    beforeFormSubmit: function(form) {
        Ajax.updateCKEditorContent(form);
        Ajax.hasManyPivotUpdate(form);
    },

    /**
     * Generate pivot data before submitting
     */
    hasManyPivotUpdate: function(form) {
        $('.has-many-pivot-root', form).each(function() {
            let results = [];
            let pivotSelection = $('.has-many-pivot-selection', this);
            let pivotSelectionDataTable = pivotSelection.DataTable();
            let fieldIndexes = {"id": pivotSelectionDataTable.column('id:name').index()};
            let valuesElement = $('.pivot-values', this);

            if ($(pivotSelection).attr('data-pivot-fields')) {
                let pivotFieldEntries = JSON.parse($(pivotSelection).attr('data-pivot-fields'));
                Object.entries(pivotFieldEntries).forEach(function(pivotField) {
                    fieldIndexes[pivotField[0]] = pivotSelectionDataTable.column(pivotField[0] + ':name').index();
                });
            }

            pivotSelectionDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
                let row = this.data();
                let data = {};
                Object.entries(fieldIndexes).forEach(function(field) {
                    data[field[0]] = row[field[1]];
                });
                results.push(data);
            });

            valuesElement.val(JSON.stringify(results));
        });

    },

    /**
     * Source elements need to be updated with rich content before posting.
     * @param form
     */
    updateCKEditorContent: function(form) {
        $('.uxcrud-ckeditor textarea', form).each(function () {
            ckeditors[this.id].updateSourceElement();
        })
    },

    /**
     * Run success or error function if function exists, otherwise output trace log to console.
     * @param func
     * @param parameter
     * @param ajaxCaller          Ajax object for the original call.
     */
    runResultFunction: function(func, parameter, ajaxCaller) {
        if (typeof window[func] === "function") {
            window[func](parameter, ajaxCaller)
        } else if (typeof func === "function") {
            func(parameter, ajaxCaller);
        } else {
            console.trace("Function `" + func + "` not found.")
        }
    },

    /**
     * Run events linked to result after success full completion
     * @param result    Result from action call
     * @param element   Element that activated the action
     * @param ajaxCaller      Ajax object for the original call.
     */
    successEventActionsResult: function(result, element, ajaxCaller) {
        // Redirect if specified
        if (result.redirect !== undefined && result.redirect !== null) {
            if (ajaxCaller.modifierKeys.shift || ajaxCaller.modifierKeys.ctrl) {
                window.open(result.redirect);
            } else {
                window.location = result.redirect;
            }
        }

        if (result.status === 'refresh') {
            if (ajaxCaller.modifierKeys && (ajaxCaller.modifierKeys.shift || ajaxCaller.modifierKeys.ctrl)) {
                window.open(document.location);
            } else {
                document.location.reload();
            }
        }
    },

    /**
     * Run events linked to result after success full completion
     * @param result    Result from action call
     * @param element   Element that activated the action
     * @param ajaxCaller      Ajax object for the original call.
     */
    successEventActionsElement: function(result, element, ajaxCaller) {
        // Call on success result functions if specified.
        if (element.dataset['onSuccess']) {
            Ajax.runResultFunction(element.dataset['onSuccess'], result, this);
        }
        // TODO MK: Fix requirement to call on success functions with lower case
        // letter separately.
        if (element.dataset['onsuccess']) {
            Ajax.runResultFunction(element.dataset['onsuccess'], result, this);
        }

        this.dtRefresh(element);

        if (element.dataset['dismiss']) {
            $('#' + element.dataset['dismiss']).modal('hide');
        }
    },

    /**
     * Run events linked to result after unsuccessful completion.
     * @param result    Result from action call
     * @param element   Element that activated the action
     * @param event     Original event
     */
    errorEventActions: function(result, element, event) {
        // Call on error result functions if specified.
        if (element.dataset['onError']) {
            Ajax.runResultFunction(element.dataset['onError'], result);
        }
        // TODO MK: Fix requirement to call on success functions with lower case
        // letter separately.
        if (element.dataset['onerror']) {
            Ajax.runResultFunction(element.dataset['onerror'], result);
        }
    },

    dtRefresh: function(element) {
        // Refresh datatable is specified
        if ($(element).data('dt-refresh')) {
            DataTable.refreshDataTable('#' + $(element).data('dt-refresh'));
        }
    }
};

/***************************************************************************
 * Modal
 * Emulate class to create Modal and present it to user.
 */
let Modal = {
    constructor: function(options) {
        this.COLOURS = ['danger', 'info', 'inverse', 'primary', 'success', 'warning'];

        this.colour = '';
        this.elementId = '';
        this.class = '';
        this.form = false;
        this.header = 'this Title';
        this.body = '';
        this.footer = '';
        this.closeButton = true;
        this.buttons = [];
        this.dataAttributes = [];

        // Model size defaults to medium. Available sizes: sm, lg, xl, xxl
        this.size = '';

        this.element = undefined;

        if (options.buttons !== undefined) {
            this.buttons = options.buttons;
        }
        let dataAttributes = JSON.stringify(options.dataAttributes);
        if (dataAttributes !== undefined) {
            this.dataAttributes = JSON.parse(dataAttributes);
        }

        this.body= options.body || this.body;
        this.class = options.class || this.class;
        if (options.closeButton !== undefined) {
            this.closeButton = options.closeButton;
        }
        this.colour = options.colour || this.colour;
        this.elementId = options.id || Modal.defaultId();
        this.footer = options.footer || this.footer;
        this.form = options.form || this.form;
        this.header = options.header || this.header;
        this.size = options.size || this.size;


        return this;
    },

    defaultId: function() {
        return 'id' + btoa(Math.random()).substring(0,16);
    },

    id: function(withHash) {
        if (withHash === undefined) {
            withHash = '';
        } else {
            withHash = '#';
        }
        return withHash + this.elementId;
    },

    append: function() {
        let closeButton = null;
        let closeX = null;
        if (this.closeButton) {
            closeButton = $(document.createElement('button'))
                .attr('type', 'button')
                .addClass('btn btn-outline-secondary')
                .attr('data-dismiss', 'modal')
                .text((this.closeButton === true) ? __('Close') : this.closeButton)
            ;
            closeX = $(document.createElement('button'))
                .attr('type', 'button')
                .addClass('close')
                .attr('data-dismiss', 'modal')
                .attr('aria-hidden', true)
                .attr('aria-label', __('Close modal'))
                .html('<i class="fas fa-times"></i>')
            ;
        }
        if (this.colour !== '') {
            this.class += ' modal-outline-' + this.colour;
        }

        let modal = $(document.createElement('div'))
            .attr('id', this.id())
            .addClass('modal fade ' + this.class)
            .attr('tabindex', -1)
            .attr('role', 'dialog')
            .attr('aria-labelledby', this.id())
        ;

        // Add data attributes to form.
        Object.entries(this.dataAttributes).forEach(function(dataAttribute) {
            $(modal).attr('data-' + dataAttribute[0], dataAttribute[1]);
        });

        let modalDialog = $(document.createElement('div'))
            .addClass('modal-dialog')
            .attr('role', 'document')
        ;
        if (this.size) {
            modalDialog.addClass('modal-' + this.size);
        }
        modal.append(modalDialog);

        let modalContent = $(document.createElement('div'))
            .addClass('modal-content');
        modalDialog.append(modalContent);

        let form = $(document.createElement('form'));
        $.each(this.form, function(key, value) {
            form.attr(key, value);
        });
        modalContent.append(form);

        if (this.header || this.closeButton) {
            let textWhite = (this.COLOURS.includes(this.colour)) ? 'text-white' : '';
            let header =  $(document.createElement('div'))
                .addClass('modal-header')
                .addClass(textWhite)
            ;
            form.append(header);

            let title = $(document.createElement('h3'))
                .addClass(textWhite)
                .addClass('modal-title')
                .append(this.header)
            ;
            header.append(title);
            header.append(closeX);
        }

        if (this.body) {
            let body = $(document.createElement('div'))
                .addClass('modal-body')
                .append(this.body)
            ;
            form.append(body);
        }

        if (this.footer || this.closeButton || this.buttons.length > 0) {
            let footer = $(document.createElement('div'))
                .addClass('modal-footer')
                .append(this.footer)
            ;
            // let row =  $(document.createElement('div'))
            //     .addClass('row');

            $(this.buttons).each(function(key, button) {
                footer.append(button);
            });
            footer.append(closeButton);
            form.append(footer);
        }

        this.element = modal;
        $(document.body).append(modal);
        return this;
    },

    show: function() {
        // Append html for modal if not present yet.
        if (this.element === undefined) {
            this.append();
        }

        // Create modal
        let modal = $(this.id(true)).modal();

        // Add events to modal
        if (this.events !== undefined) {
            let events = this.events;
            Object.keys(events).forEach(function (key, index) {
                for (let i = 0; i < events[key].length; i++) {
                    modal.on(key, events[key][i]);
                }
            });
        }

        // Setup event to remove element when closed
        let modalElement = this.element;
        modal.on('hidden.bs.modal', function(e) {
            modalElement.remove();
            // Fix for disappearing scroll bar on closing stacked modals.
            if ($('.modal:visible').length > 0) {
                $('body').addClass('modal-open');
            }
        });
        return this;
    },
};

/***************************************************************************
 * Translations
 * Emulate class to translate JS.
 */
let uxcrudLang = {};

let Translate = {
    constructor: function(options) {
        if (options === undefined) {
            options = {};
        }
        this.lang = $('html').attr('lang');

        this.lang = options.lang || this.lang;
        return this;
    },

    trans: function(string) {
        if (uxcrudLang[this.lang] !== undefined && uxcrudLang[this.lang][string] !== undefined) {
            return uxcrudLang[this.lang][string];
        } else {
            return string;
        }
    }
};

let translate = Translate.constructor();

function __(string) {
    return translate.trans(string);
}
/***************************************************************************
 * Actions
 * Emulate class to package Actions
 */
let Actions = {
    /**
     * Send ajax request and process result.
     * @param button
     * @param e
     * @param params    jQuery.param(object) containing additional parameters
     * @returns {boolean}
     */
    ajax: function(button, e, params) {
        this.initialiseOptions(button, e, params);

        this.options.onSuccess = function(result, ajax) {
            let modal = Modal.constructor(result).append();
            $(modal.element).on('show.bs.modal', function() {
                UxcrudForm.initFormFields(modal.element);
            });
            modal.show();
        }

        let ajax = Ajax.constructor(this.options);
        ajax.call();
        return false;
    },

    /**
     * Show a confirmation dialog. When confirmed send request to server and
     * process the result.
     * @param button
     * @param e
     * @returns {boolean}
     */
    dataConfirm: function(button, e, params) {
        this.initialiseOptions(button, e, params);

        // Create unique id for clicked element if not present
        if (!button.id) {
            button.id = Modal.defaultId();
        }

        let alert = 'danger';
        if ($(button).attr('data-confirm-alert') !== undefined) {
            alert = $(button).attr('data-confirm-alert');
        }

        let buttonLabel = __('Confirm');
        if ($(button).attr('data-confirm-button-label') !== undefined) {
            buttonLabel = $(button).attr('data-confirm-button-label');
        }

        let buttonIcon = 'fas fa-times-circle';
        if ($(button).attr('data-confirm-button-icon') !== undefined) {
            buttonIcon = $(button).attr('data-confirm-button-icon');
        }

        let ajaxCaller = this;

        let confirmButton = $(document.createElement(button.nodeName))
            .html('<i class="' + buttonIcon + '"></i> ' + buttonLabel)
            .addClass('btn btn-outline-' + alert + ' toast-action')
            .attr('data-dismiss', 'modal')
            .on('click', function(e) {
                // Close confirmation modal
                $(confirmButton).closest('.modal').modal('hide');

                // Add confirmed attribute
                $(button).attr('data-confirmed', 'confirmed');
                // Click original button passing event parameters to process confirmed action
                $(button).trigger('click', [e]);
                return false;
            })
        ;

        let modal = Modal.constructor({
            closeButton: true,
            colour: alert,
            header: $(button).data('confirm'),
            //size: 'sm',
            buttons: [
                confirmButton
                ,
                //'<button type="button" class="btn btn-outline-success" data-dismiss="modal"><i class="fas fa-times"></i> Cancel</button>\n'
            ]
        });
        modal.show();
        return false;
    },

    /**
     * Show content provided in either data element or referenced element
     * in a modal window
     * @param button
     * @param e
     */
    showModal: function(button, e) {
        console.log(button)
        let options = {
            colour: 'info',
            closeButton: true,
            id: 'modal-',
            body: '',
            header: '',
            size: 'l'
        };

        options.header = $(button).attr('title');

        // Set id and body depending on settings used.
        if ($(button).data('modal-content-id') === undefined) {
            options.id += Modal.defaultId();
            options.body = $(button).data('modal-content')
        } else {
            options.id += $(button).data('modal-content-id');
            options.body = $('#' + $(button).data('modal-content-id'))[0].innerHTML;
            options.size = 'xl';
        }

        if ($(button).data('modal-size') !== undefined) {
            options.size = $(button).data('modal-size');
        }

        let modal = Modal.constructor(options).append();
        modal.show();
    },

    /**
     * Send ajax request and show result in toast notification.
     * @param button
     * @param e
     * @returns {boolean}
     */
    toast: function(button, e, params) {
        this.initialiseOptions(button, e, params);

        /**
         * @param result    Result from action call
         * @param ajaxCaller      Ajax object for the original call.
         */
        this.options.onSuccess = function (result, ajaxCaller) {
            // Run generic actions on element clicked.
            Ajax.successEventActionsElement(result, button, ajaxCaller);
            // Run generic actions on results returned.
            Ajax.successEventActionsResult(result, button, ajaxCaller);

            result.type = "toast";
            Actions.processResult(result);
        };

        let ajax = Ajax.constructor(this.options).call();
        return false;
    },

    processResult: function(result) {
        switch(result.type) {
            case "toast":
                if (result.header !== null || result.body !== null || result.icon !== undefined) {
                    let delay = 3500;
                    if (result.status !== 'success') {
                        delay = false;
                    }
                    $.toast({
                        heading: result.header,
                        text: result.body,
                        position: 'top-right',
                        icon: result.status,
                        hideAfter: delay,
                    });
                }
                break;
            case "modal":
                let modal = Modal.constructor(result).append();
                $(modal.id).on('show.bs.modal', function() {
                    UxcrudForm.initFormFields(modal);
                });
                modal.show();

                break;
        }
    },

    /**
     * Submit the form using an ajax request and process the return.
     * @param button
     * @param event
     * @returns {boolean}
     */
    uxcrudSubmit: function(button, event, params) {
        let multiPageForm = false;
        this.initialiseOptions(button, event, params);
        let form = $(button).closest('form')[0];

        // Check whether action is overridden in button, event or params.
        let url = this.getUrl(button, event, params);

        if (url !== undefined) {
            form.action = url;
        }

        Ajax.beforeFormSubmit(form);

        let currentTab = $(button).closest('.tab-pane');
        let formElements = $(form).find('input,select,textarea').not('.uxcrud-form-datatable *');
        let formFileElements = $(form).find('input[type="file"]');

        if (button.hasAttribute('data-uxcrud-form-page')) {
            // Remove all elements that are on non visible tabs.
            let pagedElements = $('.uxcrud-pages', form).find('input,select,textarea').not('.uxcrud-form-datatable *');

            formElements = formElements.filter(function(value, element, arr) {
                let tabPane = $(element).closest('.tab-pane');
                if (tabPane.length === 0) {
                    // Field not in tab so include
                    return true;
                }
                return (tabPane[0].id === currentTab[0].id);
            });
        }

        let formData = new FormData();
        // Add data elements from button (stored in options) to form data
        if (this.options.data instanceof FormData) {
            formData = this.options.data;
        }
        if (button.hasAttribute('data-dt-with-filters')) {
            let dt = $('#' + $(button).attr('data-dt-with-filters')).DataTable();
            params = DataTable.lastRequestParameters(dt);
            // Add filters from data table to form data
            formData = Utils.addObjectToFormData(formData, params);
        }

        if (button.hasAttribute('data-uxcrud-form-page')) {
            // Use set instead of append to avoid duplicating fields on form submit
            formData.set('uxcrud-page', currentTab[0].id);
            multiPageForm = true;
        }

        $(formElements).each(function() {
            if (this.name.endsWith(']')) {
                let name = this.name;
                switch (this.type) {
                    case "checkbox":
                        if (this.checked) {
                            // Use set instead of append to avoid duplicating fields on form submit
                            formData.set(name, "on");
                        }
                        break;
                    case "radio":
                        if (this.checked) {
                            // Use set instead of append to avoid duplicating fields on form submit
                            formData.set(this.name, this.value);
                        }
                        break;
                    default:
                        if (Array.isArray($(this).val())) {
                            // Get list of existing values
                            let values = formData.getAll(name);
                            $($(this).val()).each(function () {
                                // Only add value if it doesn't exist yet.
                                if (!values.includes(String(this))) {
                                    // Use append as we're adding multiple values.
                                    formData.append(name, String(this));
                                }
                            });
                        } else {
                            // Changed to allow nesting. Returned error for saving Insight > Clients > Communications Log
                            //formData.append(name, null);
                            // Use set instead of append to avoid duplicating fields on form submit
                            formData.set(name, this.value);
                        }
                        break;
                }
            } else {
                switch(this.type) {
                    case "checkbox":
                        if (this.checked) {
                            // Use set instead of append to avoid duplicating fields on form submit
                            formData.set(this.name, this.checked);
                        }
                        break;
                    case "radio":
                        if (this.checked) {
                            // Use set instead of append to avoid duplicating fields on form submit
                            formData.set(this.name, this.value);
                        }
                        break;
                    default:
                        // Use set instead of append to avoid duplicating fields on form submit
                        formData.set(this.name, this.value);
                        break;
                }
            }
        });

        $(formFileElements).each(function() {
            let name = this.name;
            $($(this).prop('files')).each(function(i, file) {
                // Use set instead of append to avoid duplicating fields on form submit
                formData.set(name, file);
            });
        });

        let ajax = Ajax.constructor({
            url: form.action,
            type: form.method,
            enctype: form.enctype,
            data: formData,
            //data: new FormData(formElements), //.serialize(),
            //data: $(formElements).serialize(),
            onSuccess: function (result) {
                // Run generic actions on form
                Ajax.successEventActionsElement(result, form);
                // Run generic actions on element clicked.
                Ajax.successEventActionsElement(result, button);
                // Run generic actions on results.
                Ajax.successEventActionsResult(result, form, event);

                let messages = UxcrudForm.resetMessages(form, 'danger');

                // Check for success function based on form id and execute.
                let functionName = "onSuccess" + form.id.studly();
                console.log("Checking for " + functionName);
                Ajax.runResultFunction(functionName, result);

                if (multiPageForm) {
                    let tab = $(button).closest('.uxcrud-pages').find('.uxcrud-form-tab a[href="#' + $(button).attr('data-uxcrud-continue-to') + '"]');
                    tab.click();
                    document.getElementById($(button).attr('data-uxcrud-continue-to') + '-nav-link').scrollIntoView();
                } else {
                    if (result.type) {
                        Actions.processResult(result);
                    }

                    if (result.close !== false) {
                        // Close model on success
                        $(button).closest('div.modal').modal('hide')
                    }
                }
            },
            onError: function(result) {
                if (result.status === 419 && result.responseJSON.message === "CSRF token mismatch.") {
                    console.log('Fix it!')
                }

                Ajax.errorEventActions(result, form);

                // Check for error function based on form id and execute.
                let functionName = "onError" + form.id.studly();
                Ajax.runResultFunction(functionName, result);

                // Clear previous messages and icons
                $('.has-danger', form).removeClass('has-danger');
                $('.is-invalid', form).removeClass('is-invalid');
                $('.form-control-feedback .danger', form).html('');
                //$('.messages', form).remove();

                let messages = UxcrudForm.resetMessages(form, 'danger');

                let message = result.responseJSON.message;
                if (!message) {
                    message = result.message;
                }
                if (!message) {
                    message = result.status + " " + result.statusText;
                }

                messages.html(message)
                    .addClass('mt-3 messages alert alert-danger');
                let errors = $(document.createElement('ul'));

                // Set current messages
                $.each(result.responseJSON.errors, function(key, message) {
                    let line = $(document.createElement('li'))
                        .addClass('pt-0');

                    // If translatable convert language.key_name to language[key_name]
                    let inputName = UxcrudForm.dotsToBrackets(key);

                    // Match inputName with name (element of type input) or
                    // data-name (other element types)
                    let field = $('[name="' + inputName + '"],[data-name="' + inputName + '"]', form).not('.uxcrud-form-datatable *');
                    if (field.length === 0) {
                        field = $('.' + key + '.uxcrud-form-datatable table', form);
                    }
                    field.removeClass('is-valid').addClass('is-invalid');

                    let uxcrudLine = $(field).closest('.uxcrud-line')
                        .addClass('has-danger');

                    let feedback = $('.form-control-feedback .danger', uxcrudLine).not('.uxcrud-form-datatable .danger *');
                    feedback.html(message);

                    let link = $(document.createElement('a')).attr('href', '#' + key);
                    link.html(message + " <i class='fas fa-highlighter'></i>" );

                    line.append(link);
                    errors.append(line);
                });
                messages.append(errors);
                return true;
            },
        }).call();

        return false;
    },

    /**
     * Determine Url to use based on parameters supplied to button and event.
     * @param button
     * @param e
     * @param params
     */
    getUrl: function(button, e, params) {
        let url;
        // Check event
        if (e.currentTarget.href !== undefined) {
            url = e.currentTarget.href;
            // Check button href
        } else if (button.href !== undefined) {
            url = button.href;
            // Check button data-href
        } else if ($(button).data('href') !== undefined) {
            url = $(button).data('href');
        }
        return url;
    },

    /**
     * Initialise options based on method either use containing url or data
     * @param button
     * @param e
     * @param params
     */
    initialiseOptions: function(button, e, params) {
        let data;

        // Retrieve url to use from
        let url = this.getUrl(button, e, params);

        // Set default method based on element type.
        let method = (e.currentTarget.nodeName === 'A') ? 'GET' : 'POST';

        // Override method based on data-method attribute
        if ($(button).data('method')) {
            method = $(button).data('method');
        }

        // Add data from data attributes and parameters to appropriate format.
        if (method === 'POST') {
            data = new FormData();
            // If clicked item is a button check for form and submit.
            if (button.nodeName === "BUTTON") {
                let form = $(button).closest('form')[0];
                if (form !== undefined) {
                    Ajax.beforeFormSubmit(form);
                    url = form.action;
                    method = form.method;
                    data = new FormData(form);
                }
            }
            // Add data  parameters of button's parameters to form
            data = Utils.addObjectToFormData(data, Object.assign($(button).removeData('href').data(), params));

        } else {
            let urlParams = $.param(Object.assign($(button).removeData('href').data(), params));
            if (urlParams) {
                url += "?" + urlParams;
            }
        }

        this.options = {
            url: url,
            data: data,
            type: method,
        }

        if (this.options.modifierKeys === undefined) {
            this.options.modifierKeys = {};
        }

        // Modifier keys are only implemented for redirects currently.
        if (e.altKey !== undefined) {
            this.options.modifierKeys.alt = e.altKey;
        }
        if (e.ctrlKey !== undefined) {
            this.options.modifierKeys.ctrl = e.ctrlKey;
        }
        if (e.shiftKey !== undefined) {
            this.options.modifierKeys.shift = e.shiftKey;
        }

        return this.options;
    },
};
/***************************************************************************
 * Session Time Out
 * Emulate class to track Session Time Out and show warning dialog.
 * User can either extend their session using a AJAX url call to keep the session alive
 * or a lock screen is shown.
 * From the lock screen a user can login again and the CSFR token is automatically refreshed.
 */

function SessionTimeoutOnSuccessUnlockSession(result) {
    // Update CSRF tokens
    $("[name=_token]").val(result.data.csrf);
    $("meta[name=csrf-token]").attr('content', result.data.csrf);

}
function onSuccessUxcrudibleFormUnlockSession(result) {
    SessionTimeout.reset(result);

}

let SessionTimeout = {
    constructor: function (options) {
        if (options === undefined) {
            options = {};
        }
        this.dialogHeader = __('Your session is about to expire.');
        this.dialogTimeout = 20 * 60; // time in seconds

        this.lockTimeout =  15; // time in seconds

        this.keepAliveURI = '/session/keep-alive';
        this.keepAliveURIType = 'POST'; // (GET/POST/PUT)
        this.loginURI = 'session/login';
        this.loginURIType = 'POST'; // (GET/POST/PUT)
        this.dialogTimer = null;

        this.dialogHeader = options.dialogHeader || this.dialogHeader;
        this.dialogTimeout = options.dialogTimeout || this.dialogTimeout;
        this.lockHeader = options.lockHeader || this.lockHeader;
        this.lockBody= options.lockBody|| this.lockBody;
        this.lockTimeout = options.lockTimeout || this.lockTimeout;

        this.keepAliveURI = options.keepAliveURI || this.keepAliveURI;
        this.keepAliveURIType = options.keepAliveURIType || this.keepAliveURIType;
        this.loginURI = options.loginURI || this.loginURI;
        this.loginURIType = options.loginURIType || this.loginURIType;

        this.startDialogTimer();

        return this;
    },

    documentReady: function() {
        $(document.body).on('click', '#lock-session',  function(e) { SessionTimeout.lockSession(e) });
        $(document.body).on('click', '#suspend-session',  function(e) { SessionTimeout.manualLock(e) });
        $(document.body).on('click', '#extend-session', function(e) { SessionTimeout.extendSession(e) });
    },

    lockSession: function (e) {
        e.preventDefault();
        this.stopLockTimer();
    },

    extendSession: function (e) {
        e.preventDefault();
        onSuccessUxcrudibleFormUnlockSession(e);
    },

    startDialogTimer: function () {
        if (this.dialogTimeout > 0) {
            SessionTimeout.dialogTimer = setTimeout(SessionTimeout.onDialogTimerEvent, SessionTimeout.dialogTimeout * 1000);
        }
    },

    stopDialogTimer: function() {
        clearTimeout(this.dialogTimer);
    },

    onDialogTimerEvent: function(classes) {
        SessionTimeout.stopDialogTimer();
        // Only show session timeout dialog when no lock screen shown.
        if($('#lock-screen').length === 0) {
            let modal = Modal.constructor({
                colour: 'info',
                id: 'sessionTimeoutDialog',
                class: 'uxcrud-model-priority' + classes,
                header: '<i class="fas fa-stopwatch"></i> ' + SessionTimeout.dialogHeader,
                closeButton: false,
                dataAttributes: {
                    backdrop: 'static',
                    keyboard: false,
                },
                buttons: [
                    '<a id="lock-session" href="/' + $('body').data('session-unlock') + '" onclick="SessionTimeout.locking()" class="modal-action btn btn-outline-info">' +
                    '<i class="fas fa-user-lock"></i> ' + __('Lock session') +
                    '<div class="progress">' +
                    '   <div class="progress-bar bg-danger" style="width: 100%; height:6px;" aria-valuenow="' + SessionTimeout.lockTimeout + '" aria-valuemin="0" aria-valuemax="' + SessionTimeout.lockTimeout + '" role="progressbar"> </div>' +
                    '</div>' +
                    '</a>'
                    ,
                    '<a id="extend-session" href="/' + $('body').data('session-extend') + '" class="toast-action btn btn-info" data-dismiss="modal"><i class="fas fa-sign-in-alt"></i> ' + __('Stay logged in') + '</a>',
                ],
            }).append();
            modal.show();
            SessionTimeout.startLockTimer();
        }
    },

    startLockTimer: function() {
        SessionTimeout.lockTimer = setTimeout(SessionTimeout.onLockTimerEvent, SessionTimeout.lockTimeout * 1000);
        SessionTimeout.lockIntervalId = setInterval(SessionTimeout.animateLockTimer, 1000)
    },

    animateLockTimer: function() {
        let progressBar = $('#lock-session .progress-bar');
        let intervalProgress = progressBar.attr('aria-valuenow');
        progressBar.attr('aria-valuenow', intervalProgress - 1);
        progressBar.css('width',
            parseFloat(intervalProgress / progressBar.attr('aria-valuemax') * 100).toFixed(2) + "%"
        )
    },

    stopLockTimer: function() {
        clearTimeout(SessionTimeout.lockTimer);
        clearInterval(SessionTimeout.lockIntervalId);
    },

    locking: function() {
        $('#sessionTimeoutDialog').modal('hide');
    },

    onLockTimerEvent: function() {
        console.log('onLockTimerEvent')
        $('#lock-session').click();
    },

    reset: function(result) {
        SessionTimeout.stopDialogTimer();
        SessionTimeout.stopLockTimer();
        SessionTimeout.startDialogTimer();
    },

    manualLock: function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        SessionTimeout.stopDialogTimer();
        // Create hidden timeout dialog
        SessionTimeout.onDialogTimerEvent(' d-none');
        // Start session lock.
        SessionTimeout.onLockTimerEvent();
        SessionTimeout.stopLockTimer();
        $('#sessionTimeoutDialog').modal('hide');
        $('.modal-backdrop.fade.show').remove();
    }
};

$(document).ready(function() {
    // Check for outdated browser versions and show message.
    if (
        window.document.documentMode // IE
    ) {
        $('.outdated-browser-warning').removeClass('hide');
    }

    let sessionTimeout = $('body').data('session-timeout');
    if (sessionTimeout) {
        SessionTimeout.constructor({
            dialogTimeout: sessionTimeout,
            lockTimeout: $('body').data('session-dialog-showtime')

        });
        SessionTimeout.documentReady();
    }

    /**
     * Add active class to li of tab.
     */
    $(document.body).on('click change', '.uxcrud-steps .nav-tabs a', function(e) {
        $(e.target).closest('ul').find('li.active').removeClass('active');
        $(e.target).closest('li').addClass('active');         // active tab
    });

    UxcrudForm.documentReady();
    //initFormFields();

    /**
     * Matrix Field quick select entries after click on table row
     */
    $(document.body).on('click', '.matrix.quick-select-row table tbody th', function() {
        var row = $(this).closest('tr');
        var tds = $('input[type=checkbox]', row);
        var setTo = !tds.first()[0].checked;
        tds.each(function() {
            this.checked = setTo;
        });
    });

    /**
     * Matrix Field quick select entries after click on table column
     */
    $(document.body).on('click', '.matrix.quick-select-column table thead th', function() {
        var tds = $(this).closest('table').find('td.' + this.className + ' input[type=checkbox]');
        var setTo = !tds.first()[0].checked;
        tds.each(function() {
            this.checked = setTo;
        });
    });

    /***************************************************************************
     * Filters
     */

    /**
     * Show fields on page load when entries are set
     */
    let runInitFormFields = false;
    $('tr.uxcrud-filters').each(
        function(i, filters) {
            $('.uxcrud-filter', filters).each(
                function(j, filter) {
                    if(filter.value) {
                        if (!$(filters).is(":visible")) {
                            $(filters).show();
                            runInitFormFields = true;
                            return true;
                        }
                        return false;
                    }
                    //return false;
                }
            );
            $('.uxcrud-filter-date', filters).each(
                function() {
                    setDateFilterOptions(this);
                }
            )
        }
    );

    //if (runInitFormFields) {
        UxcrudForm.initFormFields();
    //}


    /**
     * Show/hide filters when expand/minimise filters button has been clicked
     */
    $(document.body).on('click', '.uxcrud-expand-filters', function() {
        // Find the tr with the filters on
        let filters = $(this).closest('.dataTables_wrapper').find('thead tr.uxcrud-filters');
        // Show/hide
        filters.toggle();
        // Fix select2 width.
        //$('.select2', filters).select2();
        UxcrudForm.initFormFields(filters);
        // Change search icon
        $($(this).find('i')).toggleClass('fa-search-plus fa-search-minus')
    });

    /**
     * Clear filters
     */
    $(document.body).on('click','.uxcrud-clear-filter', function() {
        // Find the tr with the filters on and get all filters contained
        let filters = $(this).closest('thead').find('tr.uxcrud-filters .uxcrud-filter');
        // Loop over all filters
        $(filters).each(function() {
            this.value = null;
            if ($(this).hasClass('select2')) {
                // If select2 option clear options from select2
                $(this).trigger('change');
            }
        });
        // Apply cleared filter
        DataTable.refreshDataTable('#' + $(this).closest('table')[0].id);

        //$('tr.uxcrud-filters .uxcrud-apply-filter').click();
    });

    /**
     * Apply filter when filter input changes
     */
    $(document.body).on('input', 'tr.uxcrud-filters .uxcrud-filter', function(event) {
        DataTable.refreshDataTable('#' + $(this).closest('table')[0].id);
    });

    /**
     * Apply filter when filter selected value changes
     */
    $(document.body).on('change', 'tr.uxcrud-filters .uxcrud-filter.select2', function(event) {
        DataTable.refreshDataTable('#' + $(this).closest('table')[0].id);
    });

    /**
     * Apply filters on change
     */
    $(document.body).on('click', 'tr.uxcrud-filters .uxcrud-apply-filter', function() {
        let filters = $(this).closest('thead').find('tr.uxcrud-filters .uxcrud-filter');
        let params = [];
        $(filters).each(function(i, field) {
            let val = $(this).val();
            if (Array.isArray(val)) {
                if (val.length > 0) {
                    val.forEach(function (val) {
                        params.push({
                            "name": field.name,
                            "value": val
                        });
                    })
                }
            } else if (val !== "") {
                params.push({
                    "name": this.name,
                    "value": val
                });
            }
        });

        DataTable.refreshDataTable('#' + $(this).closest('table')[0].id);
    });

    /**
     * Set date filter options
     */
    $(document.body).on('click', 'tr.uxcrud-filters .uxcrud-filter-date', function() {
        setDateFilterOptions(this);
    });

    /**
     * Toggle locale selection for datatable.
     */
    $(document.body).on('click', '.uxcrud-datatable-select-locale input[name=locale]', function() {
        console.log('datatable locale clicked');
        $(this).closest('.btn-group').children().removeClass('active');
        $(this).closest('.btn').addClass('active');

        DataTable.refreshDataTable('#' + $(this).closest('table')[0].id);
    });

    /**
     * Toggle locale selection for edit modal.
     */
    $(document.body).on('click', '.uxcrud-modal-select-locale input[name=locale]', function() {
        console.log('modal locale clicked');
        // Get locales from data-locales attribute.
        let locales = $(this).closest('.btn-group').data('locales');

        // Toggle radio button to reflect click.
        $(this).closest('.btn-group').children().removeClass('active');
        $(this).closest('.btn').addClass('active');


        // Get current modal.
        let modal = $(this).closest('.uxcrud-edit-modal');

        // Remove any previously selected locales
        $(locales).each(function() {
            modal.removeClass('locale-' + this);
        });


        if (this.value == 'all') {
            $(locales).each(function() {
                modal.addClass('locale-' + this);
            });

        } else {
            modal.addClass('locale-' + this.value);
        }


    });

    /**
     * Detect clicks on Uxcrud Page Next and Previous buttons and select appropriate tab.
     */
    $(document.body).on('click', '.uxcrud-pages .tab-pane a[data-toggle=uxcrud-page]', function() {
        let uxcrudPages = $(this).closest('.uxcrud-pages');
        let hash = this.href.split('#')[1];
        $(uxcrudPages).find('a.nav-link[href="#' +  hash + '"]').click();
    });


        /***************************************************************************
     * Form fields
     */
    $(document.body).on('keyup', 'input.uxcrud-icon-field', function() {
        $(this).closest('.input-group').find('.uxcrud-icon-preview i').removeClass().addClass(this.value);
    });

    // On change of value update depended detail fields depending on value set.
    $(document.body).on('keyup change', UxcrudForm.detailFormElements, function() {
        UxcrudForm.toggleTargetDetail(this);
    });

    /***************************************************************************
     * Page Model
     */
    /**
     * Automatically preset slug based on name field and locale.
     */
    let pageNameInputs = 'form.uxcrudible-form.page .uxcrud-line.name input';
    let pageSlugInputs = 'form.uxcrudible-form.page .uxcrud-line.slug input';

    function setLinkIfNameMatchesSlug(locale) {
        $(pageSlugInputs + "[data-locale=" + locale + "]").data('linkedToName',
            $(pageNameInputs + "[data-locale=" + locale + "]").val().kebab() == $(pageSlugInputs + "[data-locale=" + locale + "]").val() ? true: false);
    }

    $(document.body).on('change', pageSlugInputs, function() {
        setLinkIfNameMatchesSlug($(this).data('locale'));
    });

    $(document.body).on('focus', pageNameInputs, function() {
        setLinkIfNameMatchesSlug($(this).data('locale'));
    });

    $(document.body).on('keyup', pageNameInputs, function() {
        let locale = $(this).data('locale');
        if ($(pageSlugInputs + "[data-locale=" + locale + "]").data('linkedToName')) {
            $(pageSlugInputs + "[data-locale=" + locale + "]").val(
                $(pageNameInputs + "[data-locale=" + locale + "]").val().kebab()
            );
        }
    });

    /***************************************************************************
     * Actions
     */
    /**
     * Show confirmation dialog on click. Enable by adding data-confirm class
     */
    $(document.body).on('click', '[data-confirm]', function(e) {
        if ($(this).attr('data-confirmed') !== 'confirmed') {
            e.stopPropagation();
            e.stopImmediatePropagation();
            return Actions.dataConfirm(this, e);
        } else {
            $(this).removeAttr('data-confirmed');
        }
    });

    /**
     * Show modal with content from element
     *      data-modal-content-id: id of element to get the content from.
     *      data-modal-content: content for the modal.
     *      data-modal-size: size of modal: s, m, l, xl, xxl
     *      title: title used as the header for the modal
     **/
    /**
     * @param event         Event passed by system click.
     * @param confirmEvent  Event passed if confirm modal dialog was shown
     */
    $(document.body).on('click', '.show-modal', function (event, confirmEvent) {
            if (confirmEvent !== undefined) {
                event = confirmEvent;
            }
            return Actions.showModal(this, event);
    });

    /** Execute Ajax script and show result in Modal **/
    /**
     * @param event         Event passed by system click.
     * @param confirmEvent  Event passed if confirm modal dialog was shown
     */
    $(document.body).on('click', '.modal-action, .ajax-action', function (event, confirmEvent) {
        if (confirmEvent !== undefined) {
            event = confirmEvent;
        }
        return Actions.ajax(this, event);
    });

    /** Execute Ajax script and show result in Toast notification **/
    /**
     * @param event         Event passed by system click.
     * @param confirmEvent  Event passed if confirm modal dialog was shown
     */
    $(document.body).on('click', '.toast-action', function (event, confirmEvent) {
        if (confirmEvent !== undefined) {
            event = confirmEvent;
        }
        return Actions.toast(this, event);
    });

    // /** Run ajax and show result in a modal **/
    // $(document.body).on('click', '.ajax-action', function (e) {
    //     let ajax = Ajax.constructor({
    //         url: this.href,
    //         type: (e.currentTarget.nodeName === 'A') ? 'GET' : 'POST',
    //         onSuccess: function (result) {
    //             let modal = Modal.constructor(result).show();
    //         },
    //     }).call();
    //     return false;
    // });

    /** Submit form using ajax **/
    $(document.body).on('click', '.ajax-action-uxcrud-submit', function (e) {
        return Actions.uxcrudSubmit(this, e);
    });

    /***************************************************************************
     * Side bar navigation fix for top level items without sub menu.
     */
    $('#sidebarnav .nav-link').on('click', function() {
        window.location.href = this.href;
    });
});
