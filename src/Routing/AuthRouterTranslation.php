<?php

namespace Heiw\Uxcrudible\Routing;

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\VerificationController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/**
 * Translated Auth routes.
 * Class AuthRouter
 * @package Heiw\Uxcrud\Routing
 */
class AuthRouterTranslation
{
    /**
     * Register the typical authentication routes for an application.
     * Method copied from Illuminate\Routing\Router->auth(...)
     *
     * @param array $options
     * @return void
     */
    public function auth(array $options = []) {
        // Authentication Routes...
        Route::get(Str::slug(__('uxcrud::slugs.login')), [LoginController::class, 'showLoginForm'])->name('login');
        Route::post(Str::slug(__('uxcrud::slugs.login')), [LoginController::class, 'login']);
        Route::post(Str::slug(__('uxcrud::slugs.logout')), [LoginController::class, 'logout'])->name('logout');

        // Registration Routes...
        if ($options['register'] ?? true) {
            Route::get(Str::slug(__('uxcrud::slugs.register')), [RegisterController::class, 'showRegistrationForm'])->name('register');
            Route::post(Str::slug(__('uxcrud::slugs.register')), [RegisterController::class, 'register']);
        }

        // Password Reset Routes...
        if ($options['reset'] ?? true) {
            $this->resetPassword();
        }

        // Email Verification Routes...
        if ($options['verify'] ?? false) {
            $this->emailVerification();
        }
    }

    /**
     * Register the typical reset password routes for an application.
     * Method copied from Illuminate\Routing\Router->resetPassword()
     *
     * @return void
     */
    public function resetPassword() {
        Route::get(Str::slug(__('uxcrud::slugs.password/reset')), [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
        Route::post(Str::slug(__('uxcrud::slugs.password/email')), [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
        Route::get(Str::slug(__('uxcrud::slugs.password/reset')) . "/{token}", [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
        Route::post(Str::slug(__('uxcrud::slugs.password/reset')), [ResetPasswordController::class, 'reset'])->name('password.update');
    }

    /**
     * Register the typical email verification routes for an application.
     * Method copied from Illuminate\Routing\Router->emailVerification()
     *
     * @return void
     */
    public function emailVerification() {
        Route::get(Str::slug(__('uxcrud::slugs.email/verify')), [VerificationController::class, 'show'])->name('verification.notice');
        Route::get(Str::slug(__('uxcrud::slugs.email/verify')) . "/{id}", [VerificationController::class, 'verify'])->name('verification.verify');
        Route::get(Str::slug(__('uxcrud::slugs.email/resend')), [VerificationController::class, 'resend'])->name('verification.resend');
    }
}
