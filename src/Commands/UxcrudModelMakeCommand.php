<?php
namespace Heiw\Uxcrudible\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

/**
 * Experiment with command lines
 * Class CreateColours
 * @package Heiw\Uxcrudible\Commands
 */
class UxcrudModelMakeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uxcrud:make-model {name : The name of the model} 
        {--factory : Indicate whether to create a factory }
        {--seeder : Indicate whether to create a seeder }
        {--migration : Indicate whether to create a migration }
        {--controller : Indicate whether to create a controller }
        {--translatable : Indicate whether a translation migration should be generated }
        {--overwrite : Indicate whether existing files should be overwritten }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make Uxcrud Controller and Model';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'UxcrudModel';

    /**
     * Get the default namespace for the corresponding model.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    public static function modelNamespace($rootNamespace, $trim = false)
    {
        $namespace = $rootNamespace.'\\'. config('uxcrud.model_namespace');
        if ($trim) {
            $namespace = trim($namespace, '\\');
        }
        return $namespace;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$this->info("Generating the rainbow");

        // Default to all
        $optionSet = false;
        foreach($this->options() as $option) {
            if ($option) {
                $optionSet = true;
            }
        }
        if (!$this->input->getOption('factory') &&
            !$this->input->getOption('migration') &&
            !$this->input->getOption('seeder') &&
            !$this->input->getOption('controller')) {
            $this->input->setOption('factory', true);
            $this->input->setOption('migration', true);
            $this->input->setOption('seeder', true);
            $this->input->setOption('controller', true);
        }

        $name = Str::snake(trim($this->input->getArgument('name')));
        $this->writeModel($name);

        if ($this->input->getOption('factory')) {
            $this->createFactory();
        }

        if ($this->input->getOption('migration')) {
            $this->createMigration();
        }

        if ($this->input->getOption('seeder')) {
            $this->createSeeder();
        }

        if ($this->input->getOption('controller')) {
            $this->createController();
        }

        return true;
    }

    public function getStub() {
        $translatable = ($this->input->getOption('translatable')) ? "_translation" :"";
        return __DIR__ . "/stubs/model{$translatable}.stub";
    }

    public function getTranslationStub() {
        $translatable = ($this->input->getOption('translatable')) ? "_translation" :"";
        return __DIR__ . "/stubs/model{$translatable}_translation.stub";
    }

    public function getOutputPathBase() {
        return $this->laravel->basePath()."/app/" . config('uxcrud.model_namespace') ."/";
    }

    /**
     * Write the controller file to disk.
     *
     * @param  string  $name
     * @return string
     */
    protected function writeModel($name)
    {
        // Generate base table
        $file = $this->getOutputPathBase() . $this->getClassName($name) . ".php";
        $replacements = $this->stubReplacements($name);

        $stub = new StubOutputGenerator($this->input);
        if ($stub->setStubLocation($this->getStub())
            ->setOutputPath($file)
            ->setReplacements($replacements)
            ->generate()
        ) {
            $this->line("<info>Created Model:</info> {$file}");
        }

        // Generate translation for base table
        if ($this->input->getOption('translatable')) {
            $fileTranslation = $this->getOutputPathBase() . $this->getClassName($name) . "Translation.php";

            $stub->setStubLocation($this->getTranslationStub())
                ->setOutputPath($fileTranslation)
                ->setReplacements($replacements)
                ->generate()
            ;

        }


        return true;
    }

    /**
     * Add additional options from original command.
     * @param $options
     * @return mixed
     */
    public function passThroughOptions($options) {
        if ($this->input->getOption('translatable')) {
            $options['translatable'] = true;
        }
        if ($this->input->getOption('overwrite')) {
            $options['overwrite'] = true;
        }
        return $options;
    }

    /**
     * Create a model factory for the model.
     *
     * @return void
     */
    protected function createFactory()
    {
        $factory = Str::studly(class_basename($this->argument('name')));

        $this->call('make:factory',
            [
                'name' => "{$factory}Factory",
                '--model' => $this->qualifyClass($this->getNameInput())
            ]
        );
    }

    /**
     * Create a migration file for the model.
     *
     * @return void
     */
    protected function createMigration()
    {
        $table = Str::snake(trim(Str::singular(class_basename($this->argument('name')))));

        $this->call('uxcrud:make-migration',
            $this->passThroughOptions([
                'name' => "create_{$table}_table",
                '--create' => $table,
            ])
        );
    }

    /**
     * Create a seeder file for the model.
     *
     * @return void
     */
    protected function createSeeder()
    {
        $table = Str::snake(Str::pluralStudly(class_basename($this->argument('name'))));

        $table = Str::singular($table);

        $this->call('uxcrud:make-seeder',
            [
                'name' => $table,
            ]
        );
    }

    /**
     * Create a controller for the model.
     *
     * @return void
     */
    protected function createController()
    {
        $controller = Str::studly(class_basename($this->argument('name')));

        $modelName = Str::singular($this->qualifyClass($this->getNameInput()));

        $this->call('uxcrud:make-controller',
            $this->passThroughOptions([
                'name' => "{$controller}",
            ])
        );
    }

    /**
     * Populate the place-holders in the migration stub.
     *
     * @param  string  $name
     * @return array
     */
    protected function stubReplacements($name)
    {
        return [
            'DummyClass' => $this->getClassName($name),
            'DummyTable' => Str::snake($name),
            'DummyModelNamespace' => UxcrudModelMakeCommand::modelNamespace('App', true)
        ];
    }


    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    protected function getClassName($name)
    {
        return Str::studly($name);
    }

}
