<?php

namespace Heiw\Uxcrudible\Commands;

use Heiw\Uxcrudible\Models\Translation;
use Heiw\Uxcrudible\Models\TranslationSource;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class UxcrudTranslationSyncCommand extends Command
{
    private const LARAVEL_TRANSLATABLE_STRING_EXPORTER_CONFIG = 'config/laravel-translatable-string-exporter.php';

    private const UXCRUD_TRANSLATION_PREFIX = 'uxcrud::';

    private const IGNORE_STRINGS = [
        'name_t',
        '+n+(r?',
        '" . $translatable_string . "',
        'name1_en',
        'name2_en',
        'name4_en',
        'nom',
        'name__',
        'name_lang',
    ];

    private $uxcrudLocation = "packages/heiw/uxcrudible/src/resources/lang/";

    protected $files = null;
    protected $copyFirstFile = null;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uxcrud:translation-sync
        {--options= : The options file to use, override by other parameters below }
        {--locale= : The locale to use }
        {--namespace= : Specify which directories to search in for laravel-translatable-string-exporter. }
        {--translation-parameter-replacement=: : If set to something other than : parameters in translation strings will 
        be delimited using the character provided.     
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Synchronises translations.     
Runs commands:
    translatable command:export
    uxcrud:translation-sync
    
If --namespace specified it will adjust laravel-translatable-string-exporter.php settings before running translatable.      
";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->loadOptions();
        if ($this->setTranslatableStringOption()) {
            $this->processFiles();
        }

        return true;
    }

    protected function loadOptions() {
        if ($this->input->hasOption('options')) {
            $options = include(base_path($this->input->getOption('options')));
            $this->input->setOption('locale', $options['locale']);
            $this->input->setOption('namespace', $options['namespace']);
            $this->copyFirstFile = $options['copyFirstFile'];
            $this->files = $options['files'];

//            $this->info(print_r($options, true));
        }
    }

    protected function processFiles() {
        $tempFilename = 'resources/lang/';
        $locale = $this->input->getOption('locale');
        if ($this->copyFirstFile) {
            // Copy existing package translation file for translatable:export
            $locale = bin2hex(random_bytes(2));
            $tempFilename .= "$locale.json";
            if (is_file(array_key_first($this->files))) {
                copy(array_key_first($this->files), $tempFilename);
            }
        }

        // Run translatable.
        $this->call("translatable:export",
            [
                'lang' => $locale
            ]
        );

        if ($this->copyFirstFile && is_file($tempFilename)) {
            // Copy processed file back to package translation
            copy($tempFilename, array_key_first($this->files));
            unlink($tempFilename);
        }

        foreach($this->files as $file => $useKeys) {
            $options = [
                'file' => $file,
                'locale' => $this->input->getOption('locale'),
            ];
            if ($useKeys) {
                $options['--use-keys'] = true;
            }
            $this->call("uxcrud:translation-sync-file",
                $options
            );
        }
    }

    /**
     * If namespace option set update laravel-translatable-string-exporter.php with settings
     * otherwise leave as is.
     */
    protected function setTranslatableStringOption() {
        if ($this->input->hasOption('namespace')) {
            $namespaceFound = false;
            $namespace = strtolower($this->input->getOption('namespace'));
            $lines = explode("\n", file_get_contents(base_path(self::LARAVEL_TRANSLATABLE_STRING_EXPORTER_CONFIG)));
            foreach($lines as $key => $line) {
                // Check for all lines that have # directories
                if (strpos($line, "# directories") !== false) {
                    // If current $namespace remove comment
                    if (strpos(strtolower($line), "# directories:$namespace") !== false) {
                        $namespaceFound = true;
                        $lines[$key] = str_replace('//', '', $line);
                    } else {
                        // Otherwise ensure line starts with //
                        $lines[$key] = '//' . trim($line, '/');
                    }
                }
            }
            if (!$namespaceFound) {
                $this->error("Namespace: $namespace not found in " . self::LARAVEL_TRANSLATABLE_STRING_EXPORTER_CONFIG);
                return false;
            } else {
                file_put_contents(base_path(self::LARAVEL_TRANSLATABLE_STRING_EXPORTER_CONFIG), implode("\n", $lines));
            }
        }
        return true;
    }
}
