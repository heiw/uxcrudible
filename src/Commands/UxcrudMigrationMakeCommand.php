<?php
namespace Heiw\Uxcrudible\Commands;

use Heiw\Uxcrudible\Form\Fields\Permission;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Database\Console\Migrations\MigrateMakeCommand;
use Illuminate\Database\Console\Migrations\TableGuesser;
use Illuminate\Database\Migrations\MigrationCreator;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Composer;
use Illuminate\Support\Str;

/**
 * Experiment with command lines
 * Class CreateColours
 * @package Heiw\Uxcrudible\Commands
 */
class UxcrudMigrationMakeCommand extends GeneratorCommand
{
    private $create = false;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uxcrud:make-migration {name : The name of the migration}
        {--create : The table to be created}
        {--table : The table to migrate}
        {--path : The location where the migration file should be created}
        {--from-uxcrud-controller : Create table columns based on uxcrud controller}
        {--translatable : Indicate whether a translation migration should be generated }
        {--overwrite : Indicate whether existing files should be overwritten }
        {--fullpath : Output the full path of the migration}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Migration';

    /**
     * Create a new migration install command instance.
     *
     * @param  \Illuminate\Support\Composer  $composer
     * @return void
     */
    public function __construct(Filesystem $files, Composer $composer)
    {
        parent::__construct($files);
        $this->creator = new MigrationCreator($files, null);
        $this->composer = $composer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // It's possible for the developer to specify the tables to modify in this
        // schema operation. The developer may also specify if this table needs
        // to be freshly created so we can create the appropriate migrations.
        $name = Str::snake(trim($this->input->getArgument('name')));

        $table = $this->input->getOption('table');

        $this->create = $this->input->getOption('create') ?: false;

        // If no table was given as an option but a create option is given then we
        // will use the "create" option as the table name. This allows the devs
        // to pass a table name into this option as a short-cut for creating.
        if (! $table && is_string($this->create)) {
            $table = $this->create;

            $this->create = true;
        }

        // Next, we will attempt to guess the table name if this the migration has
        // "create" in the name. This will allow us to provide a convenient way
        // of creating migrations that create new tables for the application.
        if (! $table) {
            [$table, $this->create] = TableGuesser::guess($name);
        }

        $table = trim($table, '_');

        // Now we are ready to write the migration out to disk. Once we've written
        // the migration out, we will dump-autoload for the entire framework to
        // make sure that the migrations are registered by the class loaders.
        $this->writeMigration($name, $table, $this->create);

        $this->composer->dumpAutoloads();

        return true;
    }

    public function getTranslationStub($translation = false) {
        $create = $this->create ? "create": "update";
        $translation = $translation ? "_translation" : "";
        return __DIR__ . "/stubs/{$create}{$translation}.stub";
    }

    public function getStub() {
        return $this->getTranslationStub();
    }

    /**
     * Get migration path (either specified by '--path' option or default location).
     *
     * @return string
     */
    protected function getMigrationPath()
    {
        return $this->laravel->databasePath().DIRECTORY_SEPARATOR.'migrations/';
    }

    /**
     * Determine if the given path(s) are pre-resolved "real" paths.
     *
     * @return bool
     */
    protected function usingRealPath()
    {
        return $this->input->hasOption('realpath') && $this->option('realpath');
    }

    /**
     * Write the migration file to disk.
     *
     * @param  string  $name
     * @param  string  $table
     * @param  bool    $create
     * @return string
     */
    protected function writeMigration($name, $table, $create)
    {
        // Generate base table
        $file = $this->getMigrationPath() . $this->getDatePrefix() . "{$name}.php";
        $replacements = $this->stubReplacements($name, $table);
//dump($replacements['DummyClass'], class_exists($replacements['DummyClass']));
        if (!$this->input->getOption('overwrite') && class_exists($replacements['DummyClass'])) {
            $this->line("<error>Migration `{$replacements['DummyClass']}` already exists</error>");
        } else {
            $stub = new StubOutputGenerator($this->input);
            $stub->setStubLocation($this->getStub())
                ->setOutputPath($file)
                ->setReplacements($replacements)
                ->generate();

            // Generate translation for base table
            if ($this->input->getOption('translatable')) {
                $fileTranslation = $this->getMigrationPath() . $this->getDatePrefix() . "{$name}_translation.php";
                $replacements = $this->stubReplacements($name, $table);

                $stub->setStubLocation($this->getTranslationStub(true))
                    ->setOutputPath($fileTranslation)
                    ->setReplacements($replacements)
                    ->generate();

            }

            if (!$this->input->getOption('fullpath')) {
                $file = pathinfo($file, PATHINFO_FILENAME);
            }

            $this->line("<info>Created Migration:</info> {$file}");
        }

        return true;
    }

    /**
     * Get the place holders for the migration stub.
     *
     * @param  string  $name
     * @param  string|null  $table
     * @return array
     */
    protected function stubReplacements($name, $table)
    {
        $replacements = [];
        $replacements['DummyClass'] = $this->getClassName($name);

        // Here we will replace the table place-holders with the table specified by
        // the developer, which is useful for quickly creating a tables creation
        // or update migration from the console instead of typing it manually.
        if (! is_null($table)) {
            $replacements['DummyTable'] = $table;
        }

        $replacements = array_merge($replacements, $this->getMigrationLines($name, $table));

        return $replacements;
    }

    protected function getMigrationLines($name, $table) {
        $replacements['DummyMigrationLines'] = null;
        $replacements['DummyTranslationMigrationLines'] = null;

        if ($this->input->getOption('from-uxcrud-controller')) {
            $controller = $this->getUxcrudClass(config('uxcrud.controller_namespace') . $this->getClassName($table));

            if (is_null($controller)) {
                $controller = '\\App\\' . config('uxcrud.controller_namespace') . $this->getClassName($table);
            }

            if (!is_null($controller)) {
                //$controller = trim($controller, '\\');
                if (is_string($controller)) {
                    $controller = new $controller();
                }
                //dump("\$controller", $controller);
                $model = $controller::getModel();
//                if (is_string($model)) {
//                    $model = new $controller();
//                }
//                dump('$model', $model);
            }

            if (!is_null($controller) && !(is_null($model))) {
                foreach($controller->getFields(Permission::VIEW, true) as $field) {
                    $type = (isset($model->translatedAttribute[$field])) ? 'DummyTranslationMigrationLines':'DummyMigrationLines';
                    if ($field->name != '') {
                        $replacements[$type] .=
                            $field->getMigrationLine();
                    }
                }
            }
        }

        return $replacements;
    }

    protected function getUxcrudClass($controller) {
        $classes = get_declared_classes();
        foreach($classes as $class) {
            if (strlen($controller)<=strlen($class) &&
                substr_compare($class, $controller, -strlen($controller)) === 0) {
                return new $class();
            }
        }
        return null;
    }

    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    protected function getClassName($name)
    {
        return Str::studly($name);
    }


    /**
     * Get the date prefix for the migration.
     *
     * @return string
     */
    protected function getDatePrefix()
    {
        return date('Y_m_d_His_');
    }

}
