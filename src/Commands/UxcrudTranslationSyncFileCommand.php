<?php

namespace Heiw\Uxcrudible\Commands;

use Heiw\Uxcrudible\Models\Translation;
use Heiw\Uxcrudible\Models\TranslationSource;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class UxcrudTranslationSyncFileCommand extends Command
{
    private const UXCRUD_TRANSLATION_PREFIX = 'uxcrud::';

    private const IGNORE_STRINGS = [
        'name_t',
        '+n+(r?',
        '" . $translatable_string . "',
        'name1_en',
        'name2_en',
        'name4_en',
        'nom',
        'name__',
        'name_lang',
    ];

    private $uxcrudLocation = "packages/heiw/uxcrudible/src/resources/lang/";

    // TODO MK Make source many to many relation so only single translation needs to be translated.

    protected $locale = null;
    protected $sources = [];
    protected $file = null;
    protected $fileParam = null;

    // First line of JS file.
    protected $header = null;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uxcrud:translation-sync-file {file : The js or php file for which to process the Translations}
        {locale : The locale to use }
        {--use-keys : Specify whether the translation file uses keys instead of full strings. }
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Read translations from js or php file and import into Translation DB and update js or php file with already translated entries. 
Only new entries are added to the Translation database. Entries marked ignore in Translation DB are not written back into the file. 
";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // It's possible for the developer to specify the tables to modify in this
        // schema operation. The developer may also specify if this table needs
        // to be freshly created so we can create the appropriate migrations.
        $this->file = $this->getFileArgument();

        $this->syncTranslationFile($this->file);
        $this->writeBackTranslations();

        return true;
    }

    protected function getFileArgument() {
        return str_replace("\\", "/", Str::snake(trim($this->input->getArgument('file'))));
    }

    public function syncTranslationFile($file) {
        if (file_exists($file)) {
            if (is_null($this->fileParam)) {
                $this->fileParam = $file;
            }
            ['dirname' => $dirname, 'extension' => $extension, 'filename' => $filename] = pathinfo($file);
            $this->locale = $this->input->getArgument(('locale')) ;// ? $this->input->getOption(('locale')) : $filename;
            switch (strtolower($extension)) {
                case 'js':
                    $fullFilename = base_path($file);
                    $lines = explode("\n", trim(file_get_contents($fullFilename), ";"));
                    $this->header = array_shift($lines);
                    // Check for { in first line.
                    if (strpos($this->header, "{") !== false) {
                        // Move { to header and correct lines.
                        $this->header = trim($this->header, "{ \t") . " ";
                        array_unshift($lines, "{");
                    }
                    $translations = json_decode(implode("\n", $lines));
                    $this->jsonErrors($file);
                    $this->processTranslations($translations, $file);
                    break;
                case 'json':
                    $fullFilename = base_path($file);
                    $translations = json_decode(file_get_contents($fullFilename));
                    $this->jsonErrors($file);
                    $this->processTranslations($translations, $file);
                    break;
                case 'php':
                    $translations = include($file);
                    $this->processTranslations($translations, $file);
                    break;
            }
        } else {
            $this->error("`$file` not found");
        }
    }

    public function jsonErrors($file) {
        switch(json_last_error()) {
            case JSON_ERROR_NONE:
                break;
            default:
                $this->error(json_last_error_msg() . ": " . $file);
                break;
        }
    }

    /**
     * @param TranslationSource[] $fileSources  Array of file sources to update
     */
    public function writeBackTranslations() {
        $this->info(__('Sources written back to:'));
        foreach($this->sources as $fileSource) {
            $builder = Translation::where('locale', $this->locale)
                ->whereHas('translationSources', function($query) use ($fileSource) {
                    $query->where('translation_source.id', $fileSource->id);
                })
                ->with('translationSources')
                ->orderBy('original');
//                ->selectRaw("REPLACE(translation, '\'', '&apos;') AS translation");
            if ($fileSource->use_keys) {
                $translations = [];
                $lines = $builder->get();
                foreach($lines as $line) {
                    if (isset($line->translationSources()->first()->pivot->key)) {
                        Arr::set($translations, $line->translationSources()->first()->pivot->key, $line->translation);
                    } else {
                        $translations[$line->original] = $line->translation;
                    }
                }
            } else {
                $translations = $builder->pluck('translation', 'original')->toArray();
            }

            switch($fileSource->fileType()) {
                case 'js':
                    file_put_contents($fileSource->name, $this->header . json_encode($translations, JSON_PRETTY_PRINT));
                    break;
                case 'json':
                    file_put_contents($fileSource->name, json_encode($translations, JSON_PRETTY_PRINT));
                    break;
                case 'php':
                    $content = "<?php\nreturn " . var_export($translations, true) . ";";
                    file_put_contents($fileSource->name, $content);
                    break;
                default:
                    $this->error(__(':file has unknown translation source type: :fileType', ['file' => $fileSource->name, 'fileType' => $fileSource->fileType()]));
                    break;
            }
            $this->info($fileSource->name);
        }
    }

    protected function stripTranslationPrefixes($prefix, &$original, &$translation) {
        // Strip prefix and types
        if ($original == $translation) {
            $translation = str_replace($prefix, "", $translation);
        }
        $original = str_replace($prefix, "", $original);
    }

    protected function findOrCreateSource($filename) {
        // Parameter use keys is present and filename matches command parameter use keys.
        $useKeys = ($this->input->getOption(('use-keys')) && $filename === $this->file);

        $source = TranslationSource::firstOrAdd($filename, $useKeys);
        // Add to list of sources.
        if (!in_array($source, $this->sources)) {
            $this->sources[$source->name] = $source;
            if($this->fileParam !== $source->name) {
                // If translation identified in external file read it's content.
                $this->syncTranslationFile($source->name);
            }
        }
        return $source;
    }

    public function processTranslations($translations, $file) {
        $fileSource = $this->findOrCreateSource($file);
        $this->sources[$fileSource->name] = $fileSource;
        $import = [
            'locale' => $this->locale,
        ];

        // Check original translation location for file names and strip those.
        $languageGroupFiles = [];
        foreach(glob(app()->langPath() . DIRECTORY_SEPARATOR . $this->locale . DIRECTORY_SEPARATOR . "*.php") as $languageFile) {
            $group = pathinfo($languageFile, PATHINFO_FILENAME);
            $relativePath = trim(str_replace(app()->basePath(), "", $languageFile), '/\\');
            $languageGroupFiles[$relativePath] = $group;
        }

        foreach($translations as $original => $translation) {
            if (!in_array($original, self::IGNORE_STRINGS)) {
                $source = $fileSource;

                // If original starts with uxcrud:: add to DB under that file.
                if (strncmp($original, self::UXCRUD_TRANSLATION_PREFIX,
                        strlen(self::UXCRUD_TRANSLATION_PREFIX)) === 0) {
                    // Strip uxcrud:: bit.
                    $type = str_replace(self::UXCRUD_TRANSLATION_PREFIX, null, $original);
                    // Get name before .
                    $type = substr($type, 0, strpos($type, "."));
                    $uxcrudFile = str_replace("\\", "/","{$this->uxcrudLocation}{$this->locale}/$type.php");
                    $source = $this->findOrCreateSource($uxcrudFile);
                    $this->stripTranslationPrefixes(self::UXCRUD_TRANSLATION_PREFIX . $type . ".", $original, $translation);
                } else {
                    // Find language group if original string starts with text.
                    $languageGroups = array_filter($languageGroupFiles, function($element) use ($original, $translation) {
                        $element .= ".";
                        return (strncmp($original, $element, strlen($element)) === 0);
                    });
                    // Set language group file if prefix found.
                    foreach($languageGroups as $path => $languageGroup) {
                        $this->stripTranslationPrefixes($languageGroup . ".", $original, $translation);
                        $source = $this->findOrCreateSource($path);
                    }
                }

                $this->processLine($original, $translation, $import, $source);
            }
        }
    }


    public function processLine($original, $translation, $import, $source, $keyChain = []) {
        if (is_array($translation)) {
            foreach($translation as $key => $line) {
                $this->processLine($original, $line, $import, $source, $keyChain + [$original,$key]);
            }
        } else {
            $import['translation'] = html_entity_decode($translation);
            $import['original'] = html_entity_decode($original, ENT_COMPAT | ENT_HTML5, 'ISO-8859-1');
            if ($source->use_keys) {
                if (count($keyChain) == 0) {
                    $keyChain = [$original];
                }
                $original = trans($translation, [], 'en');
                $import['original'] = $original;
                $import['key'] = implode(".", $keyChain);
            } else {
                $import['key'] = null;
            }

            $object = null;
            try {
                Translation::create(
                    $import
                );
            } catch (QueryException $e) {
                // Ignore duplicate entries.
            }
            $lines = Translation::where('original', $original)
//                ->whereDoesntHave('translationSources', function (Builder $query) use ($source) {
//                    $query->where('translation_source_id', $source->id);
//                })
                ->each(function ($item, $index) use ($source, $import) {
                    $pivotData = ['key' => $import['key']];
                    $pivot = $item->translationSources()->where('id', $source->id)->count();
                    if ($pivot == 0) {
                        // Attach new relation instead.
                        $item->translationSources()->attach($source->id, $pivotData);
                    } else {
                        // Update relation
                        $result = $item->translationSources()->updateExistingPivot($source->id, $pivotData);
                    }
                    //$item->translationSources()->sync([$source->id, ['key' => $baseKey]], false);
                });
        }
    }
}
