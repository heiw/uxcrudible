<?php

namespace Heiw\Uxcrudible\Commands;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class UxcrudControllerMakeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uxcrud:make-controller {name : The base name of the controller}
        {--translatable : Indicate whether a translation migration should be generated }
        {--overwrite : Indicate whether existing files should be overwritten }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new controller class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getControllerNamespace($rootNamespace)
    {
        return self::controllerNamespace($rootNamespace);
    }

        /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    public static function controllerNamespace($rootNamespace)
    {
        return '\\' .$rootNamespace.'\\'. config('uxcrud.controller_namespace');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // It's possible for the developer to specify the tables to modify in this
        // schema operation. The developer may also specify if this table needs
        // to be freshly created so we can create the appropriate migrations.
        $name = Str::snake(trim($this->input->getArgument('name')));

        // Now we are ready to write the migration out to disk. Once we've written
        // the migration out, we will dump-autoload for the entire framework to
        // make sure that the migrations are registered by the class loaders.
        $this->writeController($name);

        return true;
    }

    public function getStub() {
        return __DIR__ . "/stubs/controller.stub";
    }

    public function getOutputPathBase() {
        return $this->laravel->basePath()."/app/" . config('uxcrud.controller_namespace') ."/";
    }


    /**
     * Write the controller file to disk.
     *
     * @param  string  $name
     * @return string
     */
    protected function writeController($name)
    {
        // Generate base table
        $file = $this->getOutputPathBase() . $this->getClassName($name) . ".php";
        $replacements = $this->stubReplacements($name);

        $stub = new StubOutputGenerator($this->input);
        if ($stub->setStubLocation($this->getStub())
            ->setOutputPath($file)
            ->setReplacements($replacements)
            ->generate()
        ) {
            $this->line("<info>Created Controller:</info> {$file}");
        }

        return true;
    }

    /**
     * Populate the place-holders in the migration stub.
     *
     * @param  string  $name
     * @return array
     */
    protected function stubReplacements($name)
    {
        return [
            'DummyClass' => $this->getClassName($name),
            'DummyModelNamespace' => UxcrudModelMakeCommand::modelNamespace('App')
        ];
    }

    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    protected function getClassName($name)
    {
        return Str::studly($name);
    }


}
