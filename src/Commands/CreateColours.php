<?php
namespace Heiw\Uxcrudible\Commands;

use Illuminate\Console\Command;

/**
 * Experiment with command lines
 * Class CreateColours
 * @package Heiw\Uxcrudible\Commands
 */
class CreateColours extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'heiw:create-colours';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create colours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public static function shadeColor($color, $percent) {
        $num = base_convert(substr($color, 1), 16, 10);
        $amt = round(2.55 * $percent);
        $r = ($num >> 16) + $amt;
        $b = ($num >> 8 & 0x00ff) + $amt;
        $g = ($num & 0x0000ff) + $amt;

        return '#'.substr(base_convert(0x1000000 + ($r<255?$r<1?0:$r:255)*0x10000 + ($b<255?$b<1?0:$b:255)*0x100 + ($g<255?$g<1?0:$g:255), 10, 16), 1);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Generating the rainbow");

        /**
         * Source colours
         */
        $find = [
            "primary" => "1e88e5",
            "lighter" => "51BBFF",
            "lightest" => "6BD5FF",
            "key" => "uxcrud-blue",
        ];

        /**
         * https://www.cssfontstack.com/oldsites/hexcolortool/
         * primary: enter as base colour
         * lighter: 3rd colour
         * lightest: 4th colour
         */

        /**
         * TODO MK move to separate configuration file
         */
        $colours = [
            "blue" => ["primary" => "0056B5", "lighter" => "005C99", "lightest" => "4DC8FF", ],
            "indigo" => ["primary" => "550BCB", "lighter" => "6000D6", "lightest" => "6E00F5",],
            "purple" => ["primary" => "502F93", "lighter" => "5933A3", "lightest" => "673CBF",],
            "pink" => ["primary" => "EA66B0", "lighter" => "FF71BF", "lightest" => "FF8BD9",],
            "red" => ["primary" => "8C183F", "lighter" => "991A4F", "lightest" => "A61C6A",],
            "orange" => ["primary" => "fd7e14", "lighter" => "FFB147", "lightest" => "FFCB61",],
            "yellow" => ["primary" => "ffc107", "lighter" => "FFF43A", "lightest" => "FFFF54",],
            "green" => ["primary" => "155624", "lighter" => "196720", "lightest" => "175E28",],
            "teal" => ["primary" => "137155", "lighter" => "158463", "lightest" => "158463",],
            "cyan" => ["primary" => "0C545F", "lighter" => "0C616E", "lightest" => "0C616E",],
        ];


        $genericCSSTemplate = file_get_contents(__DIR__ . '/../assets/colors/uxcrudible.css.stub');
        $genericCSS = '';
        foreach($colours as $key => $colour) {
            $this->info($key);
            $uxcrudKey = "uxcrud-$key";
            $colour["key"] = $uxcrudKey;
            $css = file_get_contents(__DIR__ . '/../assets/colors/theme.css');
            file_put_contents(__DIR__ . "/../assets/colors/{$uxcrudKey}.css",  str_replace($find, $colour, $css));

            $genericCSS .= str_replace($find, $colour, $genericCSSTemplate);

            $svg = file_get_contents(__DIR__ . '/../assets/colors/trianglify-uxcrud-theme.svg');
            file_put_contents(__DIR__ . "/../assets/colors/trianglify-{$uxcrudKey}.svg",  str_replace($find, $colour, $svg));
        }

        file_put_contents(__DIR__ . '/../assets/colors/uxcrud-buttons.css', $genericCSS);


    }
}
