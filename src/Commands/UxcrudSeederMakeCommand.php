<?php

namespace Heiw\Uxcrudible\Commands;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class UxcrudSeederMakeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uxcrud:make-seeder {name : The base name of the seeder}
        {--translatable : Indicate whether a translation should be included }
        {--overwrite : Indicate whether existing files should be overwritten }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new seeder class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Seeder';

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getControllerNamespace($rootNamespace)
    {
        return self::controllerNamespace($rootNamespace);
    }

        /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    public static function controllerNamespace($rootNamespace)
    {
        return '\\' .$rootNamespace.'\\'. config('uxcrud.controller_namespace');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // It's possible for the developer to specify the tables to modify in this
        // schema operation. The developer may also specify if this table needs
        // to be freshly created so we can create the appropriate migrations.
        $name = Str::snake(trim($this->input->getArgument('name')));

        // Now we are ready to write the migration out to disk. Once we've written
        // the migration out, we will dump-autoload for the entire framework to
        // make sure that the migrations are registered by the class loaders.
        $this->writeController($name);

        return true;
    }

    public function getTranslationStub($translation = false) {
        $translation = ($this->input->getOption(('translatable'))) ? "_translation" : "";
        return __DIR__ . "/stubs/seeder{$translation}.stub";
    }

    public function getStub() {
        return $this->getTranslationStub();
    }

    public function getOutputPathBase() {
        return $this->laravel->basePath()."/database/seeds/";
    }


    /**
     * Write the controller file to disk.
     *
     * @param  string  $name
     * @return string
     */
    protected function writeController($name)
    {
        // Generate base table
        $file = $this->getOutputPathBase() . $this->getClassName($name) . "TableSeeder.php";
        $replacements = $this->stubReplacements($name);

        $stub = new StubOutputGenerator($this->input);
        if ($stub->setStubLocation($this->getStub())
            ->setOutputPath($file)
            ->setReplacements($replacements)
            ->generate()
        ) {
            $this->line("<info>Created Seeder:</info> {$file}");
        }

        return true;
    }

    /**
     * Populate the place-holders in the migration stub.
     *
     * @param  string  $name
     * @return array
     */
    protected function stubReplacements($name)
    {
        return [
            'DummyClass' => $this->getClassName($name),
            'DummyTable' => Str::snake($name),
            'DummyModelNamespace' => UxcrudModelMakeCommand::modelNamespace('App')
        ];
    }

    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    protected function getClassName($name)
    {
        return Str::studly($name);
    }


}
