<?php

namespace Heiw\Uxcrudible\Commands;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\ConsoleOutput;

class StubOutputGenerator
{
    public $stubLocation;

    public $outputPath;

    /**
     * @return mixed
     */
    public $replacements;

    private $input;

    public function __construct($input) {
        $this->input = $input;
        return $this;
    }

    public function setStubLocation($stubLocation) {
        $this->stubLocation = $stubLocation;
        return $this;
    }

    public function setOutputPath($outputPath) {
        $this->outputPath = $outputPath;
        return $this;
    }

    /**
     * @param $replacements[] Key value pairs for replacements
     */
    public function setReplacements($replacements) {
        $this->replacements = $replacements;
        return $this;
    }

    public function getStubContent() {
        $files = new Filesystem();
        return $files->get($this->stubLocation);
    }
    
    public function output($content) {
        $files = new Filesystem();
        if (!file_exists($this->outputPath) || $this->input->getOption('overwrite')) {
            return $files->put($this->outputPath, $content);
        } else {
            $output = new ConsoleOutput();
            $output->writeln("<error>File `{$this->outputPath}` already exists</error>");
            return false;
        }
    }
    
    public function generate() {
        $content = $this->getStubContent();
        $content = str_replace(array_keys($this->replacements), array_values($this->replacements), $content);
        return $this->output($content);
    }
}
