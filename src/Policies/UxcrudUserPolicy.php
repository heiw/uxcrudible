<?php

namespace Heiw\Uxcrudible\Policies;

use App\User;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class UxcrudUserPolicy extends UxcrudModelPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update it's own account.
     *
     * @param User $user
     * @param string|UxcrudModel $model
     * @return bool
     */
    public function view(User $user, $model)
    {
        // Allow users to see their own profile
        if ($model->id === $user->id) {
            return true;
        }
        return $user->hasModelPermission('update', $model);

    }

    /**
     * Determine whether the user can update it's own account.
     *
     * @param User $user
     * @param string|UxcrudModel $model
     * @return bool
     */
    public function edit(User $user, $model)
    {
        // Allow users to edit their own profile
        if ($model->id === $user->id) {
            return true;
        }
        return $user->hasModelPermission('update', $model);

    }

    /**
     * Determine whether the user can update it's own account.
     *
     * @param User $user
     * @param string|UxcrudModel $model
     * @return bool
     */
    public function update(User $user, $model)
    {
        // Allow users to update their own profile
        if ($model->id === $user->id) {
            return true;
        }
        return $user->hasModelPermission('update', $model);

    }

    /**
     * Determine whether the user can delete a user's account.
     *
     * @param User $user
     * @param string|UxcrudModel $model
     * @return bool
     */
    public function delete(User $user, $model)
    {
        return $user->canBeDeleted();
    }


    /**
     * Determine whether the user can impersonate.
     *
     * @param  \App\User  $user
     * @param  UxcrudModel $model
     * @return mixed
     */
    public function impersonate(User $user, UxcrudModel $model = null)
    {
        return $user->canBeImpersonated();
    }

    /**
     * Determine whether the user can send emails to users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function sendEmailTemplate(User $user)
    {
        return $user->canEditRoles();
    }
}
