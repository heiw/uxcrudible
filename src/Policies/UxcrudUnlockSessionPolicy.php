<?php

namespace Heiw\Uxcrudible\Policies;

use App\User;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class UxcrudUnlockSessionPolicy extends UxcrudModelPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update it's own account.
     *
     * @param User $user
     * @param string|UxcrudModel $model
     * @return bool
     */
    public function showUnlockForm(User $user, $model)
    {
        return true;
    }
}
