<?php

namespace Heiw\Uxcrudible\Policies\Traits;

use App\User;
use Heiw\Uxcrudible\Models\Model;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Access\Response;

/**
 * Implements policies after checking that user has Role permission in database.
 * Trait RestrictToUserPolicyTrait
 * @package Heiw\Uxcrudible\Policies\Traits
 */
trait RestrictToUserPolicyTrait {
    /**
     * Implement genericPolicy to deny access based on the user.
     *
     * @param string $permissionName
     * @param User $user
     * @param UxcrudModel null $model
     * @return bool|\Illuminate\Auth\Access\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    abstract public function restrictToUser(string $permissionName, User $user, UxcrudModel $model = null);

    /**
     * Implement genericPolicy to deny access on multiple actions in one go.
     *
     * @param string $permissionName
     * @param User $user
     * @param UxcrudModel null $model
     * @return bool|\Illuminate\Auth\Access\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function genericPolicy(string $permissionName, User $user, UxcrudModel $model = null) {
        // Check generic policies
        if (method_exists(get_parent_class($this), 'genericPolicy')) {
            $result = parent::genericPolicy($permissionName, $user, $model);
        } else {
            $result = true;
        }

        // If parent policies check out, check whether we need to restrict by user.
        if ($result === true || (get_class($result) === Response::class && $result->allowed()))  {
            $result = $this->restrictToUser($permissionName, $user, $model);
        }
        return $result;
    }

    /**
     * Determine whether the user can index the uxcrud model.
     *
     * @param User $user
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index(User $user)
    {
        $result = $user->hasModelPermission('read');
        if ($result) {
            $result = $this->genericPolicy('read', $user);
        }
        return $result;
    }

    /**
     * Determine whether the user export the data.
     *
     * @param  \App\User  $user
     * @param  UxcrudModel $model
     * @return mixed
     */
    public function export(User $user, UxcrudModel $model = null)
    {
        return $this->read($user, $model);
    }

    /**
     * Determine whether the user can view the uxcrud model.
     *
     * @param \App\User $user
     * @param string|UxcrudModel $model
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function read(User $user, $model = null)
    {
        $result = $user->hasModelPermission('read', $model);
        if ($result) {
            $result = $this->genericPolicy('read', $user, $model);
        }
        return $result;
    }

    /**
     * Determine whether the user can create uxcrud models.
     *
     * @param \App\User $user
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function create(User $user, UxcrudModel $model = null)
    {
        $result = $user->hasModelPermission('create', $model);
        if ($result) {
            $result = $this->genericPolicy('create', $user, $model);
        }
        return $result;
    }

    /**
     * Determine whether the user can update the uxcrud model.
     *
     * @param User $user
     * @param string|UxcrudModel $model
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function update(User $user, $model) {
        $result = $user->hasModelPermission('update', $model);
        if ($result) {
            $result = $this->genericPolicy('update', $user, $model);
        }
        return $result;
    }

    /**
     * Determine whether the user can update the uxcrud model.
     *
     * @param User $user
     * @param string|UxcrudModel $model
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function rowReorder(User $user, $model) {
        $result = $user->hasModelPermission('update', $model);
        if ($result) {
            $result = $this->genericPolicy('update', $user, $model);
        }
        return $result;
    }

    /**
     * Determine whether the user can delete the uxcrud model.
     *
     * @param \App\User $user
     * @param string|UxcrudModel $model
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function delete(User $user, $model)
    {
        $result = $user->hasModelPermission('delete', $model);
        if ($result) {
            $result = $this->genericPolicy('delete', $user, $model);
        }
        return $result;
    }

    /**
     * Determine whether the user can restore the uxcrud model.
     *
     * @param \App\User $user
     * @param string|UxcrudModel $model
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function restore(User $user, $model)
    {
        $result = $user->hasModelPermission('restore', $model);
        if ($result) {
            $result = $this->genericPolicy('restore', $user, $model);
        }
        return $result;
    }

    /**
     * Determine whether the user can permanently delete the uxcrud model.
     *
     * @param \App\User $user
     * @param string|UxcrudModel $model
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function forceDelete(User $user, $model)
    {
        $result = $user->hasModelPermission('force delete', $model);
        if ($result) {
            $result = $this->genericPolicy('force delete', $user, $model);
        }
        return $result;
    }
}
