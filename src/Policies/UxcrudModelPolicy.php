<?php

namespace Heiw\Uxcrudible\Policies;

use App\User;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class UxcrudModelPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can index the uxcrud model.
     *
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return $user->hasModelPermission('read');
    }

    /**
     * Determine whether the user export the data.
     *
     * @param  \App\User  $user
     * @param  UxcrudModel $model
     * @return mixed
     */
    public function export(User $user, UxcrudModel $model = null)
    {
        //dd(func_get_args(), $model);
        if (auth()->user()->hasModelPermission('read', $model)) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the uxcrud model.
     *
     * @param  \App\User  $user
     * @param  string|UxcrudModel  $model
     * @return mixed
     */
    public function read(User $user, $model = null)
    {
        if ($user->hasModelPermission('read', $model)) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create uxcrud models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, UxcrudModel $model = null)
    {
        return $user->hasModelPermission('create', $model);
    }

    /**
     * Determine whether the user can update the uxcrud model.
     *
     * @param User $user
     * @param string|UxcrudModel $model
     * @return bool
     */
    public function update(User $user, $model)
    {
        return $user->hasModelPermission('update', $model);
    }

    /**
     * Determine whether the user can update the uxcrud model.
     *
     * @param User $user
     * @param string|UxcrudModel $model
     * @return bool
     */
    public function rowReorder(User $user, $model)
    {
        return $user->hasModelPermission('update', $model);
    }

    /**
     * Determine whether the user can delete the uxcrud model.
     *
     * @param  \App\User  $user
     * @param  string|UxcrudModel  $model
     * @return mixed
     */
    public function delete(User $user, $model)
    {
        return $user->hasModelPermission('delete', $model);
    }

    /**
     * Determine whether the user can restore the uxcrud model.
     *
     * @param  \App\User  $user
     * @param  string|UxcrudModel  $model
     * @return mixed
     */
    public function restore(User $user, $model)
    {
        return $user->hasModelPermission('restore', $model);
    }

    /**
     * Determine whether the user can permanently delete the uxcrud model.
     *
     * @param  \App\User  $user
     * @param  string|UxcrudModel  $model
     * @return mixed
     */
    public function forceDelete(User $user, $model)
    {
        return $user->hasModelPermission('force delete', $model);
    }


    /**
     * Determine whether the user can create uxcrud models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function selectEmailTemplate(User $user, UxcrudModel $model = null)
    {
        return $user->hasModelPermission('create', $model);
    }
}
