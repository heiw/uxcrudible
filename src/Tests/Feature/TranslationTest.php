<?php

namespace Heiw\Uxcrud\Tests\Feature;

use App\Models\User;
use Heiw\Uxcrudible\Models\Page;
use Heiw\Uxcrudible\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TranslationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function canStoreTranslations() {
        $this->actingAsUser('admin');

        $this->markTestIncomplete();
    }
}
