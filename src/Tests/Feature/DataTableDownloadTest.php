<?php

namespace Heiw\Uxcrud\Tests\Feature;

use App\Models\User;
use Heiw\Uxcrudible\Models\Page;
use Heiw\Uxcrudible\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DataTableDownloadTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function onlyLoggedInUsersCanAccess()
    {
        $response = $this->get('/clients')
            ->assertRedirect('/login');


    }

    /** @test */
    public function authenticatedUsersCanAccessClients() {
        $this->actingAs();
        $response = $this->get('/clients')
            ->assertOk();
    }

    /** @test */
    public function anAdminCanCreateAPage() {
        $this->actingAs(factory(User::class)->create());

        $resposne = $this->post('/page', [
            'name' => 'Test',
            'param' => 1
        ]);

        $this->assertCount(1, Page::all());
    }

}
