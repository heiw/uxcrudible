<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderColumnToModelPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('model_page')) {
            if (!Schema::hasColumn('model_page', 'order')) {
                Schema::table('model_page', function (Blueprint $table) {
                    $table->integer('order')->after('page_id')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('model_page')) {
            if (Schema::hasColumn('model_page', 'order')) {
                Schema::table('model_page', function (Blueprint $table) {
                    $table->dropColumn('order');
                });
            }
        }
    }
}
