<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplateTableTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_template_translation')) {
            Schema::create('email_template_translation', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('email_template_id');
                $table->string('locale')->index();

                $table->string('subject')->nullable();
                $table->mediumText('content')->nullable();

                $table->timestamps();
                $table->unique(['email_template_id', 'locale']);
                $table->foreign('email_template_id')->references('id')->on('email_template')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_template_translation');
    }
}
