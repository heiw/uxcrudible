<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSnippetTableTranslation extends Migration
{
    static $model = [
        ['id' => '9',  'namespace' => 'Heiw\\Uxcrudible\\',   'name' => 'Snippet'],
    ];

    static $model_translation = [
                [	'model_id' => '9',  'locale' => 'en', 'display' => 'Snippet',       ],
                [	'model_id' => '9',  'locale' => 'cy', 'display' => 'Snippet',       ],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('snippet_translation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('snippet_id');
            $table->string('locale')->index();

            $table->mediumText('content');

            $table->timestamps();
            $table->unique(['snippet_id', 'locale']);
            $table->foreign('snippet_id')->references('id')->on('snippet')->onDelete('cascade');
        });

        if (Schema::hasTable('model') && isset(static::$model)) {
            DB::table('model')->insertOrIgnore(static::$model);
        }
        if (Schema::hasTable('model_translation') && isset(static::$model_translation)) {
            DB::table('model_translation')->insertOrIgnore(static::$model_translation);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('snippet_translation');

        if (Schema::hasTable('model') && isset(static::$model)) {
            DB::table('model')->whereIn('id', array_column(static::$model, 'id'))->delete();
        }
        if (Schema::hasTable('model_translation') && isset(static::$model_translation)) {
            DB::table('model_translation')->whereIn('model_id', array_column(static::$model, 'id'))->delete();
        }
    }
}
