<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeDisplayOrderColumnTypeOnPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Check legacy database definition
        if (DB::getSchemaBuilder()->getColumnType('page', 'display_order') === 'string') {
            DB::statement('ALTER TABLE page MODIFY display_order BIGINT(20);');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
