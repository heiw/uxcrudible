<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameDefaultToOriginalTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('translation', 'default')) {
            Schema::table('translation', function (Blueprint $table) {
                $table->renameColumn('default', 'original');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('translation', 'default')) {
            Schema::table('translation', function (Blueprint $table) {
                $table->renameColumn('original', 'default');
            });
        }
    }
}
