<?php

use Heiw\Uxcrudible\Classes\UxcrudModelMigration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Heiw\Uxcrudible\Seeds\ModelTableSeeder;

if (!class_exists('CreateTranslationTable')) {
    class CreateTranslationTable extends UxcrudModelMigration
    {
        public static function models() {
            return [
                ['id' => ModelTableSeeder::TRANSLATION_ID, 'namespace' => 'Heiw\\Uxcrudible\\', 'name' => 'Translation']
            ];
        }

        public static function modelTranslations() {
            return [
                ['model_id' => ModelTableSeeder::TRANSLATION_ID, 'locale' => 'en', 'display' => 'Translation',],
                ['model_id' => ModelTableSeeder::TRANSLATION_ID, 'locale' => 'cy', 'display' => __('Translation'),],
            ];
        }

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up() {
            Schema::create('translation', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->mediumText('original')->charset('utf8mb4')->collation('utf8mb4_bin')->nullable();

                $table->mediumText('translation')->nullable();
                $table->string('locale')->index();

                $table->timestamps();
                $table->softDeletes();
            });

            // Add unique for the first 191 characters.
            DB::statement('ALTER TABLE `translation`    ADD UNIQUE INDEX `translation_original_unique` (`original` (191) );');

            self::upModel();

            if (Schema::hasTable('model') && self::models()) {
                foreach (self::models() as $model) {
                    \Heiw\Uxcrudible\Seeds\ModelTableSeeder::addFullRightsForSysAdmin($model['id']);
                    \Heiw\Uxcrudible\Seeds\PageTableSeeder::addModelToPage($model['id'], 3);
                }
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down() {
            Schema::dropIfExists('translation');

            self::downModel();
        }
    }
}
