<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormCreatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('form_creator')) {
            Schema::create('form_creator', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('name')->nullable();

                // Json encoded field definition
                $table->mediumText('form')->nullable();


                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_creator');
    }
}

