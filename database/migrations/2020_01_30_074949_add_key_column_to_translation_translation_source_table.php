<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeyColumnToTranslationTranslationSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('translation_translation_source')) {
            Schema::table('translation_translation_source', function (Blueprint $table) {
                $table->string('key')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('translation_translation_source')) {
            Schema::table('translation_translation_source', function (Blueprint $table) {
                $table->dropColumn('key');
            });
        }
    }
}
