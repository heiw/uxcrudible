<?php

use Heiw\Uxcrudible\Classes\UxcrudModelMigration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Heiw\Uxcrudible\Seeds\ModelTableSeeder;

if (!class_exists('CreateTranslationSourceTable')) {
    class CreateTranslationSourceTable extends UxcrudModelMigration
    {
        public static function models() {
            return [
                [
                    'id' => ModelTableSeeder::TRANSLATION_SOURCE_ID,
                    'namespace' => 'Heiw\\Uxcrudible\\',
                    'name' => 'TranslationSource'
                ]
            ];
        }

        public static function modelTranslations() {
            return [
                [
                    'model_id' => ModelTableSeeder::TRANSLATION_SOURCE_ID,
                    'locale' => 'en',
                    'display' => 'Translation Source',
                ],
                [
                    'model_id' => ModelTableSeeder::TRANSLATION_SOURCE_ID,
                    'locale' => 'cy',
                    'display' => __('Translation Source'),
                ],
            ];
        }

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up() {
            Schema::create('translation_source', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('name')->nullable();
                $table->boolean('use_keys')->nullable();

                $table->timestamps();
                $table->softDeletes();
            });

            self::upModel();

            if (Schema::hasTable('model') && self::models()) {
                foreach (self::models() as $model) {
                    \Heiw\Uxcrudible\Seeds\ModelTableSeeder::addFullRightsForSysAdmin($model['id']);
                    \Heiw\Uxcrudible\Seeds\PageTableSeeder::addModelToPage($model['id'],
                        \Heiw\Uxcrudible\Seeds\PageTableSeeder::SETTINGS_ID);
                }
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down() {
            Schema::dropIfExists('translation_source');

            self::downModel();
        }
    }
}
