<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeNameColumnsUniqueForSeeding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('role')) {
            Schema::table('role', function (Blueprint $table) {
                $table->unique(['name']);
            });
        }
        if (Schema::hasTable('permission')) {
            Schema::table('permission', function (Blueprint $table) {
                $table->unique(['name']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('role', 'name')) {
            Schema::table('role', function (Blueprint $table) {
                $table->dropUnique(['name']);
            });
        }
        if (Schema::hasColumn('permission', 'name')) {
            Schema::table('permission', function (Blueprint $table) {
                $table->dropUnique(['name']);
            });
        }
    }
}
