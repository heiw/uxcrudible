<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_translation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('page_id');
            $table->string('locale')->index();

            $table->string('path')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('title')->nullable();
            $table->mediumText('description')->nullable();
            $table->longText('content')->nullable();

            $table->timestamps();

            $table->unique(['page_id', 'locale']);
            $table->foreign('page_id')->references('id')->on('page')->onDelete('cascade');
        });

//        // We insert the old attributes into the fresh translation table:
//        DB::statement("insert into page_translations (page_id, path, name, slug, title, description, content, locale) select id, path, name, slug, title, description, content, 'en' from page");
//
//        // We drop the translation attributes in our main table:
//        Schema::table('page', function ($table) {
//            $table->dropColumn('path');
//            $table->dropColumn('name');
//            $table->dropColumn('slug');
//            $table->dropColumn('title');
//            $table->dropColumn('description');
//            $table->dropColumn('content');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_translations');
    }
}
