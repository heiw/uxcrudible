<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormCreatorTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('form_creator_translation')) {
            Schema::create('form_creator_translation', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('form_creator_id');
                $table->string('locale')->index();

                $table->string('title')->nullable();

                $table->timestamps();

                $table->unique(['form_creator_id', 'locale']);
                $table->foreign('form_creator_id')->references('id')->on('form_creator')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_creator_translation');
    }
}
