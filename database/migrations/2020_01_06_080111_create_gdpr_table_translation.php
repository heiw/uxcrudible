<?php

use Heiw\Uxcrudible\Classes\UxcrudModelMigration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGdprTableTranslation extends UxcrudModelMigration
{
    public static function models() {
        return [
            ['id' => '11',  'namespace' => 'Heiw\\Uxcrudible\\',   'name' => 'Gdpr'],
        ];
    }

    public static function modelTranslations() {
        return [
            ['model_id' => '11', 'locale' => 'en', 'display' => 'GDPR',],

            ['model_id' => '11', 'locale' => 'cy', 'display' => __('GDPR'),],
        ];
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gdpr_translation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('gdpr_id');
            $table->string('locale')->index();

            $table->string('name');
            $table->mediumText('content')->nullable();

            $table->timestamps();
            $table->unique(['gdpr_id', 'locale']);
            $table->foreign('gdpr_id')->references('id')->on('gdpr')->onDelete('cascade');
        });

        self::upModel();

        if (Schema::hasTable('model') && self::models()) {
            foreach(self::models() as $model) {
                \Heiw\Uxcrudible\Seeds\ModelTableSeeder::addFullRightsForSysAdmin($model['id']);
                \Heiw\Uxcrudible\Seeds\PageTableSeeder::addModelToPage($model['id'], 3);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gdpr_translation');

        self::downModel();

    }
}
