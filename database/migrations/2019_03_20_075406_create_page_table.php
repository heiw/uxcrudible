<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tree_order')->nullable();
//            $table->string('path')->nullable();
            $table->bigInteger('display_order')->nullable();
//            $table->string('name');
//            $table->string('slug');
            $table->string('page_type')->nullable();
//            $table->string('title')->nullable();
//            $table->string('description')->nullable();
//            $table->string('content')->nullable();
            $table->string('icon')->nullable();
            $table->boolean('public')->default(0);
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')
                ->references('id')
                ->on('page')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_translation');
        Schema::dropIfExists('page');
    }
}
