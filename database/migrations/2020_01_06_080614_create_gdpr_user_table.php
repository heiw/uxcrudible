<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGdprUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gdpr_user', function (Blueprint $table) {
            $table->unsignedBigInteger('gdpr_id');
            $table->unsignedBigInteger('user_id');
            $table->primary(['gdpr_id', 'user_id']);

            $table->foreign('gdpr_id')
                ->references('id')
                ->on('gdpr')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->dateTime('sign_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gdpr_user');
    }
}
