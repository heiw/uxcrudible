<?php

use Heiw\Uxcrudible\Classes\UxcrudModelMigration;
use Heiw\Uxcrudible\Seeds\ModelTableSeeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailLogTable extends UxcrudModelMigration
{
    public static function models() {
        return [
            ['id' => ModelTableSeeder::EMAIL_LOG_ID,   'namespace' => 'Heiw\\Uxcrudible\\',     'name' => 'EmailLog'],
        ];
    }

    public static function modelTranslations() {
        return [
            ['model_id' => ModelTableSeeder::EMAIL_LOG_ID,   'locale' => 'en', 'display' => 'Email Log'],

            ['model_id' => ModelTableSeeder::EMAIL_LOG_ID,   'locale' => 'cy', 'display' => __('Email Log',[], 'cy')],
        ];
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_log')) {

            Schema::create('email_log', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->dateTime('date')->nullable();
                $table->unsignedBigInteger('sender_id')->nullable();
                $table->foreign('sender_id')
                    ->references('id')
                    ->on('user')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

                $table->unsignedBigInteger('recipient_id')->nullable();
                $table->foreign('recipient_id')
                    ->references('id')
                    ->on('user')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

                $table->unsignedBigInteger('email_template_id')->nullable();
                $table->foreign('email_template_id')
                    ->references('id')
                    ->on('email_template')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

                $table->boolean('success')->nullable();
                $table->longText('addresses')->nullable();
                $table->longText('errors')->nullable();
                $table->longText('content')->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }

        self::upModel();

        if (Schema::hasTable('model') && self::models()) {
            foreach (self::models() as $model) {
                \Heiw\Uxcrudible\Seeds\ModelTableSeeder::addFullRightsForSysAdmin($model['id']);
                \Heiw\Uxcrudible\Seeds\PageTableSeeder::addModelToPage($model['id'], 3);
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_log');

        self::downModel();
    }
}
