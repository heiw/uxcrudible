<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_page', function (Blueprint $table) {
            $table->unsignedBigInteger('model_id');
            $table->unsignedBigInteger('page_id');

            $table->foreign('model_id')
                ->references('id')
                ->on('model')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('page_id')
                ->references('id')
                ->on('page')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->primary(['model_id', 'page_id']);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_page');
    }
}
