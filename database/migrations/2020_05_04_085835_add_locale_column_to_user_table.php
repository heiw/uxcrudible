<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLocaleColumnToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('user', 'locale')) {
            Schema::table('user', function (Blueprint $table) {
                $table->string('locale')->index()->nullable()->after('remember_token');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('user', 'locale')) {
            Schema::table('user', function (Blueprint $table) {
                $table->removeColumn('locale');
            });
        }
    }
}
