<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailTemplateFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_template_file')) {
            Schema::create('email_template_file', function (Blueprint $table) {
                $table->unsignedBigInteger('email_template_id');
                $table->unsignedBigInteger('file_id');
                $table->string('locale')->index();
                $table->primary(['email_template_id', 'file_id', 'locale']);

                $table->foreign('email_template_id')
                    ->references('id')
                    ->on('email_template')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                $table->foreign('file_id')
                    ->references('id')
                    ->on('file')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_template_file');
    }
}
