<?php

use Heiw\Uxcrudible\Classes\UxcrudModelMigration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Heiw\Uxcrudible\Seeds\ModelTableSeeder;

class CreateEmailTemplateTable extends UxcrudModelMigration
{
    public static function models() {
        return [
            ['id' => ModelTableSeeder::EMAIL_TEMPLATE_ID,'namespace' => 'Heiw\\Uxcrudible\\',   'name' => 'EmailTemplate'],
        ];
    }

    public static function modelTranslations() {
        return [
            ['model_id' => ModelTableSeeder::EMAIL_TEMPLATE_ID, 'locale' => 'en', 'display' => 'Email Template'],

            ['model_id' => ModelTableSeeder::EMAIL_TEMPLATE_ID, 'locale' => 'cy', 'display' => __('Email Template',[], 'cy')],
        ];
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable('email_template')) {
            Schema::create('email_template', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('name')->nullable();
                $table->boolean('include_snippet_signature')->nullable();
                $table->unsignedBigInteger('priority')->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }

        self::upModel();

        if (Schema::hasTable('model') && self::models()) {
            foreach (self::models() as $model) {
                \Heiw\Uxcrudible\Seeds\ModelTableSeeder::addFullRightsForSysAdmin($model['id']);
                \Heiw\Uxcrudible\Seeds\PageTableSeeder::addModelToPage($model['id'], 3);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('email_template');

        self::downModel();
    }
}

