<?php

use Heiw\Uxcrudible\Classes\UxcrudModelMigration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationTranslationSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation_translation_source', function (Blueprint $table) {
            $table->unsignedBigInteger('translation_source_id');
            $table->unsignedBigInteger('translation_id');
            $table->primary(['translation_id', 'translation_source_id'], 'tts_primary_id');

            $table->foreign('translation_id')
                ->references('id')
                ->on('translation')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('translation_source_id')
                ->references('id')
                ->on('translation_source')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translation_translation_source');
    }
}
